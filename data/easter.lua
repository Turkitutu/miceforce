players = {}
toDespawn={}
currentPlayersCount = -2
yumurtaGizle = 0
maps = {450716, 450717, 450718, 450721, 450722, 417074, 451269, 451270, 451271, 451273, 451274}
tfm.exec.disableAutoShaman(true)
tfm.exec.disablePhysicalConsumables(true)
tfm.exec.disableAfkDeath(true)
system.disableStats(false)

translation = {
    ["TR"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Tebrikler! Başarılı bir şekilde yumurtaları farelerden korudun. Artık hepsi senin!",
        ["failed"] = "<r>• Üzgünüm! Fareler yumurtalarını geri aldıkları için tavşanlık görevini tamamlayamadın. :[",
        ["failed2"] = "<r>• Üzgünüm! Tavşanlık görevini tamamlayamadın.",
        ["tavsansinleynn"] = "<vp>• Tavşan sensin! Bu turda başarılı olmak için yumurtalarını sadece <b>${tavsan}</b> fareye verebilirsin. <i>(kimseye vermemeye çalış!! *şeytani gülüş*)</i>"

    },

    ["EN"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Great job! You have protected the eggs from the mice during this round. They're all yours!",
        ["failed"] = "<r>• Oh snap! You couldn't accomplish your task as a rabbit during this round because the mice have gotten the eggs back :[",
        ["failed2"] = "<r>• Oh snap! You couldn't accomplish your task as a rabbit during this round.",
        ["tavsansinleynn"] = "<vp>• You are the rabbit now! To accomplish this round, you can give your eggs out only <b>${tavsan}</b> mice. <i>(try not to give it to anyone!! *evil laugh*)</i>"
    },

    ["HU"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Nagyszerű munka! Megvédted a tojásokat az egerektől ebben a körben. Mind a tiéd!",
        ["failed"] = "<r>• A fenébe! Nem tudtad teljesíteni a feladatod nyúlként ebben a körben, mert az egerek megszerezték a tojásokat. :[",
        ["failed2"] = "<r>• A fenébe! Nem tudtad teljesíteni a feladatod nyúlként ebben a körben.",
        ["tavsansinleynn"] = "<vp>• Te vagy a nyúl! Ennek a körnek a teljesítéséhez a tojásaidat csak <b>${tavsan}</b> egérnek adhatod oda.  <i>(Törekedj arra, hogy ne add oda senkinek!! *ördögi nevetés*)</i>"
    },

    ["ES"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• ¡Excelente trabajo! Has protegido los huevos de los ratones durante esta ronda. ¡Son todos tuyos!",
        ["failed"] = "<r>• ¡Lástima! No pudiste completar esta tarea como conejo de pascua durante esta ronda porque los ratones se han llevado los huevos :[",
        ["failed2"] = "<r>• ¡Lástima! No pudiste completar esta tarea como conejo de pascua durante esta ronda.",
        ["tavsansinleynn"] = "<vp>• ¡Eres el conejo de pascua! Para completar esta ronda,  puedes darle tus huevos únicamente a <b>${tavsan}</b> ratones. <i>(¡Trata de no darlos a nadie! *muahahaha*)</i>"
    },

    ["PL"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Dobra robota! Udało Wam się ochronić jajka przed myszami. Jajka są teraz Wasze!",
        ["failed"] = "<r>• O nie! Nie udało się wykonać zadania jako zając, gdyż myszom udało się odzyskać jajka :[",
        ["failed2"] = "<r>• O nie! Nie udało się wykonać zadania jako zając.",
        ["tavsansinleynn"] = "<vp>• Jesteś zającem! Aby wygrać rundę, możesz dać jajka nie więcej niż <b>${tavsan}</b> myszkom <i>(ale nikt nie może się do nich dostać!! *szyderczy śmiech*)</i>"
    },

    ["RO"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Bine lucrat! Ai protejat ouăle șoarecilor în această rundă.  Toate sunt ale tale!",
        ["failed"] = "<r>• Oh! Nu ai reușit să îndeplinești sarcina ca iepure în această rundă, deoarece șoarecii au primit ouăle înapoi :[",
        ["failed2"] = "<r>• Oh! Nu ai putut îndeplini sarcina ca iepure în această rundă.",
        ["tavsansinleynn"] = "<vp>• Ești iepure acum! Pentru finalizarea acestei runde, poți da ouă doar la <b>${tavsan}</b>  șoareci. <i>( încearcă să nu le dai nimănui!! *râs diabolic*)</i>"
    },

    ["AR"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• عمل جيد! لقد حميت البيضات من الفئران في هذا الشوط، كلهم لك!",
        ["failed"] = "<r>• يا الهي! لم تستطع ان تكمل عملك كأرنب في هذا الشوط لأن الفئران أخذو كل البيضات :[",
        ["failed2"] = "<r>• .يا الهي! لم تستطع ان تكمل عملك كأرنب في هذا الشوط.",
        ["tavsansinleynn"] = "<vp>• انت الارنب الان! لإنجاز هذه الجولة ، يمكنك إعطاء البيض الخاص بك فقط <b>${tavsan}</b> للفأر. <i>(حاول ألا تعطيها لأي شخص اخر !! ضحكة شريرة)</i>"
    },

    ["BR"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Excelente trabalho! Você protegeu os ovos dos ratos durante essa rodada. Eles são todos seus!",
        ["failed"] = "<r>• Que pena! Você não conseguiu completar esta tarefa como coelho durante essa rodada porque os ratos conseguiram levar os ovos :[",
        ["failed2"] = "<r>• Que pena! Você não conseguiu completar esta tarefa como coelho durante essa rodada.",
         ["tavsansinleynn"] = "<vp>• Você é o coelho agora! Para realizar esta rodada, você pode dar os seus ovos apenas para <b>${tavsan}</b> ratos. <i>(tente não dar a ninguém!! *risada maléfica*)</i>"
    },

    ["FR"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Bon travail! Vous avez protégé les œufs des souris sur ce niveau. Ils sont tout à vous!",
        ["failed"] = "<r>• Oh zut! Vous n'avez pas réussi votre tâche comme un lapin sur ce niveau parce que les souris ont repris les œufs  :[",
        ["failed2"] = "<r>• Oh zut! Vous n'avez pas réussi votre tâche comme un lapin sur ce niveau.",
        ["tavsansinleynn"] = "<vp>• Vous êtes le lapin maintenant! Pour accomplir ce niveau, vous pouvez donner vos œufs juste à <b>${tavsan}</b> souris. <i>(essayez de ne pas le donner à personne!! *rire effrayant*)</i>"
    },

    ["CZ"] = {
        ["successfully"] = "<font color=\"#FEB1FC\"> • Dobrá práca! Počas tohto kola ste chránili vajíčka od potkanov. Teraz sú všetky vaše!" ,
        ["failed"] = "<r>  • Aká škoda!, že ste sa nepodarilo ako králik v tomto kole, pretože potkany vzali vajíčka  :[",
        ["failed2"] = "<r> • Aká škoda!, že ste nemohli dokončiť túto úlohu ako králik počas tohto kola.",
        ["tavsansinleynn"] = "<vp>• Teraz ste králik! Ak chcete dokončiť toto kolo, môžete dať svoje vajcia iba <b>${tavsan}</b> potkanov. <i>(Snažte sa nedávať dať na nikto!! *Hravý smiech*)</i>"
    },

    ["RU"] = {
        ["successfully"] = "<font color=\"#FEB1FC\">• Отличная работа! Вы защитили яйца от мышек в этом раунде. Теперь они все твои!",
        ["failed"] = "<r>• О нет! В этом раунде вы не могли выполнить свою задачу как кролик, потому что эти злобные мышки отыскали ваши яйца :[",
        ["failed2"] = "<r>• Упс! Вы не смогли выполнить свою задачу в роли кролика в этом раунде.",
        ["tavsansinleynn"] = "<vp>• Ты теперь кролик! Чтобы выполнить этот раунд, вы можете дать свои яйца только <b>${tavsan}</b> мышке(ам). <i>(постарайтесь не отдать его кому-либо! *злобно хихикает*)</i>"
    }
}

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end


function chooseRabbit()
    local score, name = -1
    for k, v in pairs(tfm.get.room.playerList) do
        if v.score > score then
            score = v.score
            name = k
        end
    end
    return name
end

rabbit = chooseRabbit()

for n in pairs(tfm.get.room.playerList) do
    if n ~= rabbit then
        mf.changeLook(n, "1;60,0,0,0,0,0,0,0,0,3")
    else
        mf.changeLook(rabbit, "16;0,0,0,17,0,0,0,0,0,3")
        tfm.exec.bindKeyboard(rabbit,32,true,true)
        tfm.exec.bindKeyboard(rabbit,69,true,true)
    end
end

function eventNewGame()
    ui.setMapName("Easter Event <bl>- @2018")
    currentPlayersCount = -2
    yumurtaGizle = 0

    tfm.exec.setGameTime(63)
    mf.catchMouse(rabbit)
    durdur = false
    local xml = tfm.get.room.xmlMapInfo
    xml = xml and xml.xml or ''

    for params in xml:gmatch('<DC (.-)/>') do
        local x = tonumber(params:match('X="(.-)"'))
        local y = tonumber(params:match('Y="(.-)"'))
        if tfm.get.room.playerList[rabbit].isDead then
            tfm.exec.respawnPlayer(rabbit)
            mf.catchMouse(rabbit)
        end
        tfm.exec.movePlayer(rabbit,x,y,false,0,50,false)
    end

    for n,d in pairs(tfm.get.room.playerList) do

        currentPlayersCount = currentPlayersCount + 1
        if n == rabbit then
            system.giveTitle(rabbit, 3242)
        end


        players[n] = {
            timestamp=os.time()
        }

    end
    tfm.exec.chatMessage(param(getMsg("tavsansinleynn"), {tavsan=math.ceil(currentPlayersCount/2)}), rabbit)

end


function eventKeyboard(name,key,down,x,y)
    if (key==69 or key==32) and not tfm.get.room.playerList[name].isDead and name == rabbit then
        if players[name].timestamp < os.time()-1050 then
            id=tfm.exec.addShamanObject(6, x, y, 0, tfm.get.room.playerList[name].isFacingRight and 20 or -20)
            tfm.exec.addImage("easteregg-png.5943", "#"..id, -15, -20)
            players[name].timestamp=os.time()
            table.insert(toDespawn,{os.time(),id})
        end
    end
end

function eventLoop(time,remaining)
    for i,cannon in ipairs(toDespawn) do
        if cannon[1] <= os.time()-3000 then
            tfm.exec.removeObject(cannon[2])
            table.remove(toDespawn,i)
        end
    end

    if remaining <= 4508 and not durdur then

        if yumurtaGizle <= math.ceil(currentPlayersCount/2) then
            tfm.exec.chatMessage(getMsg("successfully"), rabbit)
            mf.ilerleme(rabbit, 32, 1)
            durdur = true
            tfm.exec.killPlayer(rabbit)
        else
            tfm.exec.chatMessage(getMsg("failed"), rabbit)
            durdur = true
            tfm.exec.killPlayer(rabbit)
        end

    end
end

function eventPlayerWon(n)
    if n ~= rabbit then
        tfm.exec.chatMessage("<ch>• Egg +1", n)
        mf.ilerleme(n, 30, 1)
    end
end

function eventPlayerGetCheese(n)
    if n ~= rabbit then
    yumurtaGizle=yumurtaGizle+1
        mf.ilerleme(n, 31, 1)
        tfm.exec.chatMessage("<ch>• Gotcha!", n)
    end
end
function eventPlayerLeft(n) 
	eventPlayerDied(n)
end
function eventPlayerDied(n)
    if n==rabbit and not durdur then
        tfm.exec.chatMessage(getMsg("failed2"), rabbit)
        for n in pairs(tfm.get.room.playerList) do
            tfm.exec.giveCheese(n)
        end
        durdur = true
    end
end

tfm.exec.newGame(maps[math.random(#maps)], true, 2)

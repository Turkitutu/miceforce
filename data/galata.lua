tfm.exec.disablePhysicalConsumables(true)
players = {}
translation = {
	["EN"] = {
		["welcome"] = "<font color='#FEB1FC'>Welcome to the Galata Tower.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>The legend of the Galata Tower says that whomever climbs this historic tower with you, you are destined to marry.</font>",
		["mapName"] = "Valentine's Day 2019 <bl>- Galata Tower | Istanbul, Turkey (TR)"
	},

	["TR"] = {
		["welcome"] = "<font color='#FEB1FC'>Galata Kulesi'ne hoş geldiniz.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>Efsaneye göre, Galata Kulesi'ne kiminle çıkarsan onunla evlenmek kaderinde vardır.</font>",
		["mapName"] = "Sevgililer Günü 2019 <bl>- Galata Kulesi | İstanbul, Türkiye (TR)"
	},

	["ES"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bienvenido a la Torre de Gálata.</font>",
        ["bunubiliyormuydun"] = "<font color='#FADE55'>La leyenda de la Torre de Gálata dice que quien quiera que escale ésta histórica torre contigo, es con quien estás destinado a casarte.</font>",
        ["mapName"] = "Día de San Valentín 2019 <bl>- Torre de Gálata | Estambul, Turquía (TR)"
    },

    ["BR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bem-vindo à Torre Gálata.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>A lenda da Torre Gálata diz que com quem quer que você suba nesta torre histórica, você está predestinado à se casar.</font>",
		["mapName"] = "Dia dos Namorados 2019 <bl>- Torre Gálata | Istambul, Turquia (TR)"
	},

	["FR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bienvenu à la Tour de Galata.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>La légende de la Tour de Galata dit que quiconque grimpe cette tour historique avec vous, vous serez destinés à les marier.</font>",
		["mapName"] = "Saint Valentin 2019 <bl>- Tour de Galata | Istanbul, Turquie (TR)"
	},

	["RU"] = {
		["welcome"] = "<font color='#FEB1FC'>Добро пожаловать в Галатскую Башню.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>Легенда о Галатской Башне гласит, что кто бы ни взбирался с вами на эту историческую Башню, вам суждено выйти за них замуж.</font>",
		["mapName"] = "День Святого Валентина 2019 <bl>- Галатская Башня | Istanbul, Turkey (TR)"
	},

	["RO"] = {
		["welcome"] = "<font color='#FEB1FC'>Bine ai venit la Turnul Galata..</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>Legenda Turnului Galata spune că oricine urcă acest turn istoric cu tine, este destinat să se căsătorească.</font>",
		["mapName"] = "Ziua Îndrăgostiților 2019 <bl>- Turnul Galata | Istanbul, Turcia (TR)"
	},

	["HU"] = {
		["welcome"] = "<font color='#FEB1FC'>Üdv a Galata toronyban.</font>",
		["bunubiliyormuydun"] = "<font color='#FADE55'>A Galata torony legendája úgy szól, aki megmássza ezt a történelmi tornyot veled, azzal házasságot kell kötnöd.</font>",
		["mapName"] = "Valentin-nap 2019 <bl>- Galata torony | Isztambul, Törökország (TR)"
	}

}

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end

function main()
	map = '<C><P Ca="" H="800" D="1550012264787.png" APS="1550013179378.png,0,280,310,260,766,0,0" /><Z><S><S X="400" Y="791" L="800" H="20" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="279" Y="534" L="10" H="399" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="354" Y="687" L="157" H="11" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="522" Y="528" L="10" H="400" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="444" Y="574" L="151" H="10" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="352" Y="460" L="153" H="15" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="332" Y="636" L="77" H="102" T="9" P="0,0,,,,0,0,0" m="" /><S X="470" Y="738" L="76" H="82" T="9" P="0,0,,,,0,0,0" m="" /><S X="468" Y="522" L="77" H="98" T="9" P="0,0,,,,0,0,0" m="" /><S X="396" Y="386" L="52" H="131" T="9" P="0,0,,,,0,0,0" m="" /><S X="296" Y="318" L="55" H="27" T="12" P="0,0,0.3,0.2,-24,0,0,0" o="324650" m="" /><S X="345" Y="305" L="47" H="27" T="12" P="0,0,0.3,0.2,-5,0,0,0" o="324650" m="" /><S X="447" Y="306" L="47" H="28" T="12" P="0,0,0.3,0.2,5,0,0,0" o="324650" m="" /><S X="499" Y="315" L="55" H="27" T="12" P="0,0,0.3,0.2,16,0,0,0" o="324650" m="" /><S X="319" Y="303" L="10" H="18" T="12" P="0,0,0.3,0.2,-17,0,0,0" o="324650" m="" /><S X="472" Y="303" L="10" H="18" T="12" P="0,0,0.3,0.2,9,0,0,0" o="324650" m="" /><S X="266" Y="323" L="50" H="10" T="12" P="0,0,0.3,0.2,38,0,0,0" o="324650" m="" /><S X="534" Y="317" L="46" H="10" T="12" P="0,0,0.3,0.2,-36,0,0,0" o="324650" m="" /><S X="202" Y="568" L="151" H="29" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="204" Y="550" L="32" H="21" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="123" Y="572" L="32" H="14" T="12" P="0,0,0.3,0.2,-55,0,0,0" o="324650" m="" /><S X="747" Y="601" L="106" H="368" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="807" Y="402" L="10" H="804" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="-8" Y="402" L="10" H="804" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="147" Y="344" L="143" H="15" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="640" Y="340" L="111" H="12" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /></S><D><DS X="109" Y="766" /></D><O></O></Z></C>'
	tfm.exec.newGame(map)
end

function eventNewPlayer(n)
	ui.setMapName(getMsg("mapName"), n)
	tfm.exec.chatMessage(getMsg("welcome"), n)
	tfm.exec.addImage("vday_galata_foreground.15728", "!2", 0, 0, n)
	tfm.exec.bindKeyboard(n, 32, true, true)
	system.addBot(-9826, "Ariana", 3051, "39;0,16_DFDFDF+EAACF8,0,0,26_DFDFDF+D77FEC,20_DFDFDF+EAACF8,0,0,0,3,0", 140, 298, n)

	players[n]={
		optu = false,
		bitti = false,
		sayi = math.random(1, 1000)
	}
end

function eventEmotePlayed(n, id)
	if players[n].optu == false then
		if id == 23 then
			if (tfm.get.room.playerList[n].x>=80 and tfm.get.room.playerList[n].x<= 205 and tfm.get.room.playerList[n].y>=260 and tfm.get.room.playerList[n].y<=325) or (tfm.get.room.playerList[n].x>=583 and tfm.get.room.playerList[n].x<= 686 and tfm.get.room.playerList[n].y>=290 and tfm.get.room.playerList[n].y<=320)then
				tfm.exec.chatMessage(getMsg("bunubiliyormuydun"), n)

				if players[n].sayi >= 1 and players[n].sayi <= 700 then
					system.giveConsumables(n, math.random(2139, 2144), 1)
				elseif players[n].sayi >= 701 and players[n].sayi <= 1000 then
					system.giveConsumables(n, math.random(2118, 2123), 1)
				end
				
				players[n].optu = true
			end
		end
	end
end

function eventNewGame()
	for n,p in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end
end

main()
system.eventMs(300)
tfm.exec.disableAutoShaman(true)
tfm.exec.disablePhysicalConsumables(true)
flames = false
players = {}
yumurtalar = {
	{x = 330, y = 359},
	{x = 492, y = 359},
	{x = 710, y = 359},
	{x = 879, y = 359},
	{x = 1023, y = 359},
	{x = 562, y = 147},
	{x = 95, y = 140},
	{x = 1065, y = 130}
}

whereisegg = {}
arttir = 0
alinan = 0
translation = {
	["EN"] = {
        ["welcome"] = "<font color='#FEB1FC'>Welcome to Altan's Garden. You should be super eggcited to find some eggs!</font>",
        ["mapName"] = "Easter 2019 <bl>- Altan's Garden",
        ["newegg"] = "<font color='#ffe5b4'>The easter egg has changed it's place. Go catch it before your opponents do!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Now take the egg to the Ancient Sevenops statue.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Thanks my dear! Now you may go back to my garden to collect more eggs!</font>"
    },

	["TR"] = {
        ["welcome"] = "<font color='#FEB1FC'>Altan'ın Bahçesi'ne hoş geldiniz. Umarım yumurtaları bulmak için çok heyecanlısınızdır!</font>",
        ["mapName"] = "Paskalya 2019 <bl>- Altan'ın Bahçesi",
        ["newegg"] = "<font color='#ffe5b4'>Paskalya yumurtası yerini değiştirdi. Git ve onu rakiplerinden önce yakala!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Şimdi bu yumurtayı Antik Sevenops heykeline götür.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Teşekkürler canım! Şimdi bahçeme dönüp daha fazla yumurta toplayabilirsin!</font>"
    },

	["BR"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bem-vindo ao Jardim do Altan. Você deve ser super dedicado e atencioso para conseguir encontrar alguns ovos de páscoa!</font>",
        ["mapName"] = "Páscoa 2019 <bl> - Jardim do Altan",
        ["newegg"] = "<font color='#ffe5b4'>O ovo de páscoa mudou de lugar. Vá pegá-lo antes que seus oponentes o pegue! </font>",
        ["toancientgod"] = "<font color='#f7cac9'>Agora pegue o ovo de páscoa e leve até a estátua do Antigo Sevenops.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Obrigado meu caro rato! Agora você pode voltar para o meu jardim para coletar mais ovos de páscoa!</font>"
    },

    ["RO"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bine ai venit în grădina lui Altan. Ar trebui să fii foarte entuziasmat să găsești niște ouă!</font>",
        ["mapName"] = "Paște 2019 <bl>- Grădina lui Altan",
        ["newegg"] = "<font color='#ffe5b4'>Oul de paște și-a schimbat locul. Du-te și prinde-l înaintea adversarilor tăi!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Acum ia oul și du-l la statuia antică a lui Sevenops</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Mulțumesc! Acum te poți întoarce în grădina mea pentru a colecta mai multe ouă!</font>"
    },

    ["ES"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bienvenido al jardín de Altan. ¡Debes estar súper emocionado por atrapar algunos huevos!</font>",
        ["mapName"] = "Pascua 2019 <bl>- Jardín de Altan",
        ["newegg"] = "<font color='#ffe5b4'>El huevo de pascua ha cambiado su lugar ¡atrápalo antes que tus oponentes!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Ahora lleva el huevo a la antigua estatua de Sevenops.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Gracias, ¡ahora puedes regresar a mi jardín para recoger más huevos!</font>"
    },

    ["HU"] = {
        ["welcome"] = "<font color='#FEB1FC'>Üdvözöllek Altan kertjében. Szuper eggcited-nek kell lenned ahhoz, hogy találj néhány tojást!</font>",
        ["mapName"] = "Húsvét 2019 <bl>- Altan kertje",
        ["newegg"] = "<font color='#ffe5b4'>A húsvéti tojás megváltoztatta a helyét. Menj, és kapd el mielőtt az ellenfeleid teszik meg!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Most vidd el a tojást az Ősi Sevenops szoborhoz.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Köszönöm, kedvesem! Most visszamehetsz a kertbe, hogy több tojást tudj gyűjteni!</font>"
    },

    ["PL"] = {
        ["welcome"] = "<font color='#FEB1FC'>Witamy w Ogrodzie Altana. Wydajesz się być jajecznie podekscytowany, aby znaleźć trochę pisanek!</font>",
        ["mapName"] = "Easter 2019 <bl>- Ogród Altana",
        ["newegg"] = "<font color='#ffe5b4'>Pisanka zmieniła swoje miejsce położenia. Znajdź ją zanim zrobią to Twoi przeciwnicy!</font>",
        ["toancientgod"] = "<font color='#f7cac9'>Teraz zabierz pisankę do Starożytnego Posągu Sevenopsa.</font>",
        ["duuh"] = "<font color='#93e4aa'>[<a href=\"event:x_mj;Altan\">Altan</a>] Dzięki mój drogi! Teraz wróć do mojego ogrodu i zbierz więcej pisanek!</font>"
    } 

}

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end

function main()	
	map = '<C><P L="1800" Ca="" /><Z><S><S X="558" Y="409" L="1115" H="69" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1458" Y="395" L="685" H="66" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1472" Y="356" L="657" H="35" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1488" Y="338" L="626" H="41" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1717" Y="314" L="170" H="20" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1736" Y="307" L="129" H="45" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1752" Y="276" L="99" H="35" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1770" Y="252" L="65" H="37" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1417" Y="238" L="470" H="14" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1210" Y="192" L="56" H="84" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1251" Y="210" L="34" H="68" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1279" Y="220" L="43" H="37" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1318" Y="231" L="45" H="17" T="12" P="0,0,0.3,0.2,6,0,0,0" o="324650" m="" /><S X="1561" Y="156" L="460" H="15" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="1796" Y="119" L="12" H="236" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="1173" Y="123" L="18" H="245" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="-9" Y="214" L="13" H="437" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="896" Y="6" L="1815" H="26" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="587" Y="179" L="37" H="17" T="12" P="0,0,0.3,0.2,29,0,0,0" o="324650" m="" /><S X="568" Y="172" L="15" H="20" T="12" P="0,0,0.3,0.2,-3,0,0,0" o="324650" m="" /><S X="556" Y="174" L="17" H="19" T="12" P="0,0,0.3,0.2,-21,0,0,0" o="324650" m="" /><S X="517" Y="206" L="15" H="22" T="12" P="0,0,0.3,0.2,-21,0,0,0" o="324650" m="" /><S X="506" Y="211" L="18" H="21" T="12" P="0,0,0.3,0.2,-31,0,0,0" o="324650" m="" /><S X="496" Y="214" L="11" H="12" T="12" P="0,0,0.3,0.2,-63,0,0,0" o="324650" m="" /><S X="490" Y="225" L="10" H="15" T="12" P="0,0,0.3,0.2,22,0,0,0" o="324650" m="" /><S X="496" Y="224" L="10" H="15" T="12" P="0,0,0.3,0.2,54,0,0,0" o="324650" m="" /><S X="524" Y="201" L="15" H="10" T="12" P="0,0,0.3,0.2,31,0,0,0" o="324650" m="" /><S X="536" Y="208" L="15" H="10" T="12" P="0,0,0.3,0.2,31,0,0,0" o="324650" m="" /><S X="546" Y="214" L="10" H="10" T="12" P="0,0,0.3,0.2,31,0,0,0" o="324650" m="" /><S X="551" Y="208" L="10" H="10" T="12" P="0,0,0.3,0.2,31,0,0,0" o="324650" m="" /><S X="554" Y="201" L="10" H="10" T="12" P="0,0,0.3,0.2,7,0,0,0" o="324650" m="" /><S X="555" Y="189" L="10" H="17" T="12" P="0,0,0.3,0.2,7,0,0,0" o="324650" m="" /><S X="546" Y="250" L="10" H="77" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="39" Y="322" L="66" H="138" T="4" P="0,0,20,0.2,-20,0,0,0" m="" /><S X="22" Y="200" L="47" H="138" T="4" P="0,0,20,0.2,-4,0,0,0" m="" /><S X="30" Y="109" L="47" H="75" T="4" P="0,0,20,0.2,22,0,0,0" m="" /><S X="87" Y="55" L="47" H="124" T="4" P="0,0,20,0.2,56,0,0,0" m="" /><S X="975" Y="229" L="57" H="217" T="9" P="0,0,,,,0,0,0" m="" /><S X="604" Y="189" L="10" H="16" T="12" P="0,0,1,0.2,4,0,0,0" o="324650" m="" /><S X="940" Y="104" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="965" Y="120" L="76" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="901" Y="73" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="791" Y="42" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="822" Y="46" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="852" Y="54" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="883" Y="62" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="920" Y="85" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="914" Y="77" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="891" Y="69" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="866" Y="52" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="830" Y="50" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="967" Y="109" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="937" Y="90" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="933" Y="96" L="35" H="21" T="9" P="0,0,,,,0,0,0" m="" /><S X="612" Y="243" L="42" H="14" T="12" P="0,0,0.3,0.2,39,0,0,0" o="324650" m="" /><S X="587" Y="232" L="16" H="20" T="12" P="0,0,0.3,0.2,-29,0,0,0" o="324650" m="" /><S X="566" Y="241" L="38" H="19" T="12" P="0,0,0.3,0.2,-21,0,0,0" o="324650" m="" /><S X="593" Y="225" L="11" H="10" T="12" P="0,0,0.3,0.2,41,0,0,0" o="324650" m="" /><S X="614" Y="250" L="10" H="10" T="12" P="0,0,0.3,0.2,7,0,0,0" o="324650" m="" /><S X="591" Y="244" L="42" H="17" T="12" P="0,0,0.3,0.2,7,0,0,0" o="324650" m="" /></S><D><DS X="190" Y="360" /></D><O></O></Z></C>'
	tfm.exec.newGame(map)
end

function fireworks(x, y, n)
    local particles = {0, 1, 2, 9, 11, 13}
    for i = 0, 150 do
        tfm.exec.displayParticle(particles[math.random(#particles)], x, y, math.cos(i) * 6 * math.random(50), math.sin(i) * 6 * math.random(50), 0, 5, n)
    end
end


function newEgg()

	tfm.exec.removeImage(yumurta)
	
	alinan = 0
	whereisegg = {}

	local numara = math.random(#yumurtalar)
    local rastgelex = yumurtalar[numara]["x"]
    local rastgeley = yumurtalar[numara]["y"]

    table.insert(whereisegg, rastgelex)
    table.insert(whereisegg, rastgeley)
    yumurta = tfm.exec.addImage("eastereggduh.17411", "?2", yumurtalar[numara]["x"]-20, yumurtalar[numara]["y"]-20)

	for n,p in pairs(tfm.get.room.playerList) do
		if players[n].aldin then 
			fireworks(yumurtalar[numara]["x"], yumurtalar[numara]["y"])
			--tfm.exec.chatMessage(getMsg("newegg"), n)
		end
	end

end

function eventNewPlayer(n)
	ui.setMapName(getMsg("mapName"), n)
	tfm.exec.chatMessage(getMsg("welcome"), n)
	tfm.exec.addImage("easterevent_background.17410", "?1", 0, 0, n)
	tfm.exec.addImage("oklastforeground.17450", "!3", 0, 0, n)
	tfm.exec.bindKeyboard(n,32,true,true)
	tfm.exec.bindKeyboard(n,3,true,true)

	players[n]={
		egg = 0,
		aldin = false,
		completed = false
	}
end

function eventNewGame()
	yumurta = tfm.exec.addImage("", "!2", 0, 0)

	for n,p in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	tfm.exec.removeImage(yumurta)
	alinan = 0
	whereisegg = {}

	local numara = math.random(#yumurtalar)
    local rastgelex = yumurtalar[numara]["x"]
    local rastgeley = yumurtalar[numara]["y"]
    table.insert(whereisegg, rastgelex)
    table.insert(whereisegg, rastgeley)
    yumurta = tfm.exec.addImage("eastereggduh.17411", "?2", rastgelex-20, rastgeley-20)
end

function flamesX(x, y, n)
    local particles = {2, 11, 13, 4}
    for i = 0, 10 do
        tfm.exec.displayParticle(particles[math.random(#particles)], x, y, math.cos(i) * 2 * math.random(50), math.sin(i) * 2 * math.random(50), 0, -15, n)
    end
end

function eventLoop()
	arttir = arttir + 1

	if arttir >= 5 then
		flames=true

		if flames then
			flamesX(math.random(1372, 1415), math.random(78, 83))
			flamesX(math.random(1648, 1687), math.random(81, 85))
		end
	end
end

function eventKeyboard(n,k,d,x,y)
	if k == 32 or k == 3 then
		if not tfm.get.room.playerList[n].isDead then
			if (math.abs(whereisegg[1] - tfm.get.room.playerList[n].x) < 10 and math.abs(whereisegg[2] - tfm.get.room.playerList[n].y) < 10) then
				if not players[n].aldin then
					tfm.exec.chatMessage(getMsg("toancientgod"), n)
					players[n].egg = players[n].egg + 1
					players[n].aldin = true
					alinan = alinan + 1
					tfm.exec.displayParticle(3,tfm.get.room.playerList[n].x,tfm.get.room.playerList[n].y,0,0,0,0,n)
					tfm.exec.displayParticle(15,tfm.get.room.playerList[n].x,tfm.get.room.playerList[n].y,0,0,0,0,n)
					
					if alinan == 5 then
						newEgg()
					end 
				end
			end

			if (tfm.get.room.playerList[n].x>=1478 and tfm.get.room.playerList[n].x<= 1587 and tfm.get.room.playerList[n].y>=133 and tfm.get.room.playerList[n].y<=133) then
				if players[n].aldin then
					tfm.exec.chatMessage(getMsg("duuh"), n)
					players[n].egg = 0
					players[n].aldin = false
					system.giveConsumables(n, 2387, 1)
					
				end
			end
		end
	end
end

main()
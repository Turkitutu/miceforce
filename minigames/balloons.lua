tfm.exec.disableAutoNewGame(true)
tfm.exec.disableAutoShaman(true)
tfm.exec.disableAutoTimeLeft(true)
admins = {"Soulzone", "Chomar", "Kmlcan"}
roomMod = {}
maps = {"0", "3", "4", "5", "@306203", "@306204", "@306205", "@310504", "@314328", "@314536", "@306206", "@306207", "@306208", "@306209", "@306210", "@306212", "@306214", "@306215", "@306216", "@306217", "@306218", "@306283", "@306220", "@306221", "@306223", "@306225", "@306226", "@306227", "@306229", "@306230", "@306231", "@306232", "@306233", "@306234", "@306235", "@306237", "@306238"}
bans = {}
players = {}
alive = 0
round = 0
won = 0
TIMER = 0
chatlog = {}
toDespawn = {}
respawn = {}
translation = {
	["TR"] = {
		["welcome"] = "<j>#balloons minioyununa hoş geldin. (v2)",
		["hMuch"] = "<V>[#] ${balloon}<BL> balonun kaldı.",
		["usedAllBalloons"] = "<V>[#]<BL> Tüm balonlarını kullandın.",
		["admin"] = "<V>[#] ${player}<BL> artık yöneticidir.",
		["alreadyAnAdmin"] = "<V>[#] ${player}<BL> Zaten bir yönetici.",
		["unadmin"] = "<V>[#] ${player}<BL> artık bir yönetici değildir.",
		["currentAdmins"] = "<V>[#]<BL> Şu anki yöneticiler:",
		["banned"] = "<V>[#] ${player}<BL> banlanmıştır.",
		["unbanned"] = "<V>[#] ${player}<BL> artık banlı değildir.",
		["bannedList"] = "<V>[#]<BL> Banlı kullanıcılar:"
	},

	["EN"] = {
		["welcome"] = "<j>Welcome to the #balloons minigame. (v2)",
		["hMuch"] = "<V>[#]<BL> You have <v>${balloon}</v> balloon(s) left.",
		["usedAllBalloons"] = "<V>[#]<BL> You have used all your balloons.",
		["admin"] = "<V>[#] ${player}<BL> is now an admin.",
		["alreadyAnAdmin"] = "<V>[#] ${player}<BL> is already an admin.",
		["unadmin"] = "<V>[#] ${player}<BL> is no longer an admin.",
		["currentAdmins"] = "<V>[#]<BL> The current room admins are:",
		["banned"] = "<V>[#] ${player}<BL> has been banned.",
		["unbanned"] = "<V>[#] ${player}<BL> has been unbanned.",
		["bannedList"] = "<V>[#]<BL> The current banned players are:"
	}
}
--[Lee] Maps={ night ={"Night map","WUi4cYe",'@5721452'},
tfm.exec.newGame(maps[math.random(#maps)])

yaz = tfm.exec.chatMessage
system.community = tfm.get.room.community
function contains(table, val) for i=1,#table do if table[i] == val then return true end end return false end
function upper(str) return (str:gsub("^%l", string.upper)) end
function table.delete(tab, val) for k,v in pairs(tab) do if val == v then table.remove(tab, k) break end end end
function aliveCheck() if alive == 0 then tfm.exec.newGame(maps[math.random(#maps)]) end end
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end

function eventNewPlayer(n)
	yaz(getMsg("welcome"), n)
	for i,k in ipairs({32, 3, 17}) do
		tfm.exec.bindKeyboard(n, k, true, true)
	end

	table.insert(players, name)

	players[n] = {
		timestamp=os.time(),
		offsets={x=2, y=10},
		haklar = 16,
		bonusBalloon = false,
		startRemoveTimer = false,
    	wcIMG = tfm.exec.addImage("22ff097881ce11e7af4488d7f643024a.png", "&1", 159, 41, n),
    	wcTime = os.time()
	}
end

function eventNewGame()
	alive = 0
	round = round + 1
	started = false
	TIMER = 0
	won = 0

    if tfm.get.room.xmlMapInfo and tfm.get.room.xmlMapInfo.author then
    	local m = tfm.get.room.xmlMapInfo
        tfm.exec.setUIMapName(m.author.."<bl> - @"..m.mapCode.." <g> | <n>Round : <CH>"..round.."/10")
    end

	for n,p in pairs(tfm.get.room.playerList) do
		alive = alive + 1
		if not contains(admins, n) then
			players[n].haklar = 15
		else
			players[n].haklar = 9e9
		end

		if contains(bans, n) then
			tfm.exec.killPlayer(n)
		end
	end

	if round == 10 then
		for n in pairs(tfm.get.room.playerList) do
			if tfm.get.room.playerList[n].score >= 100 then
				if tfm.get.room.unique >= 3 then
					system.giveConsumables(n, 1808, 5)
				end
			end
			tfm.exec.setPlayerScore(n, 0)
		end
		round = 0
	end
end

function eventPlayerDied(n)
	alive = alive - 1
	aliveCheck()
end

function eventPlayerWon(n)
	alive = alive - 1
	won = won + 1
	aliveCheck()

	if tfm.get.room.unique >= 3 then
		if won == 1 then
			system.giveConsumables(n, 1811, 3)
			system.giveConsumables(n, 1808, 5)
		else
			system.giveConsumables(n, 1811, 1)
		end
	end
end

function eventPlayerLeft(n)
	table.delete(players, name)
	alive = alive - 1
	aliveCheck()
end


function eventKeyboard(n, k, d, x, y)
    if (k==32 or k==3) and not tfm.get.room.playerList[n].isDead and started then
        if players[n].haklar > 0 then
            if players[n].timestamp < os.time()-500 then
            	local id
                if tfm.get.room.playerList[n].isFacingRight then
                    id = tfm.exec.addShamanObject(28, x+players[n].offsets.x, y+players[n].offsets.y)
                else
                    id = tfm.exec.addShamanObject(28, x+players[n].offsets.x, y+players[n].offsets.y)
                end

                players[n].haklar = players[n].haklar - 1
                players[n].timestamp=os.time()
                yaz(param(getMsg("hMuch"), { balloon = players[n].haklar }), n)
                table.insert(toDespawn,{os.time(),id})

                if players[n].haklar == 0 then
                    yaz(getMsg("usedAllBalloons"), n)
                end
            end
        end
    end
end

function eventLoop(time, remaining)
	for n in pairs(tfm.get.room.playerList) do
		if players[n].wcIMG and os.time() - players[n].wcTime >= 4000 then
        	tfm.exec.removeImage(players[n].wcIMG)
        	players[n].wcIMG = nil
        end
    end

	for i,balloon in ipairs(toDespawn) do
		if balloon[1] <= os.time()-3000 then
			tfm.exec.removeObject(balloon[2])
			table.remove(toDespawn,i)
		end
	end


    if time >= 1000 and not started then
        started=true
    end

    if remaining <= 0 then
        tfm.exec.newGame(maps[math.random(#maps)])
    end

    --local r=math.floor(remaining/1000) if tfm.get.room.xmlMapInfo and tfm.get.room.xmlMapInfo.author then if r==116 and TIMER==0 then TIMER=TIMER+1 sayac = tfm.exec.addImage("4e792e5e7ebc11e7af4488d7f643024a.png", "!1", 300, 240) end if r==115 and TIMER==1 then tfm.exec.removeImage(sayac) TIMER=TIMER+1 sayacc = tfm.exec.addImage("52699bde7ebc11e7af4488d7f643024a.png", "!1", 300, 240) end if r==114 and TIMER==2 then tfm.exec.removeImage(sayacc) TIMER=TIMER+1 sayaccc = tfm.exec.addImage("509dc32a7ebc11e7af4488d7f643024a.png", "!1", 300, 240) end if r==113 and TIMER==3 then tfm.exec.removeImage(sayaccc) TIMER=TIMER+2 sayacccc = tfm.exec.addImage("49e9f7ba7ebc11e7af4488d7f643024a.png", "!1", 300, 240) end if r==111 and TIMER==5 then tfm.exec.removeImage(sayacccc) end end
end

function eventChatCommand(name, cmd)
	if contains(admins, name) then
		table.insert(chatlog, "<v>["..name.."]<n> !"..cmd.."")
	end

	local arg={}
	local avatar = system.bilgi(name).avatar
    local gender = system.bilgi(name).gender

	for argument in cmd:gmatch("[^%s]+") do
		table.insert(arg,argument)
	end

	if gender == 2 then
      players[name].gender = "<img src=\"http://mforum.ist/data/garcon.png\">"
    elseif gender == 1 then
      players[name].gender = "<img src=\"http://mforum.ist/data/fille.png\">"
    else
      players[name].gender = ""
    end

    if arg[1]=="cmdlog" and contains(admins, name) then
		ui.addLog(table.concat(chatlog,"\n"), name)
	end

	if arg[1]=="c" and contains(admins, name) then
	    for n in pairs(tfm.get.room.playerList) do
    		ui.addTextArea(1, "", n, 161, 73, 478, 254, 0x010101, 0x010101, 0.7)
    		ui.addTextArea(2, "", n, 171, 87, 164, 187, 0x010101, 0x010101, 0.3)
    		ui.addTextArea(3, "<img src=\"http://mforum.ist/avatar/"..avatar..".jpg\">", n, 195, 88, 124, 133, 0x000000, 0x000000, 0)
    		ui.addTextArea(4, "<font size=\"13\">"..cmd:sub(#arg[1]+2), n, 352, 86, 278, 229, 0x010101, 0x010101, 0.3)
    		ui.addTextArea(5, "<p align=\"center\"><font size=\"13\"><vp>"..string.upper(name).."</font>", n, 170, 201, 164, 28, 0x010101, 0x010101, 0)
    		ui.addTextArea(6, "<p align=\"center\"><a href=\"event:close\"><font size=\"13\">Close</font></a>", n, 172, 290, 164, 24, 0x010101, 0x010101, 0.3)
    		ui.addTextArea(7, players[name].gender, n, 184, 77, 100, 100, 0x000000, 0x000000, 0)
    	end
	end

	if arg[1]=="admin" and contains(admins, name) then if arg[2] == nil then yaz("<r>[#] error", name) else if not contains(admins, upper(arg[2])) then table.insert(admins, upper(arg[2])) yaz(param(getMsg("admin"), { player = upper(arg[2]) })) else yaz("<r>[#] error", name) end end end
	if arg[1]=="unadmin" and contains(admins, name) then if arg[2] == nil then yaz("<r>[#] error", name) else if contains(admins, upper(arg[2])) then table.delete(admins, upper(arg[2])) yaz(param(getMsg("unadmin"), { player = upper(arg[2]) })) end end end
	if arg[1]=="admins" then yaz(getMsg("currentAdmins").." "..table.concat(admins,", "), name) end
	if arg[1]=="ban" and contains(admins, name) then 
		if arg[2] == nil then 
			yaz("<r>[#] error", name) 
		else 

		if not contains(bans, upper(arg[2])) then 
			table.insert(bans, upper(arg[2]))
			tfm.exec.killPlayer(upper(arg[2]))
			yaz(param(getMsg("banned"), { player = upper(arg[2]) }))
			end 
		end 
	end
	if arg[1]=="unban" and contains(admins, name) then if arg[2] == nil then yaz("<r>[#] error", name) else if contains(bans, upper(arg[2])) then table.delete(bans, upper(arg[2])) yaz(param(getMsg("unbanned"), { player = upper(arg[2]) })) tfm.exec.respawnPlayer(upper(arg[2])) alive = alive + 1 end end end	
	if arg[1]=="bannedList" then yaz(getMsg("bannedList").." "..table.concat(bans,", "), name) end
	if arg[1]=="respawn" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.respawnPlayer(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.respawnPlayer(n) end end end
	if arg[1]=="kill" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.killPlayer(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.killPlayer(n) end end end
	if arg[1]=="victory" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.giveCheese(a) tfm.exec.playerVictory(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.giveCheese(n) tfm.exec.playerVictory(n) end end end
	if arg[1]=="cheese" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.giveCheese(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.giveCheese(arg[2]) end end end
	if arg[1]=="meep" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.giveMeep(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.giveMeep(n) end end end
	if arg[1]=="vampire" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.setVampirePlayer(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.setVampirePlayer(n) end end end
	if arg[1]=="shaman" and contains(admins, name) then local a = nil if arg[2] == nil or arg[2] == "me" then a = name elseif arg[2] == "all" then a = nil else a = arg[2] end if a ~= nil then tfm.exec.setShaman(a) else for n,d in pairs(tfm.get.room.playerList) do tfm.exec.setShaman(n) end end end
    if arg[1]=="np" or arg[1]=="map" and contains(admins, name) then local a = nil if arg[2] == nil then a = maps[math.random(#maps)] else a = arg[2] end if a ~= nil then tfm.exec.newGame(a) else tfm.exec.newGame(maps[math.random(#maps)]) end end
end

function eventTextAreaCallback(id, name, cb)
	if cb == "close" then ui.removeTextArea(1, name) ui.removeTextArea(2, name) ui.removeTextArea(3, name) ui.removeTextArea(4, name) ui.removeTextArea(5, name) ui.removeTextArea(6, name) ui.removeTextArea(7, name) end
	if cb == "closeCMDLog" then ui.removeTextArea(999, name) ui.removeTextArea(998, name) ui.removeTextArea(997, name) ui.removeTextArea(996, name) end
end


--[[for n in pairs(tfm.get.room.playerList) do
	eventNewPlayer(n)
end--]]

commands = {"np", "map", "respawn", "kill", "victory", "cheese", "meep", "vampire", "shaman", "emote", "c", "admin", "unadmin", "admins", "ban", "unban", "bannedList", "help", "cmdlog"}
for i,c in pairs(commands) do system.disableChatCommandDisplay(c) end
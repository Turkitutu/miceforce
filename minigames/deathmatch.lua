-- DEATHMATCH v0.1b --

admins = {Kmlcan = math.huge}
players = {}
game = {timer = 0, count = 0}
cmd = {"drag", "inv", "d", "cn", "off", "offset"}

maps = {401994, 401991, 105225, 113654, 114094, 118794, 150325, 103019, 112733, 108684, 115230, 122650, 150477, 134064, 145180, 114996, 105911, 103560, 111662}
cannons = {
		{
			img = "01_golden.4436",
			left = "01_golden_left.4491",
			right = "01_golden_right.4490",
			name = "Golden Cannon",
			desc = "You have to be first\non the seasonal leaderboard at the\nend of a season.",
			special = true
		},
		{
			img = "02_silver.4437",
			left = "02_silver_left.4492",
			right = "02_silver_right.4493",
			name = "Silver Cannon",
			desc = "You have to be\nsecond or higher\non the seasonal leaderboard at the\nend of a season.",
			special = true
		},
		{
			img = "03_bronze.4438",
			left = "03_bronze_left.4494",
			right = "03_bronze_right.4495",
			name = "Bronze Cannon",
			desc = "You have to be\nthird or higher\non the seasonal leaderboard at the\nend of a season.",
			special = true
		},
		{
			img = "04_balanced.4439",
			right = "04_balanced_right.4496",
			name = "Balanced Cannon",
			desc = "It might be time for a diet, you should eat more cheese."
		},
		{
			img = "05_plate_spike.4440",
			name = "Plate-Spike Cannon",
			desc = "This cannon was reinforced with metal plating and spikes. It must not have been deadly enough yet."
		},
		{
			img = "06_death.4441",
			left = "06_death_left.4497",
			right = "06_death_right.4498",
			name = "Death Cannon",
			desc = "This cannon was\nforged by\ndeath himself."
		},
		{
			img = "07_diamond_ore.4442",
			right = "07_diamond_right.4499",
			size = 11,
			name = "Diamond Ore Cannon",
			desc = "Why is there diamond in my cannon?"
		},
		{
			img = "08_lightning.4443",
			left = "08_lightning_left.4501",
			right = "08_lightning_right.4500",
			name = "Lightning Cannon",
			desc = "This cannon is\nlightning fast and hits as thunder."
		},
		{
			img = "09_star_cannon_of_death.4444",
			left = "09_star_left.4503",
			right = "09_star_right.4504",
			size = 11,
			name = "Star Cannon of Death",
			desc = "Not much a star and\nnot much death.\nYou can hit mice\nwith it though."
		},
		{
			img = "10_eyecannon.4445",
			right = "10_eye_right.4505",
			name = "Eye Cannon",
			desc = "Oh sh*t, we woke it up, how do we stop it?!"
		},
		{
			img = "11_baffbot.4446",
			left = "11_baffbot_left.4506",
			right = "11_baffbot_right.4507",
			name = "Baffbot Cannon",
			desc = "We shall never forget! Baffbot will remain in our hearts."
		},
		{
			img = "12_girly.4447",
			left = "12_girly_left.4508",
			right = "12_girly_right.4509",
			name = "Girly Cannon",
			desc = "Oh isn't that a cute\nball. No cheese\nfor you! ^_^"
		},
		{
			img = "13_disturbed_eye.4448",
			left = "13_disturbed_left.4510",
			right = "13_disturbed_right.4511",
			size = 11,
			name = "Disturbed Eye Cannon",
			desc = "There is no stopping\nit now, what have\nwe done?!"
		},
		{
			img = "14_cowcannon.4449",
			name = "Cow Cannon",
			desc = "Moooo~"
		},
		{
			img = "15_magma.4450",
			name = "Magma Cannon",
			desc = "Apply water to\nthe burn."
		},
		{
			img = "16_demon.4451",
			left = "16_demon_left.4512",
			right = "16_demon_right.4513",
			name = "Demon Cannon",
			desc = "A demon has corrupted this cannon and it hungers for more\nthan blood, it\nwants your soul!"
		},
		{
			img = "17_striped.4452",
			left = "17_striped_left.4514",
			right = "17_striped_right.4515",
			name = "Striped Cannon",
			desc = "It took a lot of effort to make this cannonball so polished and shiny."
		},
		{
			img = "18_vinecannon.4453",
			left = "18_vine_left.4516",
			right = "18_vine_right.4517",
			name = "Vine Cannon",
			desc = "It is said that with this cannonball you can end lives within 6 seconds."
		},
		{
			img = "19_recycle.4454",
			name = "Recycle Cannon",
			desc = "Come on, let's recycle some mice and show them what we are\nmade of."
		},
		{
			img = "20_water.4455",
			name = "Water Cannon",
			desc = "This cannonball is infused with a water spirit, making it the ideal weapon\nagainst fire."
		},
		{
			img = "21_aircannon.4456",
			name = "Air Cannon",
			desc = "This cannonball is infused with an air spirit, making it the ideal weapon for the people who love to fly."
		},
		{
			img = "22_flame.4457",
			name = "Flame Cannon",
			desc = "This cannonball is infused with a flame spirit, making it the ideal weapon for taking people down in style."
		},
		{
			img = "23_nature.4458",
			name = "Nature Cannon", 
			desc = "This cannonball is infused with a nature spirit, making it the ideal weapon for pretty much nothing."
		},
		{
			img = "24_music.4459",
			left = "24_music_left.4518",
			right = "24_music_right.4519",
			name = "Music Cannon",
			desc = "Going at the speed of sound, you will witness a sonic boom. Great for blowing mice off\nthe map."
		},
		{
			img = "25_godly.4460",
			left = "25_godly_left.4520",
			right = "25_godly_right.4521",
			name = "Godly Cannon",
			desc = "This cannon belongs\nto the true Gods\nof Deathmatch.", x = 25, y = 25
		},
		{
			img = "26_contest.4461",
			left = "26_contest_left.4522",
			right = "26_contest_right.4523",
			name = "Contest Cannon",
			desc = "You are a winner! You have earned yourself a star, with the power of fusion you can\ndefeat anyone!"
		},
		{
			img = "27_spiky.4463",
			left = "27_spiky_left.4524",
			right = "27_spiky_right.4525",
			name = "Spiky Cannon",
			desc = "A spike with the same force as a round ball can do more damage than just a ball.", x = 25, y = 25
		},
		{
			img = "27_power.4462",
			name = "Power Cannon",
			desc = "This cannonball has marks of war you can be proud of."
		},
		{
			img = "28_cookie.4464",
			name = "Cookie Cannon",
			desc = "They say don't play\nwith your food, but whenever I press this button they just\nkeep appearing."
		},
		{
			img = "29_sweet.4465",
			left = "29_sweet_left.4526",
			right = "29_sweet_right.4527",
			name = "Sweet Cannon",
			desc = "This cannon is very sweet but it's not very good for your teeth, unless you want\nbroken teeth."
		},
		{
			img = "30_daisy.4466",
			left = "30_daisy_left.4528",
			right = "30_daisy_right.4529",
			name = "Daisy Cannon",
			desc = "By putting a cute flower on my steel range weapon of death, it seems a lot less dangerous.", x = 25, y = 25
		},
		{
			img = "31_basscannon.4467",
			name = "Bass Cannon",
			desc = "Let the bass cannon kick it. With this wobbling force you\nare unstoppable."
		},
		{
			img = "32_drop_the_bass.4468",
			size = 11,
			name = "Drop the Bass Cannon",
			desc = "Fuwp fuwp fuwp fuwuwp! GUHYEEOWWWWWWW."
		},
}

function main()
	tfm.exec.disableAutoNewGame(true)
	tfm.exec.disableAutoShaman(true)
	tfm.exec.disableAutoTimeLeft(true)

	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	for k, v in pairs(cmd) do
		system.disableChatCommandDisplay(v)
	end

	playMap()
	for k, v in pairs(cannons) do
		v.special = true
		if v.right and not v.left then
			v.left = v.right
		end
	end
end

function eventNewGame()
	tfm.exec.setGameTime(123)
	tfm.exec.removeImage(game.count)

	game.started = nil
	game.timer = 0
end

function eventNewPlayer(n)
	local now = os.time()
	if not players[n] then
		players[n] = {
			offset = {x = -2, y = 10},
			images = {},
			cannons = {},
			last = now,
			lastUI = now,
			cannon = 0,
			wins = 0,
			rounds = 0,
			alive = 0,
			page = 1,
		}
	end

	local p = players[n]
	if not p.joined then
		tfm.exec.chatMessage("<VP>Fire a cannon using <J>spacebar<VP> or <J>duck<VP>!\nLast mouse alive wins!", n)
		sendLog(n, "You can now press <CH>U <BL>or type <CH>!inv <BL>to open the inventory.")
		p.logo = tfm.exec.addImage("dm_logo.4483", "&0", 202, 45, n)
		p.joined = now
	end

	for _, k in pairs({0, 2, 3, 32, 76, 80, 85}) do
		system.bindKeyboard(n, k, true, true)
	end
end

function eventChatCommand(n, c)
	local a = {}
	for b in c:gmatch("[^%s]+") do
		table.insert(a, b)
	end
	if has(a[1], "off", "offset") then
		if tonumber(a[2]) and tonumber(a[3]) then
			if math.abs(tonumber(a[2])) <= 20 and math.abs(tonumber(a[3])) <= 20 then
				players[n].offset = {x = tonumber(a[2]), y = tonumber(a[3])}
				sendLog(n, ("Your new offsets are now X: %d, Y: %d."):format(tonumber(a[2]), tonumber(a[3])))
			else
				sendLog(n, "Your offset must be between -20 and 20.")
			end
		else
			sendLog(n, "Syntax: !"..a[1].." X Y")
		end
	elseif has(a[1], "inv", "drag", "d", "cn") then
		if admins[n] and tonumber(a[2]) then
			local tar = a[3] or n
			table.insert(players[tar].cannons, tonumber(a[2]))
			if players[tar].inventory then
				showInventory(tar, true)
			end
		else
			showInventory(n)
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	local p = players[n]
	if has(k, 3, 32) and os.time() - p.last >= 1000 and game.started then
		if not tfm.get.room.playerList[n].isDead then
			local c = p.facingLeft
			if p.cid then
				tfm.exec.removeObject(p.cid)
			end
			p.cid = tfm.exec.addShamanObject(17, x + (c and -p.offset.x or p.offset.x), y + p.offset.y, c and -90 or 90)
			if p.cannon > 0 then
				local inf = cannons[p.cannon]
				tfm.exec.addImage(inf.right and (c and inf.left or inf.right) or inf.img, "#"..p.cid, inf.x and -inf.x or -16, inf.y and -inf.y or -16)
			end
			p.last = os.time()
		end
	elseif has(k, 0, 2) then
		p.facingLeft = k == 0 and true or nil
	elseif has(k, 85) then
		showInventory(n)
	end
end

function eventTextAreaCallback(i, n, c)
	local p, uiD = players[n]
	if c == "close" then
		if i == 112 then
			showInventory(n)
		end
	elseif has(c, "previous", "next") then
		if c == "previous" then
			if p.page > 1 then
				p.page = p.page - 1
				showInventory(n, true)
			end
		else
			if p.page < math.ceil(#cannons / 3) then
				p.page = p.page + 1
				showInventory(n, true)
			end
		end
	elseif c:sub(1, 5) == "equip" then
		local can = tonumber(c:sub(6))
		if p.cannon == can then
			p.cannon = 0
		else
			p.cannon = can
		end
		showInventory(n, true)
	end
end

function eventPlayerDied(n)
	local i = 0
	for k, v in pairs(tfm.get.room.playerList) do
		if not v.isDead then
			i = i + 1
			w = k
		end
	end

	if mouseCount() > 1 and i == 1 then
		ui.setMapName(tfm.get.room.xmlMapInfo.author.." <BL>- @"..tfm.get.room.xmlMapInfo.mapCode.."   <G>|   <N>Winner : <V>"..w)
		tfm.exec.setGameTime(10)
	elseif checkAlive() == 0 then
		tfm.exec.removeImage(game.count)
		tfm.exec.setGameTime(3)
		playMap()
	end
end

function eventPlayerLeft(n)
	eventPlayerDied(n)
end

function eventLoop(t, r)
	game.timer = game.timer + 0.5
	if game.timer == 3 then
		game.count = tfm.exec.addImage("countdown3.4482","&-1")
	elseif game.timer == 4 then
		tfm.exec.removeImage(game.count)
		game.count = tfm.exec.addImage("countdown2.4481","&-1")
	elseif game.timer == 5 then
		tfm.exec.removeImage(game.count)
		game.count = tfm.exec.addImage("countdown1.4480","&-1")
	elseif game.timer == 6 then
		tfm.exec.removeImage(game.count)
		game.count = tfm.exec.addImage("countdown0.4479","&-1")
		game.started = true
	elseif game.timer == 7 then
		tfm.exec.removeImage(game.count)
	end

	if r <= 0 then
		if checkAlive() == 1 then
			local n = checkAlive(true)
			tfm.exec.giveCheese(n)
			tfm.exec.playerVictory(n)
			players[n].wins = players[n].wins + 1
		end
		playMap()
	end

	for k, v in pairs(tfm.get.room.playerList) do
		local p = players[k]

		if p.logo and os.time() - p.joined >= 3000 then
			tfm.exec.removeImage(p.logo)
			p.logo = nil
		end

		if p.cid and os.time() - p.last >= 3000 then
			tfm.exec.removeObject(p.cid)
			p.cid = nil
		end
	end
end

function showInventory(n, u)
	if players[n].inventory then
		for k, v in pairs(players[n].images) do
			tfm.exec.removeImage(v)
		end
		players[n].images = {}
		for i = 100, 112 do
			ui.removeTextArea(i, n)
		end
		if not u then
			players[n].inventory = nil
			return
		end
	end

	players[n].inventory = true
	table.insert(players[n].images, tfm.exec.addImage("ui_shop_bg.4300", "&-1", 100, 60, n))
	table.insert(players[n].images, tfm.exec.addImage("dm_button_previous.4431", "&-1", 120, 80, n))
	table.insert(players[n].images, tfm.exec.addImage("dm_button_next.4430", "&-1", 660, 110, n))
	table.insert(players[n].images, tfm.exec.addImage("x_button.4303", "&-1", 660, 80, n))
	ui.addTextArea(110, "<a href='event:previous'>"..string.rep("\n", 30), n, 120, 80, 20, 260, 0, 1, 0)
	ui.addTextArea(111, "<a href='event:next'>"..string.rep("\n", 30), n, 660, 110, 20, 230, 0, 1, 0)
	ui.addTextArea(112, "<a href='event:close'>"..string.rep("\n", 30), n, 660, 80, 20, 20, 0, 1, 0)

	local buttons = {"dm_button_equip.4425", "dm_button_unequip.4426", "dm_button_buy.4427", "dm_button_notenough.4428", "dm_button_unavailable.4470"}
	local i, maxPage = 0, players[n].page
	for k, v in pairs(cannons) do
		if k > maxPage * 3 - 3 and k <= maxPage * 3 then
			i = i + 1
			table.insert(players[n].images, tfm.exec.addImage("dm_itembg.4429", "&-1", (i * 170) - 20, 80, n))
			table.insert(players[n].images, tfm.exec.addImage(v.img, "&0", (i * 170) + 44 - (v.x and v.x - 16 or 0), 144 - (v.y and v.y - 16 or 0), n))
			table.insert(players[n].images, tfm.exec.addImage((players[n].cannon == k and buttons[2]) or (table.contains(players[n].cannons, k) and buttons[1]) or (v.special and buttons[5]) or buttons[4], "&-1", (i * 170) - 5, 305, n))
			ui.addTextArea(100 + i, "<p align='center'><J><font size='"..(v.size or 12).."'>"..(v.name or ''), n, (i * 170) - 10, 215, 140, 90, 0, 1, 0)
			ui.addTextArea(103 + i, "<p align='center'><N><font size='10'>"..(v.desc or ''), n, (i * 170) - 10, 233, 140, 80, 0, 1, 0)
			if players[n].cannon == k or table.contains(players[n].cannons, k) then
				ui.addTextArea(106 + i, "<a href='event:equip"..k.."'>\n\n", n, (i * 170) - 5, 305, 130, 22, 0, 1, 0)
			end
		end
	end
end

function playMap()
	tfm.exec.newGame(maps[math.random(#maps)])
end

function sendLog(n, m, p)
	tfm.exec.chatMessage("<V>[#] <BL>"..m..((p and " = "..tostring(p)) or (p ~= nil and " = false") or ""), n)
end

function mouseCount()
	local i = 0
	for n in pairs(tfm.get.room.playerList) do
		i = i + 1
	end
	return i
end

function checkAlive(n)
	local i = 0
	for k, v in pairs(tfm.get.room.playerList) do
		if not v.isDead then
			i = i + 1
			w = k
		end
	end
	return n and w or i
end

function has(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

function table.contains(t, e)
	if e ~= nil then
		for k, v in pairs(t) do
			if v == e then
				return true
			end
		end
	end
	return false
end

main()
admins = {Kmlcan = math.huge, Chomar = 1, ["+wanheda"] = 10, Lee = 1, Kinta = 1}
tester = {Equalife = 1, Jany = 1, Jeun = 1, Jasbear = 1, Faolan = 1, Faye = 1, Kasuto = 1}
players, board, season, bans, transfer = {}, {}, {}, {}, {}

cmd = {"myui", "grank", "transfer", "set", "settings", "pw", "ver", "bans", "ban", "rank", "join", "lb", "board", "sb", "setlb", "exit", "off", "offset", "a", "map", "m", "np", "kmlcan", "cn", "ai", "ri", "shop", "market", "ffarp", "profile", "profil", "o", "p", "data", "resdata", "changedata", "cdata", "maps", "log", "rlog", "roomlog", "df"}
lang = {
	en = {
		welcome = "<ROSE>Welcome to FFA Race, %s! First mouse to finish the map using their cannon wins the round! Come on, it's time to race!",
		new = "<CH>New: <BL>Stone cannon now has a different texture and it's on sale. The shop design has been slightly changed.",
		diff = "Difficulty",
		diffNum = {"Easy", "Medium", "Hard"},
		antiFFA = "<R>You can't spawn cannons inside the dark gray grounds.",
		doubleFFA = "<CH>You can spawn two cannons inside the light gray grounds.",
		shop = "Shop",
		next = "Next",
		previous = "Previous",
		buy = "Buy",
		gift = "Gift",
		equip = "Equip",
		unequip = "Unequip",
		cannon = "Cannon",
		cannons = "Cannons",
		unavailable = "Unavailable",
		notEnough = "Not enough",
		mapCount = "<R>The minigame currently has %d maps.",
		hiddenCannon = "%s is now using the <CH>%s <BL>cannon.",
		settings = {notif = "Show special cannon notification.", num = "Hide cannon numbers.", challenge = "Let people challlenge me", mycn = "Show my owned cannons to other players.", logo = "Show the minigame logo everytime I join a room."},
		profile = "Profile",
		profStats = {"Gathered cheese: ", "Cheese gathered first: ", "Deaths: ", "Owned cannons: "},
		newOffset = "Your new offsets are now X: %d, Y: %d.",
		wrongOffset = "Your offset must be between -20 and 20.",
		alreadyHas = "This player already has this cannon.",
		giftedCannon = "<CH>%s <BL>has gifted you a cannon!",
		showCannons = "Show cannons owned",
		goback = "Back",
		leaderboard = "Leaderboard",
		lbTable = {"RANKINGS", "Rank", "Username", "Cheese", "First", "Death", "MMR"}
	}
}
shop = {
	{img = "ee7c8842a79811e7af4488d7f643024a.png", cost = 250, desc = "The cannon that belonged to the FFA Race gods... Or it's just a broken cannon. We're not too sure."},
	{img = "ffarace_hypno.4346", cost = 300, desc = "You might get hypnotized if you look at this cannon long enough. Be careful!"},
	{img = "ffarace_paint.4347", cost = 500, desc = "Someone spilled some on this cannon."},
	{img = "ffarace_hearts.4348", cost = 500, desc = "A cute design for people that are in love, or the ones that just like hearts."},
	{img = "ffarace_eye.4349", cost = 650, desc = "You don't need wings to fly, you need cannons."},
	{img = "3024eaaaa79911e7af4488d7f643024a.png", cost = 750},
	{img = "5910c3d0a79911e7af4488d7f643024a.png", cost = 750},
	{img = "64f41292a79911e7af4488d7f643024a.png", cost = 750, desc = "It's lit."},
	{img = "70665e96a79911e7af4488d7f643024a.png", cost = 750},
	{img = "77d5e00ca79911e7af4488d7f643024a.png", cost = 1000},
	{img = "82e0cd4aa79911e7af4488d7f643024a.png", cost = 1000},
	{img = "928e0e6aa79911e7af4488d7f643024a.png", cost = 1250},
	{img = "ffarace_dch.4350", cost = 500, desc = "This cannon contains the souls of the blue and pink shamans."},
	{f = 0, img = "ffarace_stone.4357", sale = 500, cost = 400, desc = "Someone stole the stone ground's texture and put it on this cannon. And we're totally fine with it."},
	{img = "aa2f3e4aa79911e7af4488d7f643024a.png", cost = 1000},
	{img = "b1344672a79911e7af4488d7f643024a.png", cost = 1000},
	{img = "b9a25a88a79911e7af4488d7f643024a.png", cost = 850},
	{img = "c134ee32a79911e7af4488d7f643024a.png", cost = 1500},
	{img = "ffarace_xd.4351", cost = 500, desc = "XD"},
	{img = "ceded8f4a79911e7af4488d7f643024a.png", cost = 750},
	{img = "ffarace_galaxy.4352", cost = 750},
	{img = "dc53463ca79911e7af4488d7f643024a.png", cost = 1500},
	{img = "e2d2a17ea79911e7af4488d7f643024a.png", cost = 750},
	{img = "1d640278a7a511e7af4488d7f643024a.png", hidden = "Contest Winner", x = -25, y = -25, desc = "Can be obtained by winning an official FFA Race competition."},
	{img = "f0f19e40a79911e7af4488d7f643024a.png", hidden = "Kmlcan", desc = "The man himself. Everyone knows him because he is the one and only. The developer of FFA Race..."},
	{img = "ffarace_mapper.4353", hidden = "Mapper", desc = "Can be obtained by making 10 or more maps for FFA Race."},
	{img = "0458187ea79a11e7af4488d7f643024a.png", cost = 750},
	{img = "ca71a1b8a7a611e7af4488d7f643024a.png", cost = 1250, x = -20, y = -20, desc = "If you always wanted to hit your friends with a bigger snowball, this cannon is perfect."},
	{img = "ffarace_skull.4354", cost = 2000, x = -21, y = -21, desc = '"Mors exspectat..."'},
	{img = "7908fa50a7a711e7af4488d7f643024a.png", cost = 2500, x = -25, y = -25, desc = "Ooh, a girly cannon!"},
	{img = "9f63f7d6a7a711e7af4488d7f643024a.png", cost = 2000},
	{img = "b3c1b952a7a711e7af4488d7f643024a.png", cost = 2500, x = -25, y = -25},
	{img = "c85869eca7a711e7af4488d7f643024a.png", cost = 2500, x = -25, y = -25},
	{img = "f7ab387ada1311e7b5f688d7f643024a.png", hidden = "Beta Tester", x = -33, y = -33, desc = "Can be obtained by being a beta tester for FFA Race."},
	{img = "ffarace_artist.4355", hidden = "Artist", x = -20, y = -20, desc = "Can be obtained by making art for FFA Race. Used to belong to Silverbea, the original artist of FFA Race."},
	{img = "f61e6b44d78411e7b5f688d7f643024a.png", cost = 1250, desc = "Ooh, it's pretty!"},
	{img = "ffarace_derp.4356", cost = 750, desc = "derp..."},
	{img = "4e8e8e48d81711e7b5f688d7f643024a.png", hidden = "Sevenops", desc = "Seven x Seven x Seven x Seven x Seven x Seven x Seven"},
	{img = "ae3a4594db7811e7b5f688d7f643024a.png", cost = 1250, desc = "Space? SPAAAACE!", new = true},
	{img = "14d9a024db7911e7b5f688d7f643024a.png", hidden = "Champion", x = -29, y = -29, desc = "Can be obtained by being top 1 on the seasonal leaderboard."},
	{img = "2bd03d08da1911e7b5f688d7f643024a.png", hidden = "Top 5", x = -29, y = -29, desc = "Can be obtained by being top 5 on the seasonal leaderboard."},
	{img = "4a5a97d4db7a11e7b5f688d7f643024a.png", hidden = "Top 10", x = -29, y = -29, desc = "Can be obtained by being top 10 on the seasonal leaderboard."}
}
room = {
	maps = {{424660, 3}, {420390, 2}, {420387, 3}, {420381, 1}, {420377, 2}, {420317, 2}, {418599, 1}, {414012, 1}, {414011, 3}, {414009, 2}, {414007, 3}, {414005, 1}, {414004, 2}, {306233, 2}, {398700, 3}, {399071, 1}, {399076, 2}, {400637, 2}, {401143, 2}, {400599, 1}, {400354, 1}, {398640, 2}, {398259, 2}, {398245, 2}, {398224, 3}, {397704, 3}, {397630, 1}, {397125, 2}, {397146, 3}, {397122, 2}, {396778, 1}, {393916, 2}, {393496, 1}, {392437, 1}, {392439, 1}, {392441, 1}, {392442, 3}, {392444, 1}, {392446, 2}, {392447, 1}, {392448, 3}, {392449, 3}, {392451, 2}, {392453, 2}, {392454, 1}, {392094, 2}, {392258, 2}, {392260, 2}, {392263, 3}, {392264, 3}, {392421, 1}, {392422, 2}, {392423, 1}, {392425, 1}, {392421, 2}, {392429, 1}, {392433, 3}, {392075, 2}, {392076, 1}, {392077, 3}, {392078, 1}, {392080, 2}, {392081, 2}, {392082, 2}, {392084, 1}, {392086, 3}, {392086, 2}, {392086, 2}, {392090, 2}, {392091, 3}, {392077, 2}, {391846, 2}, {391843, 3}, {391841, 3}, {391837, 3}, {391821, 3}, {391822, 3}, {391823, 3}, {391824, 1}, {391825, 2}, {391826, 1}, {391827, 1}, {391828, 1}, {391829, 3}, {391820, 2}, {390742, 1}, {390788, 3}, {390789, 2}, {390883, 2}, {391056, 2}, {391060, 1}, {391061, 1}, {391062, 1}, {391063, 2}, {391064, 2}, {391065, 2}, {391066, 2}, {391067, 1}, {391112, 2}, {391114, 2}, {391115, 3}, {391117, 3}, {391118, 2}, {391119, 2}, {391120, 2}, {391121, 1}, {391122, 1}},
	antiFFA = {},
	doubleFFA = {},
	lastRespawn = os.time(),
	round = 10,
	log = "<J><p align='center'><font size='30'>ROOM LOG</font></p>",
	lastLBUP = os.time() - 295000,
	version = 1.3
}

function main()
	system.loadPlayerData("_ffaracebans")
	nextMap()

	local com = tfm.get.room.community:lower()
	tfm.exec.disableAutoShaman(true)
	tfm.exec.disableAutoNewGame(true)
	tfm.exec.disableAutoTimeLeft(true)
	tfm.exec.disableAfkDeath(true)
	tfm.exec.disableAutoScore(true)

	room.com = lang[com] and com or "en"

	for k, v in pairs(cmd) do
		system.disableChatCommandDisplay(v)
	end

	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	setShop()
end

function eventNewGame()
	room.first = nil
	if mouseCount() == 1 then room.startGame = true else room.startGame = nil end
	room.lastRespawn = os.time()
	system.loadPlayerData("_ffaraceversion")

	if room.round == 10 then
		room.round = 0
		sendLog(nil, trans("new"))
	else
		room.round = room.round + 1
	end

	checkFFA()

	if #room.antiFFA > 0 then tfm.exec.chatMessage(trans("antiFFA")) end
	if #room.doubleFFA > 0 then tfm.exec.chatMessage(trans("doubleFFA")) end

	for n in pairs(tfm.get.room.playerList) do
		if bans[n:lower()] then
			tfm.exec.killPlayer(n)
		end
	end

	mapName()
end

function eventNewPlayer(n)
	if not players[n] then
		players[n] = {
			offset = {x = -2, y = 10},
			global = {win = 0, death = 0, cheese = 0, rank = "-"},
			season = {win = 0, death = 0, cheese = 0, rank = "-"},
			set = {},
			ui = {},
			lastCannon = os.time(),
			images = {},
			page = 0,
			lbPage = 0,
			ffarp = 0,
			cannons = {},
			win = 0,
			death = 0,
			cheese = 0,
			cannon = 0,
			lastUI = os.time(),
			hiddenCannon = os.time() - 30000,
			data = "",
			id = system.bilgi(n) and system.bilgi(n).code or nil,
			board = true
		}
	end
	if not players[n].welcomeMessage then
		tfm.exec.chatMessage(trans("welcome"):format(n), n)
		system.loadPlayerData(players[n].id and "ffadata"..players[n].id or n)
		players[n].welcomeMessage = true
	end
	for _, k in pairs({0, 2, 3, 32, 75, 76, 80, 85}) do
		system.bindKeyboard(n, k, true, true)
	end
end

function eventPlayerWon(n, t, r)
	local p = players[n]
	tfm.exec.respawnPlayer(n)
	tfm.exec.setPlayerScore(n, room.first and 1 or 5, true)
	if tfm.get.room.unique > 4 then
		p.global.cheese = p.global.cheese + 1
		--p.season.cheese = p.season.cheese + 1
		p.ffarp = p.ffarp + 2
	end
	if not room.first then
		room.first = true
		tfm.exec.setGameTime(20)
		if mouseCount() > 4 then
			p.global.win = p.global.win + 1
			--p.season.win = p.season.win + 1
			p.ffarp = p.ffarp + 8
		end
	end
	savePlayerData(n)
end

function eventPlayerLeft(n)
	savePlayerData(n)
	players[n] = nil
end

function eventPlayerDied(n)
	local p = players[n]
	if tfm.get.room.unique > 4 then
	    p.global.death = p.global.death + 1
	end
	--p.season.death = p.season.death + 1
	savePlayerData(n)
end

function eventPlayerDataLoaded(n, d)
	if n:sub(1, 1) == "*" then return end

	if n == "_ffaraceranking" then
		gRank = d
		local data = split(d, ";")
		local b = {}
		for k, v in pairs(board) do
			b[v.n] = {w = v.w, c = v.c, d = v.d, r = v.r}
		end

		for k, v in pairs(data) do
			local t = split(v, ",")
			b[t[1]] = {c = tonumber(t[2]), w = tonumber(t[3]), d = tonumber(t[4]), r = tonumber(t[5]), mmr = tonumber(t[6])}
		end

		for k, v in pairs(tfm.get.room.playerList) do
			local p = players[k]
			local ra = (p.global.win * 100) / p.global.cheese
			b[k] = {w = p.global.win, c = p.global.cheese, d = p.global.death, r = (ra >= 0 and ra <= 100) and ra or 0}
		end

		board = {}
		for k, v in pairs(b) do
			if not bans[k:lower()] then
				table.insert(board, {n = k, w = v.w, c = v.c, d = v.d, r = v.r, mmr = math.ceil(((v.c > 100 and (v.r * 2) or 0) + (v.w * 5) + (v.c / 2) - (v.d / 2)) / 2)})
			end
		end

		table.sort(board, function(a,b) return a.mmr > b.mmr end)
		saveLeaderboard()

		for k in pairs(tfm.get.room.playerList) do
			if players[k] then
				checkRank(k)
				if players[k].ui[3] then
					showLeaderboard(k, "")
				end
			end
		end
		return
	elseif n == "_ffaraceseasonal" then
		local data = split(d, ";")
		local b = {}
		for k, v in pairs(season) do
			b[v.n] = {w = v.w, c = v.c, d = v.d, r = v.r}
		end

		for k, v in pairs(data) do
			local t = split(v, ",")
			b[t[1]] = {c = tonumber(t[2]), w = tonumber(t[3]), d = tonumber(t[4]), r = tonumber(t[5]), mmr = tonumber(t[6])}
		end

		for k, v in pairs(tfm.get.room.playerList) do
			local p = players[k]
			local ra = (p.season.win * 100) / p.season.cheese
			b[k] = {w = p.season.win, c = p.season.cheese, d = p.season.death, r = (ra >= 0 and ra <= 100) and ra or 0}
		end

		season = {}
		for k, v in pairs(b) do
			if not bans[k:lower()] then
				table.insert(season, {n = k, w = v.w, c = v.c, d = v.d, r = v.r, mmr = math.ceil(((v.c > 100 and (v.r * 2) or 0) + (v.w * 5) + (v.c / 2) - (v.d / 2)) / 2)})
			end
		end

		table.sort(season, function(a,b) return a.mmr > b.mmr end)
		saveSeasonal()


		--[[local rank = 0
		for k, v in pairs(season) do
			rank = rank + 1
			if rank < 11 then
				if tfm.get.room.playerList[v.n] then
					if rank < 11 and not table.contains(players[v.n].cannons, 42) then
						table.insert(players[v.n].cannons, 42)
						sendLog(v.n, "Acquired cannon: Season 1 Top 10")
					end

					if rank < 6 and not table.contains(players[v.n].cannons, 41) then
						table.insert(players[v.n].cannons, 41)
						sendLog(v.n, "Acquired cannon: Season 1 Top 5")
					end

					if rank == 1 and not table.contains(players[v.n].cannons, 40) then
						table.insert(players[v.n].cannons, 40)
						sendLog(v.n, "Acquired cannon: Season 1 Champion")
					end
				end
			end
		end]]

		for k in pairs(tfm.get.room.playerList) do
			if players[k] then
				checkRank(k)
				if players[k].ui[3] then
					showLeaderboard(k, "")
				end
			end
		end
		return
	elseif n == "_ffaracebans" then
		local data = split(d, ";")
		for k, v in pairs(data) do
			bans[v] = true
		end
		saveBans()
		return
	elseif n == "_ffaraceversion" then
		local ver = tonumber(d) or 1.0
		if room.version ~= ver then
			tfm.exec.chatMessage("<FC>You're playing an older version of FFA Race. Please leave this room to play the updated version.")
		end
		return
	end

	local id, player = tonumber(n:sub(8))
	if n:sub(1, 7) == "ffadata" then
		for k, v in pairs(tfm.get.room.playerList) do
			if id == system.bilgi(k).code then
				player = k
			end
		end
	end

	if #d < 1 then
		for k, v in pairs(tfm.get.room.playerList) do
			if id == system.bilgi(k).code then
				player = k
			end
		end
		if player then
			system.loadPlayerData(player)
		end
		return
	end

	player = player or n
	local p = players[player]
	if d:find(";") then
		local data = split(d, ";")
		p.data = d
		p.ffarp = tfm.get.room.name == "#kc" and math.huge or tonumber(data[1]) or 0
		p.cannon = tonumber(data[2]) or 0
		p.global.win = tonumber(data[3]) or 0
		p.global.death = tonumber(data[4]) or 0
		p.global.cheese = tonumber(data[5]) or 0
		p.offset = {x = tonumber(data[6]) or -2, y = tonumber(data[7]) or 10}
		p.set.notif = tonumber(data[9]) == 1 and true or nil
		p.set.num = tonumber(data[10]) == 1 and true or nil
		--p.set.challenge = tonumber(data[11]) == 1 and true or nil
		p.set.mycn = tonumber(data[12]) == 1 and true or nil
		p.set.logo = tonumber(data[13]) == 1 and true or nil
		p.season.win = tonumber(data[14]) or 0
		p.season.death = tonumber(data[15]) or 0
		p.season.cheese = tonumber(data[16]) or 0
		if data[8] and data[8] ~= "0" then
			local cannons = split(data[8], ",")
			local numCannons = {}
			for k, v in pairs(cannons) do
				table.insert(numCannons, tonumber(v))
			end
			p.cannons = numCannons
		end
		if not p.set.logo then
			p.wcIMG = tfm.exec.addImage("ffarace_logo.4345", "&0", 181, 50, player)
			p.wcTime = os.time()
		end
		checkRank(player)
	end
	if not table.contains(p.cannons, 34) and tester[player] then
		table.insert(p.cannons, 34)
		sendLog(n, "Acquired cannon: Beta Tester")
	end
end

function eventLoop(t, r)
	if r <= 0 then
		nextMap()
		for n in pairs(tfm.get.room.playerList) do
			savePlayerData(n)
		end
	end

	if room.lastLBUP + 300000 - os.time() <= 0 then
		room.lastLBUP = os.time() - 1
		system.loadPlayerData("_ffaraceranking")
		--system.loadPlayerData("_ffaraceseasonal")
		system.loadPlayerData("_ffaracebans")
	end

	if t >= 3000 and not room.startGame then
		room.startGame = true
	end

	if os.time() - room.lastRespawn >= 5000 then
		for n, p in pairs(tfm.get.room.playerList) do
			if p.isDead and not bans[n:lower()] then
				tfm.exec.respawnPlayer(n)
			end
		end
		room.lastRespawn = os.time()
	end
	for n in pairs(tfm.get.room.playerList) do
		if players[n].ui[3] then
			updateCountdown(n)
		end
		if players[n].backToShop and os.time() - players[n].backToShop >= 200 then
			players[n].ui[22] = {}
			removeUI(22, n)
			removeButton(51, n)
			removeButton(52, n)
			ui.removeTextArea(53, n)
			players[n].backToShop = nil
		end
		if players[n].lastCannon then
			if players[n].cannonID and os.time() - players[n].lastCannon >= 3000 then
				tfm.exec.removeObject(players[n].cannonID)
				players[n].cannonID = nil
				if players[n].cannonID2 then
					tfm.exec.removeObject(players[n].cannonID2)
					players[n].cannonID2 = nil
				end
			end
			if players[n].wcIMG and players[n].wcTime and os.time() - players[n].wcTime >= 3000 then
				tfm.exec.removeImage(players[n].wcIMG)
				players[n].wcIMG = nil
			end
		end
	end
end

function eventChatCommand(n, c)
	room.log = room.log.."<V>["..n.."] <N>!"..c.."\n"
	local a = {}
	for b in c:gmatch("[^%s]+") do
		table.insert(a, b)
	end
	if a[1] ~= "a" then
		for k, v in pairs(admins) do
			if players[k] and players[k].log then
				sendLog(k, "["..n.."] <BL>!"..c)
			end
		end
	end
	
	if has(a[1], "off", "offset") then
		if tonumber(a[2]) and tonumber(a[3]) then
			if math.abs(tonumber(a[2])) <= 20 and math.abs(tonumber(a[3])) <= 20 then
				players[n].offset = {x = tonumber(a[2]), y = tonumber(a[3])}
				sendLog(n, trans("newOffset"):format(tonumber(a[2]), tonumber(a[3])))
			else
				sendLog(n, trans("wrongOffset"))
			end
		else
			sendLog(n, "Syntax: !"..a[1].." X Y")
		end
	elseif a[1] == "maps" then
		sendLog(n, trans("mapCount"):format(#room.maps))
	elseif has(a[1], "shop", "market") then
		showShop(n)
	elseif has(a[1], "lb", "board") then
		showLeaderboard(n)
	elseif has(a[1], "set", "settings") then
		showSettings(n)
	elseif has(a[1], "transfer") then
		if a[2] then
			local player = a[2]:lower()
			if transfer[player] and transfer[player] == n:lower() then
				local fixedP = player:sub(1, 1):upper()..player:sub(2):lower()
				local p1, p2 = players[fixedP], players[n]
				local data1 = (p1.ffarp or 0)..";"..(p1.cannon or "")..";"..(p1.global.win or 0)..";"..(p1.global.death or 0)..";"..(p1.global.cheese or 0)..";"..(p1.offset.x or -2)..";"..(p1.offset.y or 10)..";"..(#p1.cannons > 0 and table.concat(p1.cannons, ",") or 0)..";"..(p1.set.notif and 1 or 0)..";"..(p1.set.num and 1 or 0)..";"..(p1.set.challenge and 1 or 0)..";"..(p1.set.mycn and 1 or 0)..";"..(p1.set.logo and 1 or 0)..";"..(p1.season.win or 0)..";"..(p1.season.death or 0)..";"..(p1.season.cheese or 0)
				local data2 = (p2.ffarp or 0)..";"..(p2.cannon or "")..";"..(p2.global.win or 0)..";"..(p2.global.death or 0)..";"..(p2.global.cheese or 0)..";"..(p2.offset.x or -2)..";"..(p2.offset.y or 10)..";"..(#p2.cannons > 0 and table.concat(p2.cannons, ",") or 0)..";"..(p2.set.notif and 1 or 0)..";"..(p2.set.num and 1 or 0)..";"..(p2.set.challenge and 1 or 0)..";"..(p2.set.mycn and 1 or 0)..";"..(p2.set.logo and 1 or 0)..";"..(p2.season.win or 0)..";"..(p2.season.death or 0)..";"..(p2.season.cheese or 0)
				sendLog(fixedP, "Succesfully transfered your data.")
				sendLog(n, "Succesfully transfered your data.")
				p1.cannons, p2.cannons = {}, {}
				system.savePlayerData("ffadata"..p1.id, data2)
				system.loadPlayerData("ffadata"..p1.id)
				system.savePlayerData("ffadata"..p2.id, data1)
				system.loadPlayerData("ffadata"..p2.id)
				for k, v in pairs(board) do
					if has(v.n, fixedP, n) then
						table.remove(board, k)
					end
				end
				for k, v in pairs(season) do
					if has(v.n, fixedP, n) then
						table.remove(season, k)
					end
				end
				saveLeaderboard()
				saveSeasonal()
			end
		else
			transferData(n)
		end
	elseif has(a[1], "profile", "profil", "p", "o") then
		showProfile(n, a[2] or n)
	elseif admins[n] then
		if has(a[1], "map", "m", "np") then
			nextMap(a[2])
		elseif has(a[1], "log") then
			players[n].log = not players[n].log
			sendLog(n, "log", players[n].log)
		elseif has(a[1], "rlog", "roomlog") then
			ui.addLog(room.log, n)
		elseif c:sub(1, 2) == "a " then
			tfm.exec.chatMessage("<R>• ["..n.."] <N>"..(admins[n] == math.huge and html(c:sub(3), true) or c:sub(3)))
		elseif has(a[1], "bans") then
			local list = {}
			for k, v in pairs(bans) do
				table.insert(list, k:sub(1,1):upper()..k:sub(2):lower())
			end
			sendLog(n, "Banned players: "..(#list > 0 and table.concat(list, ", ") or "None")..".")
		elseif has(a[1], "ban") and a[2] then
			local banned, msg = a[2]:lower()
			local upper = banned:sub(1,1):upper()..banned:sub(2):lower()
			if bans[banned] then
				msg = "<CH>"..upper.." <BL>has been unbanned."
				bans[banned] = nil
			else
				local reason
				if a[3] then
					table.remove(a, 1)
					table.remove(a, 1)
					reason = table.concat(a, " ")
				end
				msg = "<CH>"..upper.." <BL>has been banned"..(reason and ", reason: "..reason or ".")
				tfm.exec.killPlayer(banned)
				bans[banned] = true
			end
			sendLog(nil, msg)
			saveBans()
		elseif admins[n] > 9 then
			if a[1] == "ai" then
				local id = tfm.exec.addImage(a[2], html(a[3], true), tonumber(a[4]), tonumber(a[5]), a[6])
				sendLog(n, "ID", id)
			elseif a[1] == "ri" then
				tfm.exec.removeImage(a[2])
			elseif a[1] == "cn" then
				local pN = a[3] and fixName(a[3]) or n
				if has(a[2], "hidden", "all") then
					for k, v in pairs(shop) do
						if a[2] == "hidden" and v.hidden then
							table.insert(players[pN].cannons, v.n)
						elseif a[2] == "all" and not v.hidden then
							table.insert(players[pN].cannons, v.n)
						end
					end
				else
					table.insert(players[pN].cannons, tonumber(a[2]))
				end
			elseif admins[n] > 99 then
				if a[1] == "resdata" then
					local pN = a[2] and fixName(a[2]) or n
					players[pN].cannons = {}
					system.savePlayerData(pN, "0;0;0;0;0;-2;10;0;0;0;0;0;0;0;0;0") --0;0;0;0;0;-2;10;0;0;0;0;0;0;0;0;0
					system.loadPlayerData(pN)
				elseif has(a[1], "data") then
					sendLog(n, players[a[2] or n].data)
				elseif has(a[1], "changedata", "cdata") then
					local pN = a[3] and fixName(a[3]) or n
					system.savePlayerData(pN, a[2])
					system.loadPlayerData(pN)
				elseif has(a[1], "df") then
					local pN = a[2] and fixName(a[2]) or n
					players[pN].df = not players[pN].df
					sendLog(n, "df["..pN.."]", players[pN].df)
				elseif has(a[1], "sb") then
					eventPlayerDataLoaded("_ffaraceranking", "")
					eventPlayerDataLoaded("_ffaraceseasonal", "")
				elseif has(a[1], "setlb") then
					saveLeaderboard()
				elseif has(a[1], "rank") then
					checkRank(a[2] or n)
				elseif has(a[1], "join") and a[2] then
					players[fixName(a[2])] = nil
					eventNewPlayer(fixName(a[2]))
				elseif a[1] == "ffarp" then
					local pN = not tonumber(a[2]) and fixName(a[2]) or n
					players[pN].ffarp = tonumber(a[2]) or tonumber(a[3]) or 0
				elseif a[1] == "ver" then
					system.savePlayerData("_ffaraceversion", room.version)
					room.log = room.log.."<V>[#] <BL>Succesfully saved the version data.\n"
				elseif has(a[1], "exit") then
					system.exit()
				elseif has(a[1], "pw") then
					tfm.exec.setRoomPassword(a[2] or "")
					sendLog(n, "Password: "..(a[2] or ""))
				elseif has(a[1], "grank") then
					ui.addLog(gRank or "", n)
				elseif has(a[1], "myui") then
					sendLog(n, tostring(getUI(n)))
				end
			end
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	local p = players[n]
	if has(k, 3, 32) and os.time() - p.lastCannon >= 1000 and room.startGame then
		local canSpawn = 1
		for a, b in pairs(room.antiFFA) do
			if b.c and circle(x, y, b.x, b.y, b.l) or checkRec(x, b.x, y, b.y, b.l, b.h, b.a) then
				return
			end
		end
		for a, b in pairs(room.doubleFFA) do
			if b.c and circle(x, y, b.x, b.y, b.l) or checkRec(x, b.x, y, b.y, b.l, b.h, b.a) then
				canSpawn = 2
				break
			end
		end
		if canSpawn and not tfm.get.room.playerList[n].isDead then
			local c = p.facingLeft
			p.offset.x, p.offset.y = p.offset.x or -2, p.offset.y or 10
			if p.cannonID then tfm.exec.removeObject(p.cannonID) end
			if p.cannonID2 then tfm.exec.removeObject(p.cannonID2) end
			p.cannonID = tfm.exec.addShamanObject(17, x + (c and -p.offset.x or p.offset.x), y + p.offset.y, c and -90 or 90)
			if canSpawn == 2 or players[n].df then p.cannonID2 = tfm.exec.addShamanObject(17, x + (c and -p.offset.x or p.offset.x), y + p.offset.y, c and -90 or 90) end
			if p.cannon and tonumber(p.cannon) > 0 then
				local cn
				for a, b in pairs(shop) do
					if b.n == p.cannon then
						cn = a
					end
				end
				tfm.exec.addImage(shop[cn].img, "#"..p.cannonID, shop[cn].x or -16, shop[cn].y or -16)
				if canSpawn == 2 or players[n].df then
					tfm.exec.addImage(shop[cn].img, "#"..p.cannonID2, shop[cn].x or -16, shop[cn].y or -16)
				end
			end
			p.lastCannon = os.time()
		end
	elseif has(k, 0) then
		players[n].facingLeft = true
	elseif has(k, 2) then
		players[n].facingLeft = nil
	elseif has(k, 75) then
		room.log = room.log.."<V>["..n.."] [K]<N> "..k.."\n"
		showSettings(n)
	elseif has(k, 76) then
		room.log = room.log.."<V>["..n.."] [K]<N> "..k.."\n"
		showLeaderboard(n)
	elseif has(k, 85) then
		room.log = room.log.."<V>["..n.."] [K]<N> "..k.."\n"
		showShop(n)
	elseif has(k, 80) then
		room.log = room.log.."<V>["..n.."] [K]<N> "..k.."\n"
		if p.ui == 31 then
			eventTextAreaCallback(140031, n, "close")
		else
			showProfile(n)
		end
	end
end

function eventTextAreaCallback(i, n, c)
	local p, uiD = players[n]
	if c == "close" then
		uiD = i - 17000
		if uiD == 33 then
			showLeaderboard(n)
		elseif uiD == 2 then
			showProfile(n)
		elseif uiD == 1 then
			showShop(n)
		elseif uiD == 4 then
			showSettings(n)
		elseif uiD == 5 then
			transferData(n)
		elseif uiD == 11 then
			removeUI(11, n)
			removeButton(12, n)
		end
	elseif c:sub(1, 11) == "showCannons" then
		local target, i1, i2, i3 = c:sub(12), 1, 1, 0
		showProfile(n)
		addUI(11, "", n, 85, 65, 630, 300)
		addButton(11, "<a href='event:close'>Close", n, 100, 335, 480)
		addButton(12, "<a href='event:goback"..target.."'>"..trans("goback"), n, 600, 335, 100)
		--textAdd(31, "", n, 400, 215, 600, 300, false, true, trans("cannons").." ("..target..")")
		--ui.addTextArea(41, "<a href='event:goback"..target.."'><font size='13'><p align='center'>"..trans("goback").."\n\n", n, 570, 335, 120, 20, 0x27373F, 0x1B252A, 1)
		--p.ui = 31
		for k, v in pairs(shop) do
			if table.contains(players[target].cannons, v.n) then
				i3 = i3 + 1
				if i3 < 41 then
					table.insert(p.ui[11], tfm.exec.addImage(v.img, "&0", 70 + (i1 * 60) + (v.x or -16), 50 + (i2 * 60) + (v.y or -16), n))
					i1 = i1 + 1
					if i1 == 11 then
						i2 = i2 + 1
						i1 = 1
					end
				end
			end
		end
	elseif c:sub(1, 6) == "goback" then
		local target = c:sub(7) or nil
		if p.ui[3] and players[target] then
			showLeaderboard(n)
			p.lastUI = 0
			showProfile(n, c:sub(7), nil, true)
		elseif p.ui[11] then
			removeUI(11, n)
			removeButton(12, n)
			showProfile(n, target)
		elseif p.ui[2] then
			showProfile(n)
			p.lastUI = 0
			showLeaderboard(n)
		elseif p.ui[21] then
			removeUI(21, n)
			p.ui[21] = nil
			p.ui[22] = nil
			showShop(n)
			p.backToShop = os.time()
		elseif p.ui[23] then
			removeUI(23, n)
			eventTextAreaCallback(23, n, "cannon"..target)
		end
	elseif c:sub(1, 6) == "offset" then
		local xy = c:sub(7, 7)
		if (p.offset[xy] == 20 and c:sub(8, 8) == "+") or (p.offset[xy] == -20 and c:sub(8, 8) == "-") then return end
		p.offset[xy] = p.offset[xy] + (c:sub(8) == "+" and 1 or -1)
		showProfile(n, nil, true)
	elseif c:sub(1, 9) == "changeset" then
		if p.set[c:sub(10)] then
			p.set[c:sub(10)] = nil
		else
			p.set[c:sub(10)] = true
		end
		showSettings(n, true)
	elseif has(c, "previous", "next") then
		showShop(n, c)
	elseif c:sub(1, 3) == "buy" then
		local cnNum = tonumber(c:sub(4))
		if p.ffarp >= shop[cnNum].cost then
			p.ffarp = p.ffarp - shop[cnNum].cost
			table.insert(p.cannons, shop[cnNum].n)
			eventTextAreaCallback(i, n, "cannon"..shop[cnNum].n)
		end
	elseif c:sub(1, 4) == "gift" then
		local given = split(c, ";")
		if given[2] then
			local cnNum = tonumber(given[1]:sub(5))
			if table.contains(players[given[2]].cannons, shop[cnNum].n) then
				sendLog(n, trans("alreadyHas"))
			else
				table.insert(players[given[2]].cannons, shop[cnNum].n)
				p.ffarp = p.ffarp - shop[cnNum].cost
				sendLog(given[2], trans("giftedCannon"):format(n))
				sendLog(n, "Your gift has been sent!")
			end
			eventTextAreaCallback(17023, n, "goback"..c:sub(5))
		else
			local msg = "<font size='13'>"
			for k, v in pairs(tfm.get.room.playerList) do
				if k ~= n then
					msg = msg.."<V>• "..trans("gift")..": <N><a href='event:"..c..";"..k.."'>"..k.."</a>\n"
				end
			end
			for k, v in pairs(p.ui) do
				removeUI(k, n)
			end
			addUI(21, msg, n, 200, 40, 400, 345, "<a href='event:goback"..c:sub(5).."'>"..trans("goback"))
			--textAdd(22, msg, n, 20, 40, 190, 345, false, false, trans("gift"))
		end
	elseif c:sub(1, 5) == "equip" then
		p.set.autoEquip = true
		eventTextAreaCallback(i, n, "cannon"..c:sub(6))
		p.set.autoEquip = nil
	elseif c:sub(1, 6) == "cannon" then
		local num, cnNum, cnTable = tonumber(c:sub(7))
		p.lastUI, p.backToShop = 0, nil
		for k, v in pairs(shop) do
			if v.n == num then
				cnTable, cnNum = v, k
			end
		end
		if p.cannon == num and p.set.autoEquip then
			p.cannon = nil
			local available = p.cannon == num and "<a href='event:equip"..num.."'>"..trans("unequip") or table.contains(p.cannons, num) and "<a href='event:equip"..num.."'>"..trans("equip") or cnTable.hidden and "<BL>"..trans("unavailable") or p.ffarp >= cnTable.cost and "<a href='event:buy"..cnNum.."'>"..trans("buy") or "<BL>"..trans("notEnough")
			local gift = cnTable.hidden and "<BL>"..trans("gift") or (p.ffarp >= cnTable.cost and "<a href='event:gift"..cnNum.."'>" or "<BL>")..trans("gift")
			local color = "0x"..((p.cannon == num and "0091c3") or (table.contains(p.cannons, num) and "935a00") or (cnTable.hidden and "777777") or (p.ffarp < cnTable.cost and "d53838") or "3B721D")

			addUI(22, "\n\n\n\n<p align='center'><H>"..(cnTable.hidden or cnTable.cost.." FFARP"), n, 220, 100, 360, 150, nil, true, color)
			addButton(51, "<p align='center'>"..available, n, 240, 185, 320, nil)
			addButton(52, "<p align='center'>"..gift, n, 240, 217, 320, nil)
		elseif table.contains(p.cannons, num) and p.set.autoEquip then
			p.cannon = num
			local available = p.cannon == num and "<a href='event:equip"..num.."'>"..trans("unequip") or table.contains(p.cannons, num) and "<a href='event:equip"..num.."'>"..trans("equip") or cnTable.hidden and "<BL>"..trans("unavailable") or p.ffarp >= cnTable.cost and "<a href='event:buy"..cnNum.."'>"..trans("buy") or "<BL>"..trans("notEnough")
			local gift = cnTable.hidden and "<BL>"..trans("gift") or (p.ffarp >= cnTable.cost and "<a href='event:gift"..cnNum.."'>" or "<BL>")..trans("gift")
			local color = "0x"..((p.cannon == num and "0091c3") or (table.contains(p.cannons, num) and "935a00") or (cnTable.hidden and "777777") or (p.ffarp < cnTable.cost and "d53838") or "3B721D")

			addUI(22, "\n\n\n\n<p align='center'><H>"..(cnTable.hidden or cnTable.cost.." FFARP"), n, 220, 100, 360, 150, nil, true, color)
			addButton(51, "<p align='center'>"..available, n, 240, 185, 320, nil)
			addButton(52, "<p align='center'>"..gift, n, 240, 217, 320, nil)
			if cnTable.hidden and #cnTable.hidden > 0 and os.time() - p.hiddenCannon >= 15000 and not p.set.notif then
				sendLog(nil, trans("hiddenCannon"):format(n, cnTable.hidden))
				p.hiddenCannon = os.time()
			end
		else
			if p.ui[1] then
				showShop(n)
			end
			--textAdd(21, "", n, 650, 175, 120, 150, false, true, trans("cannon"))
			local available = p.cannon == num and "<a href='event:equip"..num.."'>"..trans("unequip") or table.contains(p.cannons, num) and "<a href='event:equip"..num.."'>"..trans("equip") or cnTable.hidden and "<BL>"..trans("unavailable") or p.ffarp >= cnTable.cost and "<a href='event:buy"..cnNum.."'>"..trans("buy") or "<BL>"..trans("notEnough")
			local gift = cnTable.hidden and "<BL>"..trans("gift") or (p.ffarp >= cnTable.cost and "<a href='event:gift"..cnNum.."'>" or "<BL>")..trans("gift")
			local color = "0x"..((p.cannon == num and "0091c3") or (table.contains(p.cannons, num) and "935a00") or (cnTable.hidden and "777777") or (p.ffarp < cnTable.cost and "d53838") or "3B721D")

			addUI(21, "", n, 200, 80, 400, 270, "<a href='event:goback'>"..trans("goback"))
			addUI(22, "\n\n\n\n<p align='center'><H>"..(cnTable.hidden or cnTable.cost.." FFARP"), n, 220, 100, 360, 150, nil, true, color)
			addButton(51, "<p align='center'>"..available, n, 240, 185, 320, nil)
			addButton(52, "<p align='center'>"..gift, n, 240, 217, 320, nil)
			if cnTable.desc then
				ui.addTextArea(53, "<p align='center'><i><BL>"..cnTable.desc, n, 220, 260, 360, 50, 0, 1, 0)
			end
			table.insert(p.ui[21], tfm.exec.addImage(cnTable.img, "&0", 400 + (cnTable.x or -16), 135 + (cnTable.y or -16), n))
		end
	elseif c:sub(1, 2) == "lb" then
		showLeaderboard(n, c:sub(3))
	elseif c == "boardStyle" then
		p.board = not p.board
		p.lbPage = 0
		showLeaderboard(n, "")
	end
end

--[[function savePlayerData(n)
	if n:sub(1, 1) == "*" then return end
	if tfm.get.room.name:find("#ffarace") or tfm.get.room.name:find("#kc") then
		local p = players[n]
		local data = (p.ffarp or 0)..";"..(p.cannon or "")..";"..(p.global.win or 0)..";"..(p.global.death or 0)..";"..(p.global.cheese or 0)..";"..(p.offset.x or -2)..";"..(p.offset.y or 10)..";"..(#p.cannons > 0 and table.concat(p.cannons, ",") or 0)..";"..(p.set.notif and 1 or 0)..";"..(p.set.num and 1 or 0)..";"..(p.set.autoEquip and 1 or 0)..";"..(p.set.mycn and 1 or 0)..";"..(p.set.logo and 1 or 0)..";"..(p.season.win or 0)..";"..(p.season.death or 0)..";"..(p.season.cheese or 0)
		system.savePlayerData(n, data)
	end
end]]

function savePlayerData(n)
	if n:sub(1, 1) ~= "*" and players[n] and (tfm.get.room.name:find("#ffarace") or tfm.get.room.name:find("#kc")) then
		local p = players[n]
		local data = (p.ffarp or 0)..";"..(p.cannon or 0)..";"..(p.global.win or 0)..";"..(p.global.death or 0)..";"..(p.global.cheese or 0)..";"..(p.offset.x or -2)..";"..(p.offset.y or 10)..";"..(#p.cannons > 0 and table.concat(p.cannons, ",") or 0)..";"..(p.set.notif and 1 or 0)..";"..(p.set.num and 1 or 0)..";"..(p.set.challenge and 1 or 0)..";"..(p.set.mycn and 1 or 0)..";"..(p.set.logo and 1 or 0)..";"..(p.season.win or 0)..";"..(p.season.death or 0)..";"..(p.season.cheese or 0)
		system.savePlayerData("ffadata"..p.id, data)
	end
end

function setShop()
	for k, v in ipairs(shop) do
		v.n = k
		if not v.cost then
			v.cost = 9e9
		end
		if not v.f then
			v.f = v.cost
		end
	end
	table.sort(shop, function(a,b) return a.f < b.f end)
end

function saveBans()
	if has(tfm.get.room.name, "#ffarace", "#kc") then
		local str = ""
		for k, v in pairs(bans) do
			str = str..k..";"
		end
		room.log = room.log.."<V>[#] <BL>Succesfully saved the bans data.\n"
		system.savePlayerData("_ffaracebans", str)
	end
end

function saveLeaderboard()
	if has(tfm.get.room.name, "#ffarace", "#kc") then
		local str = ""
		for k, v in pairs(board) do
			if k == 101 then break end
			str = str..v.n..","..v.c..","..v.w..","..v.d..","..v.r..","..v.mmr..";"
		end
		room.log = room.log.."<V>[#] <BL>Succesfully saved the global data.\n"
		system.savePlayerData("_ffaraceranking", str)
	end
end

function saveSeasonal()
	return
end--[[
	if has(tfm.get.room.name, "#ffarace", "#kc") then
		local str = ""
		for k, v in pairs(season) do
			if k == 101 then break end
			str = str..v.n..","..v.c..","..v.w..","..v.d..","..v.r..","..v.mmr..";"
		end
		room.log = room.log.."<V>[#] <BL>Succesfully saved the seasonal data.\n"
		system.savePlayerData("_ffaraceseasonal", str)
	end
end]]

function transferData(n)
	local p = players[n]
	if p.ui[5] then
		if not u then
			removeUI(5, n)
			p.lastUI = os.time()
			ui.addPopup(999, 2, "", n, 9e9, 9e9, 10, true)
			return
		end
	elseif table.count(p.ui) > 0 and not p.ui[5] then
		return
	end

	local msg = "<J><p align='center'><font size='18'>About stats getting reset after name changes</font></p><font size='12'><V>•<N> Make sure you make an alt account with your old username so that your data still remains in your hands.\n<V>• <N>This is the page for manual data transfering. Please close this page if you haven't changed your username.\n<V>• <N>Log on with your old username and go to a #ffarace room, and put your username here. After that type <ROSE>!transfer "..n.."<N> on your new account.\n<V>• <N>If you do these steps correctly, your data will switch.\n<V>• <N>If your stats are fine, it means that your data has been transferred automatically. You don't have to use this page."
	addUI(5, msg, n, 150, 60, 500, 300, "<a href='event:close'>Close")
	ui.addPopup(999, 2, "", n, 165, 245, 470, true)
end

function eventPopupAnswer(i, n, a)
	if i == 999 then
		if #a > 0 then
			a = a:lower()
			transfer[n:lower()] = a
			sendLog(n, "Please use <CH>!transfer "..n.." <BL>on your new account to finish the data transfer.")
			transferData(n)
		else
			ui.addPopup(999, 2, "", n, 165, 245, 470, true)
		end
	end
end

function showSettings(n, u)
	local p = players[n]
	if os.time() - p.lastUI < 500 then return end

	if n == "Kmlcan" then
		local msg = {}
		for k, v in pairs(p.ui) do
			table.insert(msg, k)
		end
		sendLog(n, table.concat(msg, ", "))
	end

	if p.ui[4] then
		if not u then
			removeUI(4, n)
			p.lastUI = os.time()
			return
		end
	elseif table.count(p.ui) > 0 and not p.ui[4] then
		return
	end

	if not u then
		p.lastUI = os.time()
	end
	local msg = "<J><p align='center'><font face='Courier New' size='20'><b>S E T T I N G S</b></font></p>"
	for k, v in pairs(lang[room.com].settings) do
		msg = msg..("<V>[<a href='event:changeset"..k.."'>"..(tostring(p.set[k]) ~= "nil" and "FALSE" or "TRUE").."</a>]<BL> ")..v.."\n"
	end
	addUI(4, msg, n, 200, 60, 400, 300, "<a href='event:close'>Close")
end

function showShop(n, u)
	local p, cns = players[n], 0
	if os.time() - p.lastUI < 500 then return end

	local maxPage = math.ceil(table.count(shop) / 4) - 1
	if p.ui[1] then

		for k, v in pairs(p.ui[120]) do
			tfm.exec.removeImage(v)
		end

		for i = 121, 127 do
			ui.removeTextArea(i, n)
			ui.removeTextArea(i + 100, n)
		end
		p.ui[120] = nil

		if u then
			if u == "previous" then
				p.page = p.page > 0 and p.page - 1 or p.page
			elseif u == "next" then
				p.page = p.page < maxPage and p.page + 1 or p.page
			end
		else
			removeUI(1, n)
			removeButton(106, n)
			removeButton(107, n)
			ui.removeTextArea(128, n)
			p.lastUI = os.time()
			return
		end
	elseif table.count(p.ui) > 0 and not p.ui[1] then
		return
	end

	local row, column, count = 0, 0, 0
	p.lastUI, p.ui[120] = os.time(), {}

	if not u then
		addUI(1, "<textformat tabstops='[35, 180]'>\t<ROSE><font face='Courier New' size='28'><b>S H O P</b></font>\t<BL>You have <CH>"..p.ffarp.." <BL>FFARP and <CH>"..table.count(p.cannons).." <BL>cannons.", n, 100, 85, 610, 250)
		addButton(1, "<a href='event:close'>Close", n, 215, 305, 380)
		addButton(106, "<a href='event:previous'>"..trans("previous"), n, 115, 305, 80)
		addButton(107, "<a href='event:next'>"..trans("next"), n, 615, 305, 80)
		table.insert(p.ui[1], tfm.exec.addImage("ffarace_ayrac.4362", "&0", 110, 125, n))
		table.insert(p.ui[1], tfm.exec.addImage("ffarace_ayrac.4362", "&0", 110, 292, n))
		--ui.addTextArea(128, "<p align='center'><ROSE><font face='Courier New' size='20'><b>S H O P</b></font>\n<BL>You have <CH>"..p.ffarp.." <BL>FFARP and <CH>"..table.count(p.cannons).." <BL>cannons.", n, 185, 105, 430, 40, 0, 0, 0)
	end

	for k, v in pairs(shop) do
		if k >= (p.page * 4) + 1 and k <= math.min((p.page * 4) + 4) then
			column = row%4 == 0 and column + 1 or column
			row = row%4 == 0 and 1 or row + 1

			local c1 = "0x"..((p.cannon == v.n and "080B1C") or (table.contains(p.cannons, v.n) and "1B1708") or (v.hidden and "191717") or (p.ffarp < v.cost and "1D0808") or "101E08")
			local c2 = "0x"..((p.cannon == v.n and "008DB7") or (table.contains(p.cannons, v.n) and "935a00") or (v.hidden and "777777") or (p.ffarp < v.cost and "CB1919") or "00AD0E")	  
			local c3 = "0x"..((p.cannon == v.n and "0E1336") or (table.contains(p.cannons, v.n) and "3F3513") or (v.hidden and "282626") or (p.ffarp < v.cost and "471010") or "254712")
			ui.addTextArea(120 + row, (p.set.num and "<p align='right'><b><font color='#0' size='9'>"..v.n.."</font>" or ""), n, row * 150 - 31, column * 120 + 25, 121, 100, c2, 0x122528, 1)
			--ui.addTextArea(220 + row, "", n, row * 150 - 25, column * 120 + 30, 110, 90, 0x122528, c2, 1)
			table.insert(p.ui[120], tfm.exec.addImage("ffarace_gradient.4360", "&0", row * 150 - 38, column * 120 + 17, n))
			table.insert(p.ui[120], tfm.exec.addImage("ffarace_cerceve.4358", "&2", row * 150 - 40, column * 120 + 15, n))
			ui.addTextArea(220 + row, "<p align='center'><font size='10'><a href='event:cannon"..v.n.."'>"..(v.hidden or (p.cannon == v.n and "Unequip") or (table.contains(p.cannons, v.n) and "Equip") or v.cost.." FFARP"), n, row * 150 - 35, column * 120 + 145, 130, nil, 0x122528, c2, 1)
			--addButton(220 + row, "<font size='10'><a href='event:cannon"..v.n.."'>"..(v.hidden or (p.cannon == v.n and "Unequip") or (table.contains(p.cannons, v.n) and "Equip") or v.cost.." FFARP"), n, row * 150 - 35, column * 120 + 145, 130, nil, 0x314e57)
			--addUI(120 + row, (p.set.num and "<p align='right'><font size='8'>"..v.n.."</font>" or ""), n, row * 150 + 35, column * 120 + 40, 130, 120, "<font size='10'><a href='event:cannon"..v.n.."'>"..(v.hidden or (p.cannon == v.n and "Unequip") or (table.contains(p.cannons, v.n) and "Equip") or v.cost.." FFARP"), true, c2)
			table.insert(p.ui[120], tfm.exec.addImage(v.img, "&4", row * 150 + 30 + (v.x or -16), (v.sale and 185 or 195) + (v.y or -16), n))
			if v.new then
				table.insert(p.ui[120], tfm.exec.addImage("0419e464d78d11e7b5f688d7f643024a.png", "&4", row * 150 - 43, column * 120 + 25, n))
			elseif v.sale then
				table.insert(p.ui[120], tfm.exec.addImage("4b17131ed78d11e7b5f688d7f643024a.png", "&4", row * 150 - 43, column * 120 + 25, n))
				ui.addTextArea(125, "<p align='center'><font size='10'>"..v.sale.." FFARP", n, row * 150 - 35, 228, 127, nil, c3, c1, 1)
				local line = "_______"
				for i = 1, #tostring(v.sale) do
					line = line.."_"
				end
				ui.addTextArea(126, "<p align='center'><font color='#FFFFFF' size='10'>"..line, n, row * 150 - 35, 224, 127, nil, 0, 1, 0)
			end
		end
	end
end

function showProfile(n1, n2, u, back)
	local n = n2 and fixName(n2) or n1
	if players[n] == nil or n:sub(1, 1) == "*" then return end

	local p, pn, trns = players[n1], players[n], lang[room.com].profStats
	if os.time() - p.lastUI < 500 and not u then return end

	if n == "Kmlcan" then
		local msg = {}
		for k, v in pairs(p.ui) do
			table.insert(msg, k)
		end
		sendLog(n, table.concat(msg, ", "))
	end

	if p.ui[2] then
		removeUI(101, n)
		if not u then
			removeUI(2, n)
			removeUI(102, n)
			removeButton(103, n)
			removeButton(104, n)
			removeButton(105, n)
			ui.removeTextArea(20, n)
			ui.removeTextArea(21, n)
			p.lastUI = os.time()
			return
		end
	elseif table.count(p.ui) > 0 and not p.ui[2] then
		return
	end

	p.lastUI = os.time()
	local nameText, stats1, cn = spaceName(n), "<font size='12'><BL>• "..trns[1].."<V>"..pn.global.cheese.." <G>/ <J>"..pn.season.cheese.."\n<BL>• "..trns[2].."<V>"..pn.global.win.." <G>/ <J>"..pn.season.win.."\n<BL>• "..trns[3].."<V>"..pn.global.death.." <G>/ <J>"..pn.season.death.."\n<BL>• Offset: "..((n == n1 and "<VP><font size='11' face='Courier New'><a href='event:offsetx-'>[-]</a></font> <V>" or "<V>")..pn.offset.x..(n == n1 and " <VP><font size='11' face='Courier New'><a href='event:offsetx+'>[+]</a></font><V>, " or ", ")..(n == n1 and "<VP><font size='11' face='Courier New'><a href='event:offsety-'>[-]</a></font> <V>" or "<V>")..pn.offset.y..(n == n1 and " <VP><font size='11' face='Courier New'><a href='event:offsety+'>[+]</a></font>" or "")).."\n\n<J>"
	local stats2 = "<font size='12'><BL>• FFARP: <V>"..pn.ffarp.."\n<BL>• "..trns[4].."<V>"..#pn.cannons
	if not u then
		addUI(2, "", n1, 160, 65, 480, 300)
		addUI(102, "", n1, 280, 235, 340, 80, nil, true)
		ui.addTextArea(20, "<font size='9'><p align='right'><ROSE>"..pn.global.rank.." <BL>/ <J>"..pn.season.rank, n1, 580, 70, 55, nil, 0x27373F, 0x1B252A, 0)
		local buttonW = 450
		if back then
			buttonW = buttonW - 120
			addButton(103, "<a href='event:goback'>"..trans("goback"), n1, 525, 335, 100)
		end
		addButton(2, "<a href='event:close'>Close", n1, 175, 335, buttonW)
		addButton(104, ((pn.set.mycn and n ~= n1) and "<BL>" or "<CH><a href='event:showCannons"..n.."'>")..trans("showCannons"), n1, 295, 250, 310)
		addButton(105, "<BL>Challenge", n1, 295, 284, 310)
	end

	addUI(101, "", n1, 180, 235, 80, 80, nil, true)
	for a, b in pairs(shop) do
		if b.n == pn.cannon then
			cn = a
		end
	end
	table.insert(p.ui[101], tfm.exec.addImage("1637ce22b31111e7b5f688d7f643024a.png", "&0", 500, 120, n1))
	table.insert(p.ui[101], tfm.exec.addImage("017fca4cd6b211e7b5f688d7f643024a.png", "&0", 180, 235, n1))
	table.insert(p.ui[101], tfm.exec.addImage(cn and shop[cn].img or "052e8fb6b31211e7b5f688d7f643024a.png", "&2", 220 + (cn and shop[cn].x or -16) + pn.offset.x, 275 + (cn and shop[cn].y or -16) + pn.offset.y, n1))
	ui.addTextArea(21, (admins[n] and "<R>" or tester[n] and "<FC>" or "<VP>").."<p align='center'><b><font face='Courier New' size='19'>"..nameText:upper().."</font></b></p>"..stats1.."<p align='center'><b><font face='Courier New' size='19'>"..spaceName(trans("shop")):upper().."</font></b></p>"..stats2, n1, 165, 70, 470, 150, 0, 1, 0)
end

function showLeaderboard(n, u)
	local p = players[n]
	if os.time() - p.lastUI < 500 and not u then return end
	if n == "Kmlcan" then
		local msg = {}
		for k, v in pairs(p.ui) do
			table.insert(msg, k)
		end
		sendLog(n, table.concat(msg, ", "))
	end
	if p.ui[3] then
		if u then
			if u == "previous" then
				p.lbPage = p.lbPage > 0 and p.lbPage - 1 or p.lbPage
			elseif u == "next" then
				p.lbPage = p.lbPage < 9 and p.lbPage + 1 or p.lbPage
			end
		else
			for i = 1, 5 do
				removeButton(30 + i, n)
			end
			removeUI(3, n)
			removeUI(100, n)
			ui.removeTextArea(36, n)
			ui.removeTextArea(18003, n)
			p.lastUI = os.time()
			return
		end
	elseif table.count(p.ui) > 0 and not p.ui[3] then
		return
	end

	p.lastUI = os.time()
	local ts = lang[room.com].lbTable
	local text = "<ROSE><font face='Courier New' size='20'><b><p align='center'>"..spaceName((p.board and "GLOBAL " or "SEASONAL ")..ts[1]).."</p></b></font><font size='12'><textformat tabstops='[50,180,280,380,480]'><J>"..ts[2].."\t"..ts[3].."\t"..ts[4].."\t"..ts[5].."\t"..ts[6].."\t"..ts[7].."\n"
	for k, v in pairs(p.board and board or season) do
		if k >= (p.lbPage * 10) + 1 and k <= math.min((p.lbPage * 10) + 10) then
			text = text.."<J>"..k.."\t"..(v.n == n and "<CH>" or "<V>").."<a href='event:goback"..v.n.."'>"..v.n.."</a>\t<BL>"..v.c.."\t"..v.w.."\t"..v.d.."\t"..v.mmr.."\n"
		end
	end

	if not u then
		addUI(100, "", n, 260, 10, 280, 38, nil, true)
		addUI(3, "", n, 130, 100, 540, 230)
		addButton(31, "<a href='event:lbprevious'><p align='center'><CH>"..trans("previous"), n, 145, 300, 80)
		addButton(32, "<a href='event:boardStyle'><p align='center'><VP>"..(p.board and "Seasonal" or "Global"), n, 245, 300, 80)
		addButton(33, "<a href='event:close'>Close", n, 345, 300, 110)
		addButton(34, "<p align='center'><J>Rank: <BL>"..(p.board and p.global.rank or p.season.rank), n, 475, 300, 80)
		addButton(35, "<a href='event:lbnext'><p align='center'><CH>"..trans("next"), n, 575, 300, 80)
		ui.addTextArea(36, "", n, 200, 25, 400, nil, 0, 0, 0)
		updateCountdown(n)
	end
	ui.addTextArea(18003, text, n, 136, 106, 528, 188, 0, 1, 0)
	ui.updateTextArea(17032, "<a href='event:boardStyle'><p align='center'><VP>"..(p.board and "Seasonal" or "Global"), n)
	ui.updateTextArea(17034, "<p align='center'><J>Rank: <BL>"..(p.board and p.global.rank or p.season.rank), n)
end

function getUI(n)
	local p, i = players[n]
	if p and p.ui then
		for k, v in pairs(p.ui) do
			i = k
		end
	end
	return i
end

function addButton(i, t, n, x, y, w, h, c)
	h, c = h or 13, c or 0x324650
	ui.addTextArea(14000 + i, "", n, x + 1, y + 3, w, h, 0x11171C, 0x11171C, 1)
	ui.addTextArea(15000 + i, "", n, x - 1, y + 1, w, h, 0x5D7D90, 0x5D7D90, 1)
	ui.addTextArea(16000 + i, "", n, x, y + 2, w, h, c, c, 1)
	ui.addTextArea(17000 + i, "<p align='center'>"..t.."\n", n, x, y, w, h + 4, 0, 0, 0)
end

function addUI(i, t, n, x, y, w, h, b, img, color)
	color = color or 0x986742
	ui.addTextArea(10000 + i, "", n, x, y, w, h, 0x241A14, 0x241A14, 0.7)
	ui.addTextArea(11000 + i, "", n, x + 1, y + 1, w - 2, h - 2, color, color, 1)
	ui.addTextArea(12000 + i, "", n, x + 4, y + 4, w - 8, h - 8, 0x24474D, 0x0C191C, 1)
	ui.addTextArea(13000 + i, t, n, x + 6, y + 6, w - 12, h - 12, 0x122528, 0x183337, 1)
	for k, v in pairs(tfm.get.room.playerList) do
		if n == nil or n == k then
			if not players[k].ui then
				players[k].ui = {}
			elseif players[k].ui[i] then
				for key, value in pairs(players[k].ui[i]) do
					tfm.exec.removeImage(value)
				end
			end
			players[k].ui[i] = {}
			if not img then
				table.insert(players[k].ui[i], tfm.exec.addImage("9d638314ccac11e7b5f688d7f643024a.png", "&"..i, x - 6, y - 7, k))
				table.insert(players[k].ui[i], tfm.exec.addImage("14519084ccac11e7b5f688d7f643024a.png", "&"..i, x - 6, h + y - 22, k))
				table.insert(players[k].ui[i], tfm.exec.addImage("d19ed16accac11e7b5f688d7f643024a.png", "&"..i, x + w - 20, y - 7, k))
				table.insert(players[k].ui[i], tfm.exec.addImage("119a2d46ccad11e7b5f688d7f643024a.png", "&"..i, x + w - 20, h + y -22, k))
			end
		end
	end

	if b then
		addButton(i, b, n, x + 15, y + h - 30, w - 30)
	end
end

function removeButton(i, n)
	for k = 4, 7 do
		ui.removeTextArea(10000 + i + (k * 1000), n)
	end
end

function removeUI(i, n)
	if players[n] and players[n].ui[i] then
		for k = 0, 3 do
			ui.removeTextArea(10000 + i + (k * 1000), n)
		end
		removeButton(i, n)
		for k, v in pairs(players[n].ui[i]) do
			tfm.exec.removeImage(v)
		end
		players[n].ui[i] = nil
	end
end

function checkRank(n)
	local p = players[n]
	for k, v in pairs(board) do
		if v.n == n then
			p.global.rank = k
		end
	end

	for k, v in pairs(season) do
		if v.n == n then
			p.season.rank = k
		end
	end

	if tonumber(p.season.rank) > 100 then
		p.season.rank = "-"
	end
	if tonumber(p.global.rank) > 100 then
		p.global.rank = "-"
	end
end

function spaceName(n)
	local p = ""
	for i = 1, #n do
		p = p..n:sub(i, i)..(i == #n and "" or " ")
	end
	return p
end

function textAdd(i, m, n, x, y, w, h, f, ce, m2, s)
	local c1, c2, o, k = 0x324650, 0x27373F, 1, "\n"..m
	if ce then
		x = x - w / 2
		y = y - h / 2
	end
	ui.addTextArea(100000 + i, k, n, x, y, w, h, c1, c2, o, f)
	ui.addTextArea(110000 + i, "", n, x - 5, y - 5, w + 10, 12, 0x27373F, 0x27373F, 1, f)
	ui.addTextArea(120000 + i, "<V><b><font size='15'>"..(m2 or '').."</font></b>", n, x - 5, y - 10, w - 15, nil, 0, 1, 0, f)
	ui.addTextArea(130000 + i, "", n, x + w - 5, y - 3, 8, 8, 0x009D9D, 0x009D9D, 1, f)
	ui.addTextArea(140000 + i, "<p align='center'><font size='17' color='#324650'><b><a href='event:close'>×", n, x + w - 10, y - 12, nil, nil, 0, 0, 0, f)
	if s then
		ui.addTextArea(150000 + i, "", n, x + w - 25, y - 3, 8, 8, 0x009D9D, 0x009D9D, 1, f)
		ui.addTextArea(160000 + i, "<p align='center'><font size='17' color='#324650'><b><a href='event:settings'>+", n, x + w - 30, y - 12, nil, nil, 0, 0, 0, f)
	end
end

function textRemove(i, n, t)
	for k = 100000, 160000, 10000 do
		ui.removeTextArea(k + i, n)
	end
end

function sendLog(n, m, p)
	tfm.exec.chatMessage("<V>[#] <BL>"..m..((p and " = "..tostring(p)) or (p ~= nil and " = false") or ""), n)
end

function nextMap(c)
	local nM = math.random(#room.maps)
	room.nextMap = c or room.maps[nM][1]
	room.nextDiff = nil
	if not c then
		room.nextDiff = room.maps[nM][2]
	end
	tfm.exec.newGame(room.nextMap)
end

function mapName()
	local m = tfm.get.room.xmlMapInfo
	ui.setMapName((#m.author > 1 and (m.author.." <BL>- @"..m.mapCode) or m.mapCode).."   <G>|   <N>"..trans("diff").." : <V>"..(room.nextDiff and lang[room.com].diffNum[room.nextDiff] or "Test"))
end

function checkFFA()
	local map = tfm.get.room.xmlMapInfo
	room.antiFFA, room.doubleFFA = {}, {}
	for k in map.xml:gmatch('<S (.-) />') do
		local b = split(k:match('P="(.-)"'), ",")
		if k:match('o="222222"') then
			table.insert(room.antiFFA, {a = tonumber(b[5]), x = tonumber(k:match('X="(.-)"')), y = tonumber(k:match('Y="(.-)"')), l = tonumber(k:match('L="(.-)"')), h = tonumber(k:match('H="(.-)"')), c = k:match('T="13"') and true or nil})
		elseif k:match('o="999999"') then
			table.insert(room.doubleFFA, {a = tonumber(b[5]) or nil, x = tonumber(k:match('X="(.-)"')), y = tonumber(k:match('Y="(.-)"')), l = tonumber(k:match('L="(.-)"')), h = tonumber(k:match('H="(.-)"')), c = k:match('T="13"') and true or nil})
		end
	end
end

function updateCountdown(n)
	local remain = (room.lastLBUP + 300000 - os.time()) / 1000
	local t1 = math.floor(remain / 60)
	local t2 = math.floor(remain % 60)
	ui.updateTextArea(36, "<p align='center'><V>Update in:<BL> 0"..t1..":"..(#tostring(t2) == 1 and '0'..t2 or t2), n)
end

function rotate(x, y, a, p)
	a = math.rad(-a)
	local s, c, xnew, ynew = math.sin(a), math.cos(a)
	p.x = p.x - x
	p.y = p.y - y
	xnew = p.x * c - p.y * s
	ynew = p.x * s + p.y * c
	p.x = xnew + x;
	p.y = ynew + y;
	return p.x, p.y
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function checkRec(x1, x2, y1, y2, l, h, a)
	if a then x1, y1 = rotate(x2, y2, a, {x = x1, y = y1}) end
	if math.abs(x2 - x1) - l/2 <= 0 and math.abs(y2 - y1) - h/2 <= 0 then
		return true
	end
end

function circle(x1, y1, x2, y2, r)
	if (((x2 - x1) ^ 2) + ((y2 - y1) ^ 2)) ^ 0.5 <= r then
		return true
	end
end

function trans(t)
	return t and lang[room.com][t] or "[ERROR]"
end

function fixName(n)
	--return n:sub(1,1):upper()..n:sub(2):lower()
	return n
end

function mouseCount()
	local i = 0
	for n in pairs(tfm.get.room.playerList) do
		i = i + 1
	end
	return i
end

function html(t, a)
	return a and t:gsub("&lt;", "<"):gsub("&gt;", ">"):gsub("&amp;", "&") or t:gsub("<", "&lt;"):gsub(">", "&gt;"):gsub("&", "&amp;")
end

function table.count(t)
	local i = 0
	for k, v in pairs(t) do
		i = i + 1
	end
	return i
end

function table.contains(t, e)
	if e ~= nil then
		for k, v in pairs(t) do
			if v == e then
				return true
			end
		end
	end
	return false
end

function has(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

main()
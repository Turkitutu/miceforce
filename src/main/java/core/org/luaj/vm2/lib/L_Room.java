/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luaj.vm2.lib;

import com.google.common.primitives.Ints;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Updates;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.data_out.LuaChat;
import mfserver.data_out.O_Death;
import mfserver.main.MFServer;
import mfserver.main.Room;
import mfserver.net.PlayerConnection;
import mfserver.util.*;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.luaj.vm2.*;
import veritabani.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.*;

/**
 * @author sevendr
 */
public class L_Room {

    public Room room;
    public boolean debug = false;
    public LUA_THREAD t;
    public Globals global;
    public boolean _eventNewPlayer = true;
    public Set<String> disabled_commands = new HashSet<>();
    public Set<String> disabled_names = new HashSet<>();
    public Set<String> disabled_names2 = new HashSet<>();
    public boolean custom = false;
    public String developer = "";
    public ArrayList<Integer> img_list = new ArrayList<>();
    public Set<Integer> text_list = new HashSet<>();
    public ScheduledFuture snowTimer;
    public LuaTable playerList = new LuaTable();
    LuaTable xmlMapInfo = new LuaTable();
    String currentMap = "0";
    private boolean _eventPlayerDied = true;
    private boolean _eventKeyboard = true;
    private boolean _eventNewGame = true;
    private boolean _eventChatCommand = true;
    private boolean _eventMouse = true;
    private boolean _eventColorPicked = true;
    private boolean _eventTextAreaCallback = true;
    private boolean _eventPlayerLeft = true;
    public boolean D_watch = false;
    public boolean D_debug = false;
    public boolean D_minimalist = false;
    public AtomicInteger call_count = new AtomicInteger(0);
    private long last_time = 0;
    public int eventMs = 500;
    public Map<String, Integer> samanMode = new ConcurrentHashMap<>();
    public String eventAd = "";
    public boolean isLooping = false;

    public L_Room(Room room) {
        this.room = room;
    }

    public L_Room(Room room, boolean dbg) {
        this.room = room;
        this.debug = dbg;
    }

    public static ByteBuf packAddTextArea(int id, String text, int x, int y, int width, int height, int backgroundColor,
                                          int borderColor, int backgroundAlpha, boolean fixedPos) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(20);
        p.writeInt(id);
        p.writeUTF(text);
        p.writeShort(x);
        p.writeShort(y);
        p.writeShort(width);
        p.writeShort(height);
        p.writeInt(backgroundColor);
        p.writeInt(borderColor);
        p.writeByte(backgroundAlpha);
        p.writeBoolean(fixedPos);
        return p.buffer();
    }

    public static ByteBuf packRemoveTextArea(int id) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(22);
        p.writeInt(id);
        return p.buffer();
    }

    public static ByteBuf packLog(String text) throws IOException {

        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(28);
        p.writeByte(46);
        p.writeInt(0);
        p.writeUTF(text);
        return p.buffer();
    }

    public void addConjuration(int x, int y, int millis) {

    }

    public boolean get_interrupted() {
        if (this.t != null) {
            return this.t.interrupted;
        }

        return true;
    }

    public void set_interrupted(boolean val) {
        if (this.t != null) {
            this.t.interrupted = val;
        }
    }

    public void resetRoom() {
        this.set_interrupted(false);
        this.global = null;
        _eventNewPlayer = true;
        playerList = new LuaTable();
        xmlMapInfo = new LuaTable();
        _eventPlayerDied = true;
        _eventKeyboard = true;
        _eventNewGame = true;
        _eventChatCommand = true;
        disabled_commands = new HashSet<>();
        currentMap = "0";
        _eventTextAreaCallback = true;
        custom = false;
        _eventPlayerLeft = true;
        developer = "";
    }

    public void sendError(Exception exception) {
        String exc = exception.toString();
        if (this.developer.equals("an") && !exc.contains("InterruptException")) {
            System.out.println(exception.toString());
        }
        //  System.out.println(MFServer.stackTraceToString(exception));
        if (this.developer.isEmpty()) {
            String roomname;
            if (this.room != null) roomname = this.room.name;
            else roomname = "UNKOWN";
            if (!exc.contains("ScriptInterruptException") && !exc.contains("java.lang.InterruptedException")) {

                MFServer.logExecutor.execute(() -> {

                    MFServer.colErrorLog.insertOne(new Document("name", roomname).append("exc", exc).append("time", System.currentTimeMillis()));

                });
            }
        }
        if (this.debug) {
            sendLuaMessage(exception.toString());
            this.room.closeLua();

            // for (StackTraceElement c :
            // Thread.currentThread().getStackTrace())
            // System.out.println(c.toString());

            // this.room.sendMessage();
        }

    }

    public void sendError(LuaError exception) {

        //   this.room.closeLua();
// for (StackTraceElement c :
// Thread.currentThread().getStackTrace())
// System.out.println(c.toString());
// this.room.sendMessage();
        // System.out.println(MFServer.stackTraceToString(exception));
        if (this.debug) throw new LuaError(exception.toString());

    }

    public void close() {
        this.set_interrupted(true);
        if (this.room != null) this.room.closeLua();

    }

    public void sendLuaMessage(String mes) {
        call_count.incrementAndGet();
        //System.out.println(mes);
        if (developer.length() != 0) {
            PlayerConnection cl = this.room.clients.get(developer);
            if (cl != null) {
                try {
                    cl.sendPacket(new LuaChat(cl, mes).data());
                } catch (IOException ex) {
                    //Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            //   if(this.debug)System.out.println("LUA" + mes);
        }
    }

    public void eventSummoningStart(String playerName, int objectType, int xPosition, int yPosition, int angle) {
        this.t.events.add(new Event("eventSummoningStart",
                new LuaValue[]{LuaString.valueOf(playerName), LuaInteger.valueOf(objectType), LuaInteger.valueOf(xPosition), LuaInteger.valueOf(yPosition), LuaInteger.valueOf(angle)}));
    }

    public void eventSummoningCancel(String playerName) {
        this.t.events.add(new Event("eventSummoningCancel",
                new LuaValue[]{LuaString.valueOf(playerName)}));
    }

    public void eventSummoningEnd(String playerName, int objectType, int xPosition, int yPosition, int angle) {
        this.t.events.add(new Event("eventSummoningEnd",
                new LuaValue[]{LuaString.valueOf(playerName), LuaInteger.valueOf(objectType), LuaInteger.valueOf(xPosition), LuaInteger.valueOf(yPosition), LuaInteger.valueOf(angle)}));
    }

    public void eventEmotePlayed(String playerName, int emoteId, String bayrak, String target) {
        this.t.events.add(new Event("eventEmotePlayed",
                new LuaValue[]{LuaString.valueOf(playerName), LuaInteger.valueOf(emoteId), LuaString.valueOf(bayrak), LuaString.valueOf(target)}));
    }

    public void eventPlayerDataLoaded(String playerName, String veri) {
        this.t.events.add(new Event("eventPlayerDataLoaded",
                new LuaValue[]{LuaString.valueOf(playerName), LuaString.valueOf(veri)}));
    }

    public void eventPlayerGetCheese(String playerName) {
        this.t.events.add(new Event("eventPlayerGetCheese",
                new LuaValue[]{LuaString.valueOf(playerName)}));
    }

    public void eventPopupAnswer(int popupId, String name, String answer) {
        this.t.events.add(new Event("eventPopupAnswer",
                new LuaValue[]{LuaInteger.valueOf(popupId), LuaString.valueOf(name), LuaString.valueOf(answer)}));

    }

    public void eventColorPicked(String name, int code, int color) {
        this.t.events.add(new Event("eventColorPicked",
                new LuaValue[]{LuaString.valueOf(name), LuaInteger.valueOf(code), LuaInteger.valueOf(color)}));
    }

    public void eventMouse(String name, int x, int y) {
        this.t.events.add(new Event("eventMouse",
                new LuaValue[]{LuaString.valueOf(name), LuaInteger.valueOf(x), LuaInteger.valueOf(y)}));
    }

    public void eventMobDamaged(int mobId, String from) {
        this.t.events.add(new Event("eventMobDamaged",
                new LuaValue[]{LuaInteger.valueOf(mobId), LuaString.valueOf(from)}));
    }

    public void eventMobDead(int mobId) {
        this.t.events.add(new Event("eventMobDead",
                new LuaValue[]{LuaInteger.valueOf(mobId)}));
    }

    public void eventNewPlayer(String name) {
        if (t == null) return;
        if (!isLooping) playerList = getPlayerList();
        this.t.events.add(new Event("eventNewPlayer", new LuaValue[]{LuaString.valueOf(name)}));
    }

    public void eventKeyboard(String name, int key, boolean down, int x, int y) {
        this.t.events.add(new Event("eventKeyboard", new LuaValue[]{LuaString.valueOf(name), LuaInteger.valueOf(key),
                LuaBoolean.valueOf(down), LuaInteger.valueOf(x), LuaInteger.valueOf(y)}));
    }

    public void eventPlayerDied(String name) {
        this.t.events.add(new Event("eventPlayerDied", new LuaValue[]{LuaString.valueOf(name)}));
    }

    public void eventChatCommand(String name, String substring) {
        this.t.events.add(
                new Event("eventChatCommand", new LuaValue[]{LuaString.valueOf(name), LuaString.valueOf(substring)}));
    }

    public void eventChatMessage(String name, String string) {
        this.t.events.add(
                new Event("eventChatMessage", new LuaValue[]{LuaString.valueOf(name), LuaString.valueOf(string)}));
    }

    public void eventTextAreaCallback(String name, int code, String event) {
        this.t.events.add(new Event("eventTextAreaCallback",
                new LuaValue[]{LuaInteger.valueOf(code), LuaString.valueOf(name), LuaString.valueOf(event)}));
    }


    public void eventFlyingChange(String isim, boolean flying) {
        this.t.events.add(new Event("eventFlyingChange",
                new LuaValue[]{LuaString.valueOf(isim), LuaBoolean.valueOf(flying)}));
    }

    public void eventNewGame() {
        this.t.events2.clear();
        this.currentMap = String.valueOf(this.room.Map.id);
        this.xmlMapInfo.set("xml", LuaString.valueOf(this.room.Map.xml));

        this.xmlMapInfo.set("author", LuaString.valueOf(this.room.Map.name));
        this.xmlMapInfo.set("mapCode", LuaInteger.valueOf(this.room.Map.id));
        this.xmlMapInfo.set("permCode", LuaInteger.valueOf(this.room.Map.perm));
        this.t.events.add(new Event("eventNewGame"));

    }

    public void eventPlayerWon(String ad, int t1, int t2) {

        this.t.events.add(new Event("eventPlayerWon",
                new LuaValue[]{LuaString.valueOf(ad), LuaInteger.valueOf(t1), LuaInteger.valueOf(t2)}));

    }

    public void eventPlayerVampire(String ad) {

        this.t.events.add(new Event("eventPlayerVampire", new LuaValue[]{LuaString.valueOf(ad)}));

    }

    public void eventPlayerLeft(String name) {

        if (!isLooping) playerList = getPlayerList();
        this.t.events.add(new Event("eventPlayerLeft", new LuaValue[]{LuaString.valueOf(name)}));
    }

    public void eventCollectable(Integer id, String name) {
        this.t.events.add(new Event("eventCollectable", new LuaValue[]{LuaInteger.valueOf(id), LuaString.valueOf(name)}));
    }

    void newGame(String code, boolean hemen, int perm, String name) {
        call_count.incrementAndGet();
        if (System.currentTimeMillis() - last_time < 3000) {
            return;
        }
        last_time = System.currentTimeMillis();
        custom = true;
        if (perm < 1000 && perm >= 0) this.room.forcePerm = perm;
        boolean codebutat = StringUtils.isNumeric(code) && Ints.tryParse(code) > 1000;
        if (code.equals("null")) {
            if (hemen) this.room.NewRound();

        } else if (StringUtils.isNumeric(code) && !codebutat) {
            Integer code2 = Ints.tryParse(code);
            if (code2 != null) {
                this.room.NextMap = VanillaMap.get(code2);

                if (hemen) this.room.NewRound();
            }
        } else if (code.startsWith("@") || codebutat) {
            if (!codebutat) code = code.substring(1);
            Integer code2 = Ints.tryParse(code);
            if (code2 == null) {
                if (hemen) this.room.NewRound();
                return;
            }
            map m = Harita.al(code2);
            if (m.id == 0) {

                this.room.NextMap = VanillaMap.get(0);

            } else {
                this.room.NextMap = m;

            }
            if (hemen) {
                this.room.NewRound();

            }
        } else if (code.startsWith("#")) {

            code = code.substring(1);
            Integer code2 = Ints.tryParse(code);
            if (code2 == null) {
                if (hemen) this.room.NewRound();
                return;
            }
            this.room.NextPerm = code2;

            if (hemen) {
                this.room.NewRound();
            }

        } else if (code.startsWith("<")) {
            map m = new map();
            m.xml = code;
            m.name = name;
            m.id = 0;
            if (perm < 100 && perm >= 0) m.perm = perm;

            this.room.NextMap = m;

            if (hemen) this.room.NewRound();

        } else {

            if (hemen) this.room.NewRound();
        }


    }

    void disableAutoShaman(boolean yes) {
        call_count.incrementAndGet();
        this.room.autoShaman = !yes;
    }

    void disableAutoNewGame(boolean yes) {
        call_count.incrementAndGet();
        this.room.autoNewGame = !yes;
    }

    void disableAfkDeath(boolean yes) {
        call_count.incrementAndGet();
        this.room.autoKillAfk = !yes;
    }

    void disableAutoScore(boolean yes) {
        call_count.incrementAndGet();
        this.room.autoScore = !yes;
    }

    void disableAllShamanSkills(boolean yes) {
        call_count.incrementAndGet();
        this.room.ShamanSkills = !yes;
    }

    void disableAutoTimeLeft(boolean yes) {
        call_count.incrementAndGet();
        this.room.AutoTimeLeft = !yes;
    }

    void bindMouse(String name, boolean yes) {
        call_count.incrementAndGet();

        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                pc.bindMouse(yes);
            } catch (IOException ex) {
            }
        }
    }

    void bindKeyboard(String name, int code, boolean down, boolean yes) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                pc.bindKeyboard(code, down, yes);
            } catch (IOException ex) {
            }
        }
    }

    void giveCheese(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                pc.giveCheese();
            } catch (IOException ex) {

            }
        }
    }
    void removeCheese(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                room.sendAll(MFServer.getBuf().writeByte(90).writeByte(6).writeInt(pc.kullanici.code).writeByte(0));
            } catch (IOException ex) {

            }
        }
    }

    void giveMeep(String name) throws IOException {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {

            pc.giveMeep();

        }
    }

    void setShaman(String name, boolean disableSkill, boolean all) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            this.room.codes = new PlayerConnection[]{pc, null};
            try {

                this.room.sendAll(MFServer.getBuf().writeByte(8).writeByte(21).writeInt(pc.kullanici.code).writeInt(0));
                ByteBuf data = MFServer.getBuf().writeByte(8).writeByte(12).writeInt(pc.kullanici.code).writeByte(pc.kullanici.samanTur).writeShort(pc.kullanici.seviye.seviye).writeShort(pc.kullanici.ikon);
                if (all) this.room.sendAll(data);
                else pc.sendPacket(data);

                pc.isShaman = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            // this.room.sendAll(new Shaman(this.room, s1, s2).data());
            if (!disableSkill) {
                Beceriler.Yolla(pc, true);
                room.allShamanSkills = new HashMap<>(pc.kullanici.beceriler);
            }
        }
    }

    void nextShaman(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            this.room.forceNextShaman = pc.kullanici.code;
        }
    }

    void playerVictory(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                pc.playerVictory(0);
            } catch (IOException ex) {

            }
        }
    }

    void setNameColor(String name, int color) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                room.NickColors.put(name, color);
                this.room.setNameColor(pc, color);
            } catch (IOException ex) {

            }
        }
    }

    void respawnPlayer(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {

            try {
                if (pc.isDead) {
                    this.room.respawn_player(pc);
                    if (this.checkStaff()) pc.revived = true;
                }

            } catch (IOException ex) {

            }
        }
    }

    int addShamanObject(int typ, int x, int y, int angle, int vx, int vy, boolean ghost, String target) {
        call_count.addAndGet(50);
        try {
            int pc = 0;
            if (!target.isEmpty()) {
                PlayerConnection playerConnection = this.room.clients.get(target);
                if (playerConnection != null) pc = playerConnection.kullanici.code;
            }
            return this.room.addShamanObject(typ, x, y, angle, vx, vy, ghost, null, pc);
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    void chatMessage(String message, String name) {
        call_count.incrementAndGet();
        if (!this.checkStaff())
            return;
        try {
            if (name.isEmpty()) {
                room.sendMessage(message);
                return;
            }
            PlayerConnection pc = this.room.clients.get(name);
            if (pc != null) {

                pc.sendMessage(message);

            }
        } catch (IOException ex) {

        }
    }

    void removeObject(int code) {
        call_count.incrementAndGet();
        try {
            this.room.removeObject(code);
        } catch (IOException ex) {

        }
    }

    void setGameTime(int time, boolean nit) {
        call_count.incrementAndGet();
        try {
            this.room.set_time(time);
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void addJoint(int id, int ground1, int physicObject2, LuaTable jointDef) {
        call_count.incrementAndGet();
        this.room.addJoint(id, ground1, physicObject2, jointDef);
    }

    void addPhysicObject(int id, int x, int y, LuaTable bodyDef, String target) {
        call_count.incrementAndGet();
        this.room.addPhysicObject(id, x, y, bodyDef, target);

    }

    void removePhysicObject(int id, String target) {
        call_count.incrementAndGet();
        ByteBufOutputStream p = MFServer.getStream();
        try {
            p.writeByte(29);
            p.writeByte(29);
            p.writeShort(id);
            if (target.length() == 0) {
                this.room.sendAll(p.buffer());
            } else {
                sendTarget(target, p.buffer());
            }
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void setUIMapName(String message) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(25);
            p.writeUTF(message);
            this.room.sendAll(p.buffer());
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void disableChatCommandDisplay(String message) {
        call_count.incrementAndGet();
        this.disabled_commands.add(message);
    }

    public void disableIsimRenk(boolean yap) {
        call_count.incrementAndGet();
        if (!checkStaff()) return;
        room.isIsimRenkDisabled=yap;

    }
    public void disableChat(String ad, boolean yap) {
        call_count.incrementAndGet();
        if (!checkStaff()) return;
        if (yap) this.disabled_names.add(ad);
        else this.disabled_names.remove(ad);

    }
    public void disableChat2(String ad, boolean yap) {
        call_count.incrementAndGet();
        if (!checkStaff()) return;
        if (yap) this.disabled_names.add(ad);
        else this.disabled_names.remove(ad);

    }


    void sendTarget(String name, ByteBuf buf) throws IOException {
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            pc.sendPacket(buf);
        } else {

            MFServer.bitti(buf);
        }
    }

    void addTextArea(int id, String text, String target, int x, int y, int width, int height, int backgroundColor,
                     int borderColor, int backgroundAlpha, boolean fixedPos) {
        call_count.incrementAndGet();
        try {
            if (backgroundAlpha > 127) {
                backgroundAlpha = 120;
            }
            ByteBuf p = packAddTextArea(id, text, x, y, width, height, backgroundColor, borderColor, backgroundAlpha, fixedPos);
            if (target.length() == 0) {
                this.room.sendAll(p);
            } else {
                sendTarget(target, p);
            }
            text_list.add(id);
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void updateTextArea(int id, String text, String target) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(21);
            p.writeInt(id);
            p.writeUTF(text);

            if (target.length() == 0) {
                this.room.sendAll(p.buffer());
            } else {
                sendTarget(target, p.buffer());
            }
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removeTextArea(int id, String target) {
        call_count.incrementAndGet();
        try {

            ByteBuf p = packRemoveTextArea(id);
            if (target.length() == 0) {
                this.room.sendAll(p);

                if (text_list.contains(id)) {
                    text_list.remove(id);
                }
            } else {
                sendTarget(target, p);
            }
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void movePlayer(String name, int x, int y, boolean positionOffset, int xSpeed, int ySpeed, boolean speedOffset)
            throws IOException {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            this.room.movePlayer(pc, x, y, positionOffset, xSpeed, ySpeed, speedOffset);
        }
    }

    void killPlayer(String name) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(name);
        if (pc != null) {
            try {
                this.room.sendDeath(pc);

            } catch (IOException ex) {
                Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void killPlayerById(int playerCode) {
        call_count.incrementAndGet();
        if (!checkStaff()) return;

        try {
            this.room.sendAll(new O_Death(playerCode, room.death_alive()[1]).data());

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void displayParticle(int particleType, int xPosition, int yPosition, int xSpeed, int ySpeed, int xAcceleration,
                         int yAcceleration, String targetPlayer) {
        call_count.incrementAndGet();
        this.room.displayParticle(particleType, xPosition, yPosition, xSpeed, ySpeed, xAcceleration, yAcceleration,
                targetPlayer);
    }

    int newTimer(LuaFunction callback, int time, boolean loop, LuaValue... args) {
        call_count.incrementAndGet();
        if (!checkStaff()) return -1;
        int id = this.room.TimerID.incrementAndGet();
        Event event = new Event(callback.name(), args);
        event.deadline = System.currentTimeMillis() + time;
        event.evId = id;
        event.fonk = callback;
        event.loop = loop;
        event.time = time;
        this.t.events2.add(event);

        return id;
    }

    void removeTimer(int id) {
        call_count.incrementAndGet();
        Iterator<Event> it = this.t.events2.iterator();
        while (it.hasNext()) {
            Event ev = it.next();
            if (ev.evId == id) {
                it.remove();
                break;
            }
        }
    }

    void explosion(int xPosition, int yPosition, int power, int radius, boolean miceOnly) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(5);
            p.writeByte(17);
            if (xPosition > 32767) {
                xPosition = 32767;
            }
            if (yPosition > 32767) {
                yPosition = 32767;
            }
            if (power > 128) {
                power = 128;
            }
            if (radius > 32767) {
                radius = 32767;
            }

            p.writeShort(xPosition);
            p.writeShort(yPosition);
            p.writeByte(power);
            p.writeShort(radius);
            p.writeBoolean(!miceOnly);

            this.room.sendAll(p.buffer());

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void setPlayerScore(String playerName, int score, boolean add) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(playerName);
        if (pc != null) {
            if (add) {
                pc.score += score;
            } else {
                pc.score = score;
            }
            if (pc.score < 0) {
                pc.score = 0;
            }
            try {
                room.sendAll(MFServer.getBuf().writeByte(8).writeByte(7).writeInt(pc.kullanici.code).writeShort(pc.score));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static ByteBuf packImage(String imageId, int id, int type, int code, int xPosition, int yPosition) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(19);
        p.writeInt(id);
        p.writeUTF(imageId);
        p.writeByte(type);

        p.writeInt(code);
        p.writeShort(xPosition);
        p.writeShort(yPosition);
        return p.buffer();
    }

    int addImage(String imageId, String target, int xPosition, int yPosition, String targetPlayer) {
        return addImage(imageId, target, xPosition, yPosition, targetPlayer, 0);
    }

    int addImage(String imageId, String target, int xPosition, int yPosition, String targetPlayer, int id) {
        call_count.incrementAndGet();
        try {
            int type = 0;
            if (target.startsWith("#")) {
                type = 1;
            } else if (target.startsWith("$")) {
                type = 2;
            } else if (target.startsWith("%")) {
                type = 3;
            } else if (target.startsWith("?")) {
                type = 4;
            } else if (target.startsWith("_")) {
                type = 5;
            } else if (target.startsWith("!")) {
                type = 6;
            } else if (target.startsWith("&")) {
                type = 7;
            }
            target = target.substring(1);
            if (id == 0) id = this.room.ImageID.incrementAndGet();

            int code;
            if (type == 2 || type == 3) {
                PlayerConnection pc = this.room.clients.get(target);
                if (pc == null) {
                    code = 0;
                } else {
                    code = pc.kullanici.code;
                }
            } else {
                code = Integer.valueOf(target);
            }
            ByteBuf pb = packImage(imageId, id, type, code, xPosition, yPosition);
            if (targetPlayer.length() == 0) {
                this.room.sendAll(pb);
            } else {
                sendTarget(targetPlayer, pb);
            }
            img_list.add(id);
            return id;
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }


    public void giveHealth(String username, int miktar) {
        PlayerConnection client = room.clients.get(username);
        if (client != null) {
            client.HalloweenHealth += miktar;
            client.sendHalloweenHealth();
        }
    }

    void moveMonster(int MID, int yon) {
        call_count.incrementAndGet();
        try {
            Mob mob = room.Mobs.get(MID);
            if (mob != null) return;

            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(26);
            p.writeByte(8);
            p.writeInt(MID);
            p.writeInt(yon);
            this.room.sendAll(p.buffer());


        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void sendStorm(int siddet) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(5);
            p.writeByte(44);
            p.writeShort(siddet);
            this.room.sendAll(p.buffer());


        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void moveThem(int MID, int last) {
        if (!get_interrupted() && room.Mobs.containsKey(MID)) {
            int x = PlayerConnection.betweenRandom(1, 4);
            int newCord;
            if (last > 0) newCord = -x;
            else {
                newCord = x;
            }
            try {
                ByteBufOutputStream p = MFServer.getStream();
                p.writeByte(26);

                p.writeByte(8);
                p.writeInt(MID);
                p.writeInt(newCord);
                this.room.sendAll(p.buffer());
                MFServer.executor.schedule(() -> {
                    if (!get_interrupted()) {

                        moveThem(MID, newCord);
                    }
                }, PlayerConnection.betweenRandom(3, 6), TimeUnit.SECONDS);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void spawnTrap(int id, int x, int y, int scaleX, int scaleY, int rotation) {
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(5);
            p.writeByte(47);
            p.writeShort(id);
            p.writeShort(x);
            p.writeShort(y);
            p.writeShort(scaleX);
            p.writeShort(scaleY);
            p.writeShort(rotation);
            this.room.sendAll(p.buffer());
        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void spawnMonster(int MID, int x, int y, String name, int can) {
        call_count.incrementAndGet();
        try {
            if (room.Mobs.containsKey(MID)) return;
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(26);
            p.writeByte(6);
            p.writeInt(MID);
            p.writeInt(x);
            p.writeInt(y);
            p.writeUTF(name);
            this.room.sendAll(p.buffer());
            Mob m = new Mob(new AtomicInteger(can));
            m.id = MID;
            m.x = x;
            m.y = y;
            m.name = name;
            room.Mobs.put(MID, m);
            if (MID > 0) {
                moveThem(MID, 2);
            }


        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void addBot(int code, String name, int title, String look, int x, int y, String targetPlayer) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(8);
            p.writeByte(30);
            p.writeInt(code);
            p.writeUTF(name);
            p.writeShort(title);
            p.writeBoolean(false);
            p.writeUTF(look);
            p.writeShort(x);
            p.writeShort(y);
            p.writeBoolean(false);
            p.writeBoolean(false);
            p.writeByte(11);
            p.writeUTF("");
            //     p.writeShort(11);
            //   p.writeShort(0);

            if (targetPlayer.length() == 0) {
                this.room.sendAll(p.buffer());
            } else {
                sendTarget(targetPlayer, p.buffer());
            }

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ByteBuf packRemoveImage(int code) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(18);
        p.writeInt(code);
        return p.buffer();
    }

    public void removeImage(int code, String targetPlayer) {
        call_count.incrementAndGet();
        try {

            ByteBuf p = packRemoveImage(code);

            if (targetPlayer.length() == 0) {
                this.room.sendAll(p);
            } else {
                sendTarget(targetPlayer, p);
            }

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void showColorPicker(int id, String target, int defaultColor, String title) {
        call_count.incrementAndGet();
        try {

            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(32);
            p.writeInt(id);
            p.writeInt(defaultColor);
            p.writeUTF(title);

            if (target.length() == 0) {
                this.room.sendAll(p.buffer());
            } else {
                sendTarget(target, p.buffer());
            }

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void addLog(String text, String target) {
        call_count.incrementAndGet();
        try {

            ByteBuf p = packLog(text);
            if (target.length() == 0) {
                this.room.sendAll(p);
            } else {
                sendTarget(target, p);
            }

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void snow(boolean durum, int duration, int power) throws IOException {
        call_count.incrementAndGet();
        ByteBuf packet = MFServer.getBuf().writeByte(5).writeByte(23).writeBoolean(durum).writeShort(power);

        this.room.sendAll(packet);
        if (duration != 0) {
            Room.iptal(this.snowTimer);
            this.snowTimer = MFServer.executor.schedule(() -> {
                try {
                    this.snow(false, 0, 10);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, (long) duration, TimeUnit.SECONDS);
        }
    }

    public void setVampirePlayer(String name) throws IOException {
        call_count.incrementAndGet();
        this.room.setVampirePlayer(name);
    }

    public LuaTable getPlayerList() {
        LuaTable ret = new LuaTable();
        for (PlayerConnection client : this.room.clients.values()) {

            LuaTable clientTable = new LuaTable();
            clientTable.set("isJumping", LuaBoolean.valueOf(client.jumping));
            clientTable.set("title", LuaInteger.valueOf(client.kullanici.unvan));
            clientTable.set("x", LuaInteger.valueOf(client.x));
            clientTable.set("y", LuaInteger.valueOf(client.y));
            clientTable.set("look", LuaString.valueOf(client.kullanici.tip));
            clientTable.set("isShaman", LuaBoolean.valueOf(client.isShaman));
            clientTable.set("isDead", LuaBoolean.valueOf(client.isDead));
            clientTable.set("vx", LuaInteger.valueOf(client.vx));
            clientTable.set("score", LuaInteger.valueOf(client.score));
            clientTable.set("inHardMode", LuaInteger.valueOf(client.kullanici.samanTur));
            clientTable.set("vy", LuaInteger.valueOf(client.vy));
            clientTable.set("movingRight", LuaBoolean.valueOf(client.direction));
            clientTable.set("hasCheese", LuaBoolean.valueOf(client.hasCheese));
            clientTable.set("registrationDate", LuaInteger.valueOf(client.kullanici.kayit));
            clientTable.set("playerName", LuaString.valueOf(client.kullanici.isim));
            clientTable.set("lang", LuaString.valueOf(client.Lang));
            if (client.tribe != null) {

                clientTable.set("tribeName", LuaString.valueOf(client.tribe.kabile.isim));
            } else {
                clientTable.set("tribeName", "");
            }
            clientTable.set("isFacingRight", LuaBoolean.valueOf(client.direction));
            ret.set(client.kullanici.isim, clientTable);
        }
        return ret;
    }

    public Varargs getObjectList() {
        call_count.incrementAndGet();
        LuaTable ret = new LuaTable();
        for (LuaTable ew : this.room.objectList.values()) {

            ret.set(ew.get("id"), ew);
        }
        return ret;
    }

    public boolean checkStaff(boolean onlyadmin) {
        call_count.incrementAndGet();
        if (!this.debug)
            return true;
        PlayerConnection pc = this.room.server.clients.get(this.developer);
        if (pc != null) {
            if (pc.kullanici.yetki == 10 || (pc.kullanici.isLuacrew && !onlyadmin))
                return true;
        }

        return false;
    }

    public boolean checkStaff() {
        return checkStaff(false);
    }

    public void disablePhysicalConsumables(boolean yes) {
        call_count.incrementAndGet();
        this.room.physicalConsumables = yes;
    }

    public void changeName(String oldName, String newName) {
        if (!this.checkStaff())
            return;
        this.room.miceNames.put(oldName, newName);
    }

    public void changeSize(String playerName, int size) throws IOException {
        if (!this.checkStaff())
            return;
        PlayerConnection pc = this.room.clients.get(playerName);
        if (pc != null && size > 0 && size < 30000) {
            this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(31).writeInt(pc.kullanici.code).writeShort(size).writeByte(0));

        }
    }

    public void savePlayerData(String playerName, String veri) {
        if (!this.checkStaff())
            return;
        LuaData.savePlayerData(playerName, veri, LuaData.getDeveloperName(this));
    }

    public LuaValue getTitleText(int title) {
        if (!this.checkStaff())
            return LuaValue.NIL;
        Document document = MFServer.colUnvanlar.find(eq("_id", title)).first();
        if (document != null) {
            return LuaValue.valueOf(document.getString("unvan"));
        }
        return LuaValue.NIL;
    }

    public LuaString getUsernameById(int playerID) {
        if (!this.checkStaff())
            return LuaString.EMPTYSTRING;

        PlayerConnection pc = this.room.server.clients2.get(playerID);
        if (pc != null){
            return LuaString.valueOf(pc.kullanici.isim);
        } else {
            return LuaString.valueOf(MFServer.getIsim(playerID)[0]);
        }
    }

    public LuaValue bilgi(String playerName) {
        if (!this.checkStaff())
            return LuaValue.NIL;
        PlayerConnection pc = this.room.server.clients.get(playerName);

        LuaTable luaTable = new LuaTable();
        Kullanici kullanici;
        boolean online = false;
        if (pc != null) {
            online = true;
            kullanici = pc.kullanici;
            luaTable.set("hidden", LuaBoolean.valueOf(pc.hidden));
            luaTable.set("multiplier", LuaInteger.valueOf(MFServer.genel_carpan));
        } else {
            kullanici = MFServer.getUserFromDb(playerName);
            if (kullanici.code == -1) {
                kullanici = null;
            }
        }
        luaTable.set("online", LuaBoolean.valueOf(online));
        if (kullanici != null) {
            LuaTable bannedCommunities = new LuaTable();
            try {
                for (String bc : kullanici.bannedCommunities) {
                    bannedCommunities.insert(0, LuaString.valueOf(bc));
                }
            } catch (Exception e) {
                System.out.println(MFServer.stackTraceToString(e));
            }
            luaTable.set("avatar", LuaString.valueOf(kullanici.avatar));
            luaTable.set("code", LuaInteger.valueOf(kullanici.code));
            luaTable.set("standalone", LuaInteger.valueOf(kullanici.stand));
            luaTable.set("gender", LuaString.valueOf(kullanici.cinsiyet));
            luaTable.set("soulmate", LuaString.valueOf(MFServer.getIsim(kullanici.ruhesi)[0]));
            luaTable.set("priv", LuaInteger.valueOf(kullanici.yetki));
            luaTable.set("isSentinel", LuaBoolean.valueOf(kullanici.isSentinel));
            luaTable.set("isFuncorp", LuaBoolean.valueOf(kullanici.isFuncorp));
            luaTable.set("isMapcrew", LuaBoolean.valueOf(kullanici.isMapcrew));
            luaTable.set("isLuacrew", LuaBoolean.valueOf(kullanici.isLuacrew));
            luaTable.set("isTester", LuaBoolean.valueOf(kullanici.isTester));
            luaTable.set("isArtist", LuaBoolean.valueOf(kullanici.isArtist));
            luaTable.set("isFashion", LuaBoolean.valueOf(kullanici.isFashion));
            try {
                luaTable.set("bannedCommunities", bannedCommunities);
            } catch (Exception e) {
                System.out.println(MFServer.stackTraceToString(e));
            }
            return luaTable;
        }
        return LuaValue.NIL;
    }

    public void loadPlayerData(String playerName) {
        if (!this.checkStaff())
            return;
        LuaData.loadPlayerData(playerName, this);
    }

    public void deleteTitle(String target, int title) {
        if (!this.checkStaff(true))
            return;
        //   if (title < 3000) return;
        if (MFServer.zodiacTitles.contains(title)) return;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {

            pc.kullanici.evInfo.titles.remove(String.valueOf(title));
            pc.kullanici.unvanlar.remove(title);
            pc.checkAndRebuildTitleList("all");

        } else {

            MFServer.colKullanici.updateOne(eq("isim", target), Updates.unset("evInfo.titles." + title));
            MFServer.colKullanici.updateOne(eq("isim", target), Updates.pull("unvanlar", title));

        }
        MFServer.logExecutor.execute(() -> MFServer.colMG.insertOne(new Document("tarih", MFServer.timestamp()).append("action", "delete").append("dev", this.developer).append("target", target).append("title", title)));

    }

    public void giveTitle(String target, int title) {
        if (!this.checkStaff(true))
            return;

        if (MFServer.zodiacTitles.contains(title)) return;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {
            if (title < 3000 && pc.kullanici.yetki != 10) return;
            try {
                pc.add_event_title(title, "lua");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Kullanici kul = MFServer.colKullanici.find(eq("isim", target)).first();
            if (kul != null) {
                kul.evInfo.titles.put(String.valueOf(title), "lua");
            }
            try {
                kul.updateOne("code", "evInfo");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        MFServer.executor.execute(() -> MFServer.colMG.insertOne(new Document("tarih", MFServer.timestamp()).append("action", "give").append("dev", this.developer).append("target", target).append("title", title)));

    }

    public void giveBadge(String target, int badge) {
        if (!this.checkStaff(true))
            return;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {
            pc.kullanici.rozetler.add(badge);

        }
        MFServer.executor.execute(() -> MFServer.colMG.insertOne(new Document("tarih", MFServer.timestamp()).append("dev", this.developer).append("target", target).append("badge", badge)));
    }

    void disableMinimalistMode(boolean yes) {
        try {
            this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(33).writeByte(1).writeByte(1).writeBoolean(yes));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int deleteConsumable(String target, int esya, int miktar) {
        if (!this.checkStaff(true))
            return -1;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {
            Nesne nesne = pc.kullanici.esyalar.get(String.valueOf(esya));
            if (nesne == null) return -2;
            int count = nesne.sayi;
            int yeni = count - miktar;

            if (yeni < 1) {
                pc.kullanici.esyalar.remove(String.valueOf(esya));
                return 0;
            } else {
                nesne.sayi = yeni;
            }
            return nesne.sayi;
        }
        return -1;
    }

    public int consumableCount(String target, int esya) {
        if (!this.checkStaff(true))
            return -1;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {
            Nesne nesne = pc.kullanici.esyalar.get(String.valueOf(esya));
            if (nesne != null) return nesne.sayi;
        }
        return -1;
    }

    public void giveConsumables(String target, int esya, int sayi) {
        if (!this.checkStaff(true))
            return;
        PlayerConnection pc = this.room.server.clients.get(target);
        if (pc != null) {
            pc.addInventoryItem(esya, sayi);
            pc.checkValentine();
        }
        MFServer.executor.execute(() -> MFServer.colMG.insertOne(new Document("tarih", MFServer.timestamp()).append("dev", this.developer).append("target", target).append("consumable", esya)));

    }

    public void addPopup(int id, int type, String text, String targetPlayer, int x, int y, int width,
                         boolean fixedPos) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(23);
            p.writeInt(id);
            p.writeByte(type);
            p.writeUTF(text);
            p.writeShort(x);
            p.writeShort(y);
            p.writeShort(width);
            p.writeBoolean(!fixedPos);
            if (id >= 7775 && id < 7790) return;
            if (targetPlayer.length() == 0) {
                this.room.sendAll(p.buffer());
            } else {
                sendTarget(targetPlayer, p.buffer());
            }

        } catch (IOException ex) {
            Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public LuaInteger unique() {
        call_count.incrementAndGet();
        return LuaInteger.valueOf(this.room.unique_playerCount());
    }

    public void playEmote(String playerName, int emoteId, String emoteArg) {
        call_count.incrementAndGet();
        PlayerConnection pc = this.room.clients.get(playerName);
        if (pc != null) {
            try {
                pc.playEmote(pc.kullanici.code, emoteId, false, emoteArg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void playEmote2(int pCode, int emoteId, String emoteArg) {
        call_count.incrementAndGet();
        ByteBufOutputStream packet = MFServer.getStream();
        try {
            packet.writeByte(8);

            packet.writeByte(1);

            packet.writeInt(pCode);
            packet.writeByte(emoteId);
            if (!emoteArg.isEmpty()) {
                packet.writeUTF(emoteArg);
            }
            packet.writeByte(0);

            this.room.sendAll(packet.buffer());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setShamanName(String shamanName) {
        call_count.incrementAndGet();
        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(26);

            p.writeUTF(shamanName);

            this.room.sendAll(p.buffer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addCollectable(int id, int cap, int x, int y, String name) {
        if (!this.checkStaff())
            return;
        Daire daire = new Daire(id, cap, x, y);
        if (name.length() == 0) {
            for (PlayerConnection playerConnection : room.clients.values()) {

                playerConnection.motor = true;
                playerConnection.daires.put(id, daire);
            }

        } else {
            PlayerConnection pc = this.room.clients.get(name);
            if (pc != null) {
                pc.motor = true;
                pc.daires.put(id, daire);
            }
        }

    }

    public void removeCollectable(int id, String name) {
        call_count.incrementAndGet();
        if (name.length() == 0) {
            for (PlayerConnection playerConnection : room.clients.values()) {
                playerConnection.daires.remove(id);
            }

        } else {
            PlayerConnection pc = this.room.clients.get(name);
            if (pc != null) {
                pc.daires.remove(id);
            }
        }
    }

    public void blockPlayer(String playerName, boolean block) {
        if (!this.checkStaff())
            return;
        PlayerConnection pc = this.room.server.clients.get(playerName);
        if (pc != null) {
            if (block) room.blockedPlayers.add(playerName);
            else room.blockedPlayers.remove(playerName);
        }
    }

    public void kickPlayer(String playerName) {
        if (!this.checkStaff())
            return;

        PlayerConnection pc = this.room.clients.get(playerName);
        if (pc != null) {
            try {
                pc.enterRoom(pc.server.recommendRoom(pc.Lang), false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setMs(int ms) {
        if (!this.checkStaff())
            return;
        this.eventMs = ms;
    }

    public void playMusic(String link, String name) {
        call_count.incrementAndGet();
        if (name.length() == 0) {
            for (PlayerConnection pc : room.clients.values()) {
                try {
                    pc.sendPacket(MFServer.pack_old(26, 12, new Object[]{link}));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            PlayerConnection pc = room.clients.get(name);
            if (pc != null) {
                try {
                    pc.sendPacket(MFServer.pack_old(26, 12, new Object[]{link}));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void removeSoulmate(String isim1) {
        call_count.incrementAndGet();
        PlayerConnection pc = room.clients.get(isim1);
        if (pc != null) {
            try {
                pc.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(48).writeByte(0).writeInt(pc.kullanici.code));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSoulmate(String isim1, String isim2) {
        call_count.incrementAndGet();
        PlayerConnection pc = room.clients.get(isim1);
        PlayerConnection pc2 = room.clients.get(isim2);
        if (pc != null && pc2 != null) {
            try {
                pc.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(48).writeByte(1).writeInt(pc.kullanici.code).writeInt(pc2.kullanici.code));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSamanMode(String isim, int typ) {
        call_count.incrementAndGet();
        if (typ >= 0 && typ < 3) samanMode.put(isim, typ);
    }

    public void catchMouse(String name, boolean peyver) {
        call_count.incrementAndGet();

        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                room.sendAllOld(8, 23, new Object[]{pc.kullanici.code});

                if (peyver) room.sendAllOld(5, 19, new Object[]{pc.kullanici.code});
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void changeLook(String isim, String tip) {
        if (!this.checkStaff())
            return;
        PlayerConnection pc = room.clients.get(isim);
        if (pc != null) {
            pc.tempTip = tip;
        }
    }

    public void changeKurkRenk(String isim, String tip) {
        if (!this.checkStaff())
            return;
        PlayerConnection pc = room.clients.get(isim);
        if (pc != null) {
            pc.tempKurkRenk = tip;
        }
    }

    public void send321(String isim, boolean ac) {
        call_count.incrementAndGet();
        PlayerConnection pc = room.clients.get(isim);
        if (pc != null) {

            pc.sendPacket(MFServer.getBuf().writeByte(100).writeByte(66).writeBoolean(ac));
//                this.room.sendAll(MFServer.getBuf().writeByte(100).writeByte(66).writeInt(pc.kullanici.code)
            //                       .writeBoolean(ac));
            //  pc.sendStartMap(ac);
        }
    }

    public int kontrol(String isim, String neyi, String veri) {
        if (!this.checkStaff())
            return -2;
        PlayerConnection pc = room.clients.get(isim);
        if (pc != null) {
            switch (neyi) {
                case "ünvan": {
                    Integer unvan = Integer.valueOf(veri);
                    boolean var = pc.kullanici.unvanlar.contains(unvan);
                    int don = 1;
                    if (!var) don = 0;
                    return don;
                }
                case "rozet": {
                    Integer rozet = Integer.valueOf(veri);
                    boolean var = pc.kullanici.rozetler.contains(rozet);
                    int don = 1;
                    if (!var) don = 0;
                    return don;
                }
                case "eşya": {
                    int don = 0;
                    Nesne nesne = pc.kullanici.esyalar.get(veri);
                    if (nesne != null) don = nesne.sayi;
                    return don;
                }
                case "görev": {
                    Integer gorev = Integer.valueOf(veri);
                    int don = 0;
                    for (Gorev G : MFServer.colGorev.find(new Document("pId", pc.kullanici.code).append("tip", gorev))) {
                        if (G.bitti && G.ilerleme >= G.bitis) don = 1;
                        else if (G.bitti && don == 0) don = 2;
                        else if (!G.bitti) don = 3;
                    }
                    return don;
                }
            }
        }
        return -1;
    }

    public void ilerleme(String isim, int tip, int nekadar) {
        if (!this.checkStaff(true) || tip < 30)
            return;
        PlayerConnection pc = room.clients.get(isim);
        if (pc != null) {
            try {
                pc.gorevIlerleme(tip, nekadar);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setStats(boolean ms) {
        if (!this.checkStaff(true))
            return;
        room.countStats = !ms;
    }

    public void duyur(String lang, String luaKey, String ileti) {
        if (!this.checkStaff(true))
            return;
        int l = ileti.length();
        String orjIleti = ileti;
        ileti = ileti.replaceAll("\\<.*?>", "");
        ileti = ileti.replace(" ", "").toLowerCase();
        boolean bulundu = false;
        if (l > 5) {
            Optional<String> ax = room.server.BAD_WORDS.stream().filter(ileti::contains).findFirst();
            bulundu = ax.isPresent();
        }
        if (bulundu) {
            MFServer.log.warning("ALERT ADV : " + room.LuaKey + "SCRIPT SCRIPT " + ileti);
            this.room.closeLua();
            MFServer.MINIGAMES.remove(room.LuaKey);

        }
        lang = lang.toUpperCase();
        for (Room room : room.server.rooms.values()) {
            if (luaKey.isEmpty() || (room.LuaKey != null && room.LuaKey.equals(luaKey))) {
                for (PlayerConnection cl : room.clients.values()) {
                    if (cl.last_duyur + 5_000 > System.currentTimeMillis()) return;
                    cl.last_duyur = System.currentTimeMillis();
                    if (cl.Lang.equals(lang) || cl.countryCode.equals(lang) || lang.isEmpty()) {

                        try {
                            cl.sendMessage(orjIleti);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


        }
    }

    public LuaTable rooms(String name) {
        LuaTable lt = new LuaTable();
        if (!this.checkStaff())
            return lt;
        int i = 0;
        for (Room rm: room.server.rooms.values()){
            if (rm.isMinigame&&rm.LuaKey!= null&&rm.LuaKey.equals(name)&&!rm.isofficial&&rm.playerCount() != rm.maxPlayers&&rm.password.isEmpty()){
                LuaTable lt2 = new LuaTable();
                lt2.set("count",LuaInteger.valueOf(rm.playerCount()));
                lt2.set("community",LuaString.valueOf(rm.community));
                lt.set(LuaString.valueOf(rm.outname),lt2);
            }
        }
        return lt;


    }


    public void movePlayer(String name, String R) {
        if (!this.checkStaff())
            return;
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                pc.enterRoom(R, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public LuaTable roomInfo(String isim) {
        if (!this.checkStaff(true))
            return new LuaTable();
        LuaTable table = new LuaTable();
        try {
            Room oda = MFServer.instance.rooms.get(isim);
            if (oda != null) {
                table.set("playerList", getPlayerList());
                table.set("playerCount", LuaInteger.valueOf(room.playerCount()));
                List<Document> l = new ArrayList<>();
                l = MFServer.colChatLog.find(and(eq("room", oda.name), gte("time", System.currentTimeMillis() - 1_000 * 60 * 60))).projection(new Document("time", 1).append("room", 1).append("name", 1).append("msg", 1).append("_id", -1)).sort(new Document("time", -1)).limit(100).into(l);
                // Collections.reverse(l);
                LuaTable chatLogs = new LuaTable();
                int i = 0;
                for (Document doc : l) {

                    LuaTable chatLog = new LuaTable();
                    chatLog.set("time", LuaInteger.valueOf((int) (doc.getLong("time") / 1000)));
                    chatLog.set("msg", LuaString.valueOf(doc.getString("msg")));
                    chatLog.set("name", LuaString.valueOf(doc.getString("name")));
                    chatLogs.set(i++, chatLog);
                }
                table.set("chatLog", chatLogs);
            }
        } catch (Exception ex) {
            MFServer.log.warning(MFServer.stackTraceToString(ex));
        }
        return table;
    }

    public void eventPlayerRespawn(String isim) {
        this.t.events.add(new Event("eventPlayerRespawn",
                new LuaValue[]{LuaString.valueOf(isim)}));
    }

    public void fishRod(String name, boolean yes) {
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                ByteBuf BUF = MFServer.getBuf();
                BUF.writeByte(4);
                BUF.writeByte(9);
                BUF.writeInt(pc.kullanici.code);
                if (yes) {
                    BUF.writeByte(4);
                    BUF.writeBoolean(yes);
                } else {
                    BUF.writeByte(1);
                    BUF.writeBoolean(false);
                }
                //System.out.println(yes);
                room.sendAll(BUF);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void sendDisableUclu() {
        try {
            ByteBufOutputStream p = MFServer.getStream();

            p.writeByte(29);
            p.writeByte(33);

            p.writeBoolean(D_watch);
            p.writeBoolean(D_debug);
            p.writeBoolean(D_minimalist);

            this.room.sendAll(p.buffer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void giveWings(String name, boolean ac) {
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                pc.wings = ac;
                pc.sendPacket(MFServer.getBuf().writeByte(8).writeByte(10).writeByte(34).writeBoolean(ac));
                pc.room.sendAll(MFServer.getBuf().writeByte(8).writeByte(15).writeInt(pc.kullanici.code).writeBoolean(ac));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playerCat(String name, boolean ac) {
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(43).writeInt(pc.kullanici.code)
                        .writeBoolean(ac));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void grapnel(String name, int x, int y) {
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            try {
                pc.sendPacket(MFServer.getBuf().writeByte(5).writeByte(37).writeInt(pc.kullanici.code)
                        .writeShort(x).writeShort(y));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public LuaValue getUserInventory(String player) {
        if (!this.checkStaff(true)) return LuaValue.NIL;
        PlayerConnection pc = room.clients.get(player);
        if (pc != null) {
            LuaTable ret = new LuaTable();
            for (Map.Entry<String, Nesne> stringNesneEntry : pc.kullanici.esyalar.entrySet()) {
                ret.set(stringNesneEntry.getKey(), LuaValue.valueOf(stringNesneEntry.getValue().sayi));
            }

            return ret;
        }
        return LuaValue.NIL;
    }

    public Varargs parkourMode(boolean parkourMode) {
        if (!this.checkStaff(true)) return LuaValue.NIL;
        room.parkourMode = parkourMode;
        return LuaValue.valueOf(parkourMode);
    }

    public Varargs checkHasTitle(String player, Integer title) {
        if (!this.checkStaff(true)) return LuaValue.NIL;
        PlayerConnection pc = room.clients.get(player);
        if (pc != null) {
            return LuaBoolean.valueOf(pc.kullanici.unvanlar.contains(title));
        }
        return LuaValue.NIL;
    }

    public void newRecord(String name, boolean r) {
        if (!this.checkStaff(true)) return;
        PlayerConnection pc = room.clients.get(name);
        if (pc != null) {
            pc.getRecord = r;
        }
    }

    public void onlyMe(boolean onlyMe) {
        if (!this.checkStaff(true)) return;
        room.onlyme = onlyMe;
    }
}

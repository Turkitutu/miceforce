/**
 * *****************************************************************************
 * Copyright (c) 2009-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import org.apache.commons.lang3.StringUtils;
import org.luaj.vm2.*;

/**
 * Subclass of {@link LibFunction} which implements the lua standard
 * {@code string} library.
 * <p>
 * Typically, this library is included as part of a call to either
 * {@link org.luaj.vm2.lib.jse.JsePlatform#standardGlobals()} or
 * <pre> {@code
 * Globals globals = JsePlatform.standardGlobals();
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * To instantiate and use it directly, link it into your globals table via
 * {@link LuaValue#load(LuaValue)} using code such as:
 * <pre> {@code
 * Globals globals = new Globals();
 * globals.load(new JseBaseLib());
 * globals.load(new PackageLib());
 * globals.load(new StringLib());
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * This is a direct port of the corresponding library in C.
 *
 * @see LibFunction
 * @see org.luaj.vm2.lib.jse.JsePlatform
 * @see <a href="http://www.lua.org/manual/5.2/manual.html#6.4">Lua 5.2 String
 * Lib Reference</a>
 */
public class MFLib extends TwoArgFunction {

    private L_Room room;

    /**
     * Construct a StringLib, which can be initialized by calling it with a
     * modname string, and a global environment table as arguments using
     * {@link #call(LuaValue, LuaValue)}.
     */
    public MFLib(L_Room room) {
        this.room = room;
    }


    public LuaValue call(LuaValue modname, LuaValue env) {

        LuaTable string = new LuaTable();

        string.set("catchMouse", new catchMouse());
        string.set("changeLook", new changeLook());
        string.set("changeKurkRenk", new changeKurkRenk());
        string.set("freeze", new send321());
        string.set("kontrol", new kontrol());
        string.set("ilerleme", new ilerleme());
        string.set("duyur", new duyur());
        string.set("roomInfo", new roomInfo());
        string.set("spawnMonster", new spawnMonster());
        string.set("moveMonster", new moveMonster());
        string.set("giveHealth", new giveHealth());
        string.set("sendStorm", new sendStorm());
        string.set("spawnTrap", new spawnTrap());
        string.set("forceAyna", new forceAyna());
        string.set("randomAyna", new randomAyna());
        string.set("getTitleText", new getTitleText());
        string.set("getUserInventory", new getUserInventory());
        string.set("checkHasTitle", new checkHasTitle());
        string.set("onlyMe", new onlyMe());
        string.set("parkourMode", new parkourMode());
        env.set("mf", string);
        return env;
    }
    final class parkourMode extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean parkourMode = args.checkboolean(1);
                return room.parkourMode(parkourMode);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class getTitleText extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int title = args.checkint(1);
                return room.getTitleText(title);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class checkHasTitle extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String player = args.checkjstring(1);
                Integer title = args.checkint(2);
                return room.checkHasTitle(player,title);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class onlyMe extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean onlyMe = args.checkboolean(1);
                room.onlyMe(onlyMe);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class getUserInventory extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String player = args.checkjstring(1);
                return room.getUserInventory(player);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class randomAyna extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                room.room.randomAyna= args.checkboolean(1);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class forceAyna extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean ayna = args.checkboolean(1);
                int i=0;
                if(ayna)i=1;
                room.room.forceAyna=i;
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class sendStorm extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int miktar = args.checkint(1);
                room.sendStorm(miktar);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class giveHealth extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                int miktar = args.checkint(2);
                room.giveHealth(name,miktar);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class moveMonster extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int mid = args.checkint(1);
                int yon = args.checkint(2);
                room.moveMonster(mid,yon);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class spawnTrap extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                int x = args.checkint(2);
                int y = args.checkint(3);
                int scaleX = args.checkint(4);
                int scaleY = args.checkint(5);
                int rotation = args.checkint(6);
                room.spawnTrap(id, x, y, scaleX,scaleY,rotation);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class spawnMonster extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int mid = args.checkint(1);
                int x = args.checkint(2);
                int y = args.checkint(3);
                String name = args.checkjstring(4);
                int can = args.optint(5, 10);
                room.spawnMonster(mid, x, y, name,can);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class roomInfo extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                return room.roomInfo(isim);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class kontrol extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                String neyi = args.checkjstring(2);
                String veri = args.checkjstring(3);

                return LuaInteger.valueOf(room.kontrol(isim, neyi, veri));
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class duyur extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String lang = args.checkjstring(1);
                String mes = args.checkjstring(2);
                String luaKey = args.optjstring(3,"");

                room.duyur(lang,luaKey, mes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class ilerleme extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                int tip = args.checkint(2);
                int nekadar = args.checkint(3);

                room.ilerleme(isim, tip, nekadar);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class send321 extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                boolean ac = args.optboolean(2, true);

                room.send321(isim, ac);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class changeLook extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                String tip = args.checkjstring(2);

                room.changeLook(isim, tip);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class changeKurkRenk extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim = args.checkjstring(1);
                String tip = args.checkjstring(2);

                room.changeKurkRenk(isim, tip);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class catchMouse extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                boolean peyver = args.optboolean(2, true);

                room.catchMouse(name, peyver);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


}

/**
 * *****************************************************************************
 * Copyright (c) 2009-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import org.apache.commons.lang3.StringUtils;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

/**
 * Subclass of {@link LibFunction} which implements the lua standard
 * {@code string} library.
 * <p>
 * Typically, this library is included as part of a call to either
 * {@link org.luaj.vm2.lib.jse.JsePlatform#standardGlobals()} or
 * {@link org.luaj.vm2.lib.jme.JmePlatform#standardGlobals()}
 * <pre> {@code
 * Globals globals = JsePlatform.standardGlobals();
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * To instantiate and use it directly, link it into your globals table via
 * {@link LuaValue#load(LuaValue)} using code such as:
 * <pre> {@code
 * Globals globals = new Globals();
 * globals.load(new JseBaseLib());
 * globals.load(new PackageLib());
 * globals.load(new StringLib());
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * This is a direct port of the corresponding library in C.
 *
 * @see LibFunction
 * @see org.luaj.vm2.lib.jse.JsePlatform
 * @see org.luaj.vm2.lib.jme.JmePlatform
 * @see <a href="http://www.lua.org/manual/5.2/manual.html#6.4">Lua 5.2 String
 * Lib Reference</a>
 */
public class UILib extends TwoArgFunction {

    private L_Room room;

    /**
     * Construct a StringLib, which can be initialized by calling it with a
     * modname string, and a global environment table as arguments using
     * {@link #call(LuaValue, LuaValue)}.
     */
    public UILib(L_Room room) {
        this.room = room;
    }

    public static int parseLuaFloat(String value) {
        if (value.contains("E")) {
            value = StringUtils.split(value, "E")[0];
        }
        if (StringUtils.isNumeric(value)) {
            return Integer.valueOf(value + "00");
        } else {
            if (value.contains(".")) {
                if (value.startsWith("0.")) {
                    value = value.substring(2);
                    return Integer.valueOf(value + "0");
                } else {
                    String n = value.replace(".", "");
                    if (n.length() == 2) {
                        n += "0";
                    }
                    return Integer.valueOf(n);
                }

            } else {
                return 0;
            }
        }
    }

    public LuaValue call(LuaValue modname, LuaValue env) {

        LuaTable string = new LuaTable();

        string.set("setMapName", new setUIMapName());
        string.set("addTextArea", new addTextArea());
        string.set("removeTextArea", new removeTextArea());
        string.set("updateTextArea", new updateTextArea());
        string.set("showColorPicker", new showColorPicker());
        string.set("addLog", new addLog());
        string.set("addPopup", new addPopup());
        string.set("setShamanName", new setShamanName());
        env.set("ui", string);
        return env;
    }

    final class setShamanName extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String text = args.checkjstring(1);

                room.setShamanName(text);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addLog extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String text = args.checkjstring(1);
                String target = args.optjstring(2, "");

                room.addLog(text, target);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addPopup extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                int type = args.checkint(2);
                String text = args.checkjstring(3);
                String targetPlayer = args.optjstring(4, "");
                int x = args.optint(5, 50);
                int y = args.optint(6, 50);
                int width = args.optint(7, 0);
                boolean fixedPos = args.optboolean(8, false);

                room.addPopup(id, type, text, targetPlayer, x, y, width, fixedPos);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class showColorPicker extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String target = args.optjstring(2, "");
                int defaultColor = args.optint(3, 0);
                String title = args.optjstring(4, "");

                room.showColorPicker(id, target, defaultColor, title);
                //room.setUIMapName(message   );
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class updateTextArea extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String text = args.checkjstring(2);
                String target = args.optjstring(3, "");

                room.updateTextArea(id, text, target);
                //room.setUIMapName(message   );
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class removeTextArea extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String target = args.optjstring(2, "");

                room.removeTextArea(id, target);
                //room.setUIMapName(message   );
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class addTextArea extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String text = args.checkjstring(2);
                String target = args.optjstring(3, "");
                int x = args.optint(4, 50);
                int y = args.optint(5, 50);
                int width = args.optint(6, 0);
                int height = args.optint(7, 0);
                int backgroundColor = args.optint(8, 0x324650);
                int borderColor = args.optint(9, 0);
                int backgroundAlpha = ExecLib.parseDouble(args.optdouble(10, 0));
                boolean fixedPos = args.optboolean(11, false);
                room.addTextArea(id, text, target, x, y, width, height, backgroundColor, borderColor, backgroundAlpha, fixedPos);
                //room.setUIMapName(message   );
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setUIMapName extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String message = args.checkjstring(1);

                room.setUIMapName(message);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

}

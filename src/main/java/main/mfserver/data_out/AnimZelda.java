/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class AnimZelda extends base {

    public AnimZelda(PlayerConnection client, Integer item) throws IOException {
        super((byte) 8, (byte) 44, client);
        buf.writeInt(client.kullanici.code);
        buf.writeByte(0);
        buf.writeInt(item);

    }
    public AnimZelda(PlayerConnection client, String url) throws IOException {
        super((byte) 8, (byte) 44, client);
        buf.writeInt(client.kullanici.code);
        buf.writeByte(5);
        buf.writeUTF(url);

    }
    public AnimZelda(PlayerConnection client, String url,int byt) throws IOException {
        super((byte) 8, (byte) 44, client);
        buf.writeInt(client.kullanici.code);
        buf.writeByte(7);
        buf.writeUTF(url);
        buf.writeByte(byt);

    }
}

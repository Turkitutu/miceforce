/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class GetCheese extends base_old {
    public GetCheese(PlayerConnection client) throws IOException {
        super((byte) 5, (byte) 19, client);
        this.objects = new Object[]{client.kullanici.code};
        client.hasCheese = true;

    }
    public GetCheese(int code) throws IOException {
        super((byte) 5, (byte) 19);
        this.objects = new Object[]{code};


    }
}

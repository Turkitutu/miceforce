/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class ItemBought extends base {

    public ItemBought(PlayerConnection client, Integer item) throws IOException {
        super((byte) 20, (byte) 2, client);
        buf.writeShort(item);
        buf.writeByte(1);

    }
}

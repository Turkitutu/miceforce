/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import io.netty.buffer.ByteBuf;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class O_Crouch extends base {

    public O_Crouch(PlayerConnection client, byte direction) throws IOException {
        super((byte) 4, (byte) 9, client);
        ByteBuf BUF = this.buf.buffer();
        BUF.writeInt(client.kullanici.code);
        BUF.writeByte(direction);
        BUF.writeByte(0);
    }
}

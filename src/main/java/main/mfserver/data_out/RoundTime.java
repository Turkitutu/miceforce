/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.Room;

import java.io.IOException;

/**
 * @author sevendr
 */
public class RoundTime extends base {

    public RoundTime(Room room, Integer time) throws IOException {
        super((byte) 5, (byte) 22, room);
        this.buf.writeShort(time);

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.Room;
import mfserver.net.PlayerConnection;
import org.jooq.lambda.Seq;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sevendr
 */
public class Shaman extends base {

    public Shaman(Room room) throws IOException {
        this(room, false, false);
    }
   public List<Integer> ruhani=Seq.of(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14).toList();
    public List<Integer> ruzgar=Seq.of(20,21,22,23,24,25,26,27,28,29,30,31,32,33,34).toList();
    public List<Integer> makinist=Seq.of(40,41,42,43,44,45,46,47,48,49,50,51,52,53,54).toList();
    public List<Integer> yabani=Seq.of(86,89,68,88,84,66,71,73,72,81,92,80,93,70,94).toList();
    public List<Integer> fizikci=Seq.of(61,62,65,69,67,82,60,64,63,74,87,83,85,90,91).toList();
    private int findCartouche(PlayerConnection pc){
        int ikon=pc.kullanici.ikon;
        if(ikon==0){
            int[] skTypes=new int[]{0,0,0,0,0};
            for(Map.Entry<String,Integer> entry:(pc.kullanici.beceriler.entrySet())){
                int key=Integer.valueOf(entry.getKey());
                int val=entry.getValue();

                if(ruhani.contains(key))skTypes[0]+=val;
                else if(ruzgar.contains(key))skTypes[1]+=val;
                else if(makinist.contains(key))skTypes[2]+=val;
                else if(yabani.contains(key))skTypes[3]+=val;
                else if(fizikci.contains(key))skTypes[4]+=val;
            }
            int[] max = new int[2];
            for (int i = 0; i < skTypes.length; i++) {
                if(skTypes[i]>max[0]){
                    max[0]=skTypes[i];
                    max[1]=i;
                }
            }
            return -max[1];

        }
        return ikon;
    }
    public Shaman(Room room, boolean s1, boolean s2) throws IOException {
        super((byte) 8, (byte) 11, room);
        int level = 0, level2 = 1;
        int ikon = 0, ikon2 = 0;
        int mode = 0, mode2 = 0;
        int code = 0, code2 = 0;
        PlayerConnection sham = room.getFirstShaman();
        PlayerConnection sham2 = room.getSecondShaman();
        if (s2) {
            sham = sham2;
            sham2 = null;
        } else if (s1) {
            sham2 = null;
        }
        if (sham != null&&sham.kullanici!=null) {
            mode = sham.kullanici.samanTur;
            if(room.l_room!=null){
                mode=room.l_room.samanMode.getOrDefault(sham.kullanici.isim,sham.kullanici.samanTur);
            }
            ikon = findCartouche(sham);
            level = sham.kullanici.seviye.seviye;
            code = sham.kullanici.code;
        }
        if (sham2 != null&&sham2.kullanici!=null) {
            mode2 = sham2.kullanici.samanTur;

            if(room.l_room!=null){
                mode2=room.l_room.samanMode.getOrDefault(sham2.kullanici.isim,sham2.kullanici.samanTur);
            }
            ikon2 = findCartouche(sham2);
            level2 = sham2.kullanici.seviye.seviye;
            code2 = sham2.kullanici.code;
        }
        if ((room.isSurvivor || room.isKutier) && mode == 2) mode = 1;
        this.buf.writeInt(code);
        this.buf.writeInt(code2);
        this.buf.writeByte(mode);
        this.buf.writeByte(mode2);
        this.buf.writeShort(level);
        this.buf.writeShort(level2);
        this.buf.writeShort(ikon);
        this.buf.writeShort(ikon2);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.main.MFServer;
import mfserver.main.Room;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class base {
    ByteBufOutputStream buf;
    private byte t1;
    private byte t2;

    // private ByteBuf Buffer;
    protected base(byte t1, byte t2, PlayerConnection client) throws IOException {
        this.buf = client.getStream();
        this.t1 = t1;
        this.t2 = t2;
        this.buf.writeByte(t1);
        this.buf.writeByte(t2);
    }

    protected base(byte t1, byte t2, Room room) throws IOException {
        this.buf = MFServer.getStream();
        this.t1 = t1;
        this.t2 = t2;
        this.buf.writeByte(t1);
        this.buf.writeByte(t2);
    }

    protected base() throws IOException {

    }

    public ByteBuf data() throws IOException {
        // System.out.println(this.buf.buffer().readableBytes());
        return this.buf.buffer();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author sevendr
 */
public class login_1 extends base {

    public login_1(PlayerConnection client) throws IOException {
        super((byte) 26, (byte) 2, client);
        if(client.kullanici==null)return;
        this.buf.writeInt(client.kullanici.code);
        this.buf.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
        this.buf.writeInt(3990312);
        this.buf.writeByte(client.LangByte);
        this.buf.writeInt(client.kullanici.code);
        this.buf.writeByte(1);
        ArrayList<Integer> arr = new ArrayList<>();
        if (client.kullanici.yetki >= 9) arr.add(10);
        if (client.kullanici.yetki >= 4) arr.add(5);
        if (client.kullanici.yetki >= 2) arr.add(3);
        if (client.kullanici.isFuncorp) arr.add(13);//Funcorp
        if (client.kullanici.isLuacrew) arr.add(12);//Lua
        if (client.kullanici.isMapcrew) arr.add(11);
        if (client.kullanici.yetki >= 5 || client.kullanici.isFuncorp || client.kullanici.isArtist) {
            //arr.add(10);
        }
        if (client.room != null && client.room.isQuiz) {

            arr.clear();
            if (client.room.codes[0].kullanici.code == client.kullanici.code) {
              arr.add(10);
            }
        }
        //   arr.add(10);
      /*  if (client.kullanici.yetki >= 2 || client.kullanici.isFuncorp || client.kullanici.isMapcrew || client.kullanici.isLuacrew) {

           arr.add(5); //Cafeyi yönetemiyor
            arr.add(7);
            arr.add(3);
            arr.add(15);
        }*/
        this.buf.writeByte(arr.size());
        for (Integer integer : arr) {
            this.buf.writeByte(integer);
        }
        //this.buf.writeByte(0);
        this.buf.writeByte(0);

    }

}

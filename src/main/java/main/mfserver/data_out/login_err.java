/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class login_err extends base {

    public login_err(PlayerConnection client, int type, String name) throws IOException {
        super((byte) 26, (byte) 12, client);
        this.buf.writeByte(type);
        this.buf.writeUTF(name);
        this.buf.writeUTF("");

    }

}

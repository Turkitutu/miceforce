package mfserver.komutlar;

import mfserver.net.PlayerConnection;

import java.io.IOException;

public class emoticon extends temel {
    public emoticon(){
        super("emoticon");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (args.length == 1) {
            try
            {
                int emoticon = Integer.parseInt(args[0]);
                if(emoticon > 0 && emoticon <= 8){

                    client.kullanici.evInfo.emoticon = emoticon;
                }
            }
            catch (NumberFormatException e)
            {
            }
        } else {
            client.kullanici.evInfo.emoticon = 0;
        }
        // client.sendMessage("<R>I've got that summertime, summertime sadness.");
        //
    }
}

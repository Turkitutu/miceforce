package mfserver.komutlar;

import io.netty.buffer.ByteBufOutputStream;
import mfserver.data_out.login_1;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import veritabani.Kullanici;

import java.io.IOException;

import static mfserver.net.PlayerConnection.safeTag;

public class etiket extends temel {
    public etiket() {
        super("etiket");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
       // if (client.kullanici.tagDeadLine < System.currentTimeMillis()) return;
        if (args.length == 0) {
            client.kullanici.tag = "";
            client.sendPacket(new login_1(client).data());
            client.sendMessage("<VP>Your name tag is now off.");
            return;
        }
        Kullanici kullanici=client.kullanici;
        String yeni=args[0];
        try {
            boolean isStaff = kullanici.tagUsage==100;
            String s;
            if(!isStaff){
                Integer t = Integer.parseInt(yeni);
                if (t < 0) t = -t;
                s = t.toString().substring(0, Math.min(4, t.toString().length()));
                s = s.replace("-", "");
                while (s.length() < 4 ) {
                    s= "0" + s;
                }
            }
            else {
                s=yeni.substring(0,Math.min(8,yeni.length()));
                if(s.contains("mod")||s.contains("sex")||s.contains("ass")||s.contains("puss")||s.contains("bitch")){
                    s="NOPE";
                }
            }
            kullanici.tag = safeTag(s);
            client.sendMessage("<VP>Wooohooo hi " + kullanici.isim + "#<R>" + kullanici.tag);

            client.sendPacket(new login_1(client).data());
            client.sendMessage("<VP>You can use /tag to remove it.");
        } catch (NumberFormatException ex) {
            client.sendMessage("<R>There is something wrong with your tag.");
        }


    }
}

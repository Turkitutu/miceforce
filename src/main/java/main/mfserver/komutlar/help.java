package mfserver.komutlar;

import mfserver.net.PlayerConnection;
import mfserver.util.Dil;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

public class help extends temel {
    public help() {
        super("help");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        TreeMap<String, temel> temelHashSet = new TreeMap<>();
        HashSet<temel> temelHashSe2t = new HashSet<>();
        for (Map.Entry<String, temel> entry : temel.KOMUTLAR.entrySet()) {
            temel val = entry.getValue();
            if (val.yetkili(client) && !val.f_c.equals("help")) {
                temelHashSet.put(val.f_c, val);
            }
        }

        ArrayList<String> txt = new ArrayList<>();
        for (temel t : temelHashSet.values()) {
            if (temelHashSe2t.contains(t)) continue;
            temelHashSe2t.add(t);
            String str = t.getDoc(client);
            if (str != null) txt.add("<R>/" + t.f_c + "</R> <J>=></J> " + Dil.yazi(client.Lang, str));
        }
        Collections.sort(txt);
        //Collections.reverse(txt);
        client.sendMessage(StringUtils.join(txt, "\n"));
    }

}

package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;
import java.util.regex.Matcher;

public class mp3 extends temel {
    public mp3() {
        super("mp3", 1);
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (client.T_Music != null) {
            client.T_Music.cancel(false);
            client.T_Music = null;
        }


        Matcher matcher = MFServer.patternYoutube.matcher(args[0]);
        switch (args[0]) {
            case "https://www.youtube.com/watch?v=Fko5fYIBJFU": //VIVA
                client.giveTitleforMusic(3077, 160);
                break;
            case "https://www.youtube.com/watch?v=1OEron4rXfk": //Lana
                client.giveTitleforMusic(3152, 200);
                break;
            case "https://www.youtube.com/watch?v=iS9lUNHWYqQ": //Orhan
                client.giveTitleforMusic(3151, 200);
                break;
            case "https://www.youtube.com/watch?v=wwWC9tqOBVQ": //Nenni
                client.giveTitleforMusic(3150, 207);
                break;
            case "https://www.youtube.com/watch?v=4CI3lhyNKfo": // Ciao Bella
                client.giveTitleforMusic(3036, 100);
                client.sendMessage("<R>E seppellire lassù in montagna, sotto l'ombra di un bel fior.");
                break;
            case "https://www.youtube.com/watch?v=0R1jecPe6UU": // Rhineland
                client.giveTitleforMusic(3161, 150);
                client.sendMessage("<J>Life, life is all right on the Rhine.");
                break;
            case "https://www.youtube.com/watch?v=NnfQu8Ob34g": // Pirate Mouse
                client.giveTitleforMusic(3162, 150);
                break;
            case "https://www.youtube.com/watch?v=f_WuQ6i2xko": // Double Trouble
                client.giveTitleforMusic(3163, 70);
                break;
            case "https://www.youtube.com/watch?v=AUChk0lxF44"://Victorius
                client.giveTitleforMusic(3512,120);
                break;
            case "https://www.youtube.com/watch?v=m5J6-bCMz3Y"://Shebnem Paker
                client.giveTitleforMusic(3519,110);
                break;
            case "https://www.youtube.com/watch?v=aS1no1myeTM"://3974
                client.giveTitleforMusic(3974,110);
                break;

        }
        if (matcher.find()) {
            args[0] = "http://mp3.miceforce.com/" + matcher.group();
            if (client.room.isTribeHouse && client.tribe == client.room.tribe
                    && client.tribe.checkPermissions("can_music", client)) {
                client.room.sendAllOld(26, 12, args);
            } else if (client.kullanici.yetki >= 4 || (client.kullanici.isFuncorp && client.room.isFuncorp)) {
                client.room.sendAllOld(26, 12, args);
            } else {
                client.sendPacket(MFServer.pack_old(26, 12, args));
            }
        }


    }
}

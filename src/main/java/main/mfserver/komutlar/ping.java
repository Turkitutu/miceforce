package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;

public class ping extends temel {
    public ping(){
        super("ping");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (args.length == 1 && client.kullanici.yetki >= 2) {
            String name = MFServer.noTag(MFServer.firstLetterCaps(args[0]));
            PlayerConnection pc = client.server.clients.get(name);
            if (pc != null) {
                client.sendMessage("<N>" + name + "'s ping : " + pc.ping);

            } else {

                client.sendMessage("<R>$Joueur_Existe_Pas");
            }
            return;
        }
        client.sendMessage("<N>Ping : " + client.ping);
    }

    public String getDoc(PlayerConnection client){
        if(client.kullanici.yetki>=2)return "command_ping2";
        return doc;
    }
}

package mfserver.komutlar;

import com.mongodb.client.model.Projections;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.apache.commons.lang3.StringUtils;
import veritabani.Kullanici;

import java.io.IOException;
import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;

public class ref extends temel {
    public ref() {
        super("ref");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        MFServer.executor.execute(() -> {
            //client.updateRefs();
            int code = client.kullanici.code;
            if (args.length == 1 && client.kullanici.yetki >= 5) {
                String name = MFServer.noTag(MFServer.firstLetterCaps(args[0]));
                code = MFServer.checkUserInDb(name);
                if (code <= 0) {
                    try {
                        client.sendMessage("<R> User couldn't be found.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
            ArrayList<String> txt = new ArrayList<>();
            txt.add("Every account must have 50.000 cheese and 0 first.");
            int i = 0;
            int i2 = 0;
            for (Kullanici kullanici : MFServer.colKullanici.find(eq("ref", code)).projection(Projections.include("refComp", "peynir", "birinci", "kayit", "isim"))) {
                String tag = "VP";
                if (!kullanici.refComp) tag = "R";
                txt.add("<" + tag + ">[" + kullanici.isim + "]</" + tag + "> " + kullanici.peynir + " cheese");
                i += 1;
                if (kullanici.peynir >= 50_000 && kullanici.refComp) {
                    i2++;
                }
            }
            txt.add("Total : " + i);
            txt.add("Counted : " + i2);
            txt.add("Your referral link is : http://www.miceforce.com/?id=" + code);
            try {
                client.sendMessage(StringUtils.join(txt, "\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

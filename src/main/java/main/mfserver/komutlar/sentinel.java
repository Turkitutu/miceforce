package mfserver.komutlar;

import mfserver.net.PlayerConnection;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class sentinel extends temel {
    public sentinel(){
        super("sentinel");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        ArrayList<String> txt = new ArrayList<>();
        HashMap<String, ArrayList<String>> stat = new HashMap<>();
        for (PlayerConnection pc : client.server.clients.values()) {
            if (pc.kullanici.isSentinel && !pc.topsecret&& !pc.hidden) {
                stat.computeIfAbsent(pc.OrjLang, k -> new ArrayList<>()).add(pc.kullanici.isim);
            }
        }
        if (stat.size() == 0) {
            txt.add("There aren't any sentinel online");
        } else {
            txt.add("Online Sentinels:");
        }
        for (Map.Entry<String, ArrayList<String>> entry : stat.entrySet()) {
            txt.add("<BL>[" + entry.getKey().toLowerCase() + "] <BV>"
                    + StringUtils.join(entry.getValue(), "<BL>, <BV>"));
        }
        client.sendMessage(StringUtils.join(txt, "\n"));



    }
}

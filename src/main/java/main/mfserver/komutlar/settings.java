package mfserver.komutlar;

import mfserver.net.PlayerConnection;

import java.io.IOException;

public class settings extends temel {
    public settings(){
        super("settings");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        client.kullanici.disableSettings=!client.kullanici.disableSettings;
        if (client.kullanici.disableSettings) {
            client.sendMessage("<R>It's disabled now.");
        }
        else{

            client.sendMessage("<VP>It's enabled now.");
        }
    }
}

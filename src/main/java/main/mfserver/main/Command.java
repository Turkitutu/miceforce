/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.primitives.Ints;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.data_out.O_Death;
import mfserver.komutlar.temel;
import mfserver.net.PlayerConnection;
import mfserver.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.bson.Document;
import org.jooq.lambda.Unchecked;
import org.json.JSONArray;
import org.json.JSONObject;
import org.luaj.vm2.lib.L_Room;
import org.luaj.vm2.lib.SystemLib;
import veritabani.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;
import static mfserver.main.MFServer.Anket;
import static mfserver.net.PlayerConnection.pack_modchat;
import static mfserver.net.PlayerConnection.safeTag;
import static org.luaj.vm2.lib.L_Room.packImage;
import static org.luaj.vm2.lib.L_Room.packRemoveImage;
import static org.luaj.vm2.lib.SystemLib.CL;

/**
 * @author sevendr
 */
public class Command {

    MFServer server;
    PlayerConnection client;

    public Command(PlayerConnection client) {
        this.client = client;
        this.server = client.server;
    }

    int randomFromSet(HashSet<Integer> hashSet) {
        int size = hashSet.size();
        int item = MFServer.random.nextInt(size); // In real life, the Random object should be rather more shared than this
        int i = 0;
        for (Integer obj : hashSet) {
            if (i == item)
                return obj;
            i++;
        }
        return 1;
    }

    static String get_lang(String orj) {
        switch (orj) {
            case "SE":
                return "Sweden";
            case "NL":
                return "Netherlands";
            case "FR":
                return "France";
            case "UY":
                return "Uruguay";
            case "DZ":
                return "Algeria";
            case "VE":
                return "Venezuela";
            case "CA":
                return "Canada";
            case "EG":
                return "Egypt";
            case "RU":
                return "Russian Federation";
            case "CL":
                return "Chile";
            case "TR":
                return "Turkey";
            case "CO":
                return "Colombia";
            case "US":
                return "United States";
            case "GB":
                return "United Kingdom";
            case "AR":
                return "Argentina";
            case "SA":
                return "Saudi Arabia";
            case "CZ":
                return "Czech Republic";
            case "HU":
                return "Hungary";
            case "RO":
                return "Romania";
            case "BR":
                return "Brazil";
            case "PL":
                return "Poland";
            case "PE":
                return "Peru";
            case "LT":
                return "Lithuania";
            case "SK":
                return "Slovakia";
            default:
                return orj;
        }
    }

    public void command(String command) throws Exception {
        if (client.kullanici.yetki < 1) {
            return;
        }
        String[] r_c = StringUtils.split(command, " ");
        String[] args = Arrays.copyOfRange(r_c, 1, r_c.length);
        String f_c = r_c[0].toLowerCase();
        int l_c = r_c.length;
        if (client.kullanici.yetki > 1 || client.kullanici.isLuacrew || client.kullanici.isMapcrew || client.kullanici.isFuncorp) {
            MFServer.logExecutor.execute(() -> {
               /* ArrayList<String> args = new ArrayList<>();
                for (int i = 0; i < r_c.length; i++) {
                    if (i != 0) args.add(r_c[i]);
                }*/
                MFServer.colCommandLog.insertOne(new Document("name", client.kullanici.isim).append("room", client.room.name).append("cmd", f_c).append("args", StringUtils.join(args, " ")).append("time", System.currentTimeMillis()));

            });
        }

        //   this.client.sendPacket(MFServer.getBuf().writeByte(25).writeByte(42).writeByte(10).writeByte(10).writeByte(10));
        //System.out.println(command);
        if (l_c == 1) {
            if (MFServer.perm.matcher(f_c).find()) {
                final int perm;

                if (this.client.kullanici.yetki == 10 || this.client.kullanici.isMapcrew) {
                    // if(true)return;
                    try {
                        perm = Integer.parseInt(r_c[0].substring(1));
                    } catch (NumberFormatException e) {
                        client.sendMessage("<R>perm must be a number");
                        return;
                    }
                    if (perm == 77 || this.client.room.Map.name.isEmpty()) {
                        client.sendMessage("<R>oh boy oh you can't do it");
                        return;

                    }
                    MFServer.executor.execute(() -> {
                        MFServer.colPermLog.insertOne(new Document("type", 2).append("name", client.kullanici.isim).append("map", this.client.room.Map.id).append("oldPerm", this.client.room.Map.perm).append("newPerm", perm).append("time", MFServer.timestamp()));


                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection
                                     .prepareStatement("update maps set perm = ? where id = ?");) {
                            statement.setInt(1, perm);
                            statement.setInt(2, this.client.room.Map.id);
                            statement.execute();
                            client.sendMessage("<VP>done.");
                            client.sendChatMC(client.kullanici.isim + " changed perm " + this.client.room.Map.perm + " > " + perm
                                    + " of @" + client.room.Map.id);
                            ArrayList<Integer> arr = this.server.perms.get(this.client.room.Map.perm);
                            if (arr != null) {
                                arr.remove(Integer.valueOf(this.client.room.Map.id));
                            }
                            arr = this.server.perms.get(perm);
                            if (arr != null) {
                                arr.add(this.client.room.Map.id);
                            }

                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    });

                }
                return;
            }
        }
        temel _temel = temel.KOMUTLAR.get(f_c);
        if (_temel != null) {
            if (_temel.yetkili(client) && args.length >= _temel.m_args) {
                _temel.isle(client, args);
            }
            return;
        }


        // client.LuaGame();

 /*       ByteBufOutputStream b = MFServer.getStream();
        b.writeByte(100);
        b.writeByte(69);
        b.writeInt(client.kullanici.code);
        b.writeUTF("$Oeuf");
        b.writeShort(-15);
        b.writeShort(0);
        this.client.room.sendAll(b.buffer());*/
        //Parasut
     /*   ByteBufOutputStream ab = MFServer.getStream();
        ab.writeByte(100);
        ab.writeByte(71);
        ab.writeInt(client.code);
        ab.writeByte(30);
        client.sendPacket(ab.buffer());*/
        //Zone
       /* ByteBufOutputStream ab = MFServer.getStream();
        ab.writeByte(100);
        ab.writeByte(82);
        ab.writeBoolean(true);
        ab.writeByte(1);

        ab.writeShort(230);
        ab.writeShort(230);
        ab.writeShort(30);
        ab.writeShort(30);
        ab.writeShort(30);
        client.sendPacket(ab.buffer());*/
        //ab = MFServer.getStream();
        // ab.writeByte(26);
        // ab.writeByte(14);
        // ab.writeUTF("1,1,1,1,1,2");

        //Steam kodu
     /*   ab.writeByte(100);
        ab.writeByte(90);
        ab.writeByte(1);
        ab.writeInt(1);
        ab.writeShort(200);
        ab.writeShort(200);
        ab.writeShort(200);
        ab.writeUTF("0419cde4d87d11e7b5f688d7f643024a.png");
        client.sendPacket(ab.buffer());*/


//        client.sendPacket(ab.buffer());
        // client.sendPacket(.writeByte(5).writeByte(34).writeInt(this.client.code).writeByte(1));
        // client.room.addDefilante(200,200,0);
        // client.move(200, 200, false, 0, 0, false);
        switch (f_c) {

            /*case "motor":
                MFServer.executor.scheduleAtFixedRate(() -> {
                   // client.sendPacket(MFServer.getBuf().writeByte(5).writeByte(34).writeInt(this.client.code).writeByte(1));
                    client.sendPacket(MFServer.getBuf().writeByte(100).writeByte(66).writeByte(0));
                },0,30,TimeUnit.MILLISECONDS);w
                break;*/
            case "admin":
                server.disconnectIPaddress(client.ip);
                break;
            case "profil":
            case "perfil":
            case "profile":
                //ByteBufOutputStream bfs=MFServer.getStream();
                //36868 ÖDÜL KAZANMA

                if (l_c == 2) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    MFServer.executor.execute(() -> {
                        try {
                            this.client.sendProfile(name);
                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    });
                }
                break;

            case "resetshop": {

                this.client.LuaGame();
                ByteBufOutputStream p = MFServer.getStream();
                p.writeByte(29);
                p.writeByte(23);
                p.writeInt(7775);
                p.writeByte(1);
                p.writeUTF(
                        "Are they sure they want to reset your fraises, cheeses, oufits ?");
                p.writeShort(300);
                p.writeShort(300);
                p.writeShort(200);
                p.writeBoolean(false);

                this.client.sendPacket(p.buffer());
                break;
            }
            case "wowitsnotacommand":
                client.sendPlayerPet(25);
                break;
            case "resetoutfits":

                this.client.kullanici.tipler.clear();
                break;
            case "resetinventory":
                this.client.kullanici.esyalar.clear();
                this.client.yollaNesneleri();
                break;
            case "logpacket":
                if (client.kullanici.yetki < 5) return;
                for (PlayerConnection playerConnection : client.room.clients.values()) {
                    playerConnection.packetWatch = System.currentTimeMillis() + 1_000 * 60;
                }
                client.sendMessage("<VP>Done.");
                break;
            case "votekick":
                if (true) return;
                if (!client.room.password.isEmpty() || client.room.isTribeHouse) {
                    client.sendMessage("<R>You can't vote in room with password or tribe house.");
                }
                if (client.susturma != null && client.susturma.surec > 0) return;
                if (System.currentTimeMillis() - this.client.lastVoteKick < 1000 * 60) return;
                if (l_c == 2) {
                    if (client.room.clients.size() >= 5) {
                        PlayerConnection pc = client.room.clients.get(MFServer.noTag(MFServer.firstLetterCaps(r_c[1])));
                        if (pc != null && !pc.hidden && pc.kullanici.yetki < 5) {
                            this.client.lastVoteKick = System.currentTimeMillis();
                            client.room.votes.put(client.ip, pc.kullanici.code);
                            client.room.checkVotes();
                            client.sendMessage("<VP>Done!");
                        } else {
                            client.sendMessage("<R>User " + r_c[1] + " couldn't be found in room.");
                        }
                    } else {
                        client.sendMessage("<R>Room must have at least 5 players.");
                    }
                } else {
                    client.sendMessage("<R>/votekick username");
                }
                if (this.client.room.isMinigame && client.room.l_room != null && !client.room.l_room.developer.equals("an"))
                    return;


                break;

            case "forceskip":
                if (this.client.room.isMusic && this.client.kullanici.yetki >= 5) {
                    client.room.playNext();
                    client.room.sendMessage("<VP>Music skipped");
                }
                break;
            case "enableevent":
                if (this.client.kullanici.yetki == 10) {
                    MFServer.enableEvent = !MFServer.enableEvent;
                    client.sendChatServer("<VP>Event stat : " + String.valueOf(MFServer.enableEvent));
                }
                break;
            case "enableevent2":
                if (this.client.kullanici.yetki == 10) {
                    MFServer.enableEvent2 = !MFServer.enableEvent2;
                    client.sendChatServer("<VP>Event 2 stat : " + String.valueOf(MFServer.enableEvent2));
                }
                break;
            case "debugmode":
                if (this.client.kullanici.isim.equals("Sevenops")) {
                    //  MFServer.debug = !MFServer.debug;
                    MFServer.benchLog.set(!MFServer.benchLog.get());
                    client.sendMessage(String.valueOf(MFServer.benchLog.get()));
                }
                break;
            case "leave_tribe": {
                if (this.client.tribe == null)
                    return;
                int tcode = client.tribe.Code;
                MFServer.executor.submit(Unchecked.runnable(() -> {
                    client.kullanici.tribeCode = 0;
                    client.kullanici.tribeJoinTime = 0;
                    client.kullanici.tribeRank = 0;
                    client.kullanici.updateOne("code", "tribeRank", "tribeCode", "tribeJoinTime");

                }));

                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(tcode, 3, new Document("auteur", client.kullanici.isim).append("membreExclu", client.kullanici.isim))));

                for (PlayerConnection pc : client.tribe.clients.values())
                    pc.TRIBULLE.kabiledenAtildi(client.kullanici.isim, client.kullanici.isim, 0);

                this.client.tribe.Members.remove(client.kullanici.code);
                PlayerConnection cl = this.server.clients.get(client.kullanici.isim);
                if (cl != null) {
                    this.client.tribe.removeClient(cl);
                    cl.tribe = null;
                }
            }


            break;
            case "kill":
            case "die":
            case "mort":
                if (!client.isDead && !(this.client.room.isMinigame && !this.client.room.is801Room)) {
                    if (client.room.autoScore) {
                        client.score += 1;
                    }
                    client.isDead = true;
                    client.room.sendDeath(client);
                    client.room.checkShouldChangeCarte();
                }
                break;
            case "module":
                if (MFServer.debug) System.out.println("User : " + client.kullanici.isim + " Command: " + command);
                if (l_c == 2) {
                    boolean fun = (client.kullanici.yetki == 10 || client.kullanici.isFuncorp) && client.room.isFuncorp;
                    if (!fun && client.kullanici.yetki != 10) return;
                    String module = r_c[1];
                    if (module.equals("stop")) {
                        if (this.client.room.l_room != null
                                && (this.client.room.l_room.developer.equals(this.client.kullanici.isim) || fun || client.kullanici.yetki == 10)) {
                            this.client.room.closeLua();
                        }
                        return;
                    }
                    if (module.equals("hal_event")) return;
                    if (!((this.client.tribe != null && this.client.room.isTribeHouse
                            && this.client.tribe == this.client.room.tribe
                            && this.client.tribe.checkPermissions("can_load_map", client)) || fun || client.kullanici.yetki == 10)) {
                        return;
                    }

                    if (module.equals("stop")) {
                        if (this.client.room.l_room != null) {
                            this.client.room.closeLua();
                        }
                    } else {
                        for (Map.Entry<String, MiniOyun> entry : this.server.MINIGAMES.entrySet()) {
                            String key = entry.getKey();
                            MiniOyun value = entry.getValue();

                            if (module.startsWith(key)) {
                                this.client.room.LUAFile = value.path;
                                this.client.room.isMinigame = true;
                                this.client.room.countStats = false;
                                if (this.client.room.l_room != null) {
                                    this.client.room.closeLua();
                                }
                                this.client.room.l_room = new L_Room(this.client.room, false);
                                this.client.room.isMinigame = true;
                                LUA_THREAD t = new LUA_THREAD(this.client.room.l_room, (z) -> {

                                });
                                t.start();
                                this.server.threads.add(t);
                                for (PlayerConnection pc : this.client.room.clients.values()) {
                                    pc.LuaGame();
                                }
                                if (this.client.room.l_room != null) {
                                    this.client.room.l_room.t = t;
                                } else {
                                    this.client.sendMessage("<R>Room is not ready to run lua.");
                                }
                                break;
                            }

                        }

                    }
                }
                break;
            case "mulodrome":
                if (this.client.room.playerCount() < 2) {
                    return;
                }
                if (this.client.room.boss.equals(this.client.kullanici.isim) && !this.client.room.mulodromeStart && (this.client.room.isRacing || this.client.room.isNormal) && !client.room.password.isEmpty()) {
                    this.client.sendPacket(MFServer.getBuf().writeByte(30).writeByte(14).writeByte(1));
                    this.client.room.sendAllOthers(MFServer.getBuf().writeByte(30).writeByte(14).writeByte(0), this.client.kullanici.code);
                    this.client.room.mulodrome = new ConcurrentHashMap<>();
                    this.client.room.mulodromeStats = new int[]{0, 0};
                    this.client.room.mulodromeStart = false;
                    this.client.room.isMulodrome = true;

                }
                break;
            case "imute":
            case "mute": {
                boolean imute = f_c.equals("imute");
                if (!imute && client.kullanici.yetki < 4) {
                    return;
                }
                if (imute && client.kullanici.yetki < 2) {
                    return;
                }
                if (l_c == 1) {
                    return;
                }
                int i2 = 0;
                int saat = 1;
                ArrayList<String> isimler = new ArrayList<>();
                ArrayList<String> sebepler = new ArrayList<>();
                for (String potansiyel : r_c) {
                    if (i2 == -100) {
                        sebepler.add(potansiyel);
                        continue;
                    }
                    if (i2++ == 0) {
                        continue;
                    }
                    try
                    {
                        // checking valid integer using parseInt() method
                        Integer saatmi = Integer.parseInt(potansiyel);
                        saat = saatmi;
                        i2 = -100;
                    }
                    catch (NumberFormatException e)
                    {
                        isimler.add(MFServer.noTag(MFServer.firstLetterCaps(potansiyel)));
                    }
                }
                String reason = StringUtils.join(sebepler, " ");
                if (saat < 0 || saat > 365) {
                    client.sendMessage("<R>hour must be a number");
                    return;
                }
                String _room = "";
                String ip = "";
                for (String ad : isimler) {

                    PlayerConnection cl = this.server.clients.get(ad);
                    if (MFServer.Forbidden_names.contains(ad) && !client.kullanici.isim.equals("Sevenops")) {
                        client.sendMessage("<R>Forbidden");
                        return;
                    }
                    server.mutePlayer(ad, client, saat, reason, imute);
                 /*   String sebeb = reason;
                    String oda = _room;
                    int sure = saat;
                    String vaziyet = durum;
                    String aypi = ip;*/


             /*       MFServer.executor.execute(() -> {
                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection.prepareStatement(
                                     "INSERT INTO `log`.`mute`(`ad`,`yetkili`,`sebep`,`oda`,`surev`,`durum`,`ip`,`time`)VALUES(?,?,?,?,?,?,?,?)")) {

                            statement.setString(1, ad);
                            statement.setString(2, client.kullanici.isim);
                            statement.setString(3, sebeb);
                            statement.setString(4, oda);
                            statement.setInt(5, sure);
                            statement.setString(6, vaziyet);
                            statement.setString(7, aypi);
                            statement.setInt(8, MFServer.timestamp());
                            statement.execute();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    });*/
                    // this.server.banPlayer(name, saat, reason, client.kullanici.isim, iban);
                }
                break;
            }
            case "showTINFO":
                try {
                    client.sendMessage(String.valueOf(client.room.T_newRound == null));
                    client.sendMessage(String.valueOf(client.room.T_newRound.getDelay(TimeUnit.SECONDS)));
                    client.sendMessage(String.valueOf(client.room.T_newRound.isDone()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "darklordmode":
                if (MFServer.Forbidden_names.contains(client.kullanici.isim)) {
                    this.client.addInventoryItem(20, 5);
                    MFServer.mylord = !MFServer.mylord;
                    if (MFServer.mylord) {
                        client.sendMessage("<R>Son we are back.");
                    } else {
                        client.sendMessage("<CH>Traitor!");
                    }
                }
                break;
            case "force_event":
                if (client.kullanici.yetki == 10) {
                    client.room.forceEvent = true;
                }
                break;
            case "look":
                this.client.sendMessage(this.client.kullanici.tip);
                break;
            case "cc":
                if (this.client.kullanici.yetki >= 5 && l_c >= 3) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    PlayerConnection pc = this.server.clients.get(name);
                    if (pc != null) {
                        pc.Lang = r_c[2].toUpperCase();
                        pc.LangByte = MFServer.tersLangs.getOrDefault(pc.Lang, 0);
                        if (l_c == 3) pc.OrjLang = r_c[2].toUpperCase();
                        ;
                        pc.enterRoom(this.server.recommendRoom(pc.Lang), false);

                        client.sendChatServer(client.kullanici.isim + " used cc " + name + " to " + pc.Lang);
                    }
                }
                break;
            /*   case "jackpot":
                this.client.sendMessage(MFServer.candyCount.toString());*/
            case "mjj":
                if (l_c == 2) {
                    this.client.enterRoom(this.client.lastRoom + r_c[1], false);
                } else if (l_c == 3) {
                    this.client.enterRoom(r_c[1] + "1", true);
                }
                break;
            case "tribe": {

                Tribe tribe = null;
                if (l_c == 2) {
                    String ad = MFServer.firstLetterCaps(r_c[1]);
                    PlayerConnection pc = this.server.clients.get(ad);
                    if (pc != null) {
                        tribe = pc.tribe;
                    }
                } else {
                    tribe = this.client.tribe;
                }
                if (tribe != null) {

                    this.client.LuaGame();
                    ByteBufOutputStream p = MFServer.getStream();
                    p.writeByte(29);
                    p.writeByte(23);
                    p.writeInt(1);
                    p.writeByte(0);
                    p.writeUTF(
                            "Name : <center><font color=\"#8E050\">" + tribe.kabile.isim + "</font><br>Tribe member count : "
                                    + tribe.Members.size() + "<br>Tribe online member count : " + tribe.clients.size());
                    p.writeShort(200);
                    p.writeShort(200);
                    p.writeShort(300);
                    p.writeBoolean(false);
                    this.client.sendPacket(p.buffer());
                } else {
                    this.client.sendMessage("<R>Error.");
                }
                break;
            }
            case "time":
                if (l_c == 2 && this.client.kullanici.yetki >= 4) {
                    Integer myInt = Ints.tryParse(r_c[1]);
                    if (myInt != null) {
                        if (myInt > 0 && myInt < 30000) {
                            client.room.set_time(myInt);
                        }
                    }
                }
                break;

            case "fcr":
                if ((this.client.kullanici.yetki == 10 || client.kullanici.isFuncorp)) {
                    if (client.room.isFuncorp) {
                        client.room.isFuncorp = false;
                        client.room.sendMessage("<R>$FunCorpDesactive");
                    } else {
                        client.room.funcorpSettings = new Room.FuncorpSettings(client.room);
                        client.room.isFuncorp = true;
                        client.room.countStats = false;
                        client.room.sendMessage("<FC>$FunCorpActive");
                    }
                }
                break;
            case "fca":
                if ((this.client.kullanici.yetki == 10 || client.kullanici.isFuncorp)) {
                    if (client.room.isFuncorp) {
                        for (PlayerConnection pc : server.clients.values()) {
                            if (pc.Lang.equals(client.Lang)) {
                                pc.sendMessage(Dil.yazi(pc.Lang, "fca", client.kullanici.isim, client.room.outname));
                            }
                        }

                    }
                }
                break;
           /* case "29ekim": {
                this.client.sendMessage("<CE>Tarihimiz şahikalarla dolu kimseye kendinizi ezdirmeyin.\nÇin'e boyun eğdiren Mete bizimdir\nAvrupa'ya boyun eğdiren Atilla bizimdir\nÖzgürlük için savaşan Bilge Kağan bizimdir\nİstanbul'u alan Fatih Mehmet bizimdir\nPlevne'yi savunan Osman Paşa bizimdir\nKafkasya'da savaşan Enver Paşa bizimdir\nCumhuriyeti kuran Mustafa Kemal bizimdir\nBütün bunları bir fare oyunundan yazıyor olsam da düşüncem budur\nGeçmişinizle gurur duyun\nÖğünün, çalışın, güvenin ve son olarak YAŞA MUSTAFA KEMAL PAŞA YAŞA</CE>");
                if (!this.client.kullanici.unvanlar.contains(3129)) {
                        this.client.add_event_title(3129);
                }

                this.client.sendPacket(MFServer.pack_old(26, 12, new Object[]{"http://mp3.miceforce.com/7jxuiDKBxg4"}));
            }*/
            case "prayforamazon": {
                this.client.sendMessage("https://twitter.com/hashtag/prayforamazon");
                this.client.add_event_title(3696, "amazon");

                break;
            }
            case "the10s": {
                this.client.add_event_title(3770, "hasan");

                break;
            }
            case "funcorp": {
                if ((client.kullanici.yetki == 10 || client.kullanici.isFuncorp) && client.room.isFuncorp && l_c >= 2) {
                    client.sendChatFCLocalRoom("/"+command);
                    String com = r_c[1];
                    if (com.equals("color") && l_c == 4) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        String color = r_c[3];
                        Integer col = Integer.valueOf(color, 16);
                        if (who.equals("All")) {
                            for (PlayerConnection cl : client.room.clients.values()) {
                                client.room.funcorpSettings.miceNickColors.put(cl.kullanici.isim, col);
                                client.room.setNameColor(cl, col);
                            }
                        } else {
                            PlayerConnection pc = client.room.clients.get(who);
                            if (pc != null) {
                                client.room.funcorpSettings.miceNickColors.put(pc.kullanici.isim, col);
                                client.room.setNameColor(pc, col);
                            }
                        }
                    } else if (com.equals("tra") && l_c == 3) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        if (who.equals("All")) {
                            for (PlayerConnection cl : client.room.clients.values()) {
                                cl.giveTransformice();
                            }
                        } else {
                            PlayerConnection pc = client.room.clients.get(who);
                            if (pc != null) {
                                pc.giveTransformice();
                            }
                        }
                    } else if (com.equals("clone") && l_c == 4) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        String to = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        PlayerConnection playerConnection = client.room.clients.get(to);
                        if (playerConnection != null) {
                            if (who.equals("All")) {
                                for (PlayerConnection cl : client.room.clients.values()) {
                                    client.room.sendDeath(cl);
                                    cl.tempTip = playerConnection.kullanici.tip;
                                    client.room.respawn_player(cl);
                                }
                            } else {

                                PlayerConnection cl = client.room.clients.get(who);
                                if (cl != null) {
                                    client.room.sendDeath(cl);
                                    cl.tempTip = playerConnection.kullanici.tip;
                                    client.room.respawn_player(cl);
                                }


                            }
                        }

                    } else if (com.equals("vamp") && l_c == 3) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        if (who.equals("All")) {
                            for (String n : client.room.clients.keySet()) {
                                client.room.setVampirePlayer(n);
                            }
                        } else {
                            client.room.setVampirePlayer(who);

                        }
                    } else if (com.equals("meep") && l_c == 3) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        if (who.equals("All")) {
                            for (PlayerConnection cl : client.room.clients.values()) {
                                cl.giveMeep();
                            }
                        } else {
                            PlayerConnection pc = client.room.clients.get(who);
                            if (pc != null) {

                                pc.giveMeep();

                            }

                        }
                    } else if (com.equals("mapstroll")) {
                        client.room.mapstroll = !client.room.mapstroll;
                        if (client.room.mapstroll) {
                            client.room.sendMessage("<FC>Troll Maps has been enabled!");
                        } else {

                            client.room.sendMessage("<FC>Troll Maps has been disabled!");
                        }

                    } else if (com.equals("rev")) {
                        if (l_c == 3) {
                            String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                            if (who.equals("All")) {
                                for (PlayerConnection cl : client.room.clients.values()) {
                                    client.room.respawn_player(cl);
                                }
                            } else {
                                PlayerConnection cl = client.room.clients.get(who);
                                if (cl != null) {
                                    client.room.respawn_player(cl);
                                }

                            }
                        }
                    } else if (com.equals("kill")) {
                        if (l_c == 3) {
                            String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                            if (who.equals("All")) {
                                List<PlayerConnection> val = new ArrayList<PlayerConnection>(client.room.clients.values());
                                Collections.shuffle(val);
                                for (PlayerConnection cl : val) {
                                    cl.isDead = true;
                                    client.room.sendDeath(cl);
                                }
                            } else {
                                PlayerConnection cl = client.room.clients.get(who);
                                if (cl != null) {
                                    cl.isDead = true;
                                    client.room.sendDeath(cl);
                                }

                            }
                        } else {

                            int i = MFServer.random.nextInt(Math.max(client.room.playerCount() / 2, 1));
                            int i2 = 0;
                            for (PlayerConnection cl : client.room.clients.values()) {
                                if (i > i2++) {
                                    cl.isDead = true;
                                    client.room.sendDeath(cl);
                                    client.room.sendMessage("<R>" + cl.kullanici.isim + " is dead");
                                } else {
                                    break;
                                }
                            }
                        }

                    } else if (com.equals("character") && l_c == 4) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));

                        Integer cid = Ints.tryParse(r_c[3]);
                        boolean random = !SystemLib.CL.contains(cid);

                        if (who.equals("All")) {
                            for (PlayerConnection pc : client.room.clients_p.values()) {
                                if (pc.funcorpChar != 0) {
                                    client.room.sendAll(packRemoveImage(pc.funcorpChar));
                                }
                                int id = client.room.ImageID.incrementAndGet();
                                if (random) cid = randomFromSet(CL);
                                ByteBuf pb = packImage("foto_character_" + cid, id, 3, pc.kullanici.code, -25, -40);
                                client.room.sendAll(pb);
                                pc.funcorpChar = id;
                            }
                        } else {
                            PlayerConnection pc = client.room.clients.get(who);
                            if (pc != null) {
                                if (pc.funcorpChar != 0) {
                                    client.room.sendAll(packRemoveImage(pc.funcorpChar));
                                }
                                int id = client.room.ImageID.incrementAndGet();
                                if (random) cid = randomFromSet(CL);
                                ByteBuf pb = packImage("foto_character_" + cid, id, 3, pc.kullanici.code, -25, -40);
                                client.room.sendAll(pb);
                                pc.funcorpChar = id;
                            }


                        }
                    } else if (com.equals("size") && l_c == 4) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));

                        Integer size = Ints.tryParse(r_c[3]);
                        if (size != null && size > 0 && size < 30000) {
                            if (who.equals("All")) {
                                for (PlayerConnection cl : client.room.clients.values()) {
                                    client.room.funcorpSettings.miceSize.put(cl.kullanici.code, size);
                                    client.room.funcorpSettings.sendSize(cl.kullanici.code);
                                }
                            } else {
                                PlayerConnection pc = client.room.clients.get(who);
                                if (pc != null) {
                                    client.room.funcorpSettings.miceSize.put(pc.kullanici.code, size);
                                    client.room.funcorpSettings.sendSize(pc.kullanici.code);
                                }
                            }
                        }
                    } else if (com.equals("disco")) {
                        for (PlayerConnection cl : client.room.clients.values()) {

                            client.room.setNameColor(cl, this.server.randomRGB());
                        }
                    } else if (com.equals("chat")) {

                        for (PlayerConnection cl : client.room.clients.values()) {
                            String color = Integer.toHexString(this.server.randomRGBBright());
                            if (l_c == 3) color = r_c[2];
                            client.room.funcorpSettings.chatColors.put(MFServer.plusReady(cl.kullanici.isim), color);
                        }
                    } else if (com.equals("name") && l_c == 4) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        String who2 = MFServer.noTag(safeTag(MFServer.firstLetterCaps(r_c[3])));
                        client.room.funcorpSettings.miceNames.put(who, who2);
                    } else if (com.equals("nameall") && l_c == 3) {
                        String who = MFServer.noTag(safeTag(MFServer.firstLetterCaps(r_c[2])));
                        for (PlayerConnection pc : this.client.room.clients.values()) {
                            client.room.funcorpSettings.miceNames.put(pc.kullanici.isim, who);
                        }
                    } else if (com.equals("linkmice")) {
                        if (l_c == 3) {
                            String komut = r_c[2];
                            if (komut.equals("*")) {
                                PlayerConnection[] pls = this.client.room.clients.values().toArray(new PlayerConnection[this.client.room.clients.size()]);
                                for (int i = 0; i < pls.length; i++) {
                                    ByteBuf buf = MFServer.getBuf().writeByte(5).writeByte(48).writeByte(1).writeInt(pls[i].kullanici.code);
                                    if (pls.length > i + 1) buf.writeInt(pls[i + 1].kullanici.code);
                                    this.client.room.sendAll(buf);
                                    i++;
                                }
                            } else if (komut.equals("off")) {
                                for (PlayerConnection pc : this.client.room.clients.values()) {
                                    pc.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(48).writeByte(0).writeInt(pc.kullanici.code));
                                }
                            }
                        } else if (l_c == 4) {
                            PlayerConnection pc1 = this.client.room.clients.get(MFServer.noTag(MFServer.firstLetterCaps(r_c[2])));
                            PlayerConnection pc2 = this.client.room.clients.get(MFServer.noTag(MFServer.firstLetterCaps(r_c[3])));
                            if (pc1 != null && pc2 != null) {
                                this.client.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(48).writeByte(1)
                                        .writeInt(pc1.kullanici.code).writeInt(pc2.kullanici.code));
                            }
                        }

                    } else if (com.equals("move")) {
                        if (l_c >= 3) {

                            String room_name = StringUtils.split(StringUtils.split(command, " ", 2)[1], " ", 2)[1];
                            Room rm=server.rooms.get(room_name);
                            if (rm != null&&rm.isMinigame&&rm.LuaKey!= null&&rm.LuaKey.equals("amongus")){
                                client.sendMessage("<R>It's among us so...");
                                return;
                            }
                            client.sendChatServer(client.kullanici.isim + " used move in funcorp from " + client.room.name + " to " + room_name);
                            MFServer.executor.execute(() -> {
                                Collection<PlayerConnection> collection = new ArrayList<>(client.room.clients.values());
                                for (PlayerConnection playerConnection : collection) {
                                    try {
                                        playerConnection.enterRoom(room_name, false);
                                        Thread.sleep(5);
                                    } catch (Exception e) {
                                        MFServer.log.warning(MFServer.stackTraceToString(e));
                                    }
                                }
                            });
                        }
                    } else if (com.equals("skip")) {
                        client.room.playNextMp3();
                    } else if (com.equals("queue")) {
                        if (l_c == 3) {
                            String url = r_c[2];
                            Matcher matcher = MFServer.patternYoutube.matcher(url);
                            if (matcher.find()) {
                                String id = matcher.group();
                                MFServer.executor.execute(() -> {
                                    try {
                                        String newurl = "https://www.googleapis.com/youtube/v3/videos?id=" + id + "&key=AIzaSyBaasC_VPn1f4fycdmLuCrGZuq2WTVCLyI&part=contentDetails,snippet";
                                        final HttpParams httpParams = new BasicHttpParams();
                                        HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
                                        DefaultHttpClient cl = new DefaultHttpClient(httpParams);
                                        HttpUriRequest request = new HttpGet(newurl);
                                        HttpResponse response = cl.execute(request);

                                        String body = CharStreams.toString(new InputStreamReader(
                                                response.getEntity().getContent(), Charsets.UTF_8));
                                        JSONObject obj = new JSONObject(body);
                                        JSONArray jsonArray = obj.getJSONArray("items");
                                        if (jsonArray.length() == 0) {

                                            try {
                                                client.sendMessage("$ModeMusic_ErreurVideo");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            return;
                                        }
                                        String title = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("title");
                                        String d = jsonArray.getJSONObject(0).getJSONObject("contentDetails").getString("duration");
                                        Duration duration = Duration.parse(d);
                                        int sec = (int) duration.getSeconds();
                                        if (sec > 300) sec = 300;
                                        for (Iterator<Room.MusicVideo> iterator = client.room.musics.iterator(); iterator.hasNext(); ) {
                                            Room.MusicVideo music = iterator.next();
                                            if (music.vid.equals(id)) {
                                                try {
                                                    client.sendMessage("$DejaPlaylist");
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                return;
                                            }
                                            if (music.kullanici.equals(client.kullanici.isim)) {

                                                iterator.remove();
                                                break;
                                            }
                                        }

                                        client.room.musics.add(new Room.MusicVideo(client.kullanici.isim, id, sec, title));

                                        try {
                                            client.sendMessage("$ModeMusic_AjoutVideo", "<PT>" + client.room.musics.size());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        if (client.room.musics.size() == 1 && client.room.currentVideo == null) {
                                            client.room.playNextMp3();
                                        }
                                    } catch (Exception e) {
                                        MFServer.log.warning(MFServer.stackTraceToString(e));
                                    }
                                });
                            }
                        }
                    } else if (com.equals("reset")) {

                        if (l_c == 2) {
                            client.room.funcorpSettings.chatColors.clear();
                            client.room.funcorpSettings.miceNames.clear();
                            client.room.funcorpSettings.miceNickColors.clear();
                            client.room.funcorpSettings.miceSize.clear();
                        } else if (l_c == 3) {

                            String toreset = r_c[2];
                            switch (toreset) {
                                case "size":
                                    client.room.funcorpSettings.miceSize.clear();
                                    break;
                                case "chat":
                                    client.room.funcorpSettings.chatColors.clear();
                                    break;
                                case "name":
                                    client.room.funcorpSettings.miceNames.clear();
                                    break;
                                case "color":
                                    client.room.funcorpSettings.miceNickColors.clear();
                                    break;

                            }
                        }

                    } else if (com.equals("kick") && l_c == 3) {
                        String who = MFServer.noTag(MFServer.firstLetterCaps(r_c[2]));
                        PlayerConnection pc = client.room.clients.get(who);
                        if (pc!=null){
                            pc.enterRoom(this.server.recommendRoom(pc.Lang), false);
                        }
                        client.room.blockedPlayers.add(who);
                    }
                } else {
                    ArrayList<String> txt = new ArrayList<>();
                    HashMap<String, ArrayList<String>> stat = new HashMap<>();
                    for (PlayerConnection pc : this.server.clients.values()) {
                        if (pc.kullanici.isFuncorp && !pc.topsecret && !pc.hidden) {
                            stat.computeIfAbsent(pc.OrjLang, k -> new ArrayList<>()).add(pc.kullanici.isim);
                        }
                    }
                    if (stat.size() == 0) {
                        txt.add("There aren't any funcorp member online");
                    } else {
                        txt.add("Online funcorp members:");
                    }
                    for (Map.Entry<String, ArrayList<String>> entry : stat.entrySet()) {
                        txt.add("<BL>[" + entry.getKey().toLowerCase() + "] <BV>"
                                + StringUtils.join(entry.getValue(), "<BL>, <BV>"));
                    }
                    String text = StringUtils.join(txt, "\n");
                    client.sendMessage(text);
                    break;
                }
            }
            break;
            case "inv":
                if (l_c != 2) {
                    return;
                }
                if (this.client.room.isTribeHouse && this.client.room.tribe == this.client.tribe
                        && this.client.tribe.checkPermissions("can_invite", this.client)) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));

                    PlayerConnection pc = server.clients.get(name);
                    if (pc != null && !pc.kullanici.engellenenler.contains(client.kullanici.code)) {
                        if (!pc.inv.containsKey(client.kullanici.isim)) {
                            pc.inv.put(client.kullanici.isim, this.client.tribe.kabile.isim);
                            ByteBufOutputStream bs = MFServer.getStream();
                            bs.writeByte(16);
                            bs.writeByte(2);
                            bs.writeUTF(this.client.kullanici.isim);
                            bs.writeUTF(this.client.tribe.kabile.isim);
                            pc.sendPacket(bs.buffer());
                            this.client.sendMessage("$InvTribu_InvitationEnvoyee", pc.kullanici.isim);
                        }

                    } else {
                        this.client.sendMessage("<R>$chat.message.joueurNonConnecte");
                    }
                }
                break;
            case "ilerle":
                if (client.kullanici.yetki == 10 && l_c == 3) {
                    client.gorevIlerleme(Integer.valueOf(r_c[1]), Integer.valueOf(r_c[2]));
                    //  System.out.println(r_c[1]+" "+r_c[2]);
                }
                break;
            case "xp":
                if (client.kullanici.yetki == 10 && l_c == 4) {
                    client.giveExp(Integer.valueOf(r_c[1]), Boolean.valueOf(r_c[2]), Integer.valueOf(r_c[3]));
                    //  System.out.println(r_c[1]+" "+r_c[2]);
                }
                break;
            case "roomlimit":
                if ((this.client.kullanici.yetki >= 5 || (this.client.kullanici.isFuncorp && this.client.room.isFuncorp)) && l_c == 2 && !this.client.room.isofficial) {
                    Integer max = Ints.tryParse(r_c[1]);
                    if (max == null) {
                        this.client.sendMessage("<R>Excuse me ?");
                        return;
                    }
                    if (max < 2) {
                        max = 100;
                    }
                    this.client.room.maxPlayers = max;
                }
                break;
            case "invkick":
                if (l_c != 2) {
                    return;
                }
                if (this.client.room.isTribeHouse && this.client.room.tribe == this.client.tribe
                        && this.client.tribe.checkPermissions("can_invite", this.client)) {
                    String name = MFServer.firstLetterCaps(r_c[1]);
                    PlayerConnection pc = server.clients.get(name);
                    if (pc != null && pc.room == this.client.room && pc.tribe != this.client.tribe) {
                        pc.room.sendMessage("<R>" + client.kullanici.isim + " kicked " + name);
                        pc.enterRoom("1", false);
                    }
                }
                break;
            case "carpan":
                if (client.kullanici.yetki == 10 && l_c == 2) {

                    Integer num = Integer.parseInt(r_c[1]);
                    MFServer.genel_carpan = num;
                    client.sendMessage("Carpan now: " + num);
                    try {
                        ByteBuf data = pack_modchat(1, "Tengri", "For next few seconds multiplier is " + MFServer.genel_carpan + ". It means now you can earn " + MFServer.genel_carpan + "x on everything.");
                        for (PlayerConnection pc : this.server.clients.values()) {
                            pc.sendPacket(data.retainedSlice());
                        }
                        data.release();
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }

                }
                break;
            case "fashion":
                if ((client.kullanici.yetki == 10 || client.kullanici.isFashion) && l_c >= 2) {
                    if ("show".equals(r_c[1])) {

                        for (Map.Entry<Integer, String[]> entry : MFServer.fashion.entrySet()) {
                            String[] data = entry.getValue();
                            client.sendMessage("#" + entry.getKey() + " (" + data[0] + ") : " + data[1]);
                        }
                    }
                    if (l_c == 3) {
                        if (r_c[1].equals("add")) {
                            if (MFServer.fashion.size() > 10) {
                                client.sendMessage("<R>POR FAVOR");
                                return;
                            }
                            int rand = MFServer.random.nextInt(100);
                            while (MFServer.fashion.containsKey(rand)) {
                                rand = MFServer.random.nextInt(100);
                            }
                            MFServer.fashion.put(rand + 100, new String[]{client.kullanici.isim, r_c[2]});
                            client.sendMessage("<VP>done");
                        } else if (r_c[1].equals("del")) {
                            Integer num = Integer.parseInt(r_c[2]);
                            MFServer.fashion.remove(num);
                            client.sendMessage("<VP>done");
                        }
                    }
                }
                break;
            case "ninja":
            case "join": {
                if (l_c == 1 || this.client.kullanici.yetki < 4) {
                    return;
                }
                boolean ninja = f_c.equals("ninja");
                String name = StringUtils.split(command, " ", 2)[1];
                PlayerConnection cl = this.server.clients.get(MFServer.noTag(MFServer.firstLetterCaps(name)));
                if (ninja) {
                    client.hidden = true;

                    client.room.sendDeath(client);
                }
                if (cl == null || cl.room == null) {
                    this.client.enterRoom(name, false);
                    ByteBuf data = new O_Death(this.client).data();
                    client.sendPacket(data);
                } else {
                    if (ninja && cl.room.isMinigame) client.sendMessage("<R>Target room contains minigame script.");
                    this.client.enterRoom(cl.room.name, false);
                }
                if (ninja) {
                    client.watch(name);
                }
                //client.sendChatServer(client.kullanici.isim + " used " + f_c + " " + name);
                break;
            }
            case "sauvertotem":
                if (client.room.isTotemEditeur) {
                    if (client.kullanici.kurtarma >= 500) {
                        client.kullanici.totem = client.totem2;
                        client.enterRoom("1", false);
                    }
                }
                break;
            case "resettotem":
                if (client.room.isTotemEditeur) {
                    this.client.totem2 = new Totem();
                    this.client.room.NewRound();
                }
                break;
//pack_modchat(1, this.kullanici.isim, message);
            case "muteips":
                if (client.kullanici.yetki > 4 && l_c == 2) {
                    MFServer.mutedIPS.add(MFServer.firstLetterCaps(r_c[1]));
                }
                break;
            case "totem":
                if (client.kullanici.kurtarma >= 500) {
                    if (client.kullanici.totem == null) client.kullanici.totem = new Totem();
                    client.totem2 = client.kullanici.totem.clone();
                    client.enterRoom(Room.s_totem + client.kullanici.isim, false);
                }
                break;
            case "ems":
                if (client.kullanici.yetki == 10) {
                    int mins = 30;
                    if (l_c == 2) {
                        try {
                            mins = Integer.valueOf(r_c[1]);
                        } catch (Exception e) {

                        }
                    }
                    for (PlayerConnection cl : client.server.clients.values()) {
                        ByteBuf data = pack_modchat(1, client.kullanici.isim, Dil.yazi(cl.Lang, "event_ads", mins));
                        cl.sendPacket(data);
                    }
                }
                break;
            case "onlyme":
                client.onlyme = !client.onlyme;
                client.sendMessage("<VP>Status: " + client.onlyme);
                break;
            case "an":
                if (l_c >= 2 && client.kullanici.yetki >= 9) {
                    String message = StringUtils.split(command, " ", 2)[1];
                    SqlData sd = new SqlData();
                    sd.typ = 5;
                    sd.type = 139;
                    sd.name = client.kullanici.isim;
                    sd.message = message;
                    server.CLD.add(sd);
                    if (client.check_message_staff(message)) {
                        return;
                    }
                    //      int id=MFServer.random.nextInt(100000);
                    for (PlayerConnection cl : client.server.clients.values()) {
                        if (!cl.kullanici.disableOnScreen) {
                            cl.sendPacket(L_Room.packAddTextArea(-7071, "<a href='event:mf_showAnnouncement'>\n\n\n\n\n", 10, 30, 20, 40, 0, 0x000000, 0, false));

                            cl.sendPacket(L_Room.packImage("announcement_notification.19423", -7070, 7, -998, 0, 30));
                        }
                    }
                    MFServer.announcement = "<R>" + client.kullanici.isim + " <BL>said:\n<N>" + message;
                    client.sendChatServer(client.kullanici.isim + " used an " + message);
                }
                break;
            case "deneme": {
                if (client.kullanici.yetki >= 10) {
                /*    ByteBufOutputStream bf = MFServer.getStream();
                    bf.writeByte(20);
                    bf.writeByte(3);
                    bf.writeBoolean(true);
                    bf.writeBoolean(true);
                    bf.writeInt(0);
                    bf.writeBoolean(true);
                    bf.writeInt(1598814542);
                    bf.writeByte(0);
                    client.sendPacket(bf);*/

                }
                break;
            }
            case "survey": {
                if (client.kullanici.yetki >= 10 && client.currentAnket != null) {
                    ByteBufOutputStream bf = MFServer.getStream();
                    bf.writeByte(26);
                    bf.writeByte(16);

                    bf.writeInt(1);//1 ise yayınla özelliği var, 2 ise oy verr
                    bf.writeUTF("");
                    bf.writeBoolean(false);//Cumhurbaşkanından gelip gelmediği
                    bf.writeUTF(client.currentAnket);
                    for (String soru : client.currentAnketSorular) {
                        bf.writeUTF(soru);
                    }
                    client.sendPacket(bf);
                }
                break;
            }
            case "survey2": {
                if (client.kullanici.yetki >= 10 && client.currentAnket != null) {
                    ByteBufOutputStream bf = MFServer.getStream();
                    bf.writeByte(26);
                    bf.writeByte(16);

                    bf.writeInt(client.currentAnketId);//1 ise yayınla özelliği var, 2 ise oy verr
                    bf.writeUTF("");
                    bf.writeBoolean(false);//Cumhurbaşkanından gelip gelmediği
                    bf.writeUTF(client.currentAnket);
                    for (String soru : client.currentAnketSorular) {
                        bf.writeUTF(soru);
                    }
                    client.sendPacket(bf);
                }
                break;
            }
            case "survey3": {
                ByteBufOutputStream bf = MFServer.getStream();
                bf.writeByte(26);
                bf.writeByte(17);
                bf.writeByte(0);
                client.sendPacket(bf);
                break;
            }
            case "survey_prize": {
                if (client.kullanici.yetki >= 10 && client.currentAnket != null) {
                    boolean correct = false;
                    if (l_c >= 4) {
                        try {
                            Integer cevap_id = Integer.parseInt(r_c[1]);
                            String type = r_c[2];
                            int num = Integer.parseInt(r_c[3]);
                            var cevaplar = Anket.get(client.currentAnketId);
                            if (type.equals("badge")) {
                                correct = true;
                                cevaplar.forEach((k, v) -> {
                                    if (v.equals(cevap_id)) {
                                        PlayerConnection pc = server.clients2.get(k);
                                        if (pc != null) {
                                            pc.kullanici.rozetler.add(num);
                                            try {
                                                pc.sendMessage("<CP>You gave the right answer and you won the prize");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else if (type.equals("title")) {
                                correct = true;
                                cevaplar.forEach((k, v) -> {
                                    if (v.equals(cevap_id)) {
                                        PlayerConnection pc = server.clients2.get(k);
                                        if (pc != null) {
                                            try {
                                                pc.sendMessage("<CP>You gave the right answer and you won the prize");
                                                pc.add_event_title(num, "survey");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else if (type.equals("consumable")) {
                                int num2 = Integer.parseInt(r_c[4]);
                                if (num2 <= 100) {
                                    correct = true;
                                    cevaplar.forEach((k, v) -> {
                                        if (v.equals(cevap_id)) {
                                            PlayerConnection pc = server.clients2.get(k);
                                            if (pc != null) {
                                                try {
                                                    pc.sendMessage("<CP>You gave the right answer and you won the prize");
                                                    pc.addInventoryItem(num, num2);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                }

                            }
                            if (correct) {
                                server.clients.forEach((k, v) -> {
                                    try {
                                        v.sendPacket(MFServer.pack_message("<CP>Correct answer is: " + MFServer.AnketCevaplar.get(client.currentAnketId).get(cevap_id)));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                });
                            }
                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }
                    if (!correct) {
                        client.sendMessage("Correct Examples:\n<cp>/survey_prize 1 badge 1000\n<cp>/survey_prize 0 title 3508\n<cp>/survey_prize 0 consumable 2605 77");
                    }

                }
                break;
            }
            case "smc":
                if (l_c >= 2 && client.kullanici.yetki >= 9) {
                    String message = StringUtils.split(command, " ", 2)[1];
                    SqlData sd = new SqlData();
                    sd.typ = 5;
                    sd.type = 77;
                    sd.name = client.kullanici.isim;
                    sd.message = message;
                    server.CLD.add(sd);
                    if (client.check_message_staff(message)) {
                        return;
                    }
                    for (PlayerConnection cl : client.server.clients.values()) {
                        if (cl.Lang.equals(client.Lang) || cl.countryCode.equals(client.Lang)) {
                            cl.sendMessage("<VP>~ [" + client.Lang + "] " + message);
                        }
                    }
                    client.sendChatServer(client.kullanici.isim + " used smc " + message);
                }
                break;
            case "fms":
                if (l_c >= 2 && (client.kullanici.yetki == 10 || client.kullanici.isFuncorp) && client.room.isFuncorp) {

                    String message = StringUtils.split(command, " ", 2)[1];
                    SqlData sd = new SqlData();
                    sd.typ = 5;
                    sd.type = 80;
                    sd.name = client.kullanici.isim;
                    sd.message = message;
                    server.CLD.add(sd);
                    if (client.check_message_staff(message)) {
                        return;
                    }
                    for (PlayerConnection cl : client.room.clients.values()) {
                        cl.sendMessage("<FC>~ [" + client.kullanici.isim + "] " + message);

                    }
                    // client.sendChatServer(client.kullanici.isim + " used fms " + message);
                }
                break;

            case "deltitle":
                if (l_c == 2) {
                    Integer tit = Ints.tryParse(r_c[1]);
                    if (tit != null) {
                        if (client.kullanici.evInfo.titles.containsKey(r_c[1])) {
                            client.kullanici.evInfo.titles.remove(r_c[1]);
                            client.kullanici.unvanlar.remove(tit);
                            client.sendMessage("Title has been removed.");
                        }
                    }

                }
                break;
            case "titles":
                client.nextTitle("defilante");
                client.nextTitle("first");
                client.nextTitle("cheese");
                client.nextTitle("bootcamp");
                break;
            case "titre":
            case "title":
                if (l_c == 1) {
                    ByteBuf buf = this.client.getBuf();
                    Map<Integer, Integer> yildizli = new HashMap<>();
                    ArrayList<Integer> yildizsiz = new ArrayList<>();
                    buf.writeByte(8);
                    buf.writeByte(14);
                    this.client.kullanici.unvanlar.forEach((Integer t) -> {
                        Integer y = client.kullanici.yildizlar.get(String.valueOf(t));
                        if (y == null) {
                            yildizsiz.add(t);
                        } else {
                            yildizli.put(t, y);
                        }
                    });
                    buf.writeShort(yildizsiz.size());
                    for (Integer t : yildizsiz) {
                        buf.writeShort(t);
                    }
                    buf.writeShort(yildizli.size());
                    yildizli.forEach((k, v) -> {
                        buf.writeShort(k);
                        buf.writeByte(v + 1);
                    });
                    this.client.sendPacket(buf);
                } else if (l_c == 2) {
                    Integer tit = Ints.tryParse(r_c[1]);
                    if (tit != null) {
                        if (this.client.kullanici.unvanlar.contains(tit)) {
                            this.client.kullanici.unvan = tit;
                            this.client.sendPacket(MFServer.getBuf().writeByte(100).writeByte(72)
                                    .writeByte(client.kullanici.yildizlar.getOrDefault(String.valueOf(tit), 0) + 1).writeShort(tit));

                        }
                    }
                }
                break;
            case "find":
            case "ip":
                if (client.kullanici.yetki < 4) {
                    return;
                }
                this.client.sendChatServer(this.client.kullanici.isim + " used /" + command);
                if (l_c == 2) {
                    String name = r_c[1].toLowerCase();
                    if (name.length() < 3) {
                        return;
                    }
                    int loop = 0;
                    if (name.contains(".")) {
                        for (PlayerConnection cl : this.server.clients.values()) {
                            if (MFServer.removeIfNull(cl)) continue;
                            if (cl.ip.contains(name)) {
                                String t = cl.kullanici.isim + " : " + cl.room.name + " (" + cl.ip + ")";
                                if (!cl.room.password.isEmpty()) t = "<R>" + t;
                                this.client.sendMessage(t);
                                if (loop++ == 15) {
                                    break;
                                }
                            }
                        }
                    } else {
                        name = MFServer.noTag(name);
                        for (PlayerConnection cl : this.server.clients.values()) {
                            if (MFServer.removeIfNull(cl)) continue;
                            if (cl.kullanici.isim.toLowerCase().contains(name)) {
                                String t = cl.kullanici.isim + " : " + cl.room.name + " (" + cl.ip + ")";
                                if (!cl.room.password.isEmpty()) t = "<R>" + t;
                                this.client.sendMessage(t);
                                if (loop++ == 15) {
                                    break;
                                }
                            }
                        }

                    }
                }
                break;
            case "logcon":
                if (l_c == 2 && client.kullanici.yetki >= 4) {
                    this.client.sendChatServer(this.client.kullanici.isim + " used /" + command);
                    MFServer.executor.execute(() -> {
                        ArrayList<String> txt = new ArrayList<>();
                        txt.add("<R> CONNECTION LOG<VP> (o.O)</R> :");
                        String name = MFServer.noTag(r_c[1]);
                        PreparedStatement statement = null;
                        try (Connection connection = MFServer.DS.getConnection()) {

                            ResultSet result;
                            if (name.contains(".")) {
                                statement = connection.prepareStatement(
                                        "SELECT ip,  name,time_stamp,city,country FROM log.login where ip =? order by time_stamp desc LIMIT 200");
                                statement.setString(1, name);
                            } else {
                                statement = connection.prepareStatement(
                                        "SELECT ip,  name,time_stamp,city,country FROM log.login where name =? order by time_stamp desc LIMIT 200");
                                statement.setString(1, MFServer.firstLetterCaps(name));
                            }
                            result = statement.executeQuery();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            TimeZone tz = TimeZone.getTimeZone(this.client.tz);
                            simpleDateFormat.setTimeZone(tz);
                            while (result.next()) {

                                int a = result.getInt(3);
                                //"<V>[NAME] </V> TS <N2>(IP - COUNTRY/CITY)</N2>";
                                txt.add("<V>[" + result.getString(2) + "]</V> " + simpleDateFormat.format(new Date((long) a * 1000)) + " <N2>(" + result.getString(1) + " - "
                                        + result.getString("city") + " / " + result.getString("country") + ")</N'>");
                            }
                            this.client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (statement != null) {
                                try {
                                    statement.close();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
                break;
            case "logip":
                if (l_c == 2 && client.kullanici.yetki >= 4) {
                    this.client.sendChatServer(this.client.kullanici.isim + " used /" + command);
                    MFServer.executor.execute(() -> {
                        ArrayList<String> txt = new ArrayList<>();
                        txt.add("<R> IP LOG<VP> (o.O)</R> :");
                        String name = MFServer.noTag(r_c[1]);
                        PreparedStatement statement = null;
                        try (Connection connection = MFServer.DS.getConnection();) {
                            boolean checkBan = false;
                            ResultSet result;
                            if (name.contains(".")) {
                                checkBan = true;
                                statement = connection.prepareStatement(
                                        "SELECT ip, MAX(`name`) as name,MAX(`time_stamp`) as ts,city,country FROM log.login where ip =? GROUP BY name order by ts desc LIMIT 30");
                                statement.setString(1, name);
                            } else {
                                statement = connection.prepareStatement(
                                        "SELECT MAX(`ip`) as ip,  name,MAX(`time_stamp`) as ts,city,country FROM log.login where name =? GROUP BY ip order by ts desc LIMIT 30");
                                statement.setString(1, MFServer.firstLetterCaps(name));
                            }
                            result = statement.executeQuery();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            TimeZone tz = TimeZone.getTimeZone(this.client.tz);
                            simpleDateFormat.setTimeZone(tz);
                            while (result.next()) {
                                String ad = result.getString(2);
                                int a = result.getInt(3);
                                if (checkBan) {
                                    Kullanici kul = MFServer.colKullanici.find(eq("isim", ad)).projection(Projections.include("ban")).first();
                                    if (kul != null) {
                                        if (kul.ban != null && ((kul.ban.saat * 3600) + kul.ban.when >= MFServer.timestamp() || kul.ban.saat >= 1800)) {
                                            ad = "<R>" + ad + "</R>";
                                        }
                                    }
                                }
                                String tx = "<BL>@<V>" + result.getString(1) + " <BL>: " + ad + " ("
                                        + simpleDateFormat.format(new Date((long) a * 1000)) + ") " + result.getString("city") + " / " + result.getString("country");
                                if (PlayerConnection.isVPN(result.getString(1), true)) {
                                    tx += "<R>VPN</R>";
                                }
                                txt.add(tx);
                            }
                            this.client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (statement != null) {
                                try {
                                    statement.close();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
                break;
            case "selfie":
                if (MFServer.Forbidden_names.contains(client.kullanici.isim)) {
                    client.playEmote(client.kullanici.code, 12, false);
                }
                break;

            case "unbanip":
                if (l_c == 2 && this.client.kullanici.yetki >= 9) {
                    String rc = r_c[1];
                    MFServer.executor.execute(() -> {
                        MFServer.IP_BANS.remove(rc);

                        MFServer.IP_STATS.remove(rc);
                        DeleteResult deleteResult = MFServer.colIpBan.deleteOne(eq("_id", rc));
                        if (deleteResult.getDeletedCount() > 0) {
                            try {
                                client.sendChatServer(client.kullanici.isim + " unbanned ip " + rc);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
                break;

            case "ipban":
                if (this.client.kullanici.yetki >= 5) {

                    String addr = null;

                    ArrayList<String> isimler = new ArrayList<>();
                    int i2 = 0;
                    for (String potansiyel : r_c) {
                        if (i2++ == 0) {
                            continue;
                        }
                        Integer saatmi = Ints.tryParse(potansiyel);
                        if (saatmi == null) {
                            isimler.add(MFServer.noTag(MFServer.firstLetterCaps(potansiyel)));
                            //isimSayisi++;
                        } else {
                            break;
                        }
                    }
                    for (String rc : isimler) {
                        if (rc.contains(".")) {
                            addr = rc;
                            for (PlayerConnection cl : this.server.clients.values()) {
                                if (cl.ip.equals(addr) && MFServer.Forbidden_names.contains(cl.kullanici.isim)) {
                                    return;
                                }
                            }
                        } else {
                            for (PlayerConnection cl : this.server.clients.values()) {
                                if (cl.kullanici.isim.equals(rc) && !MFServer.Forbidden_names.contains(cl.kullanici.isim)) {
                                    addr = cl.ip;
                                }
                            }
                        }
                        int h = 1800;
                        if (client.kullanici.yetki < 9) {
                            h = 1440;
                        }
                        if (addr != null) {
                            boolean banned = false;
                            for (PlayerConnection cl : this.server.clients.values()) {
                                if (cl.ip.equals(addr)) {
                                    this.server.banPlayer(cl.kullanici.isim, h, "IP BAN", client.kullanici.isim, false, null);
                                    banned = true;
                                    client.sendChatServer(client.kullanici.isim + " ip banned " + cl.kullanici.isim);
                                    MFServer.ipBan(addr, h / 2 / 10);
                                }
                            }
                            if (!banned) {

                                client.sendChatServer(client.kullanici.isim + " ip banned " + addr);
                                MFServer.ipBan(addr, h / 2 / 10);
                            }
                        }
                    }


                }
                break;
            case "ls":
                if (client.kullanici.yetki >= 5) {
                    final String ara;
                    if (l_c == 2) {
                        ara = r_c[1];
                    } else {
                        ara = "";
                    }
                    int[] total = new int[]{0};
                    ArrayList<String> txt = new ArrayList<>();
                    server.rooms.forEach((key, oda) -> {
                        if (ara.isEmpty() || key.contains(ara)) {
                            int top = oda.playerCount();
                            String hid = "";
                            if (!oda.password.isEmpty()) hid = "<R>Room is locked</R>";
                            txt.add("<G>" + key + "</G><N2> (" + oda.community + ") : </N2>" + "<PT>" + top + "</PT>" + hid);
                            total[0] += top;
                        }
                    });
                    txt.add("\n<J>TOTAL PLAYERS : <R>" + total[0]);
                    client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));
                }
                break;
            case "clearipban":
                if (this.client.kullanici.yetki == 10) {
                    MFServer.executor.execute(() -> MFServer.colIpBan.deleteMany(new Document()));
                    client.sendChatServer(client.kullanici.isim + " cleared ipban list");
                    MFServer.IP_STATS.clear();
                    ;
                }
                break;
            case "ch":
                boolean can_use = false;
                if (this.client.kullanici.yetki >= 4) {
                    can_use = true;
                } else if (this.client.tribe != null && this.client.room.isTribeHouse
                        && this.client.tribe == this.client.room.tribe
                        && this.client.tribe.checkPermissions("can_load_map", client)) {
                    can_use = true;
                }

                if (can_use) {
                    if (l_c == 2) {

                        String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                        PlayerConnection pc = this.client.room.clients.get(name);
                        if (pc != null) {
                            this.client.room.forceNextShaman = pc.kullanici.code;
                            this.client.sendMessage("$ProchaineChamane", name);
                        } else {

                            this.client.sendMessage("$PasProchaineChamane", name);
                        }
                    }
                }
                break;
            case "closeroom":
                if (this.client.kullanici.yetki >= 5 && l_c >= 2) {
                    String r_name = StringUtils.split(command, " ", 2)[1];
                    if (r_name.length() > 2 && r_name.charAt(2) != Room.tire && !r_name.startsWith("*")) {
                        r_name = client.Lang + "-" + r_name;
                    }

                    Room room = server.rooms.remove(r_name);

                    if (room != null) {

                        for (PlayerConnection pc : room.clients.values()) {
                            if (pc.kullanici != null && pc.room == room) {
                                try {
                                    pc.enterRoom("*village" + MFServer.getRandom(5), false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        client.sendMessage("<VP>Done.");
                    }

                }
                break;
            case "rare":
                if (this.client.kullanici.unvanlar.size() > 100) {
                    client.kullanici.rareTitles = !client.kullanici.rareTitles;
                    if (client.kullanici.rareTitles) {
                        client.sendMessage("Enabled");
                    } else {

                        client.sendMessage("Disabled");
                    }
                }
            case "lsroom":
                if (this.client.kullanici.yetki >= 5 && l_c >= 2) {
                    String r_name = StringUtils.split(command, " ", 2)[1];
                    if (r_name.length() > 2 && r_name.charAt(2) != Room.tire && !r_name.startsWith("*")) {
                        r_name = client.Lang + "-" + r_name;
                    }

                    Room room = server.rooms.get(r_name);
                    if (room != null) {
                        ArrayList<String> txt = new ArrayList<>();
                        for (PlayerConnection pc : room.clients.values()) {
                            txt.add(pc.kullanici.isim + " : " + pc.ip + "(" + pc.isp + ")[" + pc.country + "] afk: " + client.isAfk);
                        }
                        this.client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));
                    } else {
                        client.sendMessage("<R>I couldn't find the room.");
                    }

                }
                break;
            case "lsmap":
            case "mymaps":
                if (this.client.kullanici.yetki >= 1) {
                    MFServer.executor.execute(() -> {
                        String arastir = client.kullanici.isim;
                        if (f_c.equals("lsmap") && (this.client.kullanici.yetki >= 5 || this.client.kullanici.isMapcrew) && l_c == 2) {
                            arastir = MFServer.plusReady(r_c[1]);
                        }
                        arastir = MFServer.firstLetterCaps(arastir);
                        ArrayList<String> txt = new ArrayList<>();
                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection
                                     .prepareStatement("SELECT id,perm FROM maps where name = ? limit 500");) {

                            ResultSet result;
                            statement.setString(1, arastir);

                            result = statement.executeQuery();
                            while (result.next()) {
                                txt.add("@" + result.getInt(1) + " - P" + result.getInt(2));
                            }
                            this.client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
                break;
            case "lsperm":
                if ((this.client.kullanici.yetki >= 5 || this.client.kullanici.isMapcrew) && l_c == 2) {
                    String perm = MFServer.firstLetterCaps(r_c[1]);
                    MFServer.executor.execute(() -> {
                        ArrayList<String> txt = new ArrayList<>();
                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection
                                     .prepareStatement("SELECT id,name FROM maps where perm = ? order by id desc limit 500 ");) {

                            ResultSet result;
                            statement.setString(1, perm);

                            result = statement.executeQuery();
                            while (result.next()) {
                                txt.add("@" + result.getInt(1) + " - " + result.getString(2));
                            }
                            client.sendPacket(L_Room.packLog(StringUtils.join(txt, "\n")));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
                break;
            case "records": {
                ArrayList<String> rekorlars = new ArrayList<>();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                TimeZone tz = TimeZone.getTimeZone(this.client.tz);
                simpleDateFormat.setTimeZone(tz);
                MFServer.executor.execute(() -> {
                    String name = this.client.kullanici.isim;
                    boolean lookupMaps = false;
                    Integer mapmi = null;
                    if (l_c == 2 && this.client.kullanici.yetki >= 4) {
                        name = MFServer.firstLetterCaps(r_c[1]);
                        if (r_c[1].startsWith("@")) {
                            mapmi = Ints.tryParse(r_c[1].replace("@", ""));
                            if (mapmi != null) {
                                lookupMaps = true;
                            }
                        }
                    }
                    if (lookupMaps) {
                        for (Rekorlar rekorlar : MFServer.colRekorlar.find(new Document("_id", mapmi))) {
                            int i = 0;
                            for (Rekor rekor : rekorlar.rekorlar) {
                                if (rekor.deadline == 0) {
                                    rekorlars.add("(" + rekor.hid + ")[" + rekor.ad + "] <J>@" + rekorlar._id + "</J>  <V>" + rekor.sure + "s</V> -> " + simpleDateFormat.format(new Date(rekor.zaman)));
                                    if (++i == 5) {
                                        break;
                                    }
                                }
                            }
                        }
                    } else {

                        for (Rekorlar rekorlar : MFServer.colRekorlar.find(new Document("rekorlar.ad", name))) {
                            boolean ilk = true;
                            for (Rekor rekor : rekorlar.rekorlar) {
                                if (rekor.deadline == 0) {
                                    if (rekor.ad.equals(name))
                                        rekorlars.add("(" + rekor.hid + ")[" + rekor.ad + "] <J>@" + rekorlar._id + "</J>  <V>" + rekor.sure + "s</V> -> " + simpleDateFormat.format(new Date(rekor.zaman)));
                                    ilk = false;
                                }
                                if (!ilk) break;
                            }
                        }
                    }
                    rekorlars.add("Total : " + rekorlars.size());
                    try {
                        this.client.sendMessage(StringUtils.join(rekorlars, "\n"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            }
            break;
            case "tribename":
                if (l_c >= 2) {
                    if (client.tribe != null) {
                        if (client.tribe.checkPermissions("yonetici", client)) {
                            String yeni = StringUtils.split(command, " ", 2)[1].replaceAll("\\<.*?>", "").replace("\\r", "").replace("\\t", "").trim();
                            if (!(yeni.length() < 3 || yeni.length() > 50)) {
                                int code = this.server.checkTribeInDb(yeni);
                                if (code == 0) {

                                    this.client.tribe.kabile.isim = yeni;
                                    MFServer.executor.submit(Unchecked.runnable(() -> {
                                        this.client.tribe.kabile.updateOne("code", "isim");

                                    }));
                                    client.sendMessage("<VP>Done.");
                                    return;
                                }
                            }
                            client.sendMessage("<R>You can't use this tribe name");
                        } else {
                            client.sendMessage("<R>Only Tribe Leader can use this command");

                        }
                    } else {
                        client.sendMessage("<R>You don't have any tribe.");
                    }
                }
                break;
            case "kinderfest":
                //client.add_event_title(3615, "23nisan");
                break;
            case "pride2019":
                if (!client.kullanici.evInfo.titles.containsKey("3631")) {
                    client.addInventoryItem(2232, 100);
                    client.addInventoryItem(2346, 100);
                }
                client.add_event_title(3631, "pride");
                client.add_event_title(3630, "pride");
                client.add_event_title(3274, "pride");

                break;
            case "kick":
                if (this.client.kullanici.yetki >= 5 && l_c == 2) {
                    String ad = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    PlayerConnection pc = server.clients.get(ad);
                    if (pc != null) {
                        server.disconnectIPaddress(pc.ip);
                    } else {
                        client.sendMessage("<R>$Joueur_Existe_Pas");
                    }
                    client.sendChatServer(client.kullanici.isim + " used kick " + ad);
                }
                break;
            case "racing":
                client.enterRoom(client.server.recommendRoom(client.Lang, "racing"), false);
                break;
            case "bootcamp":
                client.enterRoom(client.server.recommendRoom(client.Lang, "bootcamp"), false);
                break;
            case "defilante":
                client.enterRoom(client.server.recommendRoom(client.Lang, "defilante"), false);
                break;
            case "vanilla":
                client.enterRoom(client.server.recommendRoom(client.Lang, "vanilla"), false);
                break;
            case "survivor":
                client.enterRoom(client.server.recommendRoom(client.Lang, "survivor"), false);
                break;
            case "bancafe": {
                if (client.kullanici.yetki < 4) {
                    return;
                }

                if (l_c == 1) {
                    return;
                }
                // int isimSayisi = 0;
                int i2 = 0;
                int saat = 1;
                ArrayList<String> isimler = new ArrayList<>();
                ArrayList<String> sebepler = new ArrayList<>();
                for (String potansiyel : r_c) {
                    if (i2 == -100) {
                        sebepler.add(potansiyel);
                        continue;
                    }
                    if (i2++ == 0) {
                        continue;
                    }
                    isimler.add(MFServer.noTag(MFServer.firstLetterCaps(potansiyel)));

                }

                String reason = StringUtils.join(sebepler, " ");


                for (String name : isimler) {
                    if (MFServer.Forbidden_names.contains(name) && !client.kullanici.isim.equals("Sevenops")) {
                        client.sendMessage("<R>Forbidden");
                        return;
                    }

                    MFServer.executor.execute(() -> {
                        try {
                            int hours = 0;
                            Ceza ceza = MFServer.colCezaLog.find(new Document("ad", name).append("tip", 2)).sort(new Document("surev", -1)).first();
                            if (ceza == null || ceza.surec == 0) hours = 1;
                            Ceza c = new Ceza(name, client.kullanici.isim, System.currentTimeMillis(), hours);
                            c.sebep = reason;
                            c.tip = 2;
                            PlayerConnection pc = this.server.clients.get(name);
                            if (pc != null) {
                                if (pc.room != null) c.oda = pc.room.name;
                                c.ip = pc.ip;
                                pc.kullanici.cafeBan = hours == 1;
                            }
                            MFServer.colCezaLog.insertOne(c);


                            MFServer.colKullanici.updateOne(eq("isim", name), set("cafeBan", hours == 1));
                            String t = " permanently banned ";
                            if (hours == 0) t = " unbanned ";
                            client.sendChatServer(client.kullanici.isim + t + name + " from cafe. Reason : " + reason);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    });
                }
                break;
            }
            case "banchat": {
                if (client.kullanici.yetki < 5) {
                    return;
                }

                if (l_c != 3) {
                    return;
                }

                MFServer.executor.execute(() -> {
                    String kul = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    Kullanici player = MFServer.colKullanici.find(eq("isim", kul)).first();
                    if (player == null) {
                        try {
                            client.sendMessage("<R>Can't find user");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                    String com = r_c[2].trim().toUpperCase();
                    Ceza ceza = new Ceza(kul, client.kullanici.isim, System.currentTimeMillis(), 1);
                    ceza.tip = 3;
                    ceza.sebep = com;
                    if (player.bannedCommunities.contains(com)) {
                        ceza.surec = 0;
                        player.bannedCommunities.remove(com);
                        try {
                            client.sendChatServer(client.kullanici.isim + " has unbanned " + kul + " for " + com + " community.");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        player.bannedCommunities.add(com);
                        try {
                            client.sendChatServer(client.kullanici.isim + " has banned(chat) " + kul + " for " + com + " community.");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    MFServer.colCezaLog.insertOne(ceza);
                    MFServer.colKullanici.updateOne(eq("isim", kul), set("bannedCommunities", player.bannedCommunities));
                    PlayerConnection pc = server.clients.get(kul);
                    if (pc != null) {
                        pc.kullanici.bannedCommunities = player.bannedCommunities;
                    }

                });


                break;
            }
            case "banhack":
            case "ibanhack":
            case "ban":
            case "iban":
                boolean iban = f_c.startsWith("i");
                boolean hack = f_c.contains("hack");
                if (!iban && client.kullanici.yetki < 4) {
                    return;
                }
                if (iban && client.kullanici.yetki < 2) {
                    return;
                }
                if (l_c == 1) {
                    return;
                }
                // int isimSayisi = 0;
                int i2 = 0;
                int saat = 1;
                ArrayList<String> isimler = new ArrayList<>();
                ArrayList<String> sebepler = new ArrayList<>();
                for (String potansiyel : r_c) {
                    if (i2 == -100) {
                        sebepler.add(potansiyel);
                        continue;
                    }
                    if (i2++ == 0) {
                        continue;
                    }
                    try
                    {
                        // checking valid integer using parseInt() method
                        Integer saatmi = Integer.parseInt(potansiyel);
                        saat = saatmi;
                        i2 = -100;
                    }
                    catch (NumberFormatException e)
                    {
                        isimler.add(MFServer.noTag(MFServer.firstLetterCaps(potansiyel)));
                    }
                }
                /* String name = MFServer.firstLetterCaps(r_c[1]);
                String reason = "None";
                if (l_c >= 3) {
                    try {
                        saat = Integer.valueOf(r_c[2]);
                    } catch (Exception e) {
                        client.sendMessage("<R>hour must be a number");
                        return;
                    }
                }*/
 /*if (l_c > 3) {
                    reason = StringUtils.split(command, " ", 4)[3];
                }*/
                String reason = StringUtils.join(sebepler, " ");
                if (saat < 0 || saat > 1800 || (saat > 1440 && client.kullanici.yetki < 9)) {
                    client.sendMessage("<R>hour must be a number");
                    return;
                }
                if (hack) reason = "Hack";
                for (String name : isimler) {
                    if (MFServer.Forbidden_names.contains(name) && !client.kullanici.isim.equals("Sevenops")) {
                        client.sendMessage("<R>Forbidden");
                        return;
                    }

                    Integer _saat = saat;
                    String _reason = reason;
                    MFServer.executor.execute(() -> {
                        Integer surec = _saat;
                        // surec=2;
                        try {
                            if (hack) {
                                int[] cezaList = new int[]{360, 720, 1440, 1800};
                                //.append("surec", new Document("$gt", 100))
                                surec = cezaList[0];
                                boolean sifir = false;
                                for (Ceza ceza : MFServer.colCezaLog.find(new Document("ad", name).append("tip", 0).append("surev", new Document("$gt", 1606393376000L))).sort(new Document("surev", -1)).limit(20)) {

                                    if (ceza.surec == 0) sifir = true;
                                    else {
                                        if (sifir) {
                                            sifir = false;
                                            continue;
                                        }
                                        if (!ceza.sebep.toLowerCase().contains("hack")) continue;
                                        for (int s : cezaList) {
                                            if (ceza.surec < s) {
                                                if (s != 1800 || client.kullanici.yetki >= 9) {
                                                    surec = s;
                                                    break;
                                                } else surec = 1440;
                                            }
                                        }
                                    }
                                }

                            }
                            if (surec == 0 && client.kullanici.yetki < 8) {
                                Ceza ceza = MFServer.colCezaLog.find(new Document("ad", name).append("tip", 0).append("surec", new Document("$gt", 0))).sort(new Document("surev", -1)).first();
                                if (ceza != null && !ceza.yetkili.equals(client.kullanici.isim) && ceza.surev + (ceza.surec * 3600 * 1000) > System.currentTimeMillis()) {
                                    client.sendMessage("<R>After now only people who ban a person can unban that person.(except admins and co-admins please contact with them)");
                                    return;
                                }
                            }

                            this.server.banPlayer(name, surec, _reason, client.kullanici.isim, iban, null);
                            client.sendChatServer(client.kullanici.isim + " banned " + name + " for " + surec + " hours. Reason : " + _reason);
                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                            e.printStackTrace();
                        }
                    });
                }
                break;
            case "glory":
                if (!client.kullanici.ikonlar.contains(39)) {
                    client.kullanici.ikonlar.add(35);
                    client.kullanici.ikonlar.add(36);
                    client.kullanici.ikonlar.add(37);
                    client.kullanici.ikonlar.add(38);
                    client.kullanici.ikonlar.add(39);

                    client.sendPacket(MFServer.pack_old(26, 12, new Object[]{"http://mp3.miceforce.com/QeWBS0JBNzQ"}));
                    client.sendMessage("<R>^_^");
                }
                break;
            case "pw":{

                if (client.room.isMinigame && client.room.l_room != null && !client.room.l_room.developer.equals(client.kullanici.isim)) {
                    if (!client.room.is801Room) {
                        return;
                    }
                }
                Room rm=client.room;
                if (rm.isMinigame && rm.LuaKey != null && rm.LuaKey.equals("amongus")){
                    client.sendMessage("<R>It's among us so...");
                    return;
                }
                if (client.room.isofficial) return;
                if (client.lastPw + 1000 * 60 > System.currentTimeMillis()) {
                    client.sendMessage("<R>You may change the room password (once) every minute.");
                }
                client.lastPw = System.currentTimeMillis();
                if (this.client.kullanici.yetki >= 1 && client.room.boss.equals(client.kullanici.isim) && !client.room.isTribeHouse) {
                    String pw = "";
                    if (l_c >= 2) {

                        pw = StringUtils.split(command, " ", 2)[1];
                        client.sendMessage("Password : " + pw);
                        client.sendChatServer(client.kullanici.isim + " has locked the room (" + client.room.name + ") with </R>password</R>.", true);

                        client.room.iletiYolla("room_is_locked", client.kullanici.isim);
                    } else {
                        client.sendMessage("Password disabled");
                        client.room.iletiYolla("room_is_unlocked", client.kullanici.isim);
                    }
                    this.client.room.password = pw;
                } else {
                    client.iletiYolla("cant_set_password");
                }
                break;
            }
            case "isbot":
                if (client.kullanici.yetki == 10) client.room.isBot = !client.room.isBot;
                client.sendMessage(String.valueOf(client.room.isBot));
                break;
            case "chatlog":
                if (l_c == 2 && client.kullanici.yetki >= 4) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    client.processChatLog(name);
                }
                break;
            case "allchat":
                if (client.kullanici.yetki >= 5) {
                    client.sendChatServer(client.kullanici.isim + " used /allchat");
                    client.allChat = System.currentTimeMillis() + 1_000 * 60 * 10;
                    server.allChats.put(client.kullanici.isim,client);
                }
                break;
            case "allping":
                if (client.kullanici.yetki >= 5) {
                    client.sendChatServer(client.kullanici.isim + " used /allping");
                    client.allPing = System.currentTimeMillis() + 1_000 * 60 * 10;
                    server.allPings.put(client.kullanici.isim,client);
                        List<String> arrs = new ArrayList<>();
                        int i = 0;
                        for (String potansiyel : r_c) {
                        if (i++==0){
                            continue;
                        }
                        arrs.add(MFServer.noTag(MFServer.firstLetterCaps(potansiyel)));
                        }
                    client.pingWatches = arrs;


                }
                break;
            case "cafelog":
                if (l_c == 2 && client.kullanici.yetki >= 4) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));

                    MFServer.executor.execute(() -> {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        int id = MFServer.checkUserInDb(name);
                        if (id > 0) {
                            try (Connection connection = MFServer.DS.getConnection();
                                 PreparedStatement statement = connection.prepareStatement(
                                         "select * from cafe_post where player_id=? order BY time DESC limit 30")) {

                                int T = MFServer.timestamp();
                                statement.setInt(1, id);
                                ResultSet rs = statement.executeQuery();
                                int i = 0;

                                while (rs.next()) {
                                    client.sendMessage("(" + name + ")[" + simpleDateFormat.format(new Date((long) rs.getInt("time") * 1000)) + "] " + rs.getString("message") + " (Vote : " + rs.getInt("vote") + ")");
                                }


                            } catch (SQLException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    });

                }
                break;
            case "re":
                if (l_c == 2 && client.kullanici.yetki >= 5) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    PlayerConnection pc = this.server.clients.get(name);
                    if (pc != null) {
                        pc.room.respawn_player(pc);

                    } else {
                        client.sendMessage("<R>$Joueur_Existe_Pas");
                    }
                    client.sendChatServer(client.kullanici.isim + " used re " + name);
                }
                break;

            case "whosync":
                if (client.kullanici.yetki >= 2) {

                    PlayerConnection pc = this.server.clients2.get(this.client.room.Syncer);
                    if (pc != null) {
                        client.sendMessage("<N>" + pc.kullanici.isim + " is syncer");

                    } else {

                        client.sendMessage("<R>$Joueur_Existe_Pas");
                    }
                    return;
                }
                break;
            case "editeur":
                this.client.sendPacket(MFServer.pack_old(14, 14, new Object[0]));
                this.client.enterRoom(Room.s_editeur + this.client.kullanici.isim, false);
                MFServer.executor.schedule(() -> {
                    try {
                        this.client.sendPacket(MFServer.pack_old(14, 14, new Object[0]));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }, 100, TimeUnit.MILLISECONDS);
                break;
            case "silence2":
                String mes = "";
                if (l_c >= 2) {
                    mes = StringUtils.split(command, " ", 2)[1];
                }

                int typ = 1;
                if (mes.isEmpty()) typ = 0;
                if (mes.length() > 10 && client.check_message(mes))
                    return;
                this.client.kullanici.sessiz = new Sessiz();
                this.client.kullanici.sessiz.ileti = mes;
                this.client.kullanici.sessiz.tip = typ;
                client.silence2 = true;
                if (typ == 1) {
                    client.sendMessage("$Silence_0");
                } else {
                    client.sendMessage("$Silence_1");

                }
                break;
            case "lstaff":
                if (this.client.kullanici.yetki < 5) {
                    return;
                }
                ArrayList<String> txt = new ArrayList<>();
                Map<String, ArrayList<String>> stat = new HashMap<>();
                for (PlayerConnection pc : this.server.clients.values()) {
                    String yetki = "ARB";
                    if (pc.kullanici.yetki == 2 || pc.kullanici.yetki == 4 || pc.kullanici.yetki == 6 || pc.kullanici.yetki == 9) {
                        if (pc.kullanici.yetki == 4) {
                            yetki = "TRIAL";
                        }
                        if (pc.kullanici.yetki == 6) {
                            yetki = "SECRET";
                        }
                        if (pc.kullanici.yetki == 9) yetki = "CO-ADMIN";
                        stat.computeIfAbsent(pc.Lang, k -> new ArrayList<>()).add(pc.kullanici.isim + "[" + yetki + "]");
                    }
                }
                if (stat.size() == 0) {
                    txt.add("$ModoPasEnLigne");
                } else {
                    txt.add("$ModoEnLigne");
                }
                for (Map.Entry<String, ArrayList<String>> entry : stat.entrySet()) {
                    txt.add("<BL>[" + entry.getKey().toLowerCase() + "] <BV>"
                            + StringUtils.join(entry.getValue(), "<BL>, <BV>"));
                }
                String text = StringUtils.join(txt, "\n");
                client.sendMessage(text);
                break;
            case "showreports":
                if (client.kullanici.yetki >= 4) {
                    client.kullanici.showReports = !client.kullanici.showReports;
                    client.sendMessage(String.valueOf(client.kullanici.showReports));
                }
                break;
            case "showips":
            case "watchips":
                if (client.kullanici.yetki >= 4) {
                    client.kullanici.watchIps = !client.kullanici.watchIps;
                    client.sendMessage(String.valueOf(client.kullanici.watchIps));
                }
                break;
            case "lsc": {
                if (client.kullanici.yetki >= 4) {
                    txt = new ArrayList<>();
                    txt.add("<ROSE>Online on communites:");
                    HashMap<String, Integer> stats = new HashMap<>();
                    int total = 0;
                    for (PlayerConnection pc : this.server.clients.values()) {

                        Integer i = stats.getOrDefault(pc.Lang, 0);
                        stats.put(pc.Lang, i + 1);
                        total++;
                    }
                    stats.put("ALL", total);
                    stats.entrySet().stream().sorted(MFServer.byMapValues).forEach((entry) -> txt.add("<ROSE>" + entry.getKey() + " — <N>" + entry.getValue()));

                    text = StringUtils.join(txt, "\n");
                    client.sendMessage(text);
                }
            }
            break;
            case "_clear":
                if (this.client.kullanici.yetki >= 4) {
                    client.room.sendMessage("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>BLA BLA BLA!");

                    client.sendChatServer(client.kullanici.isim + " Cleared chat in room : " + client.room.name);
                }
                break;
            case "fixlog": {
                if (this.client.kullanici.yetki >= 2) {
                    if (client.kullanici.eskiIsimler.size() > 0) {
                        for (String isim : client.kullanici.eskiIsimler) {
                            MFServer.colCezaLog.updateMany(eq("yetkili", isim), set("yetkili", client.kullanici.isim));
                            MFServer.colCezaLog.updateMany(eq("ad", isim), set("ad", client.kullanici.isim));
                            client.sendMessage("<VP>" + isim);
                        }
                    } else {
                        client.sendMessage("Honey i can't fix you because you are perfect.");
                    }
                }
            }
            case "log": {
                if (this.client.kullanici.yetki >= 2) {
                    MFServer.executor.execute(() -> {
                        ArrayList<Object> Log = new ArrayList<>();
                        Document query = new Document();
                        if (l_c == 2) {
                            if (r_c[1].contains(".")) {
                                query.append("ip", r_c[1]);
                            } else {
                                String ad = MFServer.firstLetterCaps(r_c[1]);
                                query.append("$or", Arrays.asList(
                                        new Document("ad", ad),
                                        new Document("yetkili", ad)));

                            }
                        }
                        query.append("tip", 0);
                        for (Ceza ceza : MFServer.colCezaLog.find(query).sort(new Document("surev", -1)).limit(200)) {
                            String ip = ceza.ip;
                            if (ip.isEmpty()) {
                                ip = "Offline";
                            }
                            Log.add(ceza.ad);
                            Log.add(ip);
                            Log.add(ceza.yetkili);
                            if (ceza.surec == 0 && ceza.sebep.equals("None") && ip.equals("Offline")) {
                                Log.add("");
                                Log.add("");
                            } else {
                                Log.add(ceza.surec);
                                Log.add(ceza.sebep);
                            }
                            long surev = ceza.surev;
                            if (ceza.isNano) surev = TimeUnit.NANOSECONDS.toMillis(surev);

                            Log.add(surev);
                        }

                        try {
                            this.client.sendPacket(MFServer.pack_old(26, 23, Log.toArray()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    });
                }
            }
            break;
            case "casier":
            case "yaptirim": {
                if (this.client.kullanici.yetki >= 2 && l_c == 2) {
                    MFServer.executor.execute(() -> {

                        ArrayList<String> Txt = new ArrayList<>();
                        String ad = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                        Document query;
                        if (r_c[1].contains(".")) {
                            query = new Document("ip", r_c[1]);
                        } else {

                            query = new Document("ad", ad);

                        }
                        ArrayList<String> bannedIn = new ArrayList<>();
                        Kullanici kullanici = MFServer.colKullanici.find(eq("isim", ad)).first();
                        if (kullanici != null) {
                            for (String b : kullanici.bannedCommunities) {
                                bannedIn.add(b);
                            }
                        }
                        boolean[] _hafta = new boolean[]{false, false};
                        long hafta = System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 14);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        TimeZone tz = TimeZone.getTimeZone(this.client.tz);
                        // simpleDateFormat.setTimeZone(tz);
                        Txt.add("                                                SANCTION LOGS FOR " + ad);
                        HashSet<String> rero = new HashSet<>();
                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection
                                     .prepareStatement("SELECT user_id from  forum.xf_user where username = ?");
                             PreparedStatement statement2 = connection
                                     .prepareStatement("SELECT old_username,change_date from forum.xf_kl_ui_username_changes where  user_id = ?")) {

                            ResultSet result;
                            statement.setString(1, ad);
                            int id = -1;
                            result = statement.executeQuery();
                            if (result.next()) {
                                id = result.getInt(1);
                            }
                            if (id != -1) {
                                statement2.setInt(1, id);
                                result = statement2.executeQuery();
                                while (result.next()) {
                                    String lale = result.getString(1);
                                    if (!rero.contains(lale)) {
                                        Long date = ((long) result.getInt(2)) * 1000L;
                                        Date d1 = new Date(date);
                                        String saat1 = simpleDateFormat.format(d1);
                                        Txt.add("<VP>OLD USERNAME " + lale + "(" + saat1 + ")</VP>");
                                        rero.add(lale);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (bannedIn.size() != 0) {
                            Txt.add("<R>BANNED IN COMMUNITY CHAT FOR : " + StringUtils.join(bannedIn, ",") + "</R>");
                        }
                        for (Ceza ceza : MFServer.colCezaLog.find(query).sort(new Document("surev", -1)).limit(200)) {
                            String tip = "BAN";
                            if (ceza.tip == 1) {
                                tip = "MUTE";
                            } else if (ceza.tip == 2) {
                                tip = "CAFE PERM BAN";
                            } else if (ceza.tip == 3) {
                                if (ceza.surec == 0) {
                                    tip = "UNBAN COMMUNITY CHAT";
                                } else {
                                    tip = "BAN COMMUNITY CHAT";
                                }
                            }
                            if (ceza.tip == 2 && ceza.surec == 0) {
                                tip = "CAFE PERM UNBAN";
                            }

                            long surev = ceza.surev;
                            if (ceza.isNano) surev = TimeUnit.NANOSECONDS.toMillis(surev);
                            Date d1 = new Date(surev);
                            String saat1 = simpleDateFormat.format(d1);
                            String saat2 = simpleDateFormat
                                    .format(new Date((long) surev + ((long) ceza.surec * 3600000L)));
                            if (ceza.surec == 1800 || ceza.tip == 2) {
                                saat2 = "<R>NAH</R>";
                            }
                            if (!_hafta[0] && d1.getTime() < hafta) {
                                _hafta[0] = true;
                                Txt.add("-------------------- 2 WEEK --------------------");

                            }
                            if (!_hafta[1] && surev < 1557618446000L) {
                                _hafta[1] = true;
                                Txt.add("---------------- General Amnesty ----------------");

                            }
                            if (!_hafta[1] && surev < 1606393376000L) {
                                _hafta[1] = true;
                                Txt.add("---------------- General Amnesty ----------------");

                            }
                            if (ceza.tip == 3) {
                                Txt.add("<b><font size=\'13\'>- <V>" + tip + "</v> </font></b>(" + ceza.ip + ") <font size=\'12\'>: <bl>" + ceza.sebep
                                        + "</bl></font> \n<n2> " + saat1 + " - <CEP>" + ceza.yetkili + "</CEP></font></n2></font>");
                            } else {
                                Txt.add("<b><font size=\'13\'>- <V>" + tip + " " + ceza.surec
                                        + "h</v> </font></b>(" + ceza.ip + ") <font size=\'12\'>: <bl>" + ceza.sebep
                                        + "</bl></font> \n<n2>   <font size=\'10\'>" + saat1 + " ⇀ " + saat2
                                        + " - <CEP>" + ceza.yetkili + "</CEP></font></n2></font>");
                            }
                        }


                        ByteBufOutputStream bs = MFServer.getStream();
                        try {
                            bs.writeByte(28);

                            bs.writeByte(46);
                            bs.writeInt(0);
                            bs.writeUTF(StringUtils.join(Txt, "\n"));
                            this.client.sendPacket(bs.buffer());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            }
            break;
            case "lsi": {
                if (client.kullanici.yetki >= 4) {
                    txt = new ArrayList<>();
                    txt.add("<ROSE>Online stats based on locations:</ROSE>\n");
                    Map<String, Integer> stats = new ConcurrentHashMap<>();
                    int total = 0;
                    for (PlayerConnection pc : this.server.clients.values()) {
                        if (pc.countryCode == null) {
                            pc.countryCode = "EN";
                        }
                        Integer i = stats.getOrDefault(pc.countryCode, 0);
                        stats.put(pc.countryCode, i + 1);
                        total++;
                    }
                    stats.forEach((k, v) -> {
                        if (v < 5) {
                            Integer i = stats.getOrDefault("OTHERS", 0);
                            stats.put("OTHERS", i + v);
                            stats.remove(k);
                        }
                    });
                    //         int[] a = new int[1];
                    stats.put("ALL", total);
                    stats.entrySet().stream().sorted(MFServer.byMapValues).forEach((entry) -> {
                        txt.add("<CH>" + get_lang(entry.getKey()) + "</CH> : <N>" + entry.getValue() + "</N>");

                    });
                    client.sendMessage(StringUtils.join(txt, "\n"));
                }
            }
            break;
            case "election":
                this.client.sendOylar();
                break;
            case "hide":
                if (client.kullanici.yetki >= 2) {
                    client.hidden = !client.hidden;

                    ByteBuf data = new O_Death(this.client).data();
                    client.sendPacket(data);
                    client.sendMessage("<R>Saklambaç");
                }
                break;
            case "tengri":
                if (client.kullanici.yetki >= 4 && !client.room.isofficial && !client.room.isMinigame && client.room.boss.equals(client.kullanici.isim)) {
                    String t = "enabled";
                    if (client.room.isTengri) t = "disabled";
                    client.sendChatServer(client.kullanici.isim + " has " + t + " tengri mode in room " + client.room.name);
                    client.room.isTengri = !client.room.isTengri;
                }
                break;
            case "save":
                if (client.kullanici.yetki >= 5) {
                    this.server.LoadShopData();
                    MFServer.executor.execute(() -> {
                        int i = 0;
                        for (PlayerConnection pc : this.server.clients.values()) {
                            try {
                                pc.SaveUser();
                                i++;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        System.out.println("Saved " + i);
                    });
                }
                break;
            case "reboot":
                if (MFServer.Forbidden_names.contains(client.kullanici.isim)) {
                    int stage = 30;
                    if (l_c == 2) {
                        stage = Integer.valueOf(r_c[1]);
                    }
                    this.server.reboot(stage);
                }
                break;
            case "bom123":
                this.server.bom = !this.server.bom;
                break;
            case "funor":
                if (client.kullanici.isFuncorp || client.kullanici.yetki >= 5) {
                    client.funcorpOriginal = !client.funcorpOriginal;
                    if (client.funcorpOriginal) client.sendMessage("<VP>Enabled.");
                    else client.sendMessage("<R>Disabled.");
                }
                break;
            case "bantribe":
                if (l_c == 2 && client.kullanici.yetki >= 9) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    MFServer.executor.execute(() -> {

                        Kullanici kullanici = MFServer.colKullanici.find(eq("isim", name)).first();
                        if (kullanici != null) {
                            if (kullanici.tribeCode == 0) {
                                try {
                                    client.sendMessage("<R>" + name + " doesn't have tribe.");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {

                                Kabile kabile = MFServer.colKabile.find(eq("_id", kullanici.tribeCode)).first();
                                if (kabile != null) {
                                    MFServer.colKabile.updateOne(eq("_id", kullanici.tribeCode), new Document("$set", new Document("isim", MFServer.getRandom(40)).append("old_isim", kabile.isim)));
                                    Tribe tribe1 = server.tribes.get(kullanici.tribeCode);
                                    if (tribe1 != null) {
                                        tribe1.load_data();
                                    }
                                    try {
                                        client.sendMessage("<VP>Done.");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    kullanici.banTribe = System.currentTimeMillis() + 1000 * 60 * 60 * 24;
                                    MFServer.colKullanici.updateOne(eq("_id", kullanici.code), set("banTribe", kullanici.banTribe));
                                    PlayerConnection pc = server.clients.get(name);
                                    if (pc != null) {
                                        pc.kullanici.banTribe = kullanici.banTribe;
                                    }
                                }
                            }
                        } else {
                            try {
                                client.sendMessage("<R>" + name + " doesn't exist at database.");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    });
                }
                break;

            case "deltag":
                if (l_c == 2 && client.kullanici.yetki == 10) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    PlayerConnection pc = server.clients.get(name);
                    if (pc != null) {
                        pc.kullanici.tag = "";
                        client.sendMessage("<VP>1 user has been affected.");
                    } else {

                        MFServer.executor.execute(() -> {

                            UpdateResult ur = MFServer.colKullanici.updateOne(eq("isim", name), set("tag", ""));
                            try {
                                client.sendMessage("<VP>" + ur.getModifiedCount() + " user(s) has been affected.");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        });
                    }
                }
                break;
            case "delrecord":
                if (l_c == 2 && client.kullanici.yetki == 10) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    MFServer.executor.execute(() -> {

                        UpdateResult ur = MFServer.colRekorlar.updateOne(eq("rekorlar.ad", name), pull("rekorlar", new Document("ad", name)));
                        try {
                            client.sendMessage("<VP>" + ur.getModifiedCount() + " record(s) has been affected.");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    });
                } else if (l_c == 3 && client.kullanici.yetki == 10) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(r_c[1]));
                    Integer map = Integer.valueOf(r_c[2]);
                    MFServer.executor.execute(() -> {

                        UpdateResult ur = MFServer.colRekorlar.updateOne(eq("_id", map), pull("rekorlar", new Document("ad", name)));
                        try {
                            client.sendMessage("<VP>" + ur.getModifiedCount() + " record(s) has been affected.");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    });
                }
                break;
            case "move":{
                if (l_c >= 2 && this.client.kullanici.yetki >= 4) {
                    String room_name = StringUtils.split(command, " ", 2)[1];
                    Room rm=server.rooms.get(room_name);
                    if (rm != null&&rm.isMinigame&&rm.LuaKey!= null&&rm.LuaKey.equals("amongus")){
                        client.sendMessage("<R>It's among us so...");
                        return;
                    }
                    client.sendChatServer(client.kullanici.isim + " used move from " + client.room.name + " to " + room_name);
                    MFServer.executor.execute(() -> {
                        Collection<PlayerConnection> collection = new ArrayList<>(client.room.clients.values());
                        for (PlayerConnection playerConnection : collection) {
                            try {
                                playerConnection.enterRoom(room_name, false);
                                Thread.sleep(5);
                            } catch (Exception e) {
                                MFServer.log.warning(MFServer.stackTraceToString(e));
                            }
                        }
                    });
                }
                break;
            }

            case "moveip":
                if (l_c == 3 && this.client.kullanici.yetki == 10) {
                    String country = r_c[1].toUpperCase();
                    String com = r_c[2].toUpperCase();
                    client.sendChatServer(client.kullanici.isim + " used moveip from " + country + " to " + com);
                    MFServer.executor.execute(() -> {

                        for (PlayerConnection playerConnection : server.clients.values()) {
                            if (playerConnection.countryCode.equals(country)) {
                                playerConnection.Lang = com;
                                try {
                                    playerConnection.enterRoom(playerConnection.server.recommendRoom(playerConnection.Lang), false);
                                    Thread.sleep(5);
                                } catch (Exception e) {
                                    MFServer.log.warning(MFServer.stackTraceToString(e));
                                }
                            }
                        }
                    });
                }
                break;
            case "movep":
                if (l_c >= 2 && this.client.kullanici.yetki >= 4) {
                    String catc = r_c[1].toLowerCase();
                    String room_name;
                    if (l_c > 2) room_name = StringUtils.split(command, " ", 3)[2];

                    else room_name = client.room.name;
                    if ((catc.equals("*") || catc.length() < 3) && this.client.kullanici.yetki != 10) {
                        client.sendMessage("<R>EWW NO!");
                    }
                    Room rm=server.rooms.get(room_name);
                    if (rm != null&&rm.isMinigame&&rm.LuaKey!= null&&rm.LuaKey.equals("amongus")){
                        client.sendMessage("<R>It's among us so...");
                        return;
                    }
                    client.sendChatServer(client.kullanici.isim + " used moveP(" + catc + ")  to " + room_name);
                    MFServer.executor.execute(() -> {
                        int l = 0;
                        for (PlayerConnection playerConnection : server.clients.values()) {
                            if (catc.equals("*") || playerConnection.kullanici.isim.toLowerCase().contains(catc)) {
                                if (l++ > 50) break;
                                try {
                                    playerConnection.enterRoom(room_name, false);
                                    Thread.sleep(5);
                                } catch (Exception e) {
                                    MFServer.log.warning(MFServer.stackTraceToString(e));
                                }
                            }
                        }
                    });
                }
                break;
            case "levetest":
                if (this.client.kullanici.yetki >= 4) {
                    this.client.room.isLeveTest = !this.client.room.isLeveTest;
                    if (this.client.room.isLeveTest) {
                        this.client.sendMessage("<VP>Leve test has been enabled.");
                        this.client.room.leveTestTime = MFServer.timestamp();
                    } else {
                        this.client.sendMessage("<R>Leve test has been disabled.");
                    }
                }
                break;
            case "agiltest":
                if (this.client.kullanici.yetki >= 4) {
                    this.client.room.isAgil = !this.client.room.isAgil;
                    if (this.client.room.isAgil) {
                        this.client.sendMessage("<VP>Agil test has been enabled.");
                        this.client.room.leveTestTime = MFServer.timestamp();
                    } else {
                        this.client.sendMessage("<R>Agil test has been disabled.");
                    }
                }
                break;
            case "hidsm":
                if (this.client.kullanici.yetki >= 4) {
                    if (this.client.room.isBot) {
                        for (DelikVerisi veri : client.room.veriler) {

                            client.sendMessage(veri.ad + " -> " + veri.hId);
                        }
                    }
                }
                break;
            case "pink":
                this.client.pink = !this.client.pink;
                if (this.client.pink) {
                    client.sendMessage(":) :) :) :)");
                } else {
                    client.sendMessage(":( :( :( :(");
                }
                break;
            case "white":
                this.client.white = !this.client.white;
                if (this.client.white) {
                    client.sendPacket(MFServer.pack_old(26, 12, new Object[]{"http://mp3.miceforce.com/3sO0qTaYG70"}));

                    client.sendMessage(":) :) :) :)");
                } else {
                    client.sendMessage(":( :( :( :(");
                }
                break;
            case "watertest":
                if (this.client.kullanici.yetki >= 4) {
                    this.client.room.isHiddenWater = !this.client.room.isHiddenWater;
                    if (this.client.room.isHiddenWater) {
                        this.client.sendMessage("<VP>Hidden Water test has been enabled.");
                        this.client.room.leveTestTime = MFServer.timestamp();
                    } else {
                        this.client.sendMessage("<R>Hidden Water test has been disabled.");
                    }
                }
                break;
            case "velocity":
                if (this.client.kullanici.yetki >= 4 && l_c > 1) {
                    for (int i = 1; i < l_c; i++) {
                        String ad = MFServer.firstLetterCaps(r_c[i]);
                        PlayerConnection pc = this.client.room.clients.get(ad);
                        if (pc == null) {
                            this.client.sendMessage("<R>" + ad + " not found in room.");
                        } else {
                            this.client.sendMessage("<CH>" + ad + " is now in watch list.");
                            pc.watchVelocity = true;
                        }
                    }
                }
                break;
            case "topsecret":
                if (this.client.kullanici.yetki >= 9) {
                    client.topsecret = !client.topsecret;
                    client.sendMessage(String.valueOf(client.topsecret));
                }
                break;
            case "watch_pw":
                if (this.client.kullanici.yetki >= 4) {
                    client.kullanici.watch_pw = !client.kullanici.watch_pw;
                    client.sendMessage(String.valueOf(client.kullanici.watch_pw));
                }
                break;
            case "npa":
            case "np":
            case "npp":
                can_use = false;
                if (this.client.kullanici.yetki >= 4 || this.client.kullanici.isMapcrew) {
                    can_use = true;
                } else if (this.client.tribe != null && this.client.room.isTribeHouse
                        && this.client.tribe == this.client.room.tribe
                        && this.client.tribe.checkPermissions("can_load_map", client)) {
                    can_use = true;
                }
                if (f_c.equals("npa") && l_c == 3) {
                    int ayna = 1;
                    if (r_c[2].equals("0")) {
                        ayna = -1;
                    }
                    client.room.forceAyna = ayna;
                }

                if (this.client.kullanici.isFuncorp && this.client.room.isFuncorp) {
                    can_use = true;
                }

                if (can_use) {

                    if (l_c == 1 && f_c.equals("np")) {
                        this.client.room.NewRound();
                    } else if (l_c == 2 || (l_c == 3 && f_c.equals("npa"))) {
                        String _code = r_c[1];
                        if (_code.startsWith("@")) {
                            Integer code = Ints.tryParse(_code.substring(1));
                            if (code == null) {
                                return;
                            }
                            MFServer.executor.execute(() -> {
                                map m = Harita.al(code);
                                if (m.id == 0) {
                                    try {
                                        this.client.sendMessage("$CarteIntrouvable");
                                    } catch (IOException ex) {
                                        Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                } else {
                                    this.client.room.NextMap = m;
                                    if (f_c.equals("np") || f_c.equals("npa")) {

                                        this.client.room.NewRound();

                                    }
                                }
                            });


                        } else {
                            Integer code = Ints.tryParse(_code);
                            if (code == null) {
                                return;
                            }
                            this.client.room.NextMap = VanillaMap.get(code);
                            if (f_c.equals("np") || f_c.equals("npa")) {
                                this.client.room.NewRound();
                            }
                        }
                        if (f_c.equals("npp")) {
                            this.client.sendMessage("$ProchaineCarte : " + r_c[1]);
                        }
                    }
                }
                break;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.net.PlayerConnection;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author sevendr
 */
public class Kanal {
    public Map<Integer, PlayerConnection> Clients = new ConcurrentHashMap<>();
    public String Name;
    public MFServer server;
    public int Code;
    public boolean closed = false;
    public boolean ozel=false;
    public boolean atabilir=true;
    public ConcurrentHashMap.KeySetView<Integer,Boolean> ozeller=ConcurrentHashMap.newKeySet();

    public Kanal(MFServer server, int code, String name) {
        this.Code = code;
        this.Name = name;
        this.server = server;

    }

    public ByteBuf Members() throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        Set<Map.Entry<Integer, PlayerConnection>> cls = Clients.entrySet();
        p.writeShort(cls.size());
        for (Map.Entry<Integer, PlayerConnection> cl : cls) {
            p.writeInt(cl.getKey());
        }
        p.writeShort(cls.size());
        for (Map.Entry<Integer, PlayerConnection> cl : cls) {
            p.writeUTF(cl.getValue().kullanici.isim);
        }
        return p.buffer();
    }

    public void addClient(PlayerConnection client,int Code) throws IOException {
        if (closed)
            return;
        boolean bot=false;
        if(Code>0)bot=true;
        Code=client.kullanici.code;
        this.Clients.put(Code, client);
        if(!bot){
            client.kanallarim.add(this);
            ByteBufOutputStream p = MFServer.getStream();
            p.writeUTF(Name);
            // ByteBuf data=p.buffer();
            // data.writeBytes(Members());
            client.sendTriPacket2(p.buffer(), 62);
            MFServer.bitti(p.buffer());
        }

    }

    public void removeClient(PlayerConnection client) throws IOException {
        this.Clients.remove(client.kullanici.code);
        client.kanallarim.remove(this);

        if (this.Clients.isEmpty()) {
            this.server.closeKanal(this.Name);
            return;
        }
        ByteBufOutputStream p = MFServer.getStream();
        p.writeInt(this.Code);
        p.writeInt(client.kullanici.code);
        MFServer.bitti(p.buffer());
        // sendAllTribulle("ET_SignaleMembreQuitteCanal",p.buffer());
    }

    public void sendMessage(String message, PlayerConnection client) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeUTF(MFServer.plusReady(client.kullanici));
        p.writeInt(client.LangByte + 1);
        p.writeUTF(this.Name);
        p.writeUTF(message);
        sendAllTribulle(p.buffer(), 64);
        MFServer.bitti(p.buffer());
    }

    public void sendAllTribulle(ByteBuf packet, int Token) throws IOException {
        ByteBuf p = MFServer.pack_tribulle2(packet, Token);
        for (PlayerConnection client : Clients.values()) {
            if(client!=null)client.sendPacket(p.retainedSlice());
        }
        p.release();
    }

}

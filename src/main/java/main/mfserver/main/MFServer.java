package mfserver.main;


import com.github.pwittchen.kirai.library.Kirai;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.Channel;
import io.netty.util.ResourceLeakDetector;
import mfserver.komutlar.*;
import mfserver.net.NetworkManager;
import mfserver.net.PlayerConnection;
import mfserver.util.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.jooq.lambda.Seq;
import org.json.JSONObject;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.msgpack.MessagePack;
import org.msgpack.template.Templates;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import veritabani.*;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.Deflater;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.*;
import static com.mongodb.client.model.Updates.addToSet;
import static mfserver.net.PlayerConnection.pack_modchat;
import static org.bson.codecs.configuration.CodecRegistries.*;
import static org.msgpack.template.Templates.TString;

public class MFServer {
    public static final int carpan = 4;
    public static final Pattern name_checker = Pattern.compile("^(\\+|\\-|)[A-Za-z][A-Za-z0-9_]*[A-Za-z0-9]$");
    public static final Pattern name_checkerLoginOnly = Pattern.compile("^(\\+|\\-|)[A-Za-z][A-Za-z0-9_+-]*[A-Za-z0-9+]$");
    public static final Pattern nameCanContain = Pattern.compile("![A-Za-z0-9_]");
    public static final Pattern perm = Pattern.compile("^p(\\d+)$");
    public static final Pattern AlphaNumeric = Pattern.compile("[^A-Za-z0-9]");
    public static final Pattern iletiKontrol = Pattern.compile("(\\<.*?>)|:|-|;|_|\\*|#|,|'|\"|!|%|$|^|\\?|&| ");
    public static final TreeMap<Integer, Integer> DivineTitleList;
    public static final TreeMap<Integer, Integer> BootTitleList;
    public static final TreeMap<Integer, Integer> HardTitleList;
    public static final TreeMap<Integer, Integer> ShopTitleList;
    public static final TreeMap<Integer, Integer> SaveTitleList;
    public static final TreeMap<Integer, Integer> FirstTitleList;
    public static final TreeMap<Integer, Integer> DefilanteTitleList;
    public static final TreeMap<Integer, Integer> CheeseTitleList;
    public static final TreeMap<Integer, Integer> LevelTitleList;
    public static final Map<Integer, Integer> BadgeList;
    public static final Map<String, Integer> T_Tokens;
    public static final ArrayList<Integer> dontcount = new ArrayList<>();
    public static final Map<String, int[]> LogList;
    public static final Map<Integer, int[]> ShamanShopList;
    public static final HashSet<String> Forbidden_names;
    public static final HashMap<String, Integer> kazananlar = new HashMap<>();
    public final static Logger log = Logger.getGlobal();
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    static final String DB_URL = "jdbc:mysql://localhost/ranking?useSSL=false";
    static final String USER = "root";
    static final String PASS = "";
    private static final String[] EMPTY_ARRAY = new String[0];
    public static Map<Integer, ArrayList<Rekor>> Rekorlar = new HashMap<>();
    public static HashMap<String, Integer> rekorStats = new HashMap<>();
    public static Map<String, Yasaklama> yasaklamalar = new ConcurrentHashMap<>();
    public static Map<String, Yasaklama> susturmalar = new ConcurrentHashMap<>();
    public static Comparator<Map.Entry<String, Raporlar>> rapor_sirala = (o1, o2) -> {
        Raporlar v2 = o1.getValue();
        Raporlar v1 = o2.getValue();
        int res = Integer.compare(v1.arr.size(), v2.arr.size());
        if (res == 0) {
            return v1.surev < v2.surev ? -1 : 1;
        } else {
            return res;
        }
    };
    public static Comparator<Map.Entry<String, Raporlar>> rapor_sirala2 = (o1, o2) -> {
        Raporlar v2 = o1.getValue();
        Raporlar v1 = o2.getValue();
        return v1.surev < v2.surev ? -1 : 1;
    };
    public static MFServer instance;
    public static ScheduledExecutorService executor = Executors.newScheduledThreadPool(1500);
    public static ScheduledExecutorService loginExecutor = Executors.newScheduledThreadPool(20);
    public static ScheduledExecutorService ListenExecutor = Executors.newScheduledThreadPool(15);
    public static ScheduledExecutorService RoomExecutor = Executors.newScheduledThreadPool(150);
    public static ScheduledExecutorService RoomExecutorOfficial = Executors.newScheduledThreadPool(300);
    public static ScheduledExecutorService TengriExecutor = Executors.newScheduledThreadPool(2);
    public static ScheduledExecutorService logExecutor = Executors.newScheduledThreadPool(4);
    public static String x01 = new String(new int[]{1}, 0, 1);
    public static int[] LEVEL = new int[]{250, 32, 0};
    public static Channel channel;
    public static List<Integer> zodiacTitles = Ints.asList(3187, 3186, 3185, 3184, 3183, 3182, 3181, 3180, 3179, 3178, 3177, 3176);
    public static int[] mg_list = new int[]{1, 2, 3, 8, 9, 10, 11, 16, 18, 77};
    public static HashSet<Integer> snormal = new HashSet<>();
    public static HashSet<Integer> forbidden_survivor = new HashSet<>();
    public static HashSet<Integer> smult = new HashSet<>();
    public static HashSet<Integer> sfull = new HashSet<>();
    public static DataSource DS;
    public static int[] badForVillage = new int[]{11, 20, 1, 5};
    public static int[] beforeFirst = new int[]{1, 5, 6, 8, 11, 20, 25, 26, 2250};
    public static Integer[] a = new Integer[]{124, 125, 127, 126, 120, 121, 122, 123, 221, 222, 223, 224, 225, 226, 1024};
    public static Integer[] a2 = new Integer[]{170, 158, 134, 132, 131, 73, 71, 65, 64, 59, 58, 57, 46, 45, 35, 34, 33,
            30, 29, 28};
    public static Integer[] a3 = new Integer[]{1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019};
    public static HashSet<Integer> doubleMaps = new HashSet<>();
    public static HashSet<Integer> types0 = new HashSet<>();
    public static HashSet<Integer> types1 = new HashSet<>();
    public static HashSet<Integer> types2 = new HashSet<>();
    public static HashSet<Integer> types3 = new HashSet<>();
    public static HashSet<Integer> forbiddens = new HashSet<>();
    public static HashSet<Integer> useAnyWay = new HashSet<>();
    public static HashSet<Integer> dont = new HashSet<>();
    public static HashSet<Integer> dont2 = new HashSet<>();
    public static Comparator<Map.Entry<String, Integer>> byMapValues = Comparator.comparing(Map.Entry::getValue);
    public static Comparator<Kayit> byTime = Comparator.comparing((Kayit left) -> left.surev);
    public static Comparator<Map.Entry<Integer, Rutbe>> byRutbe = Comparator
            .comparing((Map.Entry<Integer, Rutbe> entry) -> entry.getValue().order);
    public static ArrayList<Etkinlik> etkinlikler = new ArrayList<>();
    public static HashMap<Integer, Integer> leveMaps = new HashMap<>();
    public static ArrayList<Integer> agilMaps = new ArrayList<>();
    public static ArrayList<Integer> hiddenWaterMaps = new ArrayList<>();
    public static HashMap<Integer, Integer> leveMaps2 = new HashMap<>();
    public static Pattern patternRep = Pattern.compile("\\<.*?>");
    public static Pattern patternYoutube = Pattern.compile("(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*");
    public static Map<String, Integer> tersLangs;
    final static Pattern lastIntPattern = Pattern.compile("[^0-9]+([0-9]+)$");
    public static AtomicLong candyCount = new AtomicLong();
    public static AtomicBoolean candChecker = new AtomicBoolean();
    public static MongoCollection<Rekorlar> colRekorlar;
    public static MongoCollection<MiniOyun> colMiniOyun;
    public static MongoCollection<Document> colSeq;
    public static MongoCollection<Document> colEnFazla;
    public static MongoCollection<Kullanici> colKullanici;
    public static MongoCollection<Kabile> colKabile;
    public static MongoCollection<KabileGecmisi> colKabileGecmisi;
    public static MongoCollection<Gorev> colGorev;
    public static MongoCollection<Rutbe> colRutbe;
    public static MongoCollection<Document> colUnvanlar;
    public static MongoCollection<Document> colMG;
    public static MongoCollection<IPBan> colIpBan;
    public static MongoCollection<Document> colIP;
    public static MongoCollection<Document> colPermLog;
    public static MongoCollection<Document> colCommandLog;
    public static MongoCollection<Document> colErrorLog;
    public static MongoCollection<Document> colBenchLog;
    public static MongoCollection<Document> colHoleLog;
    public static MongoCollection<Document> colChatLog;
    public static MongoCollection<Document> colPMLog;
    public static MongoCollection<Document> colRefs;
    public static MongoCollection<Document> colSiralama;
    public static MongoCollection<Document> colPacketLog;
    public static MongoCollection<Uyari> colUyarilar;
    public static HashMap<Integer, String[]> fashion = new HashMap<>();
    public static int[] IpHashes = new int[4];
    public static HashMap<Integer, Integer> columnHashes = new HashMap<>();
    public static boolean mylord = false;
    public static boolean debug;
    public static boolean enableEvent = true;
    public static TreeMap<Integer, Integer> RareTitleList;
    public static Map<Integer, Gorev> GorevListesi = new HashMap<>();
    public static Map<Integer, String> KurkResim = new HashMap<>();
    public static Map<Integer, Gorev> GorevListesiLama = new HashMap<>();
    public static HashSet<String> mutedIPS = new HashSet<>();
    public static AtomicBoolean benchLog = new AtomicBoolean(false);
    static HashMap<String, String> emojiList = new HashMap<String, String>();

    public static ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> Anket= new ConcurrentHashMap<>();
    public static ConcurrentHashMap<Integer, ArrayList<String>> AnketCevaplar= new ConcurrentHashMap<>();
    public static LoadingCache<Integer, String[]> isimCache = CacheBuilder.newBuilder()
            .maximumSize(5000)
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build(
                    new CacheLoader<Integer, String[]>() {
                        public String[] load(Integer key) {
                            return _getIsim(key);
                        }
                    });
    public static LoadingCache<String, Kullanici> userCache = CacheBuilder.newBuilder()
            .maximumSize(5000)
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build(
                    new CacheLoader<String, Kullanici>() {
                        public Kullanici load(String key) {
                            return _getUser(key);
                        }
                    });
    public static String announcement;
    public static Map<Integer, Integer> ShamanTitleList;
    public static boolean enableEvent2 = false;

    static {
        ShamanTitleList = new HashMap<>();
        ShamanTitleList.put(1, 3661);
        ShamanTitleList.put(25, 3664);
        ShamanTitleList.put(100, 3663);

    }

    static {
        KurkResim.put(200, "fur_200_img.21866");
        KurkResim.put(201, "fur_200_img.21867");
        KurkResim.put(202, "fur_202_img.18782");
        KurkResim.put(203, "fur_202_img.18786");
        KurkResim.put(204, "fur_204_img.18783");
        KurkResim.put(205, "fur_205_img.18784");
        KurkResim.put(206, "fur_200_img.21869");
        Seq<Gorev> seq = Seq.of(new Gorev("task4", new Document("odulTip", 0).append("unvan", 3235), 36 * 3600, 77, 4));
        seq = seq.append(new Gorev("task9", new Document("odulTip", 0).append("unvan", 3236), 4 * 3600, 150, 9));
        seq = seq.append(new Gorev("task10", new Document("odulTip", 0).append("unvan", 3239), 48 * 3600, 150, 10));
        seq = seq.append(new Gorev("task5", new Document("odulTip", 0).append("unvan", 3238), 48 * 3600, 6, 5));

        seq = seq.append(new Gorev("task7", new Document("img", "chomar_1-png.5501").append("odulTip", 2).append("kurk", 200), 72 * 3600, 666, 7));
        seq = seq.append(new Gorev("task8", new Document("img", "chomar_1-png.5501").append("odulTip", 2).append("kurk", 200), 24 * 3600, 400, 8));
        seq = seq.append(new Gorev("task6", new Document("img", "chomar_1-png.5501").append("odulTip", 2).append("kurk", 200), 24 * 3600, 700, 6));

        seq = seq.append(new Gorev("task0", new Document("img", "chomar_2-png.5502").append("odulTip", 2).append("kurk", 201), 24 * 3600, 77_777, 0));
        seq = seq.append(new Gorev("task1", new Document("img", "tasks_hyena.5419").append("odulTip", 2).append("kurk", 202), 48 * 3600, 40_000, 1));
        seq = seq.append(new Gorev("task2", new Document("img", "tasks_teushan.5420").append("odulTip", 2).append("kurk", 203), 48 * 3600, 99, 2));
        seq = seq.append(new Gorev("task3", new Document("img", "tasks_pikachu.5925").append("odulTip", 2).append("kurk", 204), 3600, 10, 3));


        int evt = 30 * 24 * 3600;
        seq = seq.append(new Gorev("task30", new Document("odulTip", 0).append("unvan", 3243), evt, 100, 30));
        seq = seq.append(new Gorev("task30", new Document("odulTip", 0).append("unvan", 3244), evt, 200, 30));
        seq = seq.append(new Gorev("task30", new Document("odulTip", 1).append("rozet", 246).append("img", "x_246-png.6162"), evt, 220, 30));

        seq = seq.append(new Gorev("task31", new Document("odulTip", 0).append("unvan", 3245), evt, 150, 31));
        seq = seq.append(new Gorev("task31", new Document("odulTip", 0).append("unvan", 3246), evt, 300, 31));
        seq = seq.append(new Gorev("task31", new Document("odulTip", 1).append("rozet", 247).append("img", "x_247-png.6163"), evt, 320, 31));

        seq = seq.append(new Gorev("task32", new Document("odulTip", 0).append("unvan", 3247), evt, 30, 32));

        seq = seq.append(new Gorev("task33", new Document("odulTip", 2).append("kurk", 205).append("img", "tasks_easter-png.6161"), evt, 7, 33));

        seq = seq.append(new Gorev("task12", new Document("unvan", 3067).append("odulTip", 0), 7 * 3600, 63, 12));
        seq = seq.append(new Gorev("task13", new Document("unvan", 3271).append("odulTip", 0), 24 * 3600, 20, 13));
        seq = seq.append(new Gorev("task14", new Document("unvan", 3270).append("odulTip", 0), 24 * 3600 * 21, 1_000, 14));
        seq = seq.append(new Gorev("task15", new Document("unvan", 3272).append("odulTip", 0), 24 * 3600 * 7, 14, 15));
        int evt2 = 3600 * 24 * 3;
        seq = seq.append(new Gorev("task34", new Document("odulTip", 0).append("unvan", 3295), evt2, 200, 34));
        seq = seq.append(new Gorev("task35", new Document("odulTip", 0).append("unvan", 3296), evt2, 300, 35));
        seq = seq.append(new Gorev("task36", new Document("odulTip", 0).append("unvan", 3297), evt2, 80, 36));
        seq = seq.append(new Gorev("task37", new Document("odulTip", 0).append("unvan", 3303), evt2, 90, 37));

        //seq=seq.append(new Gorev("task31",new Document("img","tasks_pikachu.5925").append("odulTip",2).append("kurk",204),3600,10,3));
        //seq=seq.append(new Gorev("task31",new Document("img","tasks_pikachu.5925").append("odulTip",2).append("kurk",204),3600,10,3));
        //seq=seq.append(new Gorev("task31",new Document("img","tasks_pikachu.5925").append("odulTip",2).append("kurk",204),3600,10,3));
        seq.forEach((Gorev gorev) -> {
            GorevListesi.put(Arrays.hashCode(new int[]{gorev.tip, gorev.son, gorev.bitis}), gorev);
        });
        //  GorevListesi.put(Arrays.hashCode())
        ArrayList<Gorev> gorevs = new ArrayList<>();
        gorevs.add(new Gorev("task0", new Document("odulTip", 0).append("unvan", 3445), 4 * 3600, 15, 0));
        gorevs.add(new Gorev("task2", new Document("odulTip", 0).append("unvan", 3448), 12 * 3600, 5, 2));
        gorevs.add(new Gorev("task3", new Document("odulTip", 0).append("unvan", 3447), 6 * 3600, 5, 3));
        gorevs.add(new Gorev("task9", new Document("odulTip", 0).append("unvan", 3449), 24 * 3600, 100, 9));
        gorevs.add(new Gorev("task11", new Document("odulTip", 0).append("unvan", 3446), 6 * 3600, 30, 11));
        gorevs.add(new Gorev("task6", new Document("odulTip", 0).append("unvan", 3450), 12 * 3600, 100, 6));
        gorevs.forEach((Gorev gorev) -> {
            GorevListesiLama.put(Arrays.hashCode(new int[]{gorev.tip, gorev.son, gorev.bitis}), gorev);
        });
    }

    static {
        /*   fashion.put(128, new String[]{"22;145_180e0a+30201a+536d45,0,26_536d45+3c4f32+25321e+ab617b+853953+bf465d,38_853953+ba8a78,28_38251e,0,22_2c3a25+633f32+38251e+ab617b+853953+853953+1b110d,1"});
        fashion.put(129, "83;145_fec49a+fa5642+a0bd35,9_fa5642,26_a0bd35+a0bd35+a0bd35+fa5642+fa5642+fa5642,5_e25d42,28_a0bd35,0,0,0");
        fashion.put(130, "7;48_23520e+bdbdbd,8,0,4,5,0,0,0");
        fashion.put(131, "84;130_965F00+EDA567+3C3C3C,5_1B1B1B,0,29_EB6B0E+71A240,9_C56100+CCCCCC,14_524436,0,4,0,0");*/
    }

    static {
//PL
        kazananlar.put("Kmav", 3510);
        kazananlar.put("Natrox", 3511);
        kazananlar.put("Arcticure", 3511);
//ES
        kazananlar.put("Psyzk", 3510);
        kazananlar.put("+assa", 3511);
        // kazananlar.put("Qeiboard", 3511);
//BR
        kazananlar.put("Snowfloky", 3510);
        //  kazananlar.put("Noiite", 3511);
        // kazananlar.put("Superatohhh", 3511);
//EN
        kazananlar.put("Sungoh", 3510);
        kazananlar.put("Whims", 3511);
        //  kazananlar.put("Savaqe", 3511);

//HU
        kazananlar.put("Ketty66", 3510);
        // kazananlar.put("Krumplibogar", 3511);
//RO
        kazananlar.put("+empathetic", 3510);
        kazananlar.put("Kyron", 3511);
//RU
        kazananlar.put("Delarensy", 3510);
        // kazananlar.put("Magiccherry", 3511);
//AR
        kazananlar.put("Kadida16", 3510);
    }

    static {
        Etkinlik etkinlik = new Etkinlik();
        etkinlik.tarih = 1496598655;
        etkinlik.rozetler.put(221, 15);
        etkinlik.rozetler.put(222, 15);
        etkinlik.rozetler.put(184, 10);
        etkinlik.kalintilar.add(779);
        etkinlik.kalintilar.add(778);
        etkinlikler.add(etkinlik);
        Etkinlik etkinlik2 = new Etkinlik();
        etkinlik2.tarih = 1500310041;
        etkinlik2.rozetler.put(223, 60);
        etkinlik2.id = 24;
        etkinlikler.add(etkinlik2);
        Etkinlik etkinlik3 = new Etkinlik();
        etkinlik3.tarih = 1502242682;
        etkinlik3.rozetler.put(224, 20);
        etkinlik3.rozetler.put(225, 20);
        etkinlik3.rozetler.put(226, 20);
        etkinlikler.add(etkinlik3);
        Etkinlik etkinlik4 = new Etkinlik();
        etkinlik4.id = 23;
        etkinlik4.tarih = 1506880148;
        etkinlik4.rozetler.put(227, 20);
        etkinlik4.rozetler.put(228, 20);
        etkinlik4.rozetler.put(229, 20);
        etkinlikler.add(etkinlik4);
        Etkinlik etkinlik5 = new Etkinlik();
        etkinlik5.tarih = 1510779468;
        etkinlik5.id = 24;
        etkinlik5.rozetler.put(231, 20);
        etkinlik5.rozetler.put(232, 30);
        etkinlik5.rozetler.put(233, 30);
        etkinlikler.add(etkinlik5);
        Etkinlik etkinlik6 = new Etkinlik();
        etkinlik6.tarih = 1518475377;
        etkinlik6.id = 9;
        etkinlik6.rozetler.put(244, 50);
        etkinlikler.add(etkinlik6);
        Etkinlik etkinlik7 = new Etkinlik();
        etkinlik7.tarih = 1522635468;
        etkinlik7.id = 23;
        etkinlik7.rozetler.put(247, 30);
        etkinlik7.rozetler.put(248, 50);
        etkinlikler.add(etkinlik7);
        Etkinlik etkinlik8 = new Etkinlik();
        etkinlik8.tarih = 1532946413;
        etkinlik8.id = 40;
        etkinlik8.rozetler.put(249, 10);
        etkinlik8.rozetler.put(250, 50);
        etkinlik8.rozetler.put(251, 50);
        etkinlik8.kalintilar.add(784);
        etkinlikler.add(etkinlik8);


        Etkinlik etkinlik9 = new Etkinlik();
        etkinlik9.id = 23;
        etkinlik9.tarih = 1539106643;
        etkinlik9.rozetler.put(252, 20);
        etkinlik9.kalintilar.add(2202);
        etkinlik9.kalintilar.add(2204);
        etkinlikler.add(etkinlik9);
        Etkinlik etkinlik10 = new Etkinlik();
        etkinlik10.id = 24;
        etkinlik10.tarih = 1545105896;
        etkinlik10.rozetler.put(212, 20);
        etkinlik10.rozetler.put(211, 40);
        etkinlik10.rozetler.put(210, 60);
        etkinlikler.add(etkinlik10);
        Etkinlik etkinlik11 = new Etkinlik();
        etkinlik11.id = 25;
        etkinlik11.tarih = 1555781698;
        etkinlik11.kalintilar.add(2387);
        etkinlik11.rozetler.put(1005, 20);
        etkinlikler.add(etkinlik11);
        Etkinlik etkinlik12 = new Etkinlik();
        etkinlik12.id = 26;
        etkinlik12.tarih = 1563054962;
        etkinlik12.kalintilar.add(2257);
        etkinlik12.rozetler.put(1007, 40);
        etkinlikler.add(etkinlik12);
    }

    static {
        leveMaps.put(48, 368369);
        leveMaps.put(368319, 368326);
        leveMaps.put(316175, 368383);
        leveMaps.put(107445, 556278);
        leveMaps.put(126236, 556279);
        //leveMaps.put(333896, 368404);
        leveMaps.put(333896, 556280);
        leveMaps.put(273954, 556281);
       // leveMaps.put(266830, 368412);
        leveMaps.put(119688, 368417);
        leveMaps.put(133376, 368421);
        leveMaps.put(434434, 434428);
        leveMaps.put(434438, 434437);
        leveMaps.put(434445, 434444);
        leveMaps.put(434450, 434449);
        leveMaps.put(434458, 434457);
        leveMaps.put(434469, 434468);
        leveMaps.put(119595, 107184);
        leveMaps.put(133430, 304340);
        leveMaps.put(260845, 323607);
        leveMaps.put(127637, 325615);
        leveMaps.put(126151, 325618);
        leveMaps.put(127459, 325620);
        leveMaps.put(144041, 326737);
        leveMaps.put(124321, 321552);
        leveMaps.put(152446, 340167);
        leveMaps.put(122934, 339483);
        leveMaps.put(120673, 339503);
        leveMaps.put(296281, 339910);
        leveMaps.put(113809, 315555);
        leveMaps.put(296015, 305836);


        leveMaps.put(525631, 537770);
        leveMaps.put(265771, 537771);
        leveMaps.put(270098, 537772);
        leveMaps.put(138084, 537773);
        leveMaps.put(391573, 537774);
        leveMaps.put(126933, 537777);
        leveMaps.put(408063, 537778);
        leveMaps.put(376944, 537780);
        leveMaps.put(266018, 537781);
        leveMaps.put(373379, 537782);
        leveMaps.put(132210, 537783);
        leveMaps.put(102965, 537784);
        leveMaps.put(374969, 537785);
        leveMaps.put(376085, 537786);
        leveMaps.put(108607, 537787);
        leveMaps.put(132085, 537788);
        leveMaps.put(400622, 537789);
        leveMaps.put(316849, 537647);
        leveMaps.put(120581, 537648);
        leveMaps.put(130179, 537649);
        leveMaps.put(366256, 537650);
        leveMaps.put(370214, 537651);
        leveMaps.put(406787, 537652);

        hiddenWaterMaps.add(461100);
        hiddenWaterMaps.add(461110);
        hiddenWaterMaps.add(461111);
        hiddenWaterMaps.add(461114);
        hiddenWaterMaps.add(461119);
        hiddenWaterMaps.add(461124);
        hiddenWaterMaps.add(461128);
        hiddenWaterMaps.add(461139);
        hiddenWaterMaps.add(461140);
        hiddenWaterMaps.add(461142);
        hiddenWaterMaps.add(461157);
        hiddenWaterMaps.add(461159);
        hiddenWaterMaps.add(460120);
        hiddenWaterMaps.add(460126);
        hiddenWaterMaps.add(460128);
        hiddenWaterMaps.add(460130);
        hiddenWaterMaps.add(460246);
        hiddenWaterMaps.add(460265);
        hiddenWaterMaps.add(460253);
        hiddenWaterMaps.add(460255);
        hiddenWaterMaps.add(460258);

        agilMaps.add(316175);
        agilMaps.add(139005);
        agilMaps.add(304849);
        agilMaps.add(322017);
        agilMaps.add(131297);
        agilMaps.add(336638);
        agilMaps.add(100113);
        agilMaps.add(342385);
        agilMaps.add(408035);
        agilMaps.add(334567);
        agilMaps.add(440746);
        agilMaps.add(307950);
        agilMaps.add(338319);
        agilMaps.add(409747);
        agilMaps.add(376799);


        agilMaps.add(534172);
        agilMaps.add(534174);
        agilMaps.add(534175);
        agilMaps.add(534176);
        agilMaps.add(534177);
        agilMaps.add(534178);
        agilMaps.add(534179);
        agilMaps.add(534180);
        agilMaps.add(534181);
        agilMaps.add(534182);
        agilMaps.add(534183);
        agilMaps.add(534184);
        agilMaps.add(534185);
        agilMaps.add(550665);
        agilMaps.add(550666);
        agilMaps.add(550667);
        agilMaps.add(550668);
        agilMaps.add(550669);
        agilMaps.add(550670);
        agilMaps.add(550671);
        agilMaps.add(550672);
        agilMaps.add(550673);
        agilMaps.add(550674);
        agilMaps.add(550675);
        agilMaps.add(550676);
        agilMaps.add(550677);

        for (Map.Entry<Integer, Integer> entry : leveMaps.entrySet()) {
            leveMaps2.put(entry.getValue(), entry.getKey());
        }
    }

    static {
        BadgeList = new HashMap<>();
        BadgeList.put(2270, 152);
        BadgeList.put(2211, 19);
        BadgeList.put(2212, 24);
        BadgeList.put(2213, 60);
        BadgeList.put(2214, 23);
        BadgeList.put(2216, 46);
        BadgeList.put(2217, 22);
        BadgeList.put(2218, 10);
        BadgeList.put(2219, 12);
        BadgeList.put(2220, 32);
        BadgeList.put(2223, 26);
        BadgeList.put(2224, 21);
        BadgeList.put(2225, 56);
        BadgeList.put(2277, 167);
        BadgeList.put(2227, 2);
        BadgeList.put(2228, 8);
        BadgeList.put(2229, 13);
        BadgeList.put(2230, 14);
        BadgeList.put(2231, 15);
        BadgeList.put(2232, 20);
        BadgeList.put(2234, 27);
        BadgeList.put(2271, 155);
        BadgeList.put(2236, 36);
        BadgeList.put(2238, 41);
        BadgeList.put(2239, 43);
        BadgeList.put(2241, 44);
        BadgeList.put(2242, 47);
        BadgeList.put(2243, 35);
        BadgeList.put(2245, 51);
        BadgeList.put(2246, 52);
        BadgeList.put(2247, 53);
        BadgeList.put(2248, 61);
        BadgeList.put(2249, 63);
        BadgeList.put(2250, 66);
        BadgeList.put(2252, 67);
        BadgeList.put(2253, 68);
        BadgeList.put(2254, 70);
        BadgeList.put(2255, 72);
        BadgeList.put(2256, 128);
        BadgeList.put(2257, 135);
        BadgeList.put(2258, 136);
        BadgeList.put(2259, 137);
        BadgeList.put(2260, 138);
        BadgeList.put(2261, 140);
        BadgeList.put(2262, 141);
        BadgeList.put(2263, 143);
        BadgeList.put(2264, 146);
        BadgeList.put(2265, 148);
        BadgeList.put(2267, 149);
        BadgeList.put(2268, 150);
        BadgeList.put(2269, 151);
        BadgeList.put(222, 4);
        BadgeList.put(227, 49);
        BadgeList.put(223, 31);
        BadgeList.put(2272, 156);
        BadgeList.put(2273, 157);
        BadgeList.put(226, 11);
        BadgeList.put(2275, 160);
        BadgeList.put(228, 3);
        BadgeList.put(229, 5);
        BadgeList.put(2278, 171);
        BadgeList.put(2279, 173);
        BadgeList.put(2280, 175);
        BadgeList.put(2281, 176);
        BadgeList.put(2282, 177);
        BadgeList.put(2283, 178);
        BadgeList.put(2284, 179);
        BadgeList.put(2285, 180);
        BadgeList.put(2286, 183);
        BadgeList.put(2287, 185);
        BadgeList.put(2288, 186);
        BadgeList.put(2289, 187);
        BadgeList.put(2290, 189);
        BadgeList.put(2291, 191);
        BadgeList.put(2292, 192);
        BadgeList.put(2293, 194);
        BadgeList.put(2294, 195);
        BadgeList.put(2295, 196);
        BadgeList.put(2296, 197);
        BadgeList.put(2297, 199);
        BadgeList.put(2298, 200);
        BadgeList.put(3492, 201);
        BadgeList.put(230101, 204);
        BadgeList.put(230102, 205);
        BadgeList.put(230103, 206);
        BadgeList.put(230104, 207);
        BadgeList.put(230105, 208);
         /*BadgeList.put(230106, 210);
       BadgeList.put(230107, 211);
        BadgeList.put(230108, 212);*/
        BadgeList.put(230109, 213);
        BadgeList.put(230110, 214);
        BadgeList.put(230111, 215);
        BadgeList.put(230112, 216);
        BadgeList.put(230113, 217);
        BadgeList.put(230114, 220);
        BadgeList.put(230115, 222);
        BadgeList.put(230116, 223);
        BadgeList.put(230117, 224);
        BadgeList.put(230119, 226);
        BadgeList.put(230120, 227);
        BadgeList.put(230121, 228);
        BadgeList.put(230122, 229);
        BadgeList.put(230123, 231);
        BadgeList.put(230124, 232);
        BadgeList.put(230125, 233);
        BadgeList.put(230126, 234);
        BadgeList.put(230127, 235);
        BadgeList.put(230128, 236);
        BadgeList.put(230129, 237);
        BadgeList.put(230130, 238);
        BadgeList.put(230131, 239);
        BadgeList.put(230132, 241);


        BadgeList.put(230100, 235);
        BadgeList.put(230202, 245);
        BadgeList.put(230205, 248);//hyena
        BadgeList.put(230208, 249);
        BadgeList.put(230210, 252);
        BadgeList.put(230211, 253);
    }

    static {
        DivineTitleList = new TreeMap<>();
        DivineTitleList.put(2000, 326);
        DivineTitleList.put(2500, 3527);
        DivineTitleList.put(4000, 327);
        DivineTitleList.put(40000, 334);
        DivineTitleList.put(25000, 332);
        DivineTitleList.put(500, 324);
        DivineTitleList.put(20000, 331);
        DivineTitleList.put(7000, 328);
        DivineTitleList.put(15000, 330);
        DivineTitleList.put(10000, 329);
        DivineTitleList.put(30000, 333);
        DivineTitleList.put(2001, 3575);

    }

    static {
        BootTitleList = new TreeMap<>();
        BootTitleList.put(1, 256);
        BootTitleList.put(3, 257);
        BootTitleList.put(5, 258);
        BootTitleList.put(7, 259);
        BootTitleList.put(10, 260);
        BootTitleList.put(15, 261);
        BootTitleList.put(20, 262);
        BootTitleList.put(25, 263);
        BootTitleList.put(30, 264);
        BootTitleList.put(40, 265);
        BootTitleList.put(50, 266);
        BootTitleList.put(60, 267);
        BootTitleList.put(70, 268);
        BootTitleList.put(80, 269);
        BootTitleList.put(90, 270);
        BootTitleList.put(100, 271);
        BootTitleList.put(120, 272);
        BootTitleList.put(140, 273);
        BootTitleList.put(160, 274);
        BootTitleList.put(180, 275);
        BootTitleList.put(200, 276);
        BootTitleList.put(250, 277);
        BootTitleList.put(300, 278);
        BootTitleList.put(350, 279);
        BootTitleList.put(400, 280);
        BootTitleList.put(500, 281);
        BootTitleList.put(600, 282);
        BootTitleList.put(700, 283);
        BootTitleList.put(800, 284);
        BootTitleList.put(900, 285);
        BootTitleList.put(1000, 286);
        BootTitleList.put(1100, 3008);
        BootTitleList.put(2000, 3009);
        BootTitleList.put(5000, 3039);
        BootTitleList.put(3500, 3060);
        BootTitleList.put(666, 3543);
        BootTitleList.put(404, 3550);
        BootTitleList.put(1500, 3542);
        BootTitleList.put(251, 3566);
        BootTitleList.put(301, 3567);
        BootTitleList.put(540, 3568);
        BootTitleList.put(760, 3569);
        BootTitleList.put(1001, 3570);
    }

    static {
        HardTitleList = new TreeMap<>();
        HardTitleList.put(500, 213);
        HardTitleList.put(2000, 214);
        HardTitleList.put(4000, 215);
        HardTitleList.put(7000, 216);
        HardTitleList.put(10000, 217);
        HardTitleList.put(14000, 218);
        HardTitleList.put(18000, 219);
        HardTitleList.put(22000, 220);
        HardTitleList.put(26000, 221);
        HardTitleList.put(30000, 222);
        HardTitleList.put(40000, 223);
        HardTitleList.put(501, 3025);
        HardTitleList.put(666, 3026);
        HardTitleList.put(777, 3027);
        HardTitleList.put(15000, 3058);
        HardTitleList.put(3000, 3573);
        HardTitleList.put(7001, 3574);
    }

    static {
        ShopTitleList = new TreeMap<>();
        ShopTitleList.put(1, 115);
        ShopTitleList.put(2, 116);
        ShopTitleList.put(5, 117);
        ShopTitleList.put(10, 118);
        ShopTitleList.put(15, 119);
        ShopTitleList.put(20, 120);
        ShopTitleList.put(25, 121);
        ShopTitleList.put(30, 122);
        ShopTitleList.put(35, 123);
        ShopTitleList.put(40, 124);
        ShopTitleList.put(45, 125);
        ShopTitleList.put(50, 126);
        ShopTitleList.put(51, 3020);
        ShopTitleList.put(60, 3021);
        ShopTitleList.put(70, 3022);
        ShopTitleList.put(77, 3023);
        ShopTitleList.put(100, 3024);
        ShopTitleList.put(200, 3036);
    }

    static {
        SaveTitleList = new TreeMap<>();
        SaveTitleList.put(10, 1);
        SaveTitleList.put(100, 2);
        SaveTitleList.put(1000, 3);
        SaveTitleList.put(2000, 4);
        SaveTitleList.put(3000, 13);
        SaveTitleList.put(4000, 14);
        SaveTitleList.put(5000, 15);
        SaveTitleList.put(6000, 16);
        SaveTitleList.put(7000, 17);
        SaveTitleList.put(8000, 18);
        SaveTitleList.put(9000, 19);
        SaveTitleList.put(10000, 20);
        SaveTitleList.put(11000, 21);
        SaveTitleList.put(12000, 22);
        SaveTitleList.put(13000, 23);
        SaveTitleList.put(14000, 24);
        SaveTitleList.put(15000, 25);
        SaveTitleList.put(16000, 94);
        SaveTitleList.put(18000, 95);
        SaveTitleList.put(20000, 96);
        SaveTitleList.put(22000, 97);
        SaveTitleList.put(24000, 98);
        SaveTitleList.put(26000, 99);
        SaveTitleList.put(28000, 100);
        SaveTitleList.put(30000, 101);
        SaveTitleList.put(35000, 102);
        SaveTitleList.put(40000, 103);
        SaveTitleList.put(45000, 104);
        SaveTitleList.put(50000, 105);
        SaveTitleList.put(55000, 106);
        SaveTitleList.put(60000, 107);
        SaveTitleList.put(65000, 108);
        SaveTitleList.put(70000, 109);
        SaveTitleList.put(75000, 110);
        SaveTitleList.put(80000, 111);
        SaveTitleList.put(85000, 112);
        SaveTitleList.put(90000, 113);
        SaveTitleList.put(100000, 200);
        SaveTitleList.put(140000, 114);
        SaveTitleList.put(1001, 3001);
        SaveTitleList.put(1500, 3002);
        SaveTitleList.put(40001, 3038);
        SaveTitleList.put(5001, 3571);
        SaveTitleList.put(7001, 3572);

    }

    static {

        DefilanteTitleList = new TreeMap<>();
        DefilanteTitleList.put(1_000, 3535);
        DefilanteTitleList.put(3_000, 3619);
        DefilanteTitleList.put(5_000, 3618);
    }

    static {
        /*
        FirstTitleList.put(200000, 3013);
		FirstTitleList.put(13000, 3012);
		FirstTitleList.put(888, 3011);
		FirstTitleList.put(1, 9);
		FirstTitleList.put(10, 10);
		FirstTitleList.put(100, 11);
		FirstTitleList.put(200, 12);
		FirstTitleList.put(300, 42);
		FirstTitleList.put(400, 43);
		FirstTitleList.put(500, 44);
		FirstTitleList.put(600, 45);
		FirstTitleList.put(700, 46);
		FirstTitleList.put(800, 47);
		FirstTitleList.put(900, 48);
		FirstTitleList.put(1000, 49);
		FirstTitleList.put(1100, 50);
		FirstTitleList.put(1200, 51);
		FirstTitleList.put(1400, 52);
		FirstTitleList.put(1600, 53);
		FirstTitleList.put(1800, 54);
		FirstTitleList.put(2000, 55);
		FirstTitleList.put(2200, 56);
		FirstTitleList.put(2400, 57);
		FirstTitleList.put(2600, 58);
		FirstTitleList.put(2800, 59);
		FirstTitleList.put(3000, 60);
		FirstTitleList.put(3200, 61);
		FirstTitleList.put(3400, 62);
		FirstTitleList.put(3600, 63);
		FirstTitleList.put(3800, 64);
		FirstTitleList.put(4000, 65);
		FirstTitleList.put(4500, 66);
		FirstTitleList.put(5000, 67);
		FirstTitleList.put(5500, 68);
		FirstTitleList.put(6000, 69);
		FirstTitleList.put(7000, 231);
		FirstTitleList.put(8000, 232);
		FirstTitleList.put(9000, 233);
		FirstTitleList.put(10000, 70);
		FirstTitleList.put(12000, 224);
		FirstTitleList.put(14000, 225);
		FirstTitleList.put(16000, 226);
		FirstTitleList.put(18000, 227);
		FirstTitleList.put(20000, 202);
		FirstTitleList.put(25000, 228);
		FirstTitleList.put(30000, 229);
		FirstTitleList.put(35000, 230);
		FirstTitleList.put(40000, 71);
		FirstTitleList.put(17000, 3010);
		FirstTitleList.put(777000, 3043);
		FirstTitleList.put(10001, 3050);
		FirstTitleList.put(10002, 3051);
		FirstTitleList.put(1000000, 3052);*/
        FirstTitleList = new TreeMap<>();
        FirstTitleList.put(400000, 3013);
        FirstTitleList.put(26000, 3012);
        FirstTitleList.put(1776, 3011);
        FirstTitleList.put(2, 9);
        FirstTitleList.put(20, 10);
        FirstTitleList.put(200, 11);
        FirstTitleList.put(400, 12);
        FirstTitleList.put(600, 42);
        FirstTitleList.put(800, 43);
        FirstTitleList.put(1000, 44);
        FirstTitleList.put(1200, 45);
        FirstTitleList.put(1400, 46);
        FirstTitleList.put(1600, 47);
        FirstTitleList.put(1800, 48);
        FirstTitleList.put(2000, 49);
        FirstTitleList.put(2200, 50);
        FirstTitleList.put(2400, 51);
        FirstTitleList.put(2800, 52);
        FirstTitleList.put(3200, 53);
        FirstTitleList.put(3600, 54);
        FirstTitleList.put(4000, 55);
        FirstTitleList.put(4400, 56);
        FirstTitleList.put(4800, 57);
        FirstTitleList.put(5200, 58);
        FirstTitleList.put(5600, 59);
        FirstTitleList.put(6000, 60);
        FirstTitleList.put(6400, 61);
        FirstTitleList.put(6800, 62);
        FirstTitleList.put(7200, 63);
        FirstTitleList.put(7600, 64);
        FirstTitleList.put(8000, 65);
        FirstTitleList.put(9000, 66);
        FirstTitleList.put(10000, 67);
        FirstTitleList.put(11000, 68);
        FirstTitleList.put(12000, 69);
        FirstTitleList.put(14000, 231);
        FirstTitleList.put(16000, 232);
        FirstTitleList.put(18000, 233);
        FirstTitleList.put(20000, 70);
        FirstTitleList.put(24000, 224);
        FirstTitleList.put(28000, 225);
        FirstTitleList.put(32000, 226);
        FirstTitleList.put(36000, 227);
        FirstTitleList.put(40000, 202);
        FirstTitleList.put(50000, 228);
        FirstTitleList.put(60000, 229);
        FirstTitleList.put(70000, 230);
        FirstTitleList.put(80000, 71);
        FirstTitleList.put(34000, 3010);
        FirstTitleList.put(1554000, 3043);
        FirstTitleList.put(20002, 3050);
        FirstTitleList.put(20004, 3051);
        FirstTitleList.put(2000000, 3052);
        FirstTitleList.put(5000000, 3237);


        FirstTitleList.put(500_000, 3353);
        FirstTitleList.put(650_000, 3354);
        FirstTitleList.put(800_000, 3355);
        FirstTitleList.put(1000_000, 3356);
        FirstTitleList.put(1300_000, 3357);
        FirstTitleList.put(1777_000, 3358);
        FirstTitleList.put(2400_000, 3359);
        FirstTitleList.put(2800_000, 3360);
        FirstTitleList.put(3200_000, 3361);
        FirstTitleList.put(3600_000, 3362);
        FirstTitleList.put(4000_000, 3363);
        FirstTitleList.put(4500_000, 3364);
        FirstTitleList.put(6000_000, 3365);
        FirstTitleList.put(7_500_000, 3366);
        FirstTitleList.put(10_000_000, 3367);


        FirstTitleList.put(555555, 3564);
        FirstTitleList.put(800_001, 3565);
        FirstTitleList.put(1_300_001, 3556);

    }

    static {

        LevelTitleList = new TreeMap<>();
        LevelTitleList.put(625, 3650);
        LevelTitleList.put(450, 3649);
        LevelTitleList.put(600, 3648);
        LevelTitleList.put(500, 3647);
        LevelTitleList.put(505, 3646);
        LevelTitleList.put(405, 3645);
        LevelTitleList.put(325, 3644);
        LevelTitleList.put(525, 3643);
        LevelTitleList.put(455, 3642);
        LevelTitleList.put(375, 3641);
        LevelTitleList.put(295, 3640);
        LevelTitleList.put(425, 3639);
        LevelTitleList.put(350, 3638);
        LevelTitleList.put(300, 3637);
        LevelTitleList.put(275, 3636);
    }

    static {
        /*
        CheeseTitleList.put(200000, 3016);
		CheeseTitleList.put(210000, 3015);
		CheeseTitleList.put(100000, 3014);
		CheeseTitleList.put(0, 0);
		CheeseTitleList.put(5, 5);
		CheeseTitleList.put(20, 6);
		CheeseTitleList.put(100, 7);
		CheeseTitleList.put(200, 8);
		CheeseTitleList.put(300, 35);
		CheeseTitleList.put(400, 36);
		CheeseTitleList.put(500, 37);
		CheeseTitleList.put(600, 26);
		CheeseTitleList.put(700, 27);
		CheeseTitleList.put(800, 28);
		CheeseTitleList.put(900, 29);
		CheeseTitleList.put(1000, 30);
		CheeseTitleList.put(1100, 31);
		CheeseTitleList.put(1200, 32);
		CheeseTitleList.put(1300, 33);
		CheeseTitleList.put(1400, 34);
		CheeseTitleList.put(1500, 38);
		CheeseTitleList.put(1600, 39);
		CheeseTitleList.put(1700, 40);
		CheeseTitleList.put(1800, 41);
		CheeseTitleList.put(2000, 72);
		CheeseTitleList.put(2300, 73);
		CheeseTitleList.put(2700, 74);
		CheeseTitleList.put(3200, 75);
		CheeseTitleList.put(3800, 76);
		CheeseTitleList.put(4600, 77);
		CheeseTitleList.put(6000, 78);
		CheeseTitleList.put(7000, 79);
		CheeseTitleList.put(8000, 80);
		CheeseTitleList.put(9001, 81);
		CheeseTitleList.put(10000, 82);
		CheeseTitleList.put(14000, 83);
		CheeseTitleList.put(18000, 84);
		CheeseTitleList.put(22000, 85);
		CheeseTitleList.put(26000, 86);
		CheeseTitleList.put(30000, 87);
		CheeseTitleList.put(34000, 88);
		CheeseTitleList.put(38000, 89);
		CheeseTitleList.put(42000, 90);
		CheeseTitleList.put(46000, 91);
		CheeseTitleList.put(50000, 92);
		CheeseTitleList.put(55000, 234);
		CheeseTitleList.put(60000, 235);
		CheeseTitleList.put(65000, 236);
		CheeseTitleList.put(70000, 237);
		CheeseTitleList.put(75000, 238);
		CheeseTitleList.put(80000, 93);*/
        CheeseTitleList = new TreeMap<>();
        CheeseTitleList.put(600000, 3016);
        CheeseTitleList.put(630000, 3015);
        CheeseTitleList.put(300000, 3014);
        CheeseTitleList.put(0, 0);
        CheeseTitleList.put(15, 5);
        CheeseTitleList.put(60, 6);
        CheeseTitleList.put(300, 7);
        CheeseTitleList.put(600, 8);
        CheeseTitleList.put(900, 35);
        CheeseTitleList.put(1200, 36);
        CheeseTitleList.put(1500, 37);
        CheeseTitleList.put(1800, 26);
        CheeseTitleList.put(2100, 27);
        CheeseTitleList.put(2400, 28);
        CheeseTitleList.put(2700, 29);
        CheeseTitleList.put(3000, 30);
        CheeseTitleList.put(3300, 31);
        CheeseTitleList.put(3600, 32);
        CheeseTitleList.put(3900, 33);
        CheeseTitleList.put(4200, 34);
        CheeseTitleList.put(4500, 38);
        CheeseTitleList.put(4800, 39);
        CheeseTitleList.put(5100, 40);
        CheeseTitleList.put(5400, 41);
        CheeseTitleList.put(6000, 72);
        CheeseTitleList.put(6900, 73);
        CheeseTitleList.put(8100, 74);
        CheeseTitleList.put(9600, 75);
        CheeseTitleList.put(11400, 76);
        CheeseTitleList.put(13800, 77);
        CheeseTitleList.put(18000, 78);
        CheeseTitleList.put(21000, 79);
        CheeseTitleList.put(24000, 80);
        CheeseTitleList.put(27003, 81);
        CheeseTitleList.put(30000, 82);
        CheeseTitleList.put(42000, 83);
        CheeseTitleList.put(54000, 84);
        CheeseTitleList.put(66000, 85);
        CheeseTitleList.put(78000, 86);
        CheeseTitleList.put(90000, 87);
        CheeseTitleList.put(102000, 88);
        CheeseTitleList.put(114000, 89);
        CheeseTitleList.put(126000, 90);
        CheeseTitleList.put(138000, 91);
        CheeseTitleList.put(150000, 92);
        CheeseTitleList.put(165000, 234);
        CheeseTitleList.put(180000, 235);
        CheeseTitleList.put(195000, 236);
        CheeseTitleList.put(210000, 237);
        CheeseTitleList.put(225000, 238);
        CheeseTitleList.put(240000, 93);
        CheeseTitleList.put(750_000, 3338);
        CheeseTitleList.put(1000_000, 3339);
        CheeseTitleList.put(1250_000, 3340);
        CheeseTitleList.put(1500_000, 3341);
        CheeseTitleList.put(1750_000, 3342);
        CheeseTitleList.put(2000_000, 3343);
        CheeseTitleList.put(2400_000, 3344);
        CheeseTitleList.put(2800_000, 3345);
        CheeseTitleList.put(3200_000, 3346);
        CheeseTitleList.put(3600_000, 3347);
        CheeseTitleList.put(4000_000, 3348);
        CheeseTitleList.put(5000_000, 3349);
        CheeseTitleList.put(6000_000, 3350);
        CheeseTitleList.put(7500_000, 3351);
        CheeseTitleList.put(10_000_000, 3352);
        CheeseTitleList.put(555_555, 3561);
        CheeseTitleList.put(760_000, 3562);
        CheeseTitleList.put(660_000, 3563);

    }

    static {
        T_Tokens = new HashMap<>();
        T_Tokens.put("ST_RepondDemandeEnMariage", 64);
        T_Tokens.put("ET_ResultatSupprimerDroitRang", 136);
        T_Tokens.put("ET_ResultatSupprimerRang", 130);
        T_Tokens.put("ST_ListeNoire", 74);
        T_Tokens.put("ET_SignaleConnexionMembre", 99);
        T_Tokens.put("ET_ResultatDemandeDivorce", 68);
        T_Tokens.put("ET_SignaleRetraitAmiBidirectionnel", 59);
        T_Tokens.put("ET_ErreurDemandeEnMariage", 62);
        T_Tokens.put("ST_ListeRangs", 121);
        T_Tokens.put("ET_ResultatRetireAmi", 47);
        T_Tokens.put("ET_ResultatQuitterTribu", 95);
        T_Tokens.put("ET_SignaleQuitteCanal", 28);
        T_Tokens.put("ET_ResultatMessageCanal", 21);
        T_Tokens.put("ST_QuitterCanal", 25);
        T_Tokens.put("ET_SignaleAjoutAmiBidirectionnel", 58);
        T_Tokens.put("ET_ResultatChangerMessageJour", 111);
        T_Tokens.put("ET_SignaleDemandeEnMariage", 63);
        T_Tokens.put("ET_ResultatDissoudreTribu", 146);
        T_Tokens.put("ET_ResultatIdentificationService", 2);
        T_Tokens.put("ET_SignaleRejointCanal", 27);
        T_Tokens.put("ET_SignaleDissolutionTribu", 147);
        T_Tokens.put("ST_ListeHistoriqueTribu", 96);
        T_Tokens.put("ET_DemandeNouveauxMessagesPrivesWebEnMasse", 162);
        T_Tokens.put("ET_CreerTribu", 79);
        T_Tokens.put("ET_SignaleAjoutAmi", 51);
        T_Tokens.put("ET_ResultatAffecterRang", 125);
        T_Tokens.put("ET_ResultatMessagePrive", 34);
        T_Tokens.put("ET_ResultatListeAmis", 49);
        T_Tokens.put("ET_ResultatInformationsTribuSimple", 89);
        T_Tokens.put("ST_SignaleChangementAvatar", 160);
        T_Tokens.put("ET_SignaleModificationLocalisationAmi", 52);
        T_Tokens.put("ST_AjouterRang", 126);
        T_Tokens.put("ST_MiseAJourLocalisation", 4);
        T_Tokens.put("ET_SignaleConnexionMembres", 101);
        T_Tokens.put("ET_ResultatRejoindreCanal", 24);
        T_Tokens.put("ET_ResultatMembresTribu", 92);
        T_Tokens.put("ET_ErreurListeRangs", 123);
        T_Tokens.put("ST_MiseAJourLocalisations", 6);
        T_Tokens.put("ST_DemandeInformationsTribuSimpleParNom", 88);
        T_Tokens.put("ET_ResultatListeNoire", 75);
        T_Tokens.put("ST_ChangerDeGenre", 157);
        T_Tokens.put("ET_SignaleDivorce", 69);
        T_Tokens.put("ET_ResultatDesignerChefSpirituel", 142);
        T_Tokens.put("ET_SignaleMembresQuittentCanal", 32);
        T_Tokens.put("ST_RetireAmi", 46);
        T_Tokens.put("ET_ResultatRepondDemandeEnMariage", 65);
        T_Tokens.put("ET_SignaleMembreRejointCanal", 29);
        T_Tokens.put("ET_SignalNouveauxMessagesPrivesWeb", 163);
        T_Tokens.put("ET_SignaleConnexionAmis", 56);
        T_Tokens.put("ET_ReponseDemandeInfosJeuUtilisateur", 166);
        T_Tokens.put("ET_RecoitMessagePriveSysteme", 35);
        T_Tokens.put("ST_AffecterRang", 124);
        T_Tokens.put("ET_ResultatAjouterRang", 127);
        T_Tokens.put("ET_RecoitMessagePrive", 36);
        T_Tokens.put("ET_SignaleExclusion", 106);
        T_Tokens.put("ET_ResultatRenommerRang", 132);
        T_Tokens.put("ET_SignaleChangementRang", 105);
        T_Tokens.put("ET_SignaleRetraitAmi", 53);
        T_Tokens.put("ET_SignaleDeconnexionAmis", 57);
        T_Tokens.put("ET_ResultatCreerTribu", 80);
        T_Tokens.put("ST_ChangerMessageJour", 110);
        T_Tokens.put("ET_ResultatInviterMembre", 117);
        T_Tokens.put("ET_SignaleMessageCanal", 22);
        T_Tokens.put("ST_AjoutAmi", 44);
        T_Tokens.put("ST_InverserOrdreRangs", 137);
        T_Tokens.put("ST_DemandeDivorce", 67);
        T_Tokens.put("ET_ResultatChangerDeGenre", 158);
        T_Tokens.put("ET_SignaleMembresRejoignentCanal", 30);
        T_Tokens.put("ST_EnvoitMessageCanal", 20);
        T_Tokens.put("ET_SignaleDeconnexionAmi", 55);
        T_Tokens.put("ST_ResultatCreerTribu", 80);
        T_Tokens.put("ST_CreerTribu", 79);
        T_Tokens.put("ST_SignalNouveauxMessagesPrivesWeb", 163);
        T_Tokens.put("ET_ResultatExclureMembre", 115);
        T_Tokens.put("ET_ErreurListeHistoriqueTribu", 98);
        T_Tokens.put("ET_ResultatDemandeEnMariage", 61);
        T_Tokens.put("ST_DesignerChefSpirituel", 141);
        T_Tokens.put("ET_SignaleNouveauMembre", 107);
        T_Tokens.put("ET_ResultatRenommerTribu", 144);
        T_Tokens.put("ST_RetireListeNoire", 72);
        T_Tokens.put("ET_ResultatDefinitModeSilence", 40);
        T_Tokens.put("ET_ResultatQuitterCanal", 26);
        T_Tokens.put("ET_ResultatListeHistoriqueTribu", 97);
        T_Tokens.put("ST_DefinitModeSilence", 39);
        T_Tokens.put("ST_QuitterTribu", 94);
        T_Tokens.put("ET_DemandeNouveauxMessagesPrivesWeb", 161);
        T_Tokens.put("ET_SignaleChangementMessageJour", 103);
        T_Tokens.put("ET_SignaleDeconnexionMembres", 102);
        T_Tokens.put("ET_ErreurMembresTribu", 93);
        T_Tokens.put("ET_ResultatChangerCodeMaisonTFM", 120);
        T_Tokens.put("ST_SupprimerDroitRang", 135);
        T_Tokens.put("ET_ErreurInformationsTribuSimple", 90);
        T_Tokens.put("ST_SignalNouveauMessagePriveWeb", 164);
        T_Tokens.put("ET_SignalNouveauMessagePriveWeb", 164);
        T_Tokens.put("ET_SignaleRetraitListeNoire", 78);
        T_Tokens.put("ST_DemandeEnMariage", 60);
        T_Tokens.put("ST_RejoindreCanal", 23);
        T_Tokens.put("ST_DemandeMembresTribu", 91);
        T_Tokens.put("ET_SignaleDepartMembre", 108);
        T_Tokens.put("ST_AjouterDroitRang", 133);
        T_Tokens.put("ET_ResultatListeRangs", 122);
        T_Tokens.put("ET_ResultatInformationsTribu", 86);
        T_Tokens.put("ET_ErreurInviterMembre", 118);
        T_Tokens.put("ET_ResultatMiseAJourLocalisation", 5);
        T_Tokens.put("ET_ResultatDemandeMembresCanal", 42);
        T_Tokens.put("ET_SignaleChangementAvatar", 160);
        T_Tokens.put("ST_ListeAmis", 48);
        T_Tokens.put("ST_DemandeMembresCanal", 41);
        T_Tokens.put("ET_SignaleDeconnexionMembre", 100);
        T_Tokens.put("ST_DissoudreTribu", 145);
        T_Tokens.put("ET_ResultatInverserOrdreRangs", 138);
        T_Tokens.put("ET_SignaleMariage", 66);
        T_Tokens.put("ET_ResultatAjoutAmi", 45);
        T_Tokens.put("ET_SignaleInvitationTribu", 82);
        T_Tokens.put("ET_ErreurInformationsTribu", 87);
        T_Tokens.put("ET_ResultatMiseAJourLocalisations", 7);
        T_Tokens.put("ST_ChangerCodeMaisonTFM", 119);
        T_Tokens.put("ST_IdentificationService", 1);
        T_Tokens.put("ET_SignaleConnexionAmi", 54);
        T_Tokens.put("ET_ErreurRepondInvitationTribu", 84);
        T_Tokens.put("ET_SignaleChangementDeGenre", 159);
        T_Tokens.put("ST_SupprimerRang", 129);
        T_Tokens.put("ST_RepondInvitationTribu", 83);
        T_Tokens.put("ET_ErreurListeNoire", 76);
        T_Tokens.put("ST_SignaleChangementDeGenre", 159);
        T_Tokens.put("ST_RequeteDemandeInfosJeuUtilisateur", 165);
        T_Tokens.put("ET_ErreurListeAmis", 50);
        T_Tokens.put("ST_AjoutListeNoire", 70);
        T_Tokens.put("ET_SignaleTribuCreee", 81);
        T_Tokens.put("ST_RenommerTribu", 143);
        T_Tokens.put("ST_EnvoitMessagePrive", 33);
        T_Tokens.put("ST_DemandeDonneesUtilisateur", 152);
        T_Tokens.put("ET_ErreurAjouterRang", 128);
        T_Tokens.put("ST_DemandeInformationsTribu", 85);
        T_Tokens.put("ET_ResultatAjoutListeNoire", 71);
        T_Tokens.put("ET_SignaleAjoutListeNoire", 77);
        T_Tokens.put("ET_ErreurDemandeMembresCanal", 43);
        T_Tokens.put("ET_ResultatAjouterDroitRang", 134);
        T_Tokens.put("ET_ErreurDemandeInfosJeuUtilisateur", 167);
        T_Tokens.put("ST_DefinitDonneesUtilisateur", 155);
        T_Tokens.put("ET_SignaleChangementCodeMaisonTFM", 104);
        T_Tokens.put("ET_SignaleMembreQuitteCanal", 31);
        T_Tokens.put("ET_ResultatRetireListeNoire", 73);
        T_Tokens.put("ST_ExclureMembre", 114);
        T_Tokens.put("ET_SignaleModificationLocalisationMembreTribu", 109);
        T_Tokens.put("ST_PingUtilisateur", 3);
        T_Tokens.put("ST_RenommerRang", 131);
        T_Tokens.put("ST_InviterMembre", 116);
        T_Tokens.put("ET_ErreurDonneesUtilisateur", 154);
        T_Tokens.put("ET_ResultatDonneesUtilisateur", 153);
        T_Tokens.put("ET_ResultatDefinitDonneesUtilisateur", 156);

    }

    static {
        dontcount.add(T_Tokens.get("ST_RepondInvitationTribu"));
    }

    static {
        LogList = new HashMap<>();
        LogList.put("survivor_killed", new int[]{28, 10000, 0, 122});
        LogList.put("racing_rounds", new int[]{30, 1500, 10000, 124});
        LogList.put("racing_comp_rounds", new int[]{31, 10000, 30000, 125});
        LogList.put("racing_podium", new int[]{33, 10000, 30000, 127});
        LogList.put("racing_first", new int[]{32, 10000, 30000, 126});
        LogList.put("survivor_rounds", new int[]{26, 1000, 40000, 120});
        LogList.put("survivor_shaman", new int[]{27, 800, 10000, 121});
        LogList.put("survivor", new int[]{29, 10000, 50000, 123});
        LogList.put("kutier_killed", new int[]{40, 10000, 0, 122});
        LogList.put("kutier_survivor", new int[]{41, 10000, 0, 122});

    }

    static {
        ShamanShopList = new HashMap<>();
        ShamanShopList.put(101, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(102, new int[]{7, 0, 0, 1500, 50});
        ShamanShopList.put(201, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(202, new int[]{6, 0, 0, 3000, 100});
        ShamanShopList.put(301, new int[]{6, 0, 0, 3000, 100});
        ShamanShopList.put(302, new int[]{6, 0, 0, 3000, 100});
        ShamanShopList.put(401, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(402, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(1002, new int[]{2, 0, 0, 2000, 50});
        ShamanShopList.put(2801, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(2802, new int[]{1, 0, 0, 3000, 100});
        ShamanShopList.put(2803, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(2804, new int[]{9, 0, 0, 3000, 100});
        ShamanShopList.put(2805, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(2806, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(403, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(2807, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(203, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(701, new int[]{4, 0, 0, 200, 10});
        ShamanShopList.put(1701, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(601, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(1003, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(404, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(303, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(204, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(602, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(1702, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(2809, new int[]{1, 0, 0, 3000, 100});
        ShamanShopList.put(2808, new int[]{7, 0, 0, 3000, 100});
        ShamanShopList.put(304, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(405, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(1703, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(205, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(103, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(206, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(2810, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(1704, new int[]{6, 0, 0, 3500, 150});
        ShamanShopList.put(406, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(407, new int[]{8, 0, 0, 3000, 100});
        ShamanShopList.put(305, new int[]{8, 0, 0, 1500, 50});
        ShamanShopList.put(2811, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(2812, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(207, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(104, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(1004, new int[]{1, 0, 0, 1000, 40});
        ShamanShopList.put(208, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(105, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(408, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(306, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(2813, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(2814, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(603, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(1705, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(604, new int[]{10, 0, 0, 1500, 50});
        ShamanShopList.put(409, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(307, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(209, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(106, new int[]{9, 0, 0, 1500, 50});
        ShamanShopList.put(107, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(210, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(1005, new int[]{1, 0, 0, 1000, 40});
        ShamanShopList.put(1006, new int[]{1, 0, 0, 1000, 40});
        ShamanShopList.put(1007, new int[]{3, 0, 0, 1000, 40});
        ShamanShopList.put(1706, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1707, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1708, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1709, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1710, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1711, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(1712, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1713, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(1714, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(1715, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(1716, new int[]{7, 0, 0, 3000, 100});
        ShamanShopList.put(1717, new int[]{4, 0, 0, 3000, 100});

        ShamanShopList.put(2815, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(211, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(212, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(213, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(214, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(215, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(216, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(217, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(218, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(219, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(220, new int[]{7, 0, 0, 3000, 100});
        ShamanShopList.put(221, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(222, new int[]{10, 1, 0, 3000, 100});
        ShamanShopList.put(223, new int[]{6, 0, 0, 3000, 100});
        ShamanShopList.put(108, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(410, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(308, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(309, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(605, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(109, new int[]{3, 0, 0, 1800, 70});
        ShamanShopList.put(110, new int[]{3, 0, 0, 1800, 70});
        ShamanShopList.put(111, new int[]{2, 0, 0, 1800, 70});
        ShamanShopList.put(112, new int[]{5, 0, 0, 1800, 70});
        ShamanShopList.put(113, new int[]{2, 0, 0, 1800, 70});
        ShamanShopList.put(114, new int[]{5, 0, 0, 1800, 70});
        ShamanShopList.put(115, new int[]{4, 0, 0, 1800, 70});
        ShamanShopList.put(116, new int[]{5, 0, 0, 1800, 70});
        ShamanShopList.put(117, new int[]{7, 0, 0, 1800, 70});
        ShamanShopList.put(118, new int[]{3, 0, 0, 1800, 70});
        ShamanShopList.put(119, new int[]{6, 0, 0, 1800, 70});
        //  ShamanShopList.put(212, new int[]{9, 0, 13, 3600, 140});
        // ShamanShopList.put(309, new int[]{9, 0, 13, 1800, 70});
        ShamanShopList.put(411, new int[]{9, 0, 0, 3300, 130});
        ShamanShopList.put(412, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(413, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(414, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(415, new int[]{4, 0, 0, 3000, 100});
        ShamanShopList.put(416, new int[]{5, 0, 0, 3000, 100});
        ShamanShopList.put(417, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(418, new int[]{6, 0, 0, 3000, 100});
        ShamanShopList.put(419, new int[]{3, 0, 0, 3000, 100});
        ShamanShopList.put(420, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(421, new int[]{2, 0, 0, 3000, 100});
        ShamanShopList.put(422, new int[]{6, 0, 0, 3000, 100});

        ShamanShopList.put(606, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(607, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(608, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(609, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(610, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(611, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(612, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(613, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(614, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(615, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(616, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(617, new int[]{6, 0, 0, 1500, 50});
        ShamanShopList.put(618, new int[]{4, 0, 0, 1500, 50});
        //
        ShamanShopList.put(2816, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(2817, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(2818, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(2819, new int[]{7, 0, 0, 1500, 50});
        ShamanShopList.put(2820, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(2821, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(2822, new int[]{1, 0, 0, 1500, 50});
        ShamanShopList.put(2823, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(2824, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(2825, new int[]{7, 0, 0, 1500, 50});
        ShamanShopList.put(2826, new int[]{7, 0, 0, 1500, 50});


        ShamanShopList.put(310, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(311, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(312, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(313, new int[]{4, 0, 0, 1500, 50});
        ShamanShopList.put(314, new int[]{5, 0, 0, 1500, 50});
        ShamanShopList.put(315, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(316, new int[]{6, 0, 0, 1500, 50});
        ShamanShopList.put(317, new int[]{3, 0, 0, 1500, 50});
        ShamanShopList.put(318, new int[]{2, 0, 0, 1500, 50});
        ShamanShopList.put(319, new int[]{6, 0, 0, 1500, 50});
    }

    static {
        Forbidden_names = new HashSet<>();
        Forbidden_names.add("");
        Forbidden_names.add("Ohmytengri");
        Forbidden_names.add("Sevenops");
        Forbidden_names.add("Sevenbot");
    }

    static {
        int[] dMaps = new int[]{44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 136, 137, 138, 139, 140, 141, 142, 143};

        final int[] normal = {3, 4, 5, 6, 8, 9, 11, 26, 29, 30, 31, 33, 34, 41, 44, 46, 47, 48, 50, 51, 52, 53, 60, 62,
                63, 64, 66, 67, 69, 70, 71, 73, 80, 81, 82, 83, 84, 86, 88, 92, 93};
        final int[] dontmult = {6, 63, 82};
        final int[] full = {7, 14, 27, 84, 86, 87, 90, 94};
        final int[] forb = {27, 89, 81, 82};
        Integer[] integerArray = ArrayUtils.toObject(normal);
        snormal = GetSetFromArray(integerArray);
        integerArray = ArrayUtils.toObject(full);
        sfull = GetSetFromArray(integerArray);
        integerArray = ArrayUtils.toObject(dontmult);
        smult = GetSetFromArray(integerArray);
        integerArray = ArrayUtils.toObject(forb);
        forbidden_survivor = GetSetFromArray(integerArray);

        integerArray = ArrayUtils.toObject(dMaps);
        doubleMaps = GetSetFromArray(integerArray);
    }

    static {
        int[] a = new int[]{2100, 2101, 2102, 2103, 2104, 2111, 2112, 2113, 2114, 2115, 2116, 2118, 2119, 2120, 2121,
                2122, 2123, 2125, 2126, 2127, 2128, 2129, 2130, 2132, 2133, 2134, 2135, 2136, 2137, 2139, 2140, 2141,
                2142, 2143, 2144, 2146, 2147, 2148, 2149, 2150, 2151, 2153, 2154, 2155, 2156, 2157, 2158, 2160, 2161,
                2162, 2163, 2164, 2165, 2167, 2168, 2169, 2170, 2171, 2172, 2174, 2175, 2176, 2177, 2178, 2179, 2190,
                2191, 2192, 2193, 2194, 2195, 2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2210, 2211, 2238, 2245};
        Integer[] integerArray = ArrayUtils.toObject(a);
        types0 = GetSetFromArray(integerArray);
    }

    static {
        int[] a = new int[]{4, 29, 30, 2241, 2330, 2351};
        Integer[] integerArray = ArrayUtils.toObject(a);
        types1 = GetSetFromArray(integerArray);
    }

    static {
        int[] a = new int[]{9, 12, 13, 17, 18, 19, 22, 27, 28, 2258};
        Integer[] integerArray = ArrayUtils.toObject(a);
        types2 = GetSetFromArray(integerArray);
    }


    static {
        int[] a = new int[]{31, 34, 2240, 2247};
        Integer[] integerArray = ArrayUtils.toObject(a);
        types3 = GetSetFromArray(integerArray);
    }

    static {
        int[] a = new int[]{2111, 2112, 2113, 2114, 2115, 2116, 2118, 2119, 2120, 2121, 2122, 2123, 2125, 2126, 2127,
                2128, 2129, 2130, 2132, 2133, 2134, 2135, 2136, 2137, 2139, 2140, 2141, 2142, 2143, 2144, 2146, 2147,
                2148, 2149, 2150, 2151, 2153, 2154, 2155, 2156, 2157, 2158, 2160, 2161, 2162, 2163, 2164, 2165, 2167,
                2168, 2169, 2170, 2171, 2172, 2174, 2175, 2176, 2177, 2178, 2179, 2190, 2191, 2192, 2193, 2194, 2195,
                2196, 2197, 2198, 2199, 2200, 2201, 2202, 2203, 2238, 2245, 2253, 2254, 2257, 2260, 2261, 901, 910,920,921};
        Integer[] integerArray = ArrayUtils.toObject(a);
        forbiddens = GetSetFromArray(integerArray);
    }

    static {

        int[] a = new int[]{800, 801, 2210, 2211, 2100, 2101, 2102, 2103, 2104, 777};
        Integer[] integerArray = ArrayUtils.toObject(a);
        useAnyWay = GetSetFromArray(integerArray);
        a = new int[]{2200, 2201, 2202, 2203, 2238, 2245, 2254, 780, 783, 784};

        integerArray = ArrayUtils.toObject(a);
        dont = GetSetFromArray(integerArray);
        a = new int[]{31, 34, 2240, 2247};
        integerArray = ArrayUtils.toObject(a);
        dont2 = GetSetFromArray(integerArray);
    }

    public static List<Integer> lll;

    static {
        Integer[] sifirla = new Integer[]{
                2263, 2264, 2265, 2266, 2267, 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2284, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2295, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305, 2306, 779, 778, 2257
        };
        lll = Arrays.asList(sifirla);
    }

    public MongoClient mongoClient;
    public MongoDatabase dbOyun;
    public MongoDatabase dbKayit;
    public HashSet<String> BASTARDO = new HashSet<>();
    public HashMap<String, ArrayList<String>> modopwetSetting = new HashMap<>();
    public HashMap<String, Raporlar> RaporSistemi = new HashMap<>();
    public HashSet<String> BAD_ISP;
    public HashMap<Integer, HashMap<String, Object>> Bilgiler = new LinkedHashMap<>();
    public AtomicInteger CHI = new AtomicInteger();
    public static Map<String, Long> IP_BANS = new ConcurrentHashMap<>();
    public static Map<String, Boolean> IP_STATS = new ConcurrentHashMap<>();
    public static Map<String, Long> IP_CREATE_ACC = new ConcurrentHashMap<>();
    public Map<String, PlayerConnection> clients = new ConcurrentHashMap<>();
    public Map<String, PlayerConnection> allChats = new ConcurrentHashMap<>();
    public Map<String, PlayerConnection> allPings = new ConcurrentHashMap<>();
    public Map<Integer, PlayerConnection> clients2 = new ConcurrentHashMap<>();
    public Map<String, Room> rooms = new ConcurrentHashMap<>();
    public ReentrantLock kilit = new ReentrantLock();
    public Map<Integer, Tribe> tribes = new ConcurrentHashMap<>();
    // public Map<String, Room> rooms = new ConcurrentHashMap<>();
    public Map<Integer, String> langs = new HashMap<>();
    public Set<LUA_THREAD> threads = Sets.newConcurrentHashSet();
    public static Random random = new Random();
    public Map<Integer, int[]> ShopList = new LinkedHashMap<>();
    public ByteBuf shop_buf;
    public byte[] shop_byte;
    public HashMap<String, ArrayList> bots = new HashMap<>();
    public static Map<Integer, ArrayList<Integer>> perms = new HashMap<>();
    public Map<String, PlayerConnection> Cafers = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String, LongAdder> _bans = new ConcurrentHashMap<>();
    public List<String> BAD_WORDS = new CopyOnWriteArrayList<>();
    public List<String> BAD_TAGS = new CopyOnWriteArrayList<>();
    public List<String> NEUTRAL_WORDS = new CopyOnWriteArrayList<>();
    public static List<String> MOD_COMMANDS = new CopyOnWriteArrayList<>();
    public Map<String, Kanal> kanallar = new ConcurrentHashMap<>();
    public static Map<Integer, Integer> AVATARS = new ConcurrentHashMap<>();
    public ConcurrentLinkedDeque<SqlData> CLD = new ConcurrentLinkedDeque<>();
    public static Map<String, MiniOyun> MINIGAMES = new ConcurrentHashMap<>();
    public boolean bom = false;
    public ConcurrentHashMap<Integer, PlayerConnection> checkForCafes = new ConcurrentHashMap<>();
    private NetworkManager networkManager;
    public int oyZaman = 1613260800;
    private MongoCollection<Document> colMarket;
    public static MongoCollection<Document> colIsp;
    public static MongoCollection<Document> colHackers;
    public static MongoCollection<Document> colRandomUUID;
    public static MongoCollection<Document> colOplog;
    public static MongoCollection<Document> colBad;
    public static MongoCollection<Ceza> colCezaLog;
    public static MongoCollection<Document> colLuaLog;
    public static MongoCollection<Document> colLuaLog2;
    public static MongoCollection<Document> colBotVerify;
    public static MongoCollection<Login> colGiris;
    public static MongoCollection<Document> colFikicoinler;
    public static MongoCollection<Document> colFriendReq;
    public static MongoCollection<Document> colBadTags;
    public static MongoCollection<Document> colBadWords;

    public static MongoCollection<Document> colNeutralWords;

    public static MongoCollection<Document> colBadISP;
    public static MongoCollection<Document> colGameSettings;
    public static int event_id = 0;
    public static String event_background;
    public static int genel_carpan = 1;


    public static <Integer> HashSet<Integer> GetSetFromArray(Integer[] array) {
        return new HashSet<>(Arrays.asList(array));
    }

    static String filter(String text) {
        return text.replaceAll("[^a-zA-Z]", "");
    }

    public static String filterName(String text) {
        return text.replaceAll("^(\\+|\\-|)[A-Za-z0-9_]", "");
    }


    //CREATE TABLE arena.online ( id int PRIMARY KEY, arena boolean,mf boolean );
    //CREATE TABLE pm (kime int ,kimden int, ileti text, dil int, surev timestamp,PRIMARY KEY(kime,surev));
    public static void main(String[] args) throws Throwable {
        MFServer.instance = new MFServer();
        MFServer.instance.run();


    }

    public static String decompose(String s) {
        return java.text.Normalizer.normalize(s, java.text.Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    public static MFServer getInstance() {
        return instance;
    }

    static public String firstLetterCaps(String data) {
        data = data.trim();
        String restLetters = "";
        String firstLetter = "";
        if (data.length() != 0) {
            firstLetter = data.substring(0, 1).toUpperCase();
        }
        if (data.length() > 1) {
            restLetters = data.substring(1).toLowerCase();
        }
        return firstLetter + restLetters;
    }
    static {
        emojiList.put(":hearteyes:", "😍");
        emojiList.put(":yum:", "😋");
        emojiList.put(":cool:", "😎");
        emojiList.put(":hot:", "🥵");
        emojiList.put(":ew:", "🤢");
        emojiList.put(":vomit:", "🤮");
        emojiList.put(":cowboy:", "🤠");
        emojiList.put(":omg:", "🤯");
        emojiList.put(":dead:", "💀");
        emojiList.put(":clown:", "🤡");
        emojiList.put(":ok:", "👌");
        emojiList.put(":eyes:", "👀");
        emojiList.put(":eye:", "👁");
        emojiList.put(":tongue:", "👅");
        emojiList.put(":mouth:", "👄");
        emojiList.put(":pumpkin:", "🎃");
        emojiList.put(":crown:", "👑");
        emojiList.put(":heart:", "❤");
        emojiList.put(":evil:", "😈");
        emojiList.put(":candy:", "🍬");
        emojiList.put(":grin:", "😀");
        emojiList.put(":rage:", "😡");
        emojiList.put(":lol:", "😂");
        emojiList.put(":smile:", "🙂");
        emojiList.put(":wink:", "😉");
    }
    public static String replaceEmojis(String msg){
        for (Map.Entry<String, String> entry : emojiList.entrySet()) {
            String textEmoji = entry.getKey();
            String emoji = entry.getValue();
            msg = msg.replace(textEmoji, emoji);
        }
        return msg;
    }
    public static String getRandom(int size) {
        return RandomStringUtils.random(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    public static String getRandomChars(int size) {
        return RandomStringUtils.random(size, "ABCDEF123456789");
    }

    public static void writeUTFBytes(String text, int length, ByteBufOutputStream os) throws IOException {

        try {
            int a = text.getBytes("utf-8").length;

            int b = 0;

            while (b + a < length) {
                text += (char) 0;
                b++;
            }
            os.writeBytes(text);

        } catch (UnsupportedEncodingException ex) {

        }

    }

    public static int timestamp() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static int timestamp2() {
        return (int) (System.currentTimeMillis() / 60000);
    }

    public static int[] get_cat_by_id(int item) {
        int[] x = new int[2];
        if(item==14564){
            x[0] = 100;
            x[1] = 7;
            return x;
        }
        int fl;
        if (item > 230000) {
            fl = 22;
            item = item - 230_000;
        } else if (item > 33000) {
            fl = 22;
            item = item - 33392;
        } else {
            if (item > 10_000) {
                item = item - 10000;
                fl = 0;
            } else {
                fl = item / 100;
                item = item % 100;

            }
        }

        x[0] = item;
        x[1] = fl;
        return x;
    }

    public static int get_id_by_cat(int item, int cat) {
        
        if (item < 100) {
            return cat * 100 + item;
        }
        return cat * 10000 + item + 10000;
    }

    public static boolean verify_name(String name) {
        return !(name.length() < 3 || name.length() > 21);
    }

    public static ByteBuf pack_tribulle(String token, ByteBuf packet) throws IOException {
        ByteBuf buf = getBuf();
        buf.writeByte(60);
        buf.writeByte(1);
        buf.writeShort(MFServer.T_Tokens.get(token));
        buf.writeBytes(packet);
        return buf;
    }

    public static ByteBuf pack_tribulle2(ByteBuf packet, int tok) throws IOException {
        ByteBuf buf = getBuf();
        buf.writeByte(60);
        buf.writeByte(3);
        buf.writeShort(tok);
        // buf.writeInt(packet.readableBytes());
        buf.writeBytes(packet);
        return buf;
    }

    public static ByteBuf pack_old(int t1, int t2, Object[] objects) throws IOException {
        ByteBufOutputStream Buffer = MFServer.getStream();
        String nPacket = "";
        if (objects.length >= 1) {
            nPacket = MFServer.x01 + StringUtils.join(objects, MFServer.x01);
        }
        int len2 = nPacket.length() + 2;
        Buffer.writeByte(1);
        Buffer.writeByte(1);
        Buffer.writeShort(len2);
        Buffer.writeByte(t1);
        Buffer.writeByte(t2);
        Buffer.writeBytes(nPacket);
        return Buffer.buffer();
    }

    public static int parseLuaFloat(String value) {
        if (value.contains("E")) {
            value = StringUtils.split(value, "E")[0];
        }
        if (StringUtils.isNumeric(value)) {
            return Integer.valueOf(value + "00");
        } else if (value.contains(".")) {
            if (value.startsWith("0.")) {
                value = value.substring(2);
                return Integer.valueOf(value + "0");
            } else {
                String n = value.replace(".", "");
                if (n.length() == 2) {
                    n += "0";
                }
                return Integer.valueOf(n);
            }

        } else {
            return 0;
        }
    }

    public static boolean checkValidXML(String XML) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(XML));
            org.w3c.dom.Document s = builder.parse(is);
            return true;

        } catch (ParserConfigurationException | SAXException | IOException error) {
            return false;
        }
    }

    public static ByteBuf getBuf() {
        return channel.alloc().buffer();
    }

    public static ByteBufOutputStream getStream() {
        return new ByteBufOutputStream(MFServer.getBuf());
    }

    public static void Zlib(ByteBufOutputStream buf, String Sxml) throws IOException {
        byte[] xml = Sxml.getBytes("utf-8");
        Deflater deflater = new Deflater();
        deflater.setInput(xml);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(xml.length);
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();
        buf.writeInt(output.length);
        buf.write(output);
    }

    public static String bytesToHex(ByteBuf bytes) {
        int length = bytes.readableBytes();
        char[] hexChars = new char[length * 2];
        for (int j = 0; j < length; j++) {
            int v = bytes.readByte() & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        MFServer.bitti(bytes);
        return new String(hexChars);
    }

    public static ByteBuf pack_messagewithcolor(String message, String color, String lang, String... args) throws IOException {
        ByteBufOutputStream p = getStream();
        p.writeByte(28);
        p.writeByte(5);
        p.writeShort(0);
        p.writeUTF("<font color='" + color + "'>• [" + lang + "] " + message + "</font>");
        p.writeByte(args.length);
        for (String arg : args) {
            p.writeUTF(arg);
        }
        return p.buffer();
    }

    public static ByteBuf pack_message(String message, String... args) throws IOException {
        ByteBufOutputStream p = getStream();
        p.writeByte(28);
        p.writeByte(5);
        p.writeShort(0);
        p.writeUTF(message);
        p.writeByte(args.length);
        for (String arg : args) {
            p.writeUTF(arg);
        }
        return p.buffer();
    }

    /*public static ByteBuf pack_message(String message) throws IOException {
        return pack_message(message);
    }*/
    public static int obj_to_int(Object object) {
        return ((Number) object).intValue();
    }

    public static Gson get_Gson() {
        return new Gson();
        /*
         * GsonBuilder gsonBuilder = new GsonBuilder();
         * gsonBuilder.registerTypeAdapter(new TypeToken<HashMap<Integer,
         * ArrayList>>() { }.getType(), new IntegerFirstSOMapDeserializer());
         *
         *
         * return gsonBuilder.create();
         */
    }

    public static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage());
        sb.append(e.toString());
        sb.append("\n");
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void rekorAl(int harita, SRC<Rekor> src) {
        MFServer.executor.execute(() -> {
            Rekorlar rekorlar = colRekorlar.find(new Document("_id", harita)).first();
            if (rekorlar == null || rekorlar.rekorlar.size() == 0) {
                src.onResult(null);
            } else {
                Rekor rek = null;
                for (Rekor rekor : rekorlar.rekorlar) {
                    if (rekor.deadline == 0) {
                        rek = rekor;
                        break;
                    }
                }
                src.onResult(rek);
            }
        });

    }

    public static void rekorKontrol(PlayerConnection cl, int harita, Double sure, SRC<Rekor> src) {
        MFServer.logExecutor.execute(() -> {
            Rekorlar rekorlar = MFServer.colRekorlar.find(new Document("_id", harita)).first();

            ArrayList<Rekor> arr;
            Rekor don = null;
            boolean oba = false;
            if (rekorlar == null || rekorlar.rekorlar.size() == 0) {
                arr = new ArrayList<>();
                oba = true;
            } else {
                Rekor rekor = null;
                arr = rekorlar.rekorlar;
                for (Rekor r : rekorlar.rekorlar) {
                    if (r.deadline == 0) {
                        rekor = r;
                        break;
                    }
                }
                if (rekor == null || (sure < rekor.sure && cl.kullanici.birinci > 100_000)) {
                    oba = true;
                } else {

                    int i = 0;
                    try {
                        boolean add = false;
                        Rekor yeni = null;
                        for (Rekor rek : arr) {
                            if (sure < rek.sure) {
                                yeni = new Rekor();
                                yeni.ad = cl.kullanici.isim;
                                yeni.sure = sure;
                                yeni.zaman = System.currentTimeMillis();
                                if (cl.lastHoleId != -1) {
                                    yeni.hid = cl.lastHoleId;
                                }
                                add = true;
                                break;
                            }
                            i++;
                        }
                        if (add) {
                            arr.add(i, yeni);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (oba) {
                Rekor yeni = new Rekor();
                yeni.ad = cl.kullanici.isim;
                yeni.sure = sure;
                yeni.zaman = System.currentTimeMillis();
                if (cl.lastHoleId != -1) {
                    yeni.hid = cl.lastHoleId;
                }
                arr.add(0, yeni);
                arr = arr.stream().filter((r) -> r.deadline == 0 || r.deadline > System.currentTimeMillis()).limit(90).collect(Collectors.toCollection(ArrayList::new));
                MFServer.colRekorlar.updateOne(eq("_id", harita), set("rekorlar", arr), new UpdateOptions().upsert(true));
                src.onResult(yeni);
            } else {

                src.onResult(null);
            }


        });

    }

    public static String plusReady(String name) {

        return plusReady(name, "");
    }

    public static String plusReady(String names[]) {

        return plusReady(names[0], names[1]);
    }

    public static String plusReady(Kullanici kul) {

        return plusReady(kul.isim, kul.tag);
    }

    public static String plusReady(String name, String tag) {
        if (name.startsWith("+")) {
            name = "+" + MFServer.firstLetterCaps(name.substring(1));
        } else if (name.startsWith("-")) {
            name = "-" + MFServer.firstLetterCaps(name.substring(1));
        }
        if (!tag.isEmpty()) name = name + "#" + tag;
        return name;
    }

    public static String noTag(String name) {
        if (name.isEmpty()) return name;
        String[] arr = name.split("#");
        if (arr.length == 0) {
            return "";
        } else {
            return arr[0];
        }
    }

    public static void bitti(ByteBuf buf) {
        if (buf == null) return;
        while (buf.refCnt() > 0) {
            buf.release();
        }
    }

    public static String getComputerName() {
        Map<String, String> env = System.getenv();

        if (env.containsKey("USER")) {
            return env.get("USER");
        } else {
            return env.getOrDefault("HOSTNAME", "Unknown Computer");
        }
    }

    public static void ipBan(String ip, long second) {

        Long deadline = System.currentTimeMillis() + (second * 1000);
        MFServer.executor.execute(() -> MFServer.colIpBan.updateOne(new Document("_id", ip), set("deadline", deadline), new UpdateOptions().upsert(true)));

        MFServer.IP_BANS.put(ip, deadline);
        MFServer.IP_STATS.remove(ip);
    }

    public static Integer get_avatar(Integer pcode) {
        Integer avatar = AVATARS.get(pcode);
        if (avatar == null) {
            Kullanici kullanici = MFServer.colKullanici.find(eq("_id", pcode)).projection(fields(include("avatar"))).first();
            if (kullanici != null) {
                avatar = kullanici.avatar;
                AVATARS.put(pcode, avatar);
            } else {
                avatar = 0;
            }
        }
        return avatar;
    }

    public static boolean removeIfNull(PlayerConnection cl) {
        if (cl.kullanici == null || cl.room == null) {
            instance.clients.entrySet().removeIf(entry -> cl.equals(entry.getValue()));
            return true;
        }
        return false;
    }


    public Logger getLogger() {
        return log;
    }

    public void rekorlariIncele() {
        /*  rekorStats.clear();
        for (ArrayList<Rekor> rekorlar : MFServer.Rekorlar.values()) {
            if (rekorlar.size() > 0) {
                Rekor rekor = rekorlar.get(0);
                for (Rekor rek : rekorlar) {
                    if (BASTARDO.contains(rek.ad)) {
                        rekorlar.remove(rek);
                    }
                }
                rekorStats.put(rekor.ad, rekorStats.getOrDefault(rekor.ad, 0) + 1);
            }
        }
        BASTARDO.clear();
        rekorStats.forEach((k, v) -> {
            PlayerConnection pc = clients.get(k);
            if (pc != null)
                pc.rekor = v;
        });*/
    }

    public void update_bots() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");

        Connection conn = null;
        Statement stmt = null;
        conn = DriverManager.getConnection(DB_URL, USER, PASS);

        stmt = conn.createStatement();
        String sql;
        sql = "select * from bot";
        ResultSet rs = stmt.executeQuery(sql);
        bots.clear();
        while (rs.next()) {
            // Retrieve by column name
            int id = rs.getInt("id");
            int title = rs.getInt("title");
            String name = rs.getString("name");
            String look = rs.getString("look");
            int x = rs.getInt("x");
            int y = rs.getInt("y");
            Statement stmt2 = conn.createStatement();
            ResultSet rs2 = stmt2.executeQuery("select * from bot_shop where bot_id=" + id);
            ArrayList AL = new ArrayList();
            while (rs2.next()) {
                ArrayList al = new ArrayList();
                int it_id = rs2.getInt("id");
                int item_type = rs2.getInt("type_item");
                int code = rs2.getInt("code");
                int coin_type = rs2.getInt("coin_type");
                int howmuch = rs2.getInt("howmuch");
                int piece = rs2.getInt("piece");
                al.add(it_id);
                al.add(id);
                al.add(item_type);
                al.add(code);
                al.add(coin_type);
                al.add(howmuch);
                al.add(piece);
                AL.add(al);

            }
            rs2.close();
            stmt2.close();
            bots.put(name, new ArrayList() {
                {
                    add(id);
                    add(name);
                    add(title);
                    add(look);
                    add(x);
                    add(y);
                    add(AL);
                }
            });
        }

        rs.close();
        stmt.close();
        conn.close();
    }

    public String recommendRoom(String flag) {

        return this.recommendRoom(flag, "");
    }

    public void reboot(int stat) {
        switch (stat) {
            case 30:
                this.reboot_notice(30 * 60);
                MFServer.executor.schedule(() -> reboot(15), 15, TimeUnit.MINUTES);
                break;
            case 15:
                this.reboot_notice(15 * 60);
                MFServer.executor.schedule(() -> reboot(14), 5, TimeUnit.MINUTES);
                break;
            case 14:
                this.reboot_notice(10 * 60);
                MFServer.executor.schedule(() -> reboot(13), 5, TimeUnit.MINUTES);
                break;
            case 13:
                this.reboot_notice(5 * 60);
                MFServer.executor.schedule(() -> reboot(12), 4, TimeUnit.MINUTES);
                break;
            case 12:
                this.reboot_notice(60);
                MFServer.executor.schedule(() -> reboot(11), 30, TimeUnit.SECONDS);
                break;
            case 11:
                this.reboot_notice(30);
                MFServer.executor.schedule(() -> reboot(10), 20, TimeUnit.SECONDS);
                break;
            case 10:
                this.reboot_notice(10);
                MFServer.executor.schedule(() -> reboot(9), 1, TimeUnit.SECONDS);
                break;
            case 9:
                this.reboot_notice(9);
                MFServer.executor.schedule(() -> reboot(8), 1, TimeUnit.SECONDS);
                break;
            case 8:
                this.reboot_notice(8);
                MFServer.executor.schedule(() -> reboot(7), 1, TimeUnit.SECONDS);
                break;
            case 7:
                this.reboot_notice(7);
                MFServer.executor.schedule(() -> reboot(6), 1, TimeUnit.SECONDS);
                break;
            case 6:
                this.reboot_notice(6);
                MFServer.executor.schedule(() -> reboot(5), 1, TimeUnit.SECONDS);
                break;
            case 5:
                this.reboot_notice(5);
                MFServer.executor.schedule(() -> reboot(4), 1, TimeUnit.SECONDS);
                break;
            case 4:
                this.reboot_notice(4);
                MFServer.executor.schedule(() -> reboot(3), 1, TimeUnit.SECONDS);
                break;
            case 3:
                this.reboot_notice(3);
                MFServer.executor.schedule(() -> reboot(2), 1, TimeUnit.SECONDS);
                break;
            case 2:
                this.reboot_notice(2);
                MFServer.executor.schedule(() -> reboot(1), 1, TimeUnit.SECONDS);
                break;
            case 1:
                this.reboot_notice(1);
                MFServer.executor.execute(() -> {
                    int i = 0;
                    for (PlayerConnection pc : this.clients.values()) {
                        try {
                            pc.SaveUser();
                            i++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("Saved " + i);
                });
                break;

        }
    }

    public void update_game_settings() {
        List<String> neutral_words = new CopyOnWriteArrayList<>();
        for (Document document : MFServer.colNeutralWords.find()) {
            neutral_words.add(document.getString("word"));
        }
        NEUTRAL_WORDS = neutral_words;

        List<String> bad_words = new CopyOnWriteArrayList<>();
        for (Document document : MFServer.colBadWords.find()) {
            bad_words.add(document.getString("word"));
        }
        BAD_WORDS = bad_words;

        HashSet<String> bad_isp = new HashSet<>();
        for (Document document : MFServer.colBadISP.find()) {
            bad_isp.add(document.getString("isp"));
        }
        BAD_ISP = bad_isp;

        List<String> bad_tags = new CopyOnWriteArrayList<>();
        for (Document document : MFServer.colBadTags.find()) {
            bad_tags.add(document.getString("tag"));
        }
        BAD_TAGS = bad_tags;
        Document ayarlar = colGameSettings.find().sort(new Document("_id", -1)).first();
        event_id = ayarlar.getInteger("event_id", 0);
        event_background = ayarlar.getString("event_background");
    }

    public void reboot_notice(int sec) {
        for (PlayerConnection client : this.clients.values()) {

            ByteBuf data = MFServer.getBuf().writeByte(28).writeByte(88).writeInt(sec * 1000);
            client.sendPacket(data);

        }
    }

    public void mutePlayer(String muted, PlayerConnection client, int hours, String reason, boolean hidden) throws IOException {
        Ceza ceza = new Ceza(muted, client.kullanici.isim, System.currentTimeMillis(), hours);
        //  ceza.isNano=true;
        ceza.sebep = reason;
        ceza.tip = 1;
        PlayerConnection cl = clients.get(muted);
        if (hours == 0 && reason.isEmpty()) {
            if (cl != null) {
                cl.susturma = null;
            }
            client.sendChatServer(client.kullanici.isim + " demuted " + muted);

        } else {
            if (cl != null) {
                String room = cl.room.name;
                String ip = cl.ip;
                ceza.oda = room;
                ceza.ip = ip;
                cl.susturma = ceza;
                if (!hidden) {
                    cl.room.sendAllOthers(MFServer.pack_message("<ROSE>• [$Moderation] $MuteInfo2",
                            cl.kullanici.isim, String.valueOf(ceza.surec), ceza.sebep), cl.kullanici.code);
                }
                try {
                    cl.sendMessage("<ROSE>• [$Moderation] $MuteInfo1",
                            String.valueOf(ceza.surec), ceza.sebep);
                } catch (Exception e) {

                }

                Raporlar raporlar = RaporSistemi.get(muted);
                if (raporlar != null && (hours > 0 || !reason.isEmpty())) {
                    giveKarma(raporlar, 2);
                }
                //MFServer.colRekorlar.deleteMany(new Document("rekorlar.ad", ad));
            }
            client.sendChatServer(client.kullanici.isim + " muted " + muted + " for " + hours + " hours. Reason : " + reason);

        }
        MFServer.executor.execute(() -> {
            MFServer.colCezaLog.insertOne(ceza);
        });

        Yasaklama yasaklama = new Yasaklama();
        yasaklama.sebep = reason;
        yasaklama.surev = hours;
        yasaklama.yasaklanan = muted;
        yasaklama.yasaklayan = client.kullanici.isim;
        MFServer.susturmalar.put(muted, yasaklama);
    }

    public void banPlayer(String banned, int hours, String reason, String banner, boolean hidden, String co_ip) throws IOException {
        if (banned.equals("Sevenops")) {
            return;
        }
        LongAdder val = this._bans.computeIfAbsent(banner, k -> new LongAdder());
        int freq = val.intValue();
        if (freq > 50 && !(Forbidden_names.contains(banner) && freq < 100)) {
            return;
        }
        if (!banner.equals("Tengri")) {
            val.increment();
        }
        executor.schedule(() -> {
            this._bans.computeIfAbsent(banner, k -> new LongAdder()).decrement();
        }, 10, TimeUnit.MINUTES);
        Ceza ceza = new Ceza(banned, banner, System.currentTimeMillis(), hours);

        if (!reason.isEmpty()) ceza.sebep = reason;
        PlayerConnection pc = this.clients.get(banned);
        if (co_ip != null) ceza.ip = co_ip;
        if (pc != null) {
            if (pc.room != null) ceza.oda = pc.room.name;
            ceza.ip = pc.ip;
            pc.banned = true;
            MFServer.ipBan(ceza.ip, ((long) Math.sqrt(hours) * 3600L));
            try {
                pc.sendPlayerBan(hours, reason, hidden);
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }
        }
        boolean ishack = reason.toLowerCase().contains("vpn farm") || reason.toLowerCase().contains("ban evade") || reason.toLowerCase().contains("hack") || reason.toLowerCase().contains("glitch") || hours == 1800;
        MFServer.executor.execute(() -> {
            long sdl = System.currentTimeMillis() + (hours * 3600 * 1000);
            if (ceza.surec == 0) {
                Ceza eski = MFServer.colCezaLog.find(new Document("ad", ceza.ad).append("tip", 0)).sort(new Document("surev", -1)).first();
                if (eski != null && eski.surec > 0 && !eski.ip.isEmpty()) {
                    MFServer.colIpBan.deleteOne(eq("_id", eski.ip));

                    MFServer.IP_BANS.remove(eski.ip);

                    MFServer.IP_STATS.remove(eski.ip);
                }
                sdl = 0;
                //    MFServer.colRekorlar.updateMany(eq("rekorlar.ad", banned), new Document("$pull", new Document("rekorlar", new Document("ad", banned))));
            }
            if (ceza.surec == 1800) sdl = Long.MAX_VALUE;
            if (ishack || ceza.surec == 0) {
                for (Rekorlar rl : MFServer.colRekorlar.find(eq("rekorlar.ad", banned))) {
                    for (Rekor rekor : rl.rekorlar) {
                        if (rekor.ad.equals(banned)&&(ceza.surec != 0 || rekor.deadline > System.currentTimeMillis())) {
                            rekor.deadline = sdl;
                        }
                    }
                    MFServer.colRekorlar.updateOne(eq("_id", rl._id), set("rekorlar", rl.rekorlar));
                }
            }

            if (ishack) MFServer.colSiralama.deleteOne(eq("pId", checkUserInDb(banned)));

            //
            try {
                MFServer.colCezaLog.insertOne(ceza);
                MFServer.colKullanici.updateOne(eq("isim", banned), set("ban", new Ban(timestamp(), hours, reason)));
                Yasaklama yasaklama = new Yasaklama();
                yasaklama.sebep = reason;
                yasaklama.surev = hours;
                yasaklama.yasaklanan = banned;
                yasaklama.yasaklayan = banner;
                yasaklamalar.put(banned, yasaklama);
                Raporlar raporlar = RaporSistemi.get(banned);
                if (raporlar != null && hours > 0) {
                    giveKarma(raporlar, 2);
                }
                RaporSistemi.remove(banned);
                // MFServer.colRekorlar.updateMany(eq("rekorlar.ad", banned), new Document("$pull", new Document("rekorlar", new Document("ad", banned))));

            } catch (Exception e) {
                MFServer.log.warning(stackTraceToString(e));
            }
        });
    }

    public void giveKarma(Raporlar raporlar, int puan) {
        MFServer.executor.execute(() -> {
            for (Rapor rapor : raporlar.arr) {
                PlayerConnection raporlayan = this.clients.get(rapor.raporlayan);
                if (raporlayan != null) {
                    raporlayan.karma += puan;
                    raporlayan.kullanici.karma.add(puan);
                    String sPuan = Integer.toString(puan);
                    String vaziyet = "<VP>";
                    if (puan > 0) {

                        sPuan = "+" + sPuan;
                    } else {
                        sPuan = "-" + sPuan;
                    }
                    try {
                        raporlayan.iletiYolla("karma_message", raporlar.ad, puan);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        raporlayan.gorevIlerleme(15, puan);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                MFServer.colKullanici.updateOne(eq("isim", rapor.raporlayan), new Document("$push", new Document("karma", puan)));

            }
        });
    }

    public void disconnectIPaddress(String ipAdr) {
        for (PlayerConnection pc : this.clients.values()) {
            if (pc.ip.equals(ipAdr) && !pc.kullanici.isim.equals("Sevenops") && !pc.kullanici.isim.equals("Ohmytengri") && !pc.kullanici.isim.equals("Sevenbot")) {
                pc.channel.close();
            }
        }
    }


    public String recommendRoom(String flag, String prefix) {
        boolean found = false;
        int x = 0;
        String result = "";
        String e = flag + "-" + prefix;
        if (flag.equals("E2")) {
            e = "*" + prefix;
        }
      /*  for (Room room : rooms.values()) {
            if (room.lang.equals(flag) && room.password.isEmpty() && room.countStats && !room.isMinigame) {
                if ((room.isVanilla || room.isNormal) && room.playerCount() > 5 &&room.playerCount()<20) {
                    return room.name;
                } else if ((room.isBootcamp || room.isRacing || room.isDefilante) && room.playerCount() > 10 &&room.playerCount()<20) {
                    return room.name;
                }
            }
        }
        for (Room room : rooms.values()) {
            if (room.lang.equals("EN") && room.password.isEmpty() && room.countStats && !room.isMinigame) {
                if ((room.isVanilla || room.isNormal) && room.playerCount() > 5 &&room.playerCount()<15) {
                    return room.name;
                } else if ((room.isBootcamp || room.isRacing || room.isDefilante) && room.playerCount() > 10 &&room.playerCount()<15) {
                    return room.name;
                }
            }
        }*/
        while (!found) {
            x += 1;
            String n = e + x;
            Room room = this.rooms.get(n);
            if (room != null) {
                if (room.playerCount() < 25) {
                    found = true;
                    result = prefix + x;
                }

            } else {
                found = true;
                result = prefix + x;
            }
        }

        return result;
    }

    public void shoptxtTodb() throws IOException {
        String content = new String(Files.readAllBytes(Paths.get("./data/shoplist.txt")), Charset.forName("UTF-8"));
        String[] sl = content.split(";");
        colMarket.deleteMany(new Document());
        for (String _item : sl) {
            String[] h = _item.split(",");
            int cat = Integer.valueOf(h[0]);
            int item = Integer.valueOf(h[1]);
            int custom = Integer.valueOf(h[2]);
            int cheese = Integer.valueOf(h[5]);
            int strawberry = Integer.valueOf(h[6]);
            colMarket.insertOne(new Document("cat", cat).append("item", item).append("cus", custom).append("pey", cheese).append("cilek", strawberry));
        }

    }

    public void LoadShopData() throws IOException {
        ShopList.clear();
        shop_buf = getBuf();
      /*  String content = new String(Files.readAllBytes(Paths.get("./data/shoplist.txt")), Charset.forName("UTF-8"));
        String[] sl = content.split(";");
        shop_buf.writeInt(sl.length);
        for (String _item : sl) {
            String[] h = _item.split(",");
            int cat = Integer.valueOf(h[0]);
            int item = Integer.valueOf(h[1]);
            int custom = Integer.valueOf(h[2]);
            int cheese = Integer.valueOf(h[5]);
            int strawberry = Integer.valueOf(h[6]);
            /*
             * shop_buf.writeInt(cat); shop_buf.writeInt(item);
			 * shop_buf.writeByte(Integer.valueOf(__item[2]));
			 * shop_buf.writeByte(custom);
			 * shop_buf.writeByte(Integer.valueOf(__item[4]));
			 * shop_buf.writeInt(cheese); shop_buf.writeInt(strawberry);
			 * shop_buf.writeShort(0);

        shop_buf.writeShort(cat).writeShort(item)
                .writeByte(custom)
                .writeByte(
                        Integer.parseInt(h[3]))
                .writeByte(0).writeInt((cheese == 1000001) ? 200 : cheese)
                .writeInt(strawberry).writeShort(0);

        ShopList.put(Arrays.hashCode(new int[]{item, cat}), new int[]{custom, cheese, strawberry});
*///db.market.insert({"cat" : NumberInt(5), "item" : NumberInt(52), "cus" : NumberInt(3), "pey" : NumberInt(100), "cilek" : NumberInt(50) })
        Map<Integer, Integer> yeniler = new HashMap<>();
        MongoCursor<Document> it = colMarket.find().iterator();//.sort(new Document("cat", -1).append("item", -1)).iterator();
        while (it.hasNext()) {
            Document doc = it.next();

            ObjectId id = doc.getObjectId("_id");
            yeniler.put(Arrays.hashCode(new int[]{doc.getInteger("item"), doc.getInteger("cat")}), id.getTimestamp());
        }
        List<Map.Entry<Integer, Integer>> list = new LinkedList<>(yeniler.entrySet());
        boolean order = false;
        // Sorting the list based on values
        list.sort((o1, o2) -> order ? o1.getValue().compareTo(o2.getValue()) == 0
                ? o1.getKey().compareTo(o2.getKey())
                : o1.getValue().compareTo(o2.getValue()) : o2.getValue().compareTo(o1.getValue()) == 0
                ? o2.getKey().compareTo(o1.getKey())
                : o2.getValue().compareTo(o1.getValue()));
        LinkedHashMap<Integer, Integer> val = list.stream().limit(4).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> b, LinkedHashMap::new));

       /* colMarket.aggregate(Arrays.asList(new Document("$group", new Document("_id", "$cat").append("item", new Document("$max", "$item"))), new Document("$sort", new Document("_id", -1)))).forEach((Block<? super Document>) (Document d) -> {
            yeniler.put(Arrays.hashCode(new int[]{d.getInteger("item"), d.getInteger("_id")}), 13);

        });*/
        shop_buf.writeInt((int) colMarket.count());
        it = colMarket.find().sort(new Document("item", -1)).iterator();//.sort(new Document("cat", -1).append("item", -1)).iterator();
        while (it.hasNext()) {
            Document doc = it.next();

            int cat = doc.getInteger("cat");
            int item = doc.getInteger("item");
            int custom = doc.getInteger("cus");
            int cheese = doc.getInteger("pey");
            int yeni = 0;
            if (val.get(Arrays.hashCode(new int[]{doc.getInteger("item"), doc.getInteger("cat")})) != null) yeni = 14;
            //  if (item == 90 && (cat == 3 || cat == 4 || cat == 6 || cat == 8)) yeni = 14;
            int strawberry = doc.getInteger("cilek");
            shop_buf.writeShort(cat).writeShort(item)
                    .writeByte(custom)
                    .writeByte(yeni)
                    .writeByte(0).writeInt(cheese)
                    .writeInt(strawberry).writeShort(0);

            ShopList.put(MFServer.get_id_by_cat(item, cat), new int[]{custom, cheese, strawberry, yeni});
        }
        shop_byte = new byte[shop_buf.writerIndex()];
        shop_buf.readBytes(shop_byte);
    }

    public void fetchMaps() {
        Harita.haritalar(this);
        /*
         * this.col_maps.find().projection(new Document("perm",
         * true)).forEach((final Document document) -> { int _id =
         * document.getInteger("_id"); int perm = document.getInteger("perm");
         * ArrayList<Integer> l = perms.get(perm);
         *
         *
         * if (l == null) { l = new ArrayList<>(); perms.put(perm, l); }
         * l.add(_id); }, (final Void result, final Throwable th) -> {
         *
         *
         * });
         */
    }

    public static map getPerms(int[] mperms) {
        if (mperms.length < 1) {
            mperms = new int[]{1, 0};
        }
        int perm = mperms[random.nextInt(mperms.length)];
        int hash = Arrays.hashCode(mperms);
        ArrayList<Integer> array = perms.get(perm);
        if (array == null) {
            array = new ArrayList<>();
            for (int p : mperms) {
                ArrayList<Integer> arr = perms.get(p);
                if (arr != null) array.addAll(arr);
            }
            //  perms.put(hash, array);
        }
        int rnd;
        if (array.size() < 1) {
            return new VanillaMap(0);
        } else {
            rnd = array.get(random.nextInt(array.size()));
        }
        return Harita.al(rnd);

    }

    public static map getPerm(int perm) {
        if (!perms.containsKey(perm)) {
            perm = 1;
        }
        ArrayList<Integer> array = perms.get(perm);

        int rnd = array.get(random.nextInt(array.size()));
        return Harita.al(rnd);
    }

    public int checkTribeInDb(String name) {
        Kabile kab = colKabile.find(eq("isim", name)).projection(fields(include("_id"))).first();
        if (kab == null) {
            return 0;
        } else {

            return kab.code;
        }
    }


    public static int getSeq(String key) {
        return colSeq.findOneAndUpdate(new Document("_id", key), new Document("$inc", new Document("val", 1)), new FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER).upsert(true)).getInteger("val");
    }

    /**
     * @param name kullanıcı ismi
     * @return playerId ya da bulunamadıysa 0, eski isimlerde bulunduysa -1
     */
    public static int checkUserInDb(String name) {

      /*  Kullanici kullanici = null;
        for (Kullanici k : colKullanici.find(or(eq("isim", name), eq("eskiIsimler", name))).projection(fields(include("_id", "isim", "eskiIsimler")))) {

        }
        if (kullanici == null) return 0;

*/

        int rv = 0;
        for (Kullanici kul : colKullanici.find(or(eq("isim", name), eq("eskiIsimler", name))).projection(fields(include("_id", "isim", "eskiIsimler")))) {
            if (kul.eskiIsimler.contains(name) && !kul.isim.equals(name)) rv = -1;
            else return kul.code;
        }
        return rv;
    }

    public static String[] getIsim(int id) {

        return isimCache.getUnchecked(id);
    }
    public static Kullanici getUserFromDb(String username) {

        return userCache.getUnchecked(firstLetterCaps(username.toLowerCase()));
    }

    public static Kullanici _getUser(String username) {
        Kullanici kul = colKullanici.find(eq("isim", username))
                .projection(fields(include("bannedCommunities", "avatar", "code", "stand", "cinsiyet", "ruhesi", "yetki", "isSentinel", "isFuncorp", "isMapcrew", "isLuacrew", "isTester", "isArtist", "isFashion"))).first();
        if (kul == null) {
            kul = new Kullanici();
            kul.code = -1;
        }
        return kul;
    }
    public static String[] _getIsim(int id) {

        Kullanici kul = colKullanici.find(eq("_id", id)).projection(fields(include("isim", "tag"))).first();
        if (kul == null || kul.isim == null) {
            return new String[]{"", ""};
        } else {
            return new String[]{kul.isim, kul.tag};
        }
    }


    public String getRoom(String ad, boolean staff) {
        String oda = "";
        if (ad == null) return "";
        PlayerConnection cl = this.clients.get(ad);
        if (cl != null) {
            if (cl.room != null && ((!cl.hidden && !cl.topsecret) || staff)) {
                oda = cl.room.name;
            } else {
                try {
                    // cl.enterRoom(cl.server.recommendRoom(cl.Lang), false);
                } catch (Exception e) {
                    log.warning("ODA HATASI [" + cl.kullanici.isim + "]" + stackTraceToString(e));
                }
            }
        }
        return oda;
    }

    public ByteBuf pack_old(int t1, int t2) throws IOException {
        return pack_old(t1, t2, new Object[]{});
    }


    public class BsonMapCodec implements Codec<HashMap> {

        public void encode(BsonWriter writer, HashMap value, EncoderContext encoderContext) {

            writer.writeStartDocument();
            writer.writeBoolean("le", true);
            writer.writeEndDocument();
        }

        public HashMap decode(BsonReader reader, DecoderContext decoderContext) {
            return new HashMap<>();
        }

        public Class<HashMap> getEncoderClass() {
            return HashMap.class;
        }
    }


    public boolean removeStar(int pId, int title, int star) {
        Kullanici kullanici = colKullanici.find(eq("_id", pId)).projection(Projections.include("yildizlar")).first();
        if (kullanici != null && kullanici.yildizlar.getOrDefault(String.valueOf(title), 0) == star) {
            colKullanici.updateOne(eq("_id", pId), and(pull("unvanlar", title), unset("yildizlar." + title)));
            PlayerConnection pl = clients2.get(pId);
            if (pl != null && pl.kullanici != null) {
                pl.kullanici.yildizlar.remove(String.valueOf(title));
                pl.kullanici.unvanlar.remove(title);
                if (pl.kullanici.unvan == title) pl.kullanici.unvan = 0;
            }
            return true;
        }
        return false;

    }

    public void addStar(int pId, int title, int star) {
        Kullanici kullanici = colKullanici.find(eq("_id", pId)).projection(Projections.include("yildizlar")).first();
        if (kullanici != null && kullanici.yildizlar.getOrDefault(String.valueOf(title), 0) < star) {
            colKullanici.updateOne(eq("_id", pId), and(addToSet("unvanlar", title), set("yildizlar." + title, star)));
            PlayerConnection pl = clients2.get(pId);
            if (pl != null && pl.kullanici != null) {
                pl.kullanici.yildizlar.put(String.valueOf(title), star);
                pl.kullanici.unvanlar.add(title);
            }
        }


    }

    public static HashMap<String, ArrayList<Integer>> racingMap = new HashMap<>();
    public static HashMap<String, ArrayList<Integer>> bootcampMap = new HashMap<>();
    public static HashMap<String, ArrayList<Integer>> defilanteMap = new HashMap<>();
    public static HashMap<String, ArrayList<Integer>> shamanMap = new HashMap<>();

    public static String getTitle(int title, String dil) {
        return getTitle(title, dil, 0);
    }

    public static String getTitle(int title, String dil, int cinsiyet) {
        String t = Dil.yazi(dil, "title" + title);
        if (cinsiyet == 0) return t;
        else {
            String[] tit = t.substring(1, t.length() - 1).split("\\|");
            if (tit.length == 1) return t;
            else {

                Pattern r = Pattern.compile("\\((.*)\\|(.*)\\)");
                Matcher m = r.matcher(t);
                if (m.find()) {
                    String rep = m.group(0);
                    if (cinsiyet == 1) rep = m.group(2);
                    return t.replace(m.group(0), rep);
                }
                return t;

            }
        }
    }

    public static String[] types = new String[]{"daily", "weekly", "monthly", "general"};

    public static Date getTTL(String type) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        switch (type) {
            case "daily":
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                break;
            case "weekly":
                calendar.add(Calendar.WEEK_OF_YEAR, 1);
                calendar.set(Calendar.DAY_OF_WEEK, 1);
                break;
            case "monthly":
                calendar.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 0);
                break;
            case "general":
                calendar.add(Calendar.YEAR, 100);
                break;
        }
        return calendar.getTime();

    }

    public static String crunchifyGenerateThreadDump() {
        final StringBuilder dump = new StringBuilder();
        final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        final ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
        for (ThreadInfo threadInfo : threadInfos) {
            dump.append('"');
            dump.append(threadInfo.getThreadName());
            dump.append(threadInfo.getThreadId());
            dump.append("\" ");
            final Thread.State state = threadInfo.getThreadState();
            dump.append("\n   java.lang.Thread.State: ");
            dump.append(state);
            final StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
            for (final StackTraceElement stackTraceElement : stackTraceElements) {
                dump.append("\n        at ");
                dump.append(stackTraceElement);
            }
            dump.append("\n\n");
        }
        return dump.toString();
    }

    public void updateRanking(String mode, HashMap<String, ArrayList<Integer>> hMap) throws Exception {
        int[] stars = new int[]{1, 3, 5, 9};
        int[] titles = new int[]{0, 0, 0};
        switch (mode) {
            case "racing":
                titles = new int[]{10000, 10001, 10002};
                break;
            case "bootcamp":
                titles = new int[]{10003, 10004, 10005};
                break;
            case "defilante":
                titles = new int[]{10006, 10007, 10008};
                break;
            case "saves":
                titles = new int[]{10009, 10010, 10011};
                break;
        }
        int size = 10;

        if (hMap.isEmpty()) {
            for (String type : types) {
                hMap.put(type, new ArrayList<>());
            }
        }
        int j = 0;
        for (Map.Entry<String, ArrayList<Integer>> entry : hMap.entrySet()) {
            String k = entry.getKey();
            ArrayList<Integer> v = entry.getValue();
            ArrayList<Integer> arr = new ArrayList<>();
            colSiralama.find(and(eq("mode", mode), eq("type", k))).sort(Sorts.descending("counter")).projection(Projections.include("pId")).limit(size).map((siralama -> siralama.getInteger("pId"))).into(arr);

            int i = 0;
            for (Integer yeni : arr) {
                boolean update;
                Integer oldPid;
                if (v.size() > i && i < titles.length) {

                    oldPid = v.get(i);
                    update = !yeni.equals(oldPid);
                    if (update) {
                        update = removeStar(oldPid, titles[i], stars[j]);
                    }
                } else update = true;

                if (update && i < titles.length) {
                    addStar(yeni, titles[i], stars[j]);
                    ArrayList<PlayerConnection> players = new ArrayList<>();
                    if (j == 0 || j == 1 || j == 2) {
                        PlayerConnection pl = clients2.get(yeni);
                        if (pl != null && pl.room != null) {
                            players.addAll(pl.room.clients.values());
                        }
                    } else if (j == 3) {
                        for (Room room : rooms.values()) {
                            if ((mode.equals("racing") && room.isRacing) || (mode.equals("bootcamp") && room.isBootcamp) || (mode.equals("defilante") && room.isDefilante) || (mode.equals("shaman") && (room.isVanilla))) {
                                players.addAll(room.clients.values());
                            }
                        }
                    } else {
                        players.addAll(clients.values());
                    }
                    PlayerConnection y = clients2.get(yeni);
                    for (PlayerConnection playerConnection : players) {
                        if (playerConnection.kullanici == null) continue;
                        if (!playerConnection.kullanici.optAnnounce) continue;
                        String tit;
                        if (y != null) {
                            tit = getTitle(titles[i], playerConnection.Lang, y.kullanici.cinsiyet);
                        } else {
                            tit = getTitle(titles[i], playerConnection.Lang);
                        }
                        try {
                            String mes = Kirai.from(Dil.yazi(playerConnection.Lang, "announcement_ranking")).put("username", MFServer.plusReady(MFServer.getIsim(yeni))).put("type", Dil.yazi(playerConnection.Lang, k)).put("title", tit).format().toString();
                            playerConnection.sendMessage(mes);
                        } catch (Exception e) {
                            System.out.println(tit);
                        }
                    }
                    //
                }
                i++;
            }
            j++;
            hMap.put(k, arr);
        }

    }


    public static void updateToRanking(int pId, String mode, int num, int Ctype, int Cnum) {
        for (String type : types) {
            MFServer.executor.execute(() -> {
                Document up = new Document("counter", num);
                switch (Ctype) {
                    case 1:
                        up.append("counter_1", Cnum);
                        break;
                    case 2:
                        up.append("counter_2", Cnum);
                        break;
                    case 3:
                        up.append("counter_3", Cnum);
                        break;
                }

                colSiralama.updateOne(and(eq("pId", pId), eq("mode", mode), eq("type", type)), new Document("$inc", up), new UpdateOptions().upsert(true));

            });
        }
    }

    static <K, V extends Comparable<? super V>>
    SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(
                new Comparator<Map.Entry<K, V>>() {
                    @Override
                    public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                        int res = e1.getValue().compareTo(e2.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    private void run() throws InterruptedException, ClassNotFoundException, SQLException, IOException, IllegalAccessException, NoSuchFieldException {

        // create_lua();
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), fromCodecs(new BsonMapCodec()),
                fromProviders(PojoCodecProvider.builder().register("veritabani").automatic(true).build()));
        /*CodecRegistry pojoCodecRegistry = fromRegistries(MongoClients.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));*/
        mongoClient = MongoClients.create(
                MongoClientSettings.builder().codecRegistry(pojoCodecRegistry).applyToConnectionPoolSettings(
                        builder -> builder.maxSize(500)

                ).build());
        //.create("localhost", MongoClientOptions.builder().connectionsPerHost(500).codecRegistry(pojoCodecRegistry).build());
        dbOyun = mongoClient.getDatabase("oyun").withCodecRegistry(pojoCodecRegistry);
        dbKayit = mongoClient.getDatabase("kayit").withCodecRegistry(pojoCodecRegistry);
        colRekorlar = dbOyun.getCollection("rekorlar", Rekorlar.class);
        colMiniOyun = dbOyun.getCollection("minioyun", MiniOyun.class);
        colMG = dbOyun.getCollection("mg");
        colMarket = dbOyun.getCollection("market");
        colEnFazla = dbOyun.getCollection("enfazla");
        colKullanici = dbOyun.getCollection("kullanicilar", Kullanici.class);
        colRutbe = dbOyun.getCollection("rutbeler", Rutbe.class);
        colUnvanlar = dbOyun.getCollection("unvanlar");
        colKabile = dbOyun.getCollection("kabileler", Kabile.class);
        colKabileGecmisi = dbOyun.getCollection("kabilegecmisi", KabileGecmisi.class);
        colGorev = dbOyun.getCollection("gorev", Gorev.class);
        colLuaLog2 = dbKayit.getCollection("lua2");
        colLuaLog = dbKayit.getCollection("lua");
        colSeq = dbKayit.getCollection("seq");
        colIpBan = dbKayit.getCollection("ipban", IPBan.class);
        colIP = dbKayit.getCollection("ip");
        colPermLog = dbKayit.getCollection("permlog");
        colCommandLog = dbKayit.getCollection("commandlog");
        colErrorLog = dbKayit.getCollection("errorlog");
        colBenchLog = dbKayit.getCollection("benchlog");
        colHoleLog = dbKayit.getCollection("holelog");
        colChatLog = dbKayit.getCollection("chatlog");
        colCezaLog = dbKayit.getCollection("cezalog", Ceza.class);
        colPMLog = dbKayit.getCollection("pmlog");
        colRefs = dbKayit.getCollection("refs");
        colSiralama = dbKayit.getCollection("siralama");
        colPacketLog = dbKayit.getCollection("packetlog");
        colUyarilar = dbOyun.getCollection("uyarilar", Uyari.class);
        colIsp = dbKayit.getCollection("isp");
        colBad = dbKayit.getCollection("badwords");
        colHackers = dbKayit.getCollection("hackers");
        colRandomUUID = dbKayit.getCollection("random_uuid");
        colOplog = dbKayit.getCollection("oplog");
        colBotVerify = dbKayit.getCollection("botverify");
        colGiris = dbKayit.getCollection("giris", Login.class);
        colFriendReq = dbOyun.getCollection("friendreq");
        colFikicoinler = dbKayit.getCollection("fikicoinler");
        //Panel

        colNeutralWords = dbKayit.getCollection("neutral_words");
        colBadWords = dbKayit.getCollection("bad_words");
        colBadISP = dbKayit.getCollection("bad_isp");
        colBadTags = dbKayit.getCollection("bad_tags");
        colGameSettings = dbOyun.getCollection("settings");
        //
        Kullanici.db = colKullanici;
        Kabile.db = colKabile;
        Rutbe.db = colRutbe;

        colKullanici.find().first();
        colKabile.find().first();
        colRutbe.find().first();

       /* PlayerConnection PC = new PlayerConnection();
        PC.kullanici = new Kullanici();
        PC.kullanici.yetki = 10;
        PC.server = this;
        PC.kullanici.isim = "Sevenops";
        PlayerConnection user = new PlayerConnection();
        user.kullanici = new Kullanici();
        user.kullanici.isim = "Lame";
        clients.put(user.kullanici.isim, user);
        Room room = new Room(this, "EN-1");
        user.room = room;
        new warn().isle(PC, new String[]{"lame", "spam"});
        Thread.sleep(100000);*/

        // updateToRanking(1, "racing", 1, 1, 1);

        colSiralama.createIndex(Indexes.ascending("ttl"), new IndexOptions().expireAfter(0L, TimeUnit.MILLISECONDS));
        colSiralama.createIndex(Indexes.compoundIndex(Indexes.ascending("mode"), Indexes.ascending("type"), Indexes.descending("counter")));
        colSiralama.createIndex(Indexes.ascending("pId"));
        //  if(true)System.exit(0);
        FileHandler fileHandler = new FileHandler("server.log", true);
        fileHandler.setFormatter(new CustomRecordFormatter());
        log.addHandler(fileHandler);


        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            File file = new File("info");
            try {
                FileOutputStream f = new FileOutputStream(file);
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(Bilgiler);
                s.close();
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }
            file = new File("fashion");
            try {
                FileOutputStream f = new FileOutputStream(file);
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(fashion);
                s.close();
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }
            file = new File("columnHashes");
            try {
                FileOutputStream f = new FileOutputStream(file);
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(columnHashes);
                s.close();
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }
            file = new File("IpHashes");
            try {
                FileOutputStream f = new FileOutputStream(file);
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(IpHashes);
                s.close();
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }
            /*  file = new File("rekorlar");
            try {
                FileOutputStream f = new FileOutputStream(file);
                ObjectOutputStream s = new ObjectOutputStream(f);
                s.writeObject(Rekorlar);
                s.close();
            } catch (IOException e) {
                log.warning(stackTraceToString(e));
            }*/
            int i = 0;
            for (PlayerConnection pc : this.clients.values()) {
                try {
                    pc.SaveUser();
                    i++;
                } catch (Exception e) {
                    log.warning(stackTraceToString(e));
                }
            }
            System.out.println("Saved " + i);
        }));
        try {
            File file = new File("info");
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(f);
            Bilgiler = (HashMap<Integer, HashMap<String, Object>>) s.readObject();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /* try {
            File file = new File("rekorlar");
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(f);
            Rekorlar = (ConcurrentHashMap<Integer, ArrayList<Rekor>>) s.readObject();
            s.close();
        } catch (IOException e) {
            Rekorlar = new ConcurrentHashMap<>();
            e.printStackTrace();
        }*/

        try {
            File file = new File("fashion");
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(f);
            fashion = (HashMap<Integer, String[]>) s.readObject();
        } catch (Exception e) {
            log.warning(stackTraceToString(e));
            fashion = new HashMap<>();
        }

        MessagePack mp = new MessagePack();
        File initialFile = new File("bad_isp");
        BAD_ISP = new HashSet<>();
        try {

            InputStream targetStream = new FileInputStream(initialFile);
            List<String> al = mp.read(targetStream, Templates.tList(TString));
            for (String s : al) {
                BAD_ISP.add(s.split(" ", 2)[0]);
            }
//			BAD_ISP.addAll(al);
        } catch (Exception e) {

            log.warning(stackTraceToString(e));
        }
        BAD_ISP.clear();
        BAD_ISP.add("AS395332");
        BAD_ISP.add("AS11878");
        BAD_ISP.add("AS8100");
        mp = new MessagePack();
        NEUTRAL_WORDS.add("discord.gg");
        NEUTRAL_WORDS.add("discord.com");
        MOD_COMMANDS.add("casier ");
        MOD_COMMANDS.add("logip ");
        MOD_COMMANDS.add("logcon ");
        MOD_COMMANDS.add("ip ");
        initialFile = new File("bad_words");
        try {

            InputStream targetStream = new FileInputStream(initialFile);
            List<String> al = mp.read(targetStream, Templates.tList(TString));
            BAD_WORDS.addAll(al);
        } catch (Exception e) {

            log.warning(stackTraceToString(e));
        }
        try {
            File file = new File("columnHashes");
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(f);
            columnHashes = (HashMap<Integer, Integer>) s.readObject();
        } catch (Exception e) {
            columnHashes = new HashMap<>();
        }
        try {
            File file = new File("IpHashes");
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(f);
            IpHashes = (int[]) s.readObject();
        } catch (Exception e) {
            IpHashes = new int[4];
        }
        langs.put(0, "EN");
        langs.put(1, "FR");
        langs.put(2, "RU");
        langs.put(3, "BR");
        langs.put(4, "ES");
        langs.put(6, "TR");
        langs.put(7, "VK");
        langs.put(8, "PL");
        langs.put(9, "HU");
        langs.put(10, "NL");
        langs.put(11, "RO");
        langs.put(12, "ID");
        langs.put(13, "DE");
        langs.put(14, "E2");
        langs.put(15, "AR");
        langs.put(16, "PH");
        langs.put(17, "LT");
        langs.put(18, "JP");
        langs.put(20, "FI");
        langs.put(21, "CZ");
        langs.put(23, "HR");
        langs.put(24, "BG");
        langs.put(25, "LV");
        langs.put(26, "HE");
        langs.put(27, "IT");
        langs.put(29, "EE");
        langs.put(31, "PT");

        tersLangs
                = langs.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        String name = getComputerName();
        HikariConfig config = new HikariConfig();
        System.out.println(name);
        String pw = "";
        if (name.equals("altanozlu")) {
            config.setJdbcUrl("jdbc:mysql://localhost/game?useSSL=false");
        } else {
            new Thread(() -> {

                RareTitleList = new TreeMap<>();
                long ttt = System.currentTimeMillis();
                for (Kullanici kullanici : colKullanici.find(gt("cevrimici", MFServer.timestamp() - 60 * 60 * 24 * 30 * 6)).projection(include("unvanlar", "evInfo.titles"))) {
                    for (Integer unvan : kullanici.unvanlar) {
                        Integer count = RareTitleList.get(unvan);
                        if (count == null) count = 0;
                        count++;
                        RareTitleList.put(unvan, count);
                    }
                }
                // prints "{A=3, B=2, C=1}"
                int I = 0;
                int L = 250;
                for (Map.Entry<Integer, Integer> x : entriesSortedByValues(RareTitleList)) {
                    int unvan = x.getKey();
                    if (I++ > L) {
                        RareTitleList.remove(unvan);
                    }
                }
                System.out.println("RareTitleList build ms : " + (System.currentTimeMillis() - ttt));
            }).start();
            new Thread(() -> {
                int count = 0;
                int[] allTitles = new int[]{10000, 10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010, 10011};
                for (int title : allTitles) {
                    for (Kullanici kullanici : colKullanici.find(eq("unvanlar", title))) {
                        UpdateResult stat = colKullanici.updateOne(eq("_id", kullanici.code), and(pull("unvanlar", title), unset("yildizlar." + title)));
                        count += stat.getModifiedCount();
                    }
                }
                System.out.println("Modified Count: " + count);
                while (true) {
                    try {
                        updateRanking("racing", racingMap);
                        updateRanking("bootcamp", bootcampMap);
                        updateRanking("defilante", defilanteMap);
                        updateRanking("saves", shamanMap);
                        for (String type : types) {
                            long t = MFServer.getTTL(type).getTime();
                            if (t - System.currentTimeMillis() < 5_000) {
                                colSiralama.deleteMany(eq("type", type));
                                Thread.sleep(10_0000);
                            }
                        }
                        Thread.sleep(500);
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                        e.printStackTrace();
                    }

                    // }
                }
            }).start();
            config.setJdbcUrl("jdbc:mysql://localhost/game?useSSL=false&amp;useUnicode=true&amp;characterEncoding=utf8");
        }
        int poolSize = 130;
      /* if(name.equals("Unknown Computer")){

            config.setJdbcUrl("jdbc:mysql://138.201.60.90/game?useSSL=false");
            pw="DenemeKodu@123Lala";
            poolSize=10;
        }*/
        config.setUsername("root");
        config.setPassword(pw);

        config.setMaximumPoolSize(poolSize);
        config.setAutoCommit(true);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        laodCommands();
        try {
            DS = new HikariDataSource(config);
        } catch (Exception e) {

        }
        new Thread(() -> {
            try {
                update_modules();
                update_bots();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).run();

        log.info("MF Server starting.");
        networkManager = new NetworkManager(this);

        try {
            networkManager.start();

        } catch (IOException ex) {
            log.warning(stackTraceToString(ex));
            // Logger.getLogger(MFServer.class.getName()).log(Level.WARNING,
            // null, ex);
        }

        // for (int i =0;i<10;i++){
        // getNextSequence((Document result, final Throwable t)
        // ->{System.out.println(result);},"users");
        // }
        // Create the tick workers
        //ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);
        log.info("MF Server started.");
        try {
            LoadShopData();
        } catch (IOException e) {
            log.warning(stackTraceToString(e));
        }


        executor.scheduleAtFixedRate(() -> {
            for (PlayerConnection cl : clients.values()) {
                if (!cl.rand.isEmpty()) {
                    Document doc = colHackers.find(and(eq("rand", cl.rand), gt("epoch", MFServer.timestamp() - 60 * 15))).first();
                    if (doc != null) {
                        try {
                            banPlayer(cl.kullanici.isim, 1800, "Using hack tools is forbidden r", "Tengri", false, cl.ip);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        cl.channel.close();
                        try {
                            cl.sendChatServer("Tengri banned " + cl.kullanici.isim + " for 1800 hours. Reason : Using hack tools is forbidden r");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }, 15, 30, TimeUnit.SECONDS);

      if(false){
          executor.scheduleAtFixedRate(() -> {
              float r = random.nextFloat();
              if (r < 0.5) {
                  genel_carpan = 1;
              } else if (r < 0.8) {
                  genel_carpan = 2;
              } else {
                  genel_carpan = 4;
              }

              if (genel_carpan != 1) {
                  try {
                      ByteBuf data = pack_modchat(1, "Tengri", "For next 15 minutes multiplier is " + genel_carpan + ". It means now you can earn " + genel_carpan + "x on everything.");
                      for (PlayerConnection pc : this.clients.values()) {
                          pc.sendPacket(data.retainedSlice());
                      }
                      data.release();
                  } catch (Exception e) {
                      log.warning(stackTraceToString(e));
                  }
              }

          }, 2, 15, TimeUnit.MINUTES);

      }
        executor.scheduleAtFixedRate(() -> {
            clients.forEach((k, v) -> {
                try {
                    v.dataCount = 0;
                    v.dataCountOld = 0;
                    v.sendPing();
                } catch (Exception e) {
                    log.warning(stackTraceToString(e));
                }
            });
        }, 0, 5, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(() -> {
            HashMap<String, Integer> stats = new HashMap<>();
            HashMap<String, Integer> docs3 = new HashMap<>();
            AtomicInteger total = new AtomicInteger();
            clients.forEach((k, v) -> {
                Integer i = stats.getOrDefault(v.Lang, 0);
                stats.put(v.Lang, i + 1);
                Integer i2 = docs3.getOrDefault(v.countryCode, 0);
                docs3.put(v.countryCode, i2 + 1);
                total.incrementAndGet();

            });
            ArrayList<Document> docs = new ArrayList<>();
            stats.forEach((k, v) -> {
                docs.add(new Document("dil", k).append("sayi", v));
            });
            /*boolean oner = rooms.values().stream().filter(room -> room.lang.equals("XX") || room.lang.equals("E2")).mapToInt(Room::playerCount).sum() > 5;
            try {
                for (Map.Entry<String, PlayerConnection> entry : clients.entrySet()) {
                    String k = entry.getKey();
                    PlayerConnection v = entry.getValue();

                    if (!v.sent && v.room != null && v.room.playerCount() < 5 && !v.room.isEditeur && stats.getOrDefault(v.Lang, 0) < 20 && oner && !v.Lang.equals("TR")) {
                        ByteBufOutputStream p = MFServer.getStream();

                        v.sent = true;
                        p.writeByte(29);

                        p.writeByte(23);
                        p.writeInt(7778);
                        p.writeByte(1);
                        p.writeUTF(Dil.yazi(v.Lang, "recommend"));
                        p.writeShort(300);
                        p.writeShort(300);
                        p.writeShort(200);
                        p.writeBoolean(false);
                        v.LuaGame();
                        v.sendPacket(p);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            colEnFazla.insertOne(new Document("tarih", MFServer.timestamp()).append("toplam", total).append("topluluklar", stats).append("topluluklar2", docs).append("topluluklar3", docs3));
        }, 0, 5, TimeUnit.SECONDS);
        fetchMaps();
        executor.scheduleAtFixedRate(() -> {
            int t = MFServer.timestamp() + 3600 * 2;
            HashSet<String> valids = new HashSet<>();
            this.RaporSistemi.values().forEach((Raporlar v) -> {
                if (v.surev > t) {
                    valids.add(v.ad);
                    //RaporSistemi.remove(v.ad);
                }

            });
            valids.forEach(RaporSistemi::remove);
            //yasaklamalar.entrySet().removeIf(entry -> !valids.contains(entry.getKey()));
        }, 0, 10, TimeUnit.MINUTES);
        executor.scheduleAtFixedRate(() -> {
            String[] keys = new String[]{"new_forum", "ads"};
            clients.forEach((k, v) -> {
                if (v.countryCode.equals("TR")) {
                    String[] msgs = new String[]{"<VP>★★★ <a href=\"https://mforum.ist/forums/discussions.30/\" target=\"_blank\">Yardıma mı ihtiyacınız var ? Forumda konu açabilirsiniz https://mforum.ist/forums/discussions.30/</a>","<VP>★★★ <a href=\"https://mforum.ist/threads/discord-miceforce-tr.10916/\" target=\"_blank\">Yeni Discord sunucumuz hakkında bilgi almak ister misiniz ? https://mforum.ist/threads/discord-miceforce-tr.10916/</a>"};
                    String msg = msgs[MFServer.random.nextInt(msgs.length)];

                    try {
                        v.sendPacket(MFServer.pack_message(msg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
               /* try {
                    String key = keys[random.nextInt(keys.length)];
                    String lang = v.Lang;
                    if (lang.equals("E2")) lang = "GL";
                    v.sendPacket(MFServer.pack_message("<font color='#439c99'>★[" + lang + "]</font><N> " + Dil.yazi(v.Lang, key)));
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            });
        }, 1, 10, TimeUnit.MINUTES);
        /*  executor.scheduleWithFixedDelay(() -> {
            if (candChecker.getAndSet(false)) {
                ByteBufOutputStream bs = MFServer.getStream();
                try {
                    bs.writeByte(16);
                    bs.writeByte(10);
                    bs.writeByte(23);
                    bs.writeByte(2);
                    bs.writeByte(1);
                    bs.writeUTF(candyCount.toString());
                } catch (IOException e) {
                  return;
                }
                this.rooms.values().stream().filter(k -> k.isBar).forEach(k -> {
                    try {
                        k.sendAll(bs.buffer().copy());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                MFServer.bitti(bs.buffer());
            }
        }, 0, 1, TimeUnit.SECONDS);*/
        // executor.scheduleAtFixedRate(this::rekorlariIncele, 0, 30, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(() -> {
            try {
                tribes.forEach((k, v) -> {
                    if (v.clients.size() == 0 & v.lock.get() != 0) {
                        closeTribe(k);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            rooms.forEach((k, v) -> {
                long t = System.currentTimeMillis() + (1000 * 60);

                if (v.clients.size() == 0) {
                    if (v.mapisReady.get() != -1 || v.sil++ > 15) {

                        try {
                            this.closeRoom(k, v);
                        } catch (Exception e) {
                            e.printStackTrace();
                            rooms.remove(k);
                        }

                    }
                    return;
                }
                if (v.isofficial && !v.isBot && !v.isMinigame && !v.isFuncorp && t - (v.Started + (v.orjroundTime * 1000)) > 60_000) {

                    if (v.sonEntropi == 0) {
                        v.entropiSayisi += 1;
                    } else if (System.currentTimeMillis() - v.sonEntropi < 60_000 * 5) {
                        v.entropiSayisi += 1;
                    }
                    if (v.entropiSayisi >= 3) {
                        rooms.remove(k);
                        System.out.println("kapıyom " + k);
                    } else {
                        System.out.println(v.entropiSayisi + "kapatmıyom " + k);
                        v.sonEntropi = System.currentTimeMillis();
                        v.NewRound();
                    }


                }
            });
            try {
                checkForCafes.forEach((k, v) -> {
                    if (this.clients2.containsKey(k)) {
                        v.checkForCafePoints();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            checkForCafes.clear();
        }, 0, 10, TimeUnit.SECONDS);


        executor.scheduleAtFixedRate(() -> {

            rooms.forEach((k, v) -> {
                for (Map.Entry<String, PlayerConnection> a : v.clients.entrySet()) {
                    PlayerConnection playerConnection = a.getValue();
                    String key = a.getKey();
                    if (playerConnection.kullanici == null) {
                        v.clients.remove(key);
                        this.clients.remove(key);
                    }
                }
            });

        }, 0, 3, TimeUnit.SECONDS);


        executor.scheduleAtFixedRate(() -> {
            int[] i = new int[1];
            long l = System.currentTimeMillis();
            try {
                clients.forEach((k, v) -> {
                    boolean lale = false;
                    try {
                        if (v.room == null) {
                            lale = true;
                            clients.remove(k);
                            if (v.channel != null) v.channel.close();
                            v.disc();

                            i[0]++;

                        } else if (v.last_data + 120_000 < l) {
                            clients.remove(k);
                            lale = true;
                            if (v.channel != null) v.channel.close();
                            v.disc();
                            i[0]++;

                        } else if (v.kullanici == null) {
                            clients.remove(k);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    } finally {
                        if (lale) clients.remove(k);
                    }


                });
            } catch (Exception e) {
                MFServer.log.warning(MFServer.stackTraceToString(e));
            }
            if (i[0] > 20) {
                System.out.println("ENTROPY : " + i[0]);
                try {
                    Runtime.getRuntime().exec("jstack `ps aux | grep Acun.jar | grep -v grep | awk '{print $2}'`");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MFServer.log.warning(crunchifyGenerateThreadDump());
            }

        }, 0, 10, TimeUnit.SECONDS);
        new Thread(() -> {

            PreparedStatement statement = null;
            while (true) {
                if (CLD.size() == 0) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                try (Connection connection = DS.getConnection()) {
                    // connection.setCatalog("log");
                    SqlData sd;
                    while (true) {
                        try {
                            sd = CLD.pop();
                        } catch (NoSuchElementException e) {
                            break;
                        }

                        switch (sd.typ) {
                            case 1:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `log`.`chat`(`name`,`room`,`message`,`time_stamp`)VALUES(?,?,?,?);");
                                statement.setString(1, sd.name);
                                statement.setString(2, sd.room);
                                statement.setString(3, sd.message);
                                statement.setInt(4, sd.time_stamp);
                                statement.execute();
                                statement.close();
                                break;
                            case 2:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `log`.`login`(`name`,`ip`,`time_stamp`,dil,city,country,tz)VALUES(?,?,?,?,?,?,?);");
                                statement.setString(1, sd.name);
                                statement.setString(2, sd.ip);
                                statement.setString(4, sd.dil);
                                statement.setInt(3, sd.time_stamp);
                                statement.setString(5, sd.city);
                                statement.setString(6, sd.country);
                                statement.setString(7, sd.tz);
                                statement.execute();
                                statement.close();
                                break;
                            case 3:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `log`.`pm`(`name`,`message`,`sent_to`,`time_stamp`)VALUES(?,?,?,?)");
                                statement.setString(1, sd.name);
                                statement.setString(2, sd.message);
                                statement.setString(3, sd.sent_to);
                                statement.setInt(4, sd.time_stamp);
                                statement.execute();
                                statement.close();
                                break;
                            case 4:
                                statement = connection
                                        .prepareStatement("INSERT IGNORE INTO log.ip(`ip`,`isp`,`dil`,city,country,tz)VALUES(?,?,?,?,?,?);");
                                statement.setString(1, sd.ip);
                                statement.setString(2, sd.isp);
                                statement.setString(3, sd.dil);
                                statement.setString(4, sd.city);
                                statement.setString(5, sd.country);
                                statement.setString(6, sd.tz);
                                statement.execute();
                                statement.close();
                                break;
                            case 5:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `log`.`mod_chat`(`name`,`message`,`type`,`time_stamp`)VALUES(?,?,?,?);");
                                statement.setString(1, sd.name);
                                statement.setString(2, sd.message);
                                statement.setInt(3, sd.type);
                                statement.setInt(4, sd.time_stamp);
                                statement.execute();
                                statement.close();
                                break;

                            case 6:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `tribe_history` (`tribe_code`, `js`, `time_stamp`, `typ`) VALUES (?,?,?,?);");
                                statement.setInt(1, sd.code);
                                statement.setString(2, sd.message);
                                statement.setInt(3, sd.time_stamp);
                                statement.setInt(4, sd.type);
                                statement.execute();
                                statement.close();
                                break;
                            case 7:
                                statement = connection.prepareStatement(
                                        "INSERT INTO `log`.`hole`(data)VALUES(?);");
                                statement.setBytes(1, sd.data);
                                statement.execute();
                                statement.close();
                                break;
                            case 8:
                                break;

                        }
                        statement = null;
                    }
                } catch (Exception e) {
                    if (statement != null) {
                        try {
                            statement.close();
                        } catch (SQLException e1) {
                            // e1.printStackTrace();
                        }
                    }
                    //log.warning(stackTraceToString(e));
                }
            }

        }).start();
        executor.scheduleAtFixedRate(() -> {
            if (enableEvent2) {
                int power = 20;
                int duration = 30;
                for(Room room:rooms.values()){
                    if (room.l_room!=null)continue;
                    ByteBuf packet = MFServer.getBuf().writeByte(5).writeByte(23).writeBoolean(true).writeShort(power);

                    try {
                        room.sendAll(packet);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Room.iptal(room.snowTimer);
                    room.snowTimer = MFServer.executor.schedule(() -> {

                        ByteBuf packetn = MFServer.getBuf().writeByte(5).writeByte(23).writeBoolean(false).writeShort(power);

                        try {
                            room.sendAll(packetn);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }, (long) duration, TimeUnit.SECONDS);
                }
            }
        },1,5,TimeUnit.MINUTES);
        new Thread(() -> {

            while (true) {
                long t = System.currentTimeMillis();
                IP_BANS.forEach((k, v) -> {
                    if (v <= t) {
                        IP_BANS.remove(k);
                    }
                });
                for (IPBan ipb : MFServer.colIpBan.find(lt("deadline", t))) {
                    IP_STATS.remove(ipb._id);
                    MFServer.colIpBan.deleteOne(eq("_id", ipb._id));
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    log.warning("Error in IPBAN CLEANER " + stackTraceToString(e));
                }

            }

        }).start();
     /*   new Thread(() -> {
            while (true) {
                int i = 0;
               for (PlayerConnection pc : clients.values()) {
                    try {
                        MFServer.session.execute("update online set mf=true where id = ?;", pc.kullanici.code);
                        com.datastax.driver.core.ResultSet rs = MFServer.session.execute("select * from pm where kime = ?;", pc.kullanici.code);
                        for (Row row : rs) {
                            int pid = row.getInt("kimden");
                            if (!pc.kullanici.engellenenler.contains(pid) && pc.friends.contains(pid)) {
                                String[] x = MFServer.getIsim(pid);
                                String ileti = row.getString("ileti");
                                int dil = row.getInt("dil");

                                pc.TRIBULLE.receivePM(MFServer.plusReady(x[0], x[1]), ileti, dil);
                                i++;
                            }
                        }
                        MFServer.session.execute("delete from pm where kime = ?;", pc.kullanici.code);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //  if (i > 0) System.out.println(i);
            }
        }).start();*/
        while (true) {
            try {
                for (LUA_THREAD lt : threads) {

                    int lim = lt.limit;
                    String dev = lt.developer;
                    if (lt.time + 4_000 < System.currentTimeMillis() || lt.interrupted || lt.l_room.call_count.get() > lim) {
                        if (!lt.interrupted) {
                            int cc = lt.l_room.call_count.get();
                            lt.l_room.close();
                            if (lt instanceof LUA_THREAD2 && cc > lim && dev != null && dev.length() >= 3) {
                                System.out.println("SCRIPT LONG RUN " + dev);
                                try {
                                    long t = 5 * 30;
                                    PlayerConnection pl = clients.get(dev);
                                    long ban = System.currentTimeMillis() + 1000 * t;
                                    if (pl.kullanici != null) {
                                        pl.kullanici.luaBan = ban;
                                    } else {
                                        MFServer.colKullanici.updateOne(eq("isim", dev), set("luaBan", ban));
                                    }

                                } catch (Exception e) {
                                    System.out.println(MFServer.stackTraceToString(e));
                                }
                            }

                        }
                        lt.interrupt();
                        threads.remove(lt);
                    }
                }
                Thread.sleep(50);
            } catch (InterruptedException rekt) {
                System.exit(1);
            } catch (Exception e) {
                MFServer.log.warning(stackTraceToString(e));
            }
        }

        // Shut down tick workers
        // We initiate all shutdowns before waiting on them to reduce shutdown
        // time
    }

    private void laodCommands() {
        music _music = new music();
        temel.KOMUTLAR.put("mp3", new mp3());
        temel.KOMUTLAR.put("ayshe", new ayshe());
        temel.KOMUTLAR.put("santa", new santa());
        temel.KOMUTLAR.put("settings", new settings());
        temel.KOMUTLAR.put("music", _music);
        temel.KOMUTLAR.put("musique", _music);
        temel.KOMUTLAR.put("crown", new crown());
        temel.KOMUTLAR.put("isimrenk", new isimrenk());
        temel.KOMUTLAR.put("color", new color());
        temel.KOMUTLAR.put("ping", new ping());
        temel.KOMUTLAR.put("mod", new mod());
        temel.KOMUTLAR.put("sentinel", new sentinel());
        temel.KOMUTLAR.put("mapcrew", new mapcrew());
        task _task = new task();
        temel.KOMUTLAR.put("task", _task);
        temel.KOMUTLAR.put("görev", _task);
        temel.KOMUTLAR.put("zadanie", _task);
        temel.KOMUTLAR.put("tarefa", _task);
        temel.KOMUTLAR.put("tarea", _task);
        temel.KOMUTLAR.put("sarcină", _task);
        temel.KOMUTLAR.put("tâche", _task);
        temel.KOMUTLAR.put("feladat", _task);
        help yardim = new help();
        temel.KOMUTLAR.put("help", yardim);
        temel.KOMUTLAR.put("yardım", yardim);
        temel.KOMUTLAR.put("ajuda", yardim);
        ref _ref = new ref();
        temel.KOMUTLAR.put("ref", _ref);
        temel.KOMUTLAR.put("refs", _ref);
        etiket _etiket = new etiket();
        temel.KOMUTLAR.put("tag", _etiket);
        temel.KOMUTLAR.put("etiket", _etiket);
        temel.KOMUTLAR.put("warn", new warn());
        skip _skip = new skip();
        temel.KOMUTLAR.put("skip", _skip);
        temel.KOMUTLAR.put("geç", _skip);
        temel.KOMUTLAR.put("saltar", _skip);
        temel.KOMUTLAR.put("voteskip", _skip);
        temel.KOMUTLAR.put("karma", new karma());
        temel.KOMUTLAR.put("bf", new bf());
        temel.KOMUTLAR.put("emoticon", new emoticon());

    }

    public int randomRGB() {
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        int rgb = ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
        return rgb;
    }

    public int randomRGBBright() {
        int max = 255;
        int min = 150;
        int r = random.nextInt(max + 1 - min) + min;
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        int rgb = ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
        return rgb;
    }

    public byte[] hexStringToByteArray(String hex) {
        int l = hex.length();
        byte[] data = new byte[l / 2];
        int i = 0;
        while (i < l) {
            data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
            i += 2;
        }
        return data;
    }

    /*   public static int[] ipArray(String sIp, int kacli) {
           int[] ip = new int[4];
           String[] parts = sIp.split("\\.");
           for (int i = 0; i < 4; i++) {
               ip[i] = Integer.parseInt(parts[i], kacli);
           }
           return ip;
       }

       public static String convertIP(String sIp) {
           return convertIP(sIp, false);
       }

       public static String decryptIP(String cIp) {
           cIp = cIp.replace("#", "");
           int[] ips = ipArray(cIp, 16);
           String[] ipArr = new String[4];
           for (int i = 0; i < ips.length; i++) {
               int crypted = ips[i];
               int I = (i + 1) * 10;
               for (Map.Entry<Integer, Integer> entry : columnHashes.entrySet()) {
                   Integer hash = entry.getKey();
                   Integer ikili = entry.getValue();
                   if (ikili.equals(crypted)) {
                       int uncrypted = (hash >> I);
                       if (uncrypted < 258 && uncrypted > 0) {
                           ipArr[i] = String.valueOf(uncrypted - 1);
                           break;
                       }
                   }

               }
           }
           return StringUtils.join(ipArr, ".");
       }

       public static String convertIP(String sIp, boolean color) {
           int[] ips = ipArray(sIp, 10);
           String[] ipArr = new String[4];
           int r = 0;
           int g = 0;
           int b = 0;
           for (int i = 0; i < ips.length; i++) {
               int ip = ips[i] + 1;
               int hash = ip << ((i + 1) * 10);
               Integer ikili;
               synchronized (IpHashes) {
                   ikili = columnHashes.get(hash);
                   if (ikili == null) {
                       int ipHash = IpHashes[i];
                       IpHashes[i] = ipHash + 1;
                       ikili = ipHash;
                       columnHashes.put(hash, ikili);
                   }
               }
               String bom = Integer.toHexString(ikili);
               if (bom.length() == 1) {
                   bom = "0" + bom;
               }
               ipArr[i] = bom.toUpperCase();
               if (i == 3) {
                   r = ikili;
               }
               if (i == 1) {
                   g = ikili;
               }
               if (i == 2) {
                   b = ikili;
               }
           }
           if (color) {
               return Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);
           }
           return "#" + StringUtils.join(ipArr, ".");
       }
   */
    public String hexToColor(ByteBuf buf) {
        buf.readByte();
        ByteBuf baf = buf.readBytes(3);
        String result = "";
        while (baf.isReadable()) {
            result = String.valueOf(result) + Integer.toString((baf.readByte() & 255) + 256, 16).substring(1);
        }
        baf.release();
        return result;
    }

    public byte[] compress(byte[] data) throws IOException {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        byte[] output = outputStream.toByteArray();
        return output;
    }

    public byte[] colorToHex(String hex) {
        byte[] ciphertext = new byte[1];
        byte[] mac = this.hexStringToByteArray(hex);
        byte[] destination = new byte[ciphertext.length + mac.length];
        System.arraycopy(ciphertext, 0, destination, 0, ciphertext.length);
        System.arraycopy(mac, 0, destination, ciphertext.length, mac.length);
        return destination;
    }

    public void addClient2Room(PlayerConnection client, String roomname) throws IOException, InterruptedException {

        try {
            kilit.lock();
            if (roomname.toLowerCase().contains("tengri") && client.kullanici.yetki < 2) {
                roomname = "*1";
            }
            Room room = this.rooms.get(roomname);
            if (room != null) {
                if ((room.playerCount() >= room.maxPlayers || room.blockedPlayers.contains(client.kullanici.isim)) && client.kullanici.yetki < 5) {
                    client.sendMessage(room.outname + " has a limit of " + room.maxPlayers + " players.");
                    Matcher matcher = lastIntPattern.matcher(roomname);
                    if (matcher.find()) {
                        String someNumberStr = matcher.group(1);
                        roomname = roomname.substring(0, roomname.length() - someNumberStr.length());
                        int lastNumberInt = Integer.parseInt(someNumberStr);
                        roomname += lastNumberInt + 1;
                    } else {
                        roomname += "2";
                    }
                    client.last_Room = 0;
                    this.addClient2Room(client, roomname);
                    return;
                }
                room.addClient(client);
            } else {
                room = new Room(this, roomname);
                this.rooms.put(roomname, room);
                room.addClient(client);
                if (!room.isofficial) {
                    room.boss = client.kullanici.isim;
                }
            }
        } finally {
            kilit.unlock();
        }

    }

    public synchronized void addClient2Tribe(PlayerConnection client, int code) throws IOException {
        Tribe tribe = this.tribes.get(code);
        if (tribe != null) {
            tribe.addClient(client);
        } else {
            Tribe tribe2 = new Tribe(this, code);
            this.tribes.put(code, tribe2);
            tribe2.create((Void n) -> {
                try {
                    tribe2.addClient(client);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        }
    }

    public synchronized HashMap get_dakika() {
        LocalDateTime ldt = LocalDateTime.now();
        int Gun = ldt.getDayOfMonth();
        if (Bilgiler.size() > 30) {
            for (Integer st : Bilgiler.keySet()) {
                Bilgiler.remove(st);
                break;
            }
        }
        return Bilgiler.computeIfAbsent(Gun, k -> new HashMap<>());
    }

    public HashMap get_bugun() {
        // PrettyTime p = new PrettyTime(new Locale("de"));

        LocalDateTime ldt = LocalDateTime.now();
        int Gun = ldt.getDayOfMonth();
        if (Bilgiler.size() > 7) {
            for (Integer st : Bilgiler.keySet()) {
                Bilgiler.remove(st);
                break;
            }
        }
        return Bilgiler.computeIfAbsent(Gun, k -> new HashMap<>());
    }

    public void update_logs(String key, Integer val) {
        HashMap hm = get_bugun();
        HashMap<String, Integer> hmm = (HashMap<String, Integer>) hm.get(key);
        hmm.put(key, val);
    }

    public int rekor(String key, int val) {
        HashMap hm = get_bugun();
        AtomicInteger ai = ((HashMap<String, AtomicInteger>) hm).computeIfAbsent(key, k -> new AtomicInteger());
        return ai.updateAndGet(x -> x < val ? val : x);
    }

    public int incr_logs(String key, int val) {
        HashMap hm = get_bugun();
        AtomicInteger ai = ((HashMap<String, AtomicInteger>) hm).computeIfAbsent(key, k -> new AtomicInteger());
        return ai.addAndGet(val);
    }

    public void yukselt_logs(String key, int val) {
        HashMap hm = get_bugun();
        AtomicInteger ai = ((HashMap<String, AtomicInteger>) hm).computeIfAbsent(key, k -> new AtomicInteger());
        ai.getAndUpdate(x -> x < val ? val : x);
    }

    public synchronized Kanal addClient2Channel(PlayerConnection client, String name, int Code) throws IOException {
        Kanal kanal = this.kanallar.get(name);
        if (kanal == null) {
            kanal = new Kanal(this, CHI.incrementAndGet(), name);
            this.kanallar.put(name, kanal);
        }
        if (kanal.ozel && !kanal.ozeller.contains(Code)) return null;
        kanal.addClient(client, Code);
        return kanal;
    }

    public Room closeRoom(String roomname, Room verify) throws IOException {

        if (this.rooms.remove(roomname, verify)) {
            // room.Round = -1;
            verify.resetRoom();
        }
        return verify;
    }

    public void closeTribe(int code) {
        Tribe t = this.tribes.remove(code);
        if (t != null) {
            t.timers.forEach((k, v) -> v.cancel(true));
            t.timers.clear();
        }
    }

    public void closeKanal(String name) {
        Kanal kanal = kanallar.get(name);
        if (kanal != null) {
            kanal.closed = true;

        }
        this.kanallar.remove(name);

    }

    public void update_modules() throws SQLException, IOException {
        for (MiniOyun miniOyun : MFServer.colMiniOyun.find()) {
            if (miniOyun.path.isEmpty()) {
                miniOyun.path = "minigames/" + miniOyun.moduleName + ".lua";
            }

            MINIGAMES.put(miniOyun.moduleName, miniOyun);
            try {
                try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(miniOyun.path), "utf-8"))) {
                    try {
                        writer.write(miniOyun.luaCode);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class CustomRecordFormatter extends Formatter {

        @Override
        public String format(final LogRecord r) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));
            sb.append("] ");
            sb.append(formatMessage(r)).append(System.getProperty("line.separator"));

            if (null != r.getThrown()) {
                sb.append("Throwable occurred: "); //$NON-NLS-1$
                Throwable t = r.getThrown();
                PrintWriter pw = null;
                try {
                    StringWriter sw = new StringWriter();
                    pw = new PrintWriter(sw);
                    t.printStackTrace(pw);
                    sb.append(sw.toString());
                } finally {
                    if (pw != null) {
                        try {
                            pw.close();
                        } catch (Exception e) {
                            // ignore
                        }
                    }
                }
            }
            return sb.toString();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import com.mongodb.Block;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.net.PlayerConnection;
import mfserver.util.Dil;
import veritabani.Kabile;
import veritabani.Kullanici;
import veritabani.Rutbe;
import veritabani.SRC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static mfserver.net.PlayerConnection.Kul;
import static mfserver.net.PlayerConnection.Tigin;

/**
 * @author sevendr
 */
public class Tribe {

    public static Map<String, Integer> DROITS;

    static {
        DROITS = new HashMap<>();
        DROITS.put("yonetici", 1);
        DROITS.put("message_edit", 2);
        DROITS.put("edit_ranks", 3);
        DROITS.put("edit_member_rank", 4);
        DROITS.put("can_invite", 5);
        DROITS.put("can_kick", 6);
        DROITS.put("can_music", 7);
        DROITS.put("can_edit_map", 8);
        DROITS.put("can_load_map", 9);
        DROITS.put("can_edit_forums", 10);
    }

    public Map<Integer, Rutbe> Ranks = new ConcurrentHashMap<>();
    public Map<Integer, Kullanici> Members = new ConcurrentHashMap<>();
    public ArrayList<Rutbe> sortedRank = new ArrayList<>();
    public Map<String, PlayerConnection> clients = new ConcurrentHashMap<>();
    public Map<String, ScheduledFuture> timers = new ConcurrentHashMap<>();
    public Kabile kabile;
    public MFServer server;
    public int Code;
    public AtomicInteger lock;
    public Kanal kanal;

    public Tribe(MFServer server, int code) {
        this.Code = code;
        this.server = server;
        this.lock = new AtomicInteger();

    }

    public static int Kayra(Rutbe rank) {
        int num = 0;
        int i = 0;
        for (boolean acaba : rank.droits) {
            if (acaba)
                num = num ^ 1 << i;
            i += 1;
        }
        return num;
    }

    public SRC create(SRC<Void> callback) {

        load_data();
        refresh_members();
        load_rank_data();
        if (this.lock.get() != 3)
            this.lock.set(1);
        callback.onResult(null);
        return callback;

    }

    public void refresh_members() {

        this.Members.clear();
        MFServer.colKullanici.find(eq("tribeCode", this.Code)).projection(fields(include("_id", "isim", "cevrimici", "cinsiyet", "tribeRank", "tribeJoinTime", "tag"))).forEach((Block<? super Kullanici>) (Kullanici kullanici) -> {
            this.Members.put(kullanici.code, kullanici);
        });


    }

    public void load_data() {
        Kabile kabile = MFServer.colKabile.find(eq("_id", this.Code)).first();

        if (kabile == null) {
            this.lock.set(3);
        } else {
            this.kabile = kabile;
        }


    }

    public void sort_ranks() {

        sortedRank.clear();
        final int[] i = new int[1];
        int size = this.Ranks.size();
        this.Ranks.entrySet().stream().sorted(MFServer.byRutbe.reversed()).forEach((entry) -> {
            Rutbe rank = entry.getValue();
            rank.index = i[0]++;
            rank.order = size - rank.index;
            sortedRank.add(rank);

        });
    }

    public void load_rank_data() {
        this.Ranks.clear();
        MFServer.colRutbe.find(eq("tribeCode", this.Code)).forEach((Block<? super Rutbe>) (Rutbe rutbe) -> {
            this.Ranks.put(rutbe.code, rutbe);
        });

        sort_ranks();

    }

    public void addClient(PlayerConnection client) throws IOException {
        if (client.kullanici == null) return;
        if (this.lock.get() == 0) {
            ScheduledFuture ft = MFServer.executor.schedule(() -> {
                try {
                    timers.remove(client.kullanici.isim);
                    this.addClient(client);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 100, TimeUnit.MILLISECONDS);
            timers.put(client.kullanici.isim, ft);
            return;

        } else if (this.lock.get() == 3) {

            client.kullanici.tribeCode = 0;
            client.tribe = null;
            client.noTribe();
            return;
        }
        if (Ranks.get(client.kullanici.tribeRank) == null) {

            client.kullanici.tribeCode = 0;
            client.tribe = null;
            client.noTribe();
            return;
        }

        this.kanal = this.server.addClient2Channel(client, "~" + kabile.isim, 0);
        this.clients.put(client.kullanici.isim, client);
        client.tribe = this;
        // this.Members.put(client.kullanici.code, client.kullanici);
        Kullanici kullanici = this.Members.get(client.kullanici.code);
        kullanici.isim = client.kullanici.isim;
        kullanici.cinsiyet = client.kullanici.cinsiyet;
        kullanici.tribeRank = client.kullanici.tribeRank;
        ByteBufOutputStream bs = MFServer.getStream();
        bs.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
        this.sendAllTribulle(bs.buffer(), 88);
        // client.tri = this.Members.get(client.code);


    }

    public void sendAllTribulle2(String Token, ByteBuf packet) throws IOException {
        ByteBuf p = MFServer.pack_tribulle(Token, packet);
        for (PlayerConnection client : clients.values()) {
            if (client.oldTribulle)
                client.sendPacket(p.retainedSlice());
        }
        p.release();
        if (packet.refCnt() == 1)
            packet.release();
    }

    public void TribeInfo(PlayerConnection client) throws IOException {
        TribeInfo(client, false);
    }

    public int getIndex(int rank) {
        int rr = 0;
        Rutbe r = this.Ranks.get(rank);
        if (r != null) {
            rr = r.index;
        } else {
            rr = newbie().index;
        }
        return rr;

    }

    public void TribeInfo(PlayerConnection client, boolean disconnected) {

        MFServer.executor.execute(() -> {
            try {
                TribeInfo2(client, disconnected);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void TribeInfo2(PlayerConnection client, boolean disconnected) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeInt(this.Code);
        p.writeUTF(this.kabile.isim);
        p.writeUTF(this.kabile.ileti);
        // p.writeInt(this.Map);
        p.writeInt(this.kabile.harita);
        if (disconnected) {
            p.writeShort(this.Members.size());

            for (Kullanici cl : this.Members.values()) {
                PlayerConnection pc = this.server.clients.get(cl.isim);
                String i = cl.isim;
                if (!cl.tag.isEmpty()) i = MFServer.plusReady(i, cl.tag);

                p.writeInt(cl.code);
                p.writeUTF(i);
                p.writeByte(cl.cinsiyet);
                p.writeInt(MFServer.get_avatar(cl.code));
                String room = "";
                if (pc != null && pc.room != null && ((!pc.hidden && !pc.room.isTengri && !pc.topsecret) || client.kullanici.yetki > 1))
                    room = pc.room.name;


                if (room.isEmpty())
                    p.writeInt(cl.cevrimici / 60);
                else
                    p.writeInt(0);
                p.writeByte(getIndex(cl.tribeRank));
                if (room.isEmpty())
                    Tigin(p, "", 1);
                else {
                    Tigin(p, room, 4);
                }
            }
        } else {
            p.writeShort(this.clients.size());
            for (PlayerConnection cl : this.clients.values()) {
                String i = cl.kullanici.isim;
                if (!cl.kullanici.tag.isEmpty()) i = MFServer.plusReady(i, cl.kullanici.tag);
                p.writeInt(cl.kullanici.code);
                p.writeUTF(i);
                p.writeByte(cl.kullanici.cinsiyet);
                p.writeInt(MFServer.get_avatar(cl.kullanici.code));
                p.writeInt(0);
                p.writeByte(getIndex(cl.kullanici.tribeRank));
                String room = "";
                if (cl.room != null) room = cl.room.name;
                Tigin(p, room, 4);
            }
        }
        List<Rutbe> newList = new ArrayList<>(sortedRank);
        p.writeShort(newList.size());
        for (Rutbe rank : newList) {
            Kul(p, Dil.yazi(client.Lang, rank.name), Kayra(rank));
        }

        client.sendTriPacket2(p.buffer(), 130);
        MFServer.bitti(p.buffer());
    }

    public boolean checkPermissions(String perm_name, PlayerConnection client) {
        return this.Ranks.get(client.kullanici.tribeRank).droits.get(DROITS.get(perm_name));
    }

    public Rutbe newbie() {
        for (Rutbe rank : Ranks.values()) {
            if (rank.o_name.equals("${trad#tribu.nouv}")) {
                return rank;
            }
        }
        return null;
    }

    public Rutbe chef() {
        for (Rutbe rank : Ranks.values()) {
            if (rank.o_name.equals("${trad#TG_9}")) {
                return rank;
            }
        }
        return null;
    }

    public void removeClient(PlayerConnection client) throws IOException {
        this.clients.remove(client.kullanici.isim);
        Kanal kanal = this.server.kanallar.get("~" + kabile.isim);
        if (kanal != null) {
            kanal.removeClient(client);
        }

        if (this.clients.isEmpty()) {
            this.server.closeTribe(this.Code);
        } else {
            ByteBufOutputStream bs = MFServer.getStream();
            bs.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
            this.sendAllTribulle(bs.buffer(), 90);
        }
    }

    public void sendAllTribe() throws IOException {
        for (PlayerConnection client : clients.values()) {
            if (client.tribeOpened)
                this.TribeInfo(client);

        }
    }

    public void sendAllOtherTribe(int code) throws IOException {
        for (PlayerConnection client : clients.values()) {
            if (client.kullanici.code != code) {

                if (client.tribeOpened)
                    this.TribeInfo(client);
            }
        }
    }

    public void sendAll(ByteBuf packet) throws IOException {

        for (PlayerConnection client : clients.values()) {
            client.sendPacket(packet.retainedSlice());
        }
        packet.release();
    }

    public void sendAllTribulle(ByteBuf packet, int tok) throws IOException {
        ByteBuf p = MFServer.pack_tribulle2(packet, tok);
        for (PlayerConnection client : clients.values()) {
            if (!client.oldTribulle)
                client.sendPacket(p.retainedSlice());
        }
        p.release();
        packet.release();
    }

    public void sendAllOthers(ByteBuf packet, int code) throws IOException {

        for (PlayerConnection client : clients.values()) {
            if (client.kullanici.code != code) {
                client.sendPacket(packet.retainedSlice());
            }
        }
        packet.release();
    }

}

package mfserver.net;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Sets;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.Block;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.UpdateOptions;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import mfserver.data_out.*;
import mfserver.main.*;
import mfserver.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jooq.lambda.Unchecked;
import org.json.JSONArray;
import org.json.JSONObject;
import org.luaj.vm2.Lua;
import org.luaj.vm2.LuaDouble;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.lib.L_Room;
import veritabani.*;

import javax.print.Doc;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.inc;
import static com.mongodb.client.model.Updates.set;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static mfserver.main.MFServer.*;
import static mfserver.main.Room.s_tribe;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

public class PlayerConnection {
    public int wrongPW;
    public boolean white;
    public boolean onlyme;
    public long allChat;
    public int[] event_coords = new int[0];
    public boolean wings;
    public boolean vpn;
    public boolean sent;
    public long lastVPNSent;
    public int funcorpChar;
    public int incID = 0;
    public long lastPw;
    public long lastVoteKick;
    public long packetWatch = 0;
    public boolean getRecord=true;
    public long allPing;
    public List<String> pingWatches;
    private int ISP = 0;
    public static String defDil = "EN";
    public static String defCity = "London";
    public static String defCountry = "United Kingdom";
    public static String defTz = "Europe/London";
    private static final double CONSTANT_FPS = 10.0 / 3.0;
    private static byte[] xml_data = "<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"*\" /><allow-access-from domain=\"*\" to-ports=\"*\" /></cross-domain-policy>"
            .getBytes();
    public final ClientHandler player;
    public final Channel channel;
    public Room room;
    public ByteBuf buf;
    public String Lang = "E2";
    public String OrjLang = "E2";
    public int LangByte = 0;
    public MFServer server;
    public int lamaCount = 0;
    public boolean isDead = false;
    public boolean hasCheese;
    public int score = 0;
    public boolean banned = false;
    public long last_data = System.currentTimeMillis();
    public long last_Room = System.currentTimeMillis();
    public long last_duyur = System.currentTimeMillis();
    public Command COMMAND;
    // public HashMap<String, Integer> log;
    public boolean hidden;
    public boolean isNew = true;
    public HashSet<Integer> friends = new HashSet<>();
    public Tribulle TRIBULLE;
    public Tribe tribe;
    public int HACK = 0;
    public ArrayList<ArrayList> invites = new ArrayList<>();
    public ArrayList<ArrayList> invites2 = new ArrayList<>();
    public boolean isShaman;
    public long playerStartTime;
    public int x;
    public int y;
    public boolean direction;
    public boolean jumping;
    public short vy;
    public short vx;
    public boolean isAfk;
    public long ping = 300;
    public boolean isSyncer;
    public boolean isVampire;
    public boolean isComp;
    public String last_message = "";
    public String ip;
    public boolean isLocal = false;
    public HashSet<Kanal> kanallarim = new HashSet<>();
    public HashSet<Integer> konular = new HashSet<>();
    public Totem totem2;
    public ConnectionState state = ConnectionState.VERIFY;

    public String countryCode = "EN";
    public Ceza susturma;
    public boolean arti = false;
    public boolean shamanRespawn;
    public HashMap<String, String> inv = new HashMap<>();
    public String lastRoom = "";
    public boolean tribeOpened = false;
    public boolean acaba = false;
    public boolean oldTribulle = false;
    public int rekor = 0;
    public boolean crown = true;
    public Map<Etkinlik, Integer> etkinlikPuan = new HashMap<>();
    public int toplamPuan = 0;
    public int karma;
    public String country = "Unkown";
    public String city = "Unkown";
    public String tz = "Europe/London";
    public boolean watchVelocity = false;
    public boolean motor;
    public HashMap<Integer, Daire> daires = new HashMap<>();
    public String isp = "";
    public boolean topsecret = false;
    public ScheduledFuture T_Music;
    private long T_Ping = 0;
    private int Ping = 0;
    private int itemSkin;
    private ArrayList pet;
    private int Oy;
    private int S_ileti = 0;
    private OldData OLD;
    private ScheduledFuture pingTimer;
    private int T_topic = 0;
    private int T_message = 0;
    private ArrayList<String> slots = new ArrayList<>();
    private int lastSyncCount = 0;
    private int lastSyncNumber = 0;
    private int normalMon = 0;
    private int promMoney = 0;
    private boolean canMeep;
    private boolean transformice;
    private int kullanilan_esya = 0;
    private boolean movingRight;
    private boolean movingLeft;
    private boolean firstShop;
    private String son_bot;
    private int defilante = 0;
    private String currentCaptcha;
    private boolean firstObjects;
    private int IceCount;
    private boolean sentTotem = false;
    private long pey;
    public int rekorTemp = 0;
    private int T_report = 0;
    private ArrayDeque<String> son_mes = new ArrayDeque<>();
    private ArrayDeque<String> son_iletiler = new ArrayDeque<>();
    private boolean inTrade;
    private boolean tradeOK;
    private int currentTrade;
    private Map<Integer, Integer> tradeItems = new HashMap<>();
    private boolean byShaman;
    private boolean fircaKullan;
    private long fircaSure;
    private int fircaBoya;
    ArrayList<Long> limitArr = new ArrayList<>();
    private boolean cantMove;
    private int moved;
    public boolean revived;
    private int summoning = 0;
    private HashMap<String, Integer> summoningObject = new HashMap<>();
    private boolean sentSyncData = false;
    public boolean silence2 = false;
    public boolean funcorpOriginal;

    public int dataCount;
    public int dataCountOld;
    public int roundCountBomb;
    public int lastroundBomb;
    private long forumDogumGunu = 0;
    private boolean sentShop;
    private long firstXYTime;
    private int[] firstXY = new int[2];
    public ArrayList<String> sendAnchors = new ArrayList<>();
    private long lastExecuteLUA = 0;
    public Kullanici kullanici;
    public HashMap<Integer, Integer> rozetler2 = new HashMap<>();
    // private double genel;
    private long sonZam;
    public int[][] gorevUI = new int[2][100];
    private int gorevSayfa;
    public long last15MS = System.currentTimeMillis();
    public int last15MSCounter = 0;
    public int lastHoleId;
    public String tempTip;
    public Sistem sistem = new Sistem();
    private int esyaKullanimi;
    public boolean siciliKotu;
    private int lel = 0;
    private int banTrigger;
    private int trig;
    public int lale;
    public ArrayList<Kullanici> refs = new ArrayList<>();
    private long lastProfile = 0;
    public ScheduledFuture<?> lamaExec;
    private boolean sentVote;
    public boolean checkPoint;
    public long checkPointTime;
    private int cmdTecError;
    public int c28;
    private long lastc28;
    public boolean pink;
    private boolean yeniSWF;
    private long lastSentVote;
    public boolean isDrawing;
    public HashSet<Integer> profileNameCache = new HashSet<>();
    private int collidableConsumableUsage = 0;
    int consumableUsage = 0;
    public ArrayList<Boolean> firstList = new ArrayList<>();
    public ArrayList<Integer> avgList = new ArrayList<>();
    public Login login = new Login();
    public int HalloweenHealth = 0;
    private HashSet<Integer> reqs = new HashSet<>();
    private boolean hediyeAldi;
    public String tempKurkRenk;
    private long last_Vote;
    private long last_login_attempt;
    public String rand = "";
    public ArrayList<String> currentAnketSorular;
    public String currentAnket;
    public int currentAnketId;

    public PlayerConnection(ClientHandler player, Channel channel) {
        this.player = player;
        this.channel = channel;
        this.server = player.server;
        this.ip = player.ip;
        this.isLocal = this.ip.equals("127.0.0.1");
    }

    public PlayerConnection() {
        this.channel = null;
        this.player = null;
    }


    public static ByteBuf pack_modchat(int typ, String name, String message, String... args) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(6);
        p.writeByte(10);
        p.writeByte(typ);
        p.writeUTF(name);
        p.writeUTF(message);
        p.writeShort(args.length);
        for (String arg : args) p.writeUTF(arg);
        p.writeShort(0);
        return p.buffer();
    }

    private static ArrayList<Integer> sort_desc(HashSet<Integer> hs) {
        ArrayList<Integer> c = new ArrayList<>();
        ArrayList<Integer> c2 = new ArrayList<>();
        c2.addAll(hs);
        for (Integer b : MFServer.a) {
            if (hs.contains(b)) {
                c.add(b);
                c2.remove(b);
            }
        }
        for (Integer b : MFServer.a2) {
            if (hs.contains(b)) {
                c.add(b);
                c2.remove(b);
            }
        }

        for (Integer b : MFServer.a3) {
            if (hs.contains(b)) {
                c.add(b);
                c2.remove(b);
            }
        }
        Collections.sort(c2);
        Collections.reverse(c2);
        c.addAll(c2);
        return c;
    }

    public static void Tigin(ByteBufOutputStream st, String ileti, int id) throws IOException {
        st.writeInt(id);
        st.writeUTF(ileti);
    }

    public static void Kul(ByteBufOutputStream st, String ileti, int id) throws IOException {
        st.writeUTF(ileti);
        st.writeInt(id);

    }

    public void checkForCafePoints() {
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT sum(puan) as topl,cafe_vote.voted_pid FROM game.cafe_vote where voted_pid=? group by voted_pid,post_id");
             PreparedStatement statement2 = connection.prepareStatement(
                     "SELECT id FROM game.cafe_post where player_id=? and length(message)>30");
             PreparedStatement statement3 = connection.prepareStatement(
                     "SELECT id FROM game.cafe_thread where player_id=? and post_count > 30;");
             PreparedStatement statement4 = connection.prepareStatement(
                     "select u.trophy_points,prof.dob_day,prof.dob_month,prof.dob_year from forum.xf_user as u INNER JOIN forum.xf_user_profile AS prof ON u.user_id = prof.user_id where u.username=?");
        ) {
            this.Oy = 0;
            if (this.kullanici == null) return;

            statement.setInt(1, this.kullanici.code);
            statement2.setInt(1, this.kullanici.code);
            statement3.setInt(1, this.kullanici.code);
            statement4.setString(1, this.kullanici.isim);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) >= 5) {
                    this.Oy += 10;
                }
            }
            rs = statement2.executeQuery();
            int limit = 0;
            while (rs.next() && limit < 10) {
                this.Oy += 1;
                limit++;
            }
            rs = statement3.executeQuery();
            limit = 0;
            while (rs.next() && limit < 10) {
                this.Oy += 3;
                limit++;
            }
            rs = statement4.executeQuery();
            if (rs.next()) {
                this.Oy += rs.getInt(1) / 5;
                int gun = rs.getInt(2);
                int ay = rs.getInt(3);
                int yil = rs.getInt(4);
                if (gun != 0 || yil > 1970) {
                    this.forumDogumGunu = new Date(yil, ay - 1, gun).getTime();
                }
            }


        } catch (Exception e) {
            //   e.printStackTrace();
        }
        this.only_login();
    }

    public ByteBuf getBuf() {
        return channel.alloc().buffer();
    }

    public ByteBufOutputStream getStream() {
        return new ByteBufOutputStream(this.getBuf());
    }

    public void sendPacket(ByteBuf packet) {
        if (!alive()) {
            packet.release();
            return;
        }

        channel.writeAndFlush(packet);
    }

    public void sendTriPacket(String token, ByteBuf packet) throws IOException {
        channel.writeAndFlush(MFServer.pack_tribulle(token, packet));
        if (packet.refCnt() == 1)
            packet.release();
        // packet.release();
    }

    public void sendTriPacket2(ByteBuf packet, int tok) throws IOException {
        channel.writeAndFlush(MFServer.pack_tribulle2(packet, tok));
        packet.release();
    }

    public void sendPacket(ByteBufOutputStream packet) {
        channel.writeAndFlush(packet.buffer());
    }

    public void sendEnterRoom() throws IOException {
        this.sendPacket(new EnterRoom(this, room).data());
        this.sendPacket(new EnterRoom2(this).data());
        this.sendPacket(new EnterRoom3(this, room).data());
    }

    public void sendMC(ByteBuf data) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki == 10 || pc.kullanici.isMapcrew) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendChatMC(String message) throws IOException {
        if (this.kullanici.yetki == 10 || this.kullanici.isMapcrew) {
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(7, this.kullanici.isim, message);
            sendMC(data);

        }
    }


    public void sendFC(ByteBuf data) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki == 10 || pc.kullanici.isFuncorp) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendChatFCLocalRoom(String message) throws IOException {
        if (this.kullanici.yetki == 10 || this.kullanici.isFuncorp) {
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(9, this.kullanici.isim, message);
            for (PlayerConnection pc : this.room.clients.values()) {
                if (pc.kullanici.yetki == 10 || pc.kullanici.isFuncorp) {
                    pc.sendPacket(data.retainedSlice());
                }
            }
            data.release();
        }
    }
    public void sendChatFC(String message) throws IOException {
        if (this.kullanici.yetki == 10 || this.kullanici.isFuncorp) {
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(9, this.kullanici.isim, message);
            sendFC(data);
        }
    }

    public void sendLua(ByteBuf data) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki == 10 || pc.kullanici.isLuacrew) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendFashion(ByteBuf data) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki == 10 || pc.kullanici.isFashion) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendChatFashion(String message) throws IOException {
        if (this.kullanici.yetki == 10 || this.kullanici.isFashion) {
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(10, this.kullanici.isim, message);
            sendLua(data);
        }
    }

    public void sendChatLua(String message) throws IOException {
        if (this.kullanici.yetki == 10 || this.kullanici.isLuacrew) {
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(8, this.kullanici.isim, message);
            sendLua(data);
        }
    }

    public void sendArb(ByteBuf data, String lang) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki >= 2 && (lang.isEmpty() || pc.Lang.equals(lang))) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendChatArb(String message, int typ, String lang) throws IOException {
        if (this.kullanici.yetki >= 2) {
            if (this.kullanici.yetki == 6) message = "(Secret) " + message;
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(typ, this.kullanici.isim, message);
            sendArb(data, lang);
        }
    }

    public void sendMod(ByteBuf data, String lang) throws IOException {
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki >= 4 && (lang.isEmpty() || pc.Lang.equals(lang))) {
                pc.sendPacket(data.retainedSlice());
            }
        }
        data.release();
    }

    public void sendChatMod(String message, int typ, String lang, String... args) throws IOException {
        if (this.kullanici.yetki >= 4) {
            if (this.kullanici.yetki == 4) message = "(Trial) " + message;
            if (this.kullanici.yetki == 6) message = "(Secret) " + message;
            if (this.kullanici.isCouncil) message = "[C] " + message;
            ByteBuf data = pack_modchat(typ, this.kullanici.isim, message, args);
            sendMod(data, lang);
        }
    }

    private ByteBuf packChatServer(String message) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(6);
        p.writeByte(20);
        p.writeByte(0);
        p.writeUTF(message);
        p.writeByte(0);
        return p.buffer();
    }

    public void sendChatServer(String message, boolean wp) throws IOException {
        SqlData sd = new SqlData();
        sd.typ = 5;
        sd.type = 99;
        sd.name = "Server";
        sd.message = message;
        server.CLD.add(sd);
        // ByteBuf data = packChatServer(message);
        // data.retain();
        for (PlayerConnection pc : this.server.clients.values()) {
            if (pc.kullanici.yetki >= 2 && (!wp || pc.kullanici.watch_pw)) {

                pc.sendPacket(packChatServer(message));
            }
        }
        MFServer.logExecutor.execute(() -> {
            MFServer.colPMLog.insertOne(new Document("from", "Tengri").append("to", kullanici.isim).append("room", "Tengri").append("msg", message).append("time", System.currentTimeMillis()));

        });
      /*  try {
            MFServer.bitti(data);
        } catch (Exception e) {
        }*/
    }

    public void sendChatServer(String message) throws IOException {
        sendChatServer(message, false);
    }

    private boolean canvote() {
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection
                     .prepareStatement("SELECT * FROM log.mayor_vote where playerCode = ?")) {
            statement.setInt(1, this.kullanici.code);
            return !statement.executeQuery().next();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private boolean canmayor() {
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection
                     .prepareStatement("SELECT * FROM log.mayor where playerCode = ?")) {
            statement.setInt(1, this.kullanici.code);
            return !statement.executeQuery().next();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public void sendOylar() {
        MFServer.executor.execute(() -> {
            int T = this.server.oyZaman - MFServer.timestamp();
            int saat = T / 86400;
            int c = 0;
            int dakika = (T % 86400) / 36000;
            if (saat < 1) {
                c = 2;
                dakika = 0;
            }
            ByteBufOutputStream bs = MFServer.getStream();
            try {
                bs.writeByte(100);
                bs.writeByte(80);
                // bs.writeShort(0);
                bs.writeByte(c);
                bs.writeBoolean(canvote());
                bs.writeBoolean(canmayor());
                bs.writeUTF(saat + "-" + dakika);
                bs.writeByte(0);
                // boolean found=false;
                try (Connection connection = MFServer.DS.getConnection();
                     PreparedStatement statement = connection.prepareStatement(
                             "SELECT * FROM log.mayor where dil = ? order by vote_count desc,time_stamp desc limit 30")) {
                    statement.setString(1, this.Lang);
                    ResultSet rs = statement.executeQuery();

                    while (rs.next()) {
                        // found=true;
                        bs.writeByte(1);
                        bs.writeUTF(rs.getString(2));
                        bs.writeByte(3);
                        bs.writeInt(-rs.getInt(9));
                        bs.writeInt(rs.getInt(5));
                        bs.writeByte(2);
                        bs.writeUTF(rs.getString(3));
                        bs.writeUTF(rs.getString(4));
                        bs.writeUTF(rs.getString(8));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // if(!found)bs.writeByte(0);
                this.sendPacket(bs.buffer());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public boolean check_message_staff(String ileti) throws IOException {
        int l = ileti.length();
        String Orjileti = ileti;
        ileti = ileti.replaceAll("\\<.*?>", "");
        ileti = ileti.replace(" ", "").toLowerCase();
        boolean bulundu = false;
        if (l > 5) {
            Optional<String> ax = this.server.BAD_WORDS.stream().filter(ileti::contains).findFirst();
            bulundu = ax.isPresent();
        }
        if (bulundu && !ileti.contains("discord")) {
            this.server.banPlayer(kullanici.isim, 1800, "GOD'S MERCY", "Tengri", false, null);
            this.sendChatServer("Tengri banned " + kullanici.isim + " for " + 1800 + " hours. Reason : GOD'S MERCY(" + ileti + ")");
            MFServer.log.warning("ALERT ADV : [" + kullanici.isim + "] " + ileti);

            this.sendChatServer("[" + this.kullanici.isim + "]  " + Orjileti);
            this.sendMessage("<R>WARNING BAD MESSAGE!!!");
            return true;
        }
        return false;
    }


    public boolean check_message(String message) throws IOException {
        return check_message(message, false);
    }

    public boolean check_message(String ileti, boolean b) throws IOException {
        if (this.kullanici.peynir == 0) {
            this.sendMessage("<R>You need get 1 cheese to talk");
            return true;
        }
        long st = System.currentTimeMillis();
        if (this.kullanici.isim.equals("Tengri")) return false;
        if (this.susturma != null && this.susturma.surec > 0) {
            long tt;
            if (susturma.isNano) {
                tt = susturma.surev + TimeUnit.NANOSECONDS.convert(susturma.surec, TimeUnit.HOURS);
            } else {
                st = System.currentTimeMillis();
                tt = susturma.surev + TimeUnit.MILLISECONDS.convert(susturma.surec, TimeUnit.HOURS);
            }
            if (tt > st) {
                this.sendMessage("<ROSE>• [$Moderation] $MuteInfo1", String.valueOf(this.susturma.surec), this.susturma.sebep);
                return true;
            } else {
                //susturma = null;
            }
        }
        if (st - this.sonZam < 30) {
            this.trig += 1;
            if (this.trig > 10) {
                this.sendChatServer("(Potential)[" + this.kullanici.isim + "]  " + ileti);
            }
            return true;
        }
        if (b) return false;
        this.sonZam = st;
        double alg = 0;
        //   double alg2=0;
        int l = ileti.length();
        for (String sm : this.son_mes) {
            double _alg = StringUtils.getJaroWinklerDistance(ileti, sm);
            if (_alg > alg) alg = _alg;

        }
        if ((this.S_ileti++ > 8 || ((alg > 0.90) && l > 15)) && kullanici.yetki < 5 && !this.room.event) {
            if (this.S_ileti > 2)
                this.sendChatServer("Flood alert [" + this.kullanici.isim + "]  " + ileti);
            this.sendMessage("$chat.message.antiFlood");
            return true;
        }


        String Orjileti = ileti;
        ileti = MFServer.iletiKontrol.matcher(ileti).replaceAll("").toLowerCase();
        this.son_mes.addFirst(Orjileti);
        if (this.son_mes.size() > 3) this.son_mes.removeLast();
        /*if(son_iletiler.size()>10){
            this.son_iletiler.pollLast();
        }*/

        boolean bulundu = false;
        boolean bulundu2 = false;
        if (l > 5) {
            Optional<String> ax = this.server.BAD_WORDS.stream().filter(ileti::contains).findFirst();
            bulundu = ax.isPresent();
        }
        if (!bulundu) {
            Optional<String> ax = this.server.NEUTRAL_WORDS.stream().filter(ileti::contains).findFirst();
            bulundu2 = ax.isPresent();
        }
        if (bulundu2 && this.kullanici.peynir < 30_000) bulundu = true;
        // if(bulundu && kullanici.yetki >=5 && ileti.contains("discord"))bulundu=false;
        if (bulundu || bulundu2) {
            //    this.genel += 3;
            this.sendChatServer("[" + this.kullanici.isim + "]  " + Orjileti);
            if (bulundu) this.sendMessage("<R>WARNING BAD MESSAGE!!!");
        }
        return bulundu;
    }

    public void disc() {
        if (this.room != null && this.kullanici != null) {
            Kullanici kullanici = this.kullanici;

            try {
                this.room.removeClient(this);

            } catch (Exception ex) {
            }
            try {
                if (this.tribe != null) {
                    this.tribe.removeClient(this);
                }


            } catch (Exception ex) {
            }

            try {
                for (Kanal aKanallarim : this.kanallarim.toArray(new Kanal[this.kanallarim.size()]))
                    aKanallarim.removeClient(this);
            } catch (Exception ex) {
            }
            Room.iptal(T_Music);
            server.clients.remove(kullanici.isim);
            server.clients2.remove(kullanici.code);
            server.allChats.remove(kullanici.isim);
            server.allPings.remove(kullanici.isim);
            server.Cafers.remove(kullanici.isim);
            this.tribe = null;
            this.room = null;
            this.COMMAND = null;
            this.OLD = null;
            this.TRIBULLE = null;
            //Room.iptal(this.pingTimer);
            this.invites.clear();
            this.invites2.clear();

            try {
                ByteBufOutputStream bs = MFServer.getStream();
                bs.writeUTF(MFServer.plusReady(kullanici));

                ByteBuf data = bs.buffer();
                data.retain();
                for (Integer pid : this.friends) {
                    PlayerConnection cl = server.clients2.get(pid);
                    if (cl != null) {
                        if (cl.friends.contains(kullanici.code)) {

                            cl.sendTriPacket2(data.retainedSlice(), 33);
                        }
                    }
                }
                MFServer.bitti(data);
                // data.release();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                if (kullanici.isFuncorp) {

                    sendFC(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#ff8547", this.Lang, this.kullanici.isim));

                }
                if (kullanici.isLuacrew) {
                    sendLua(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#98E2EB", this.Lang, this.kullanici.isim));

                }
                if (kullanici.isFashion) {
                    sendFashion(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#F0A78E", this.Lang, this.kullanici.isim));

                }
                if (kullanici.isMapcrew) {
                    sendMC(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#52C6FF", this.Lang, this.kullanici.isim));
                }
                if (kullanici.yetki == 2) {
                    this.sendArb(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#B993CA", this.Lang, this.kullanici.isim), "");
                }
                if (kullanici.yetki >= 4 && !this.kullanici.isim.equals("Tengri")) {
                    this.sendMod(MFServer.pack_messagewithcolor("$chat.tribu.signaleDeconnexionMembre", "#C565FE", this.Lang, this.kullanici.isim), "");

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            MFServer.executor.execute((() -> {
                if (!silence2) kullanici.sessiz = null;
                SaveUser();
                int id = kullanici.code;
                this.kullanici = null;
                //   MFServer.session.execute("update online set mf=false where id = ?;", id);
            }));
        }
    }

    public void openMP(String dil) throws IOException {
        openMP(dil, true, false);
    }

    private void openMP(String dil, boolean time, boolean only) throws IOException {
        HashSet<String> disc = new HashSet<>();
        ArrayList<Yasaklama> banneds = new ArrayList<>();
        ArrayList<Raporlar> silinenler = new ArrayList<>();
        ByteBufOutputStream bs = MFServer.getStream();
        ByteBufOutputStream bs2 = MFServer.getStream();
        int i = 0;
        int T = MFServer.timestamp();
        int t = T - 60 * 30;
        boolean all = dil.equals("ALL");
        // Den("Bombas2");
        //  Den("Bombas");
        // Den("Sevenops");
        List<Map.Entry<String, Raporlar>> entryList = new ArrayList<>(this.server.RaporSistemi.entrySet());
        if (time) entryList.sort(MFServer.rapor_sirala2);
        else entryList.sort(MFServer.rapor_sirala);
        for (Map.Entry<String, Raporlar> rps : entryList) {
            Raporlar raporlar = rps.getValue();
            String ad = rps.getKey();

            if (i > 60)
                break;
            int i2 = 0;
            if (!all && !dil.equals(raporlar.dil)) {
                continue;
            }
            if (raporlar.surev < t) {
                this.server.RaporSistemi.remove(ad);
                Yasaklama yasaklama = MFServer.yasaklamalar.remove(ad);
                continue;
            }
            int rsize = raporlar.arr.size();
            //  if(only&&rsize>1)continue;
            boolean kotu = false;
            boolean discon = false;
            String oda = this.server.getRoom(ad, true);
            Yasaklama yasaklama = MFServer.yasaklamalar.get(ad);
            if (yasaklama != null) {
                banneds.add(yasaklama);
                kotu = true;
            } else if (!raporlar.silen.isEmpty()) {
                silinenler.add(raporlar);
                kotu = true;
            } else if (oda.isEmpty()) {
                disc.add(ad);
                kotu = true;
                discon = true;
            }

            ByteBufOutputStream ls = bs;
            if (kotu) continue;
            ls.writeBoolean(discon);
            ls.writeShort(raporlar.surev);

            Room room = this.server.rooms.get(oda);
            if (room != null) {
                raporlar.dil = room.lang;
            }
            ls.writeUTF(raporlar.dil);
            ls.writeUTF(ad);
            ls.writeUTF(oda);
            ArrayList<PlayerConnection> bakanlar;
            if (room != null) bakanlar = room.findModsInRoom();
            else bakanlar = new ArrayList<>();

            ls.writeByte(bakanlar.size());

            for (PlayerConnection bakan : bakanlar) {
                ls.writeUTF(bakan.kullanici.isim);
            }
            ls.writeInt((T - raporlar.surev) / 3600);

            ls.writeByte(Math.min(rsize, 10));
            for (Rapor rapor : raporlar.arr) {
                if (i2++ >= 10)
                    break;
                ls.writeUTF(rapor.raporlayan);
                ls.writeShort(rapor.karma);
                ls.writeUTF(rapor.sebep);
                ls.writeByte(rapor.typ);
                ls.writeShort(i2);


            }
            Yasaklama susturma = MFServer.susturmalar.get(ad);
            ls.writeBoolean(susturma != null);
            if (susturma != null) {
                ls.writeUTF(susturma.yasaklayan);
                ls.writeShort(susturma.surev);
                ls.writeUTF(susturma.sebep);
                disc.remove(ad);

            }

            i++;
        }
/*        i += 1;
        bs.writeByte(i);
        bs.writeUTF("TR");
        bs.writeUTF("Hacker");
        bs.writeUTF("");
        bs.writeByte(7);
        bs.writeUTF("Player1");
        bs.writeUTF("Player2");
        bs.writeUTF("Hacker");
        bs.writeUTF("Hacker2");
        bs.writeUTF("Hacker3");
        bs.writeUTF("Hacker33");
        bs.writeUTF("Hacker334");
        bs.writeInt(7);

        bs.writeByte(1);
        bs.writeUTF("Reporter");
        bs.writeShort(333);
        bs.writeUTF("sebep");
        bs.writeByte(2);
        bs.writeShort(56);
        bs.writeByte(1);
        bs.writeUTF("Tengri");
        bs.writeShort(4);
        bs.writeUTF("nadwqwdqwddwqdqwdqwqdwh");

*/
        this.sendPacket(MFServer.getBuf().writeByte(25).writeByte(2).writeByte(i).writeBytes(bs.buffer()).writeBytes(bs2.buffer()));
        MFServer.bitti(bs.buffer());
        MFServer.bitti(bs2.buffer());
      /*  for (String ad : disc) {
            ByteBufOutputStream bo = MFServer.getStream();
            bo.writeByte(25);
            bo.writeByte(6);
            bo.writeUTF(ad);
            this.sendPacket(bo);
        }
        for (Raporlar raporlar : silinenler) {
           // silindi(raporlar);
        }

        for (Yasaklama yasaklama : banneds) {
            ByteBufOutputStream bo = MFServer.getStream();
            bo.writeByte(25);
            bo.writeByte(5);
            bo.writeUTF(yasaklama.yasaklanan);
            bo.writeBoolean(true);
            bo.writeUTF(yasaklama.yasaklayan);
            bo.writeInt(yasaklama.surev);
            bo.writeUTF(yasaklama.sebep);
            this.sendPacket(bo);
        }*/
    }

    /* public static class RunnableRaporlar implements Runnable {
         Raporlar raporlar;
         String ip;

         public RunnableRaporlar(Raporlar raporlar, String ip) {
             this.raporlar = raporlar;
             this.ip = ip;
         }

         public void run() {
             try (Connection connection = MFServer.DS.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement("SELECT ip,name FROM log.login where ip =? GROUP BY name LIMIT 5")) {
                 preparedStatement.setString(1, this.ip);
                 ResultSet resultSet = preparedStatement.executeQuery();
                 while (resultSet.next()) {
                     String yan = resultSet.getString(2);
                     if (!yan.equals(this.raporlar.ad)) raporlar.yanlar.add(yan);
                 }
             } catch (Exception exception) {

             }
         }
     }
 */
    private void silindi(Raporlar raporlar) throws IOException {
        ByteBufOutputStream bo = MFServer.getStream();
        bo.writeByte(25);
        bo.writeByte(7);
        bo.writeUTF(raporlar.ad);
        bo.writeUTF(raporlar.silen);
        this.sendPacket(bo);
    }

    private AbstractMap.SimpleImmutableEntry<String, String> splitQueryParameter(String it) {
        final int idx = it.indexOf("=");
        final String key = idx > 0 ? it.substring(0, idx) : it;
        final String value = idx > 0 && it.length() > idx + 1 ? it.substring(idx + 1) : null;
        return new AbstractMap.SimpleImmutableEntry<>(key, value);
    }

    private Map<String, List<String>> splitQuery(String url) {

        return Arrays.stream(url.split("&"))
                .map(this::splitQueryParameter)
                .collect(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry::getKey, LinkedHashMap::new, mapping(Map.Entry::getValue, toList())));
    }

    void handle(ByteBuf buf) throws Exception {
        long TimeStamp = System.currentTimeMillis();
        if (this.packetWatch > TimeStamp) {
            byte[] bytes = new byte[buf.readableBytes()];
            int readerIndex = buf.readerIndex();
            buf.getBytes(readerIndex, bytes);
            MFServer.logExecutor.execute(() -> {
                String isim = kullanici == null ? "UNKOWN" : kullanici.isim;
                MFServer.colPacketLog.insertOne(new Document("ad", isim).append("surev", TimeStamp).append("ip", ip).append("data", bytes));
            });
        }
        if (this.banned) return;
        dataCount += 1;
        last_data = TimeStamp;
        ByteBufInputStream bis = new BIS(buf);
        int cmdtec = bis.readByte();
        if (this.state != ConnectionState.VERIFY) {
            this.incID = (byte) ((this.incID + 1) % 100);
            if (cmdtec != incID) {
                if (cmdTecError++ > 5) {

                    this.sendPlayerBan(0, "Please reconnect", true);
                    return;
                }
                incID = cmdtec;
            }
        }
        int Token1, Token2;
        Token1 = bis.readByte();
        Token2 = bis.readByte();
        if ((dataCount > 1000 || (dataCount > 800 && this.room != null && this.room.l_room != null)) && !this.isDrawing) {

            //    System.out.println(ip + " - " + Token1 + " - " + Token2 + " | " + buf.readableBytes());
            channel.close();
            return;
        }
     /*   if (!(Token1 == 4 && (Token2 == 3 || Token2 == 4))) {
            System.out.println(Token1 + " " + Token2 + " " + MFServer.bytesToHex(buf.copy()));
        }*/


        boolean found = true;
        if (this.state == ConnectionState.VERIFY) {
            if (Token1 == 112) {
                // Channel channel=this.channel;
                channel.writeAndFlush(PlayerConnection.xml_data).addListener(ChannelFutureListener.CLOSE);
            } else if (Token1 == 28) {
                if (Token2 == 1) {

                    int version = bis.readShort();
                    String key = bis.readUTF();
                    login.client_type = bis.readUTF();
                    login.user_agent = bis.readUTF();
                    bis.readInt();
                    bis.readShort();
                    bis.readUTF();
                    String QS = bis.readUTF();
                    Boolean stat = MFServer.IP_STATS.get(ip);
                    if (stat != null) {
                        if (stat) {


                            this.sendPlayerBan(0, "IP BAN", true);
                            return;

                        }
                        checkVersion(version, key, QS);
                        return;
                    } else {
                        MFServer.ListenExecutor.execute(() -> {

                            Boolean st = MFServer.colIpBan.countDocuments(new Document("_id", ip)) > 0;
                            MFServer.IP_STATS.put(ip, st);
                            if (st) {
                                try {
                                    this.sendPlayerBan(0, "IP BAN", true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                            try {
                                checkVersion(version, key, QS);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return;
                        });
                    }
                   /* MFServer.ListenExecutor.execute(() -> {
                        try {
                            IPBan ipBan = MFServer.colIpBan.find(new Document("_id", ip)).first();
                            if (ipBan != null) {
                                if (ipBan.deadline < System.currentTimeMillis()) {
                                    MFServer.executor.execute(() -> MFServer.colIpBan.deleteOne(eq("_id", ipBan._id)));

                                } else {

                                    this.sendPlayerBan(0, "IP BAN", true);
                                    return;
                                    // System.out.println("ip banned :" + ip);
                                }
                            }

                        } catch (IOException e) {

                        }
                    });*/


                }
            } else if (Token1 == 7 && Token2 == 7 && this.ip.equals("127.0.0.1")) {
                ByteBuf buf2 = buf.copy();
                MFServer.executor.execute(() -> {
                    try {
                        API.query(this, buf2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        } else if (this.state == ConnectionState.VERIFIED || this.state == ConnectionState.LOGINED) {

            if (!(Token1 == 26 || Token2 == 8 || Token2 == 7 || (Token1 == 8 && Token2 == 2) || (Token1 == 28 && Token2 == 17))
                    && this.kullanici == null)
                return;
            if (Token1 == 26) {
                if (Token2 == 8) {

                    String name = bis.readUTF();
                    boolean isMail = name.contains("@");
                    if (!isMail) name = MFServer.noTag(MFServer.firstLetterCaps(name));
                    String password = bis.readUTF();
                    String url = bis.readUTF();
                    boolean loc = this.ip.equals("127.0.0.1");
                    String room = "1";//;bis.readUTF();
                    if ((!MFServer.name_checkerLoginOnly.matcher(name).find() || name.length() > 20 || name.length() < 3) && !loc) {
                        return;
                    }
                    boolean stand = url.startsWith("app:/");
                    //  System.out.println(MFServer.bytesToHex(buf.copy()));
                    // System.out.println(buf.readableBytes());
                    //
                    if (!(url.startsWith("http://www.miceforce.com/MiceForce.swf") || url.startsWith("app:/") || url.startsWith("https://www.miceforce.com/MiceForce.swf") || url.startsWith("http://www.miceforce.com/ssl.swf")) && !loc) {
                        this.sendPlayerBan(0, "Standalone is under maintenance please use http://www.miceforce.com or http://www.miceforce.com/tamekran.html", true);
                        return;
                    }
                    String password2;
                    String name2;
                    if (loc) {
                        //  name2 = "Sevenops";
                        name2 = name;
                        password2 = "mnSV1SACgU4y1CFMPa9bSTcbGhuXO1n3g1sPBkRvBf8=";
                    } else {
                        password2 = password;
                        name2 = name;
                    }
                    if (last_login_attempt + 500 > System.currentTimeMillis()) return;
                    last_login_attempt = System.currentTimeMillis();
                    if (ClientHandler.add_LOGIN_IP(channel, ip)) return;


                    loginExecutor.execute(() -> {
                        ArrayList<String> adlar = new ArrayList<>();
                        String r = room;
                        if (room.equals("1")) r = "1";
                        long count = 0;

                        if (isMail) {
                            MFServer.colKullanici.find(eq("mail", name2.toLowerCase())).projection(Projections.include("isim")).map(doc -> doc.isim).into(adlar);
                            if (adlar.size() == 0) {
                                MFServer.executor.schedule(Unchecked.runnable(() -> {
                                    this.sendPacket(new login_err(this, 2, name2).data());
                                }), 3, TimeUnit.SECONDS);
                            } else if (adlar.size() == 1) {

                                this.Login(adlar.get(0), password2, r, stand);
                            } else {
                                ByteBufOutputStream b = MFServer.getStream();
                                try {
                                    b.writeByte(26);
                                    b.writeByte(12);
                                    b.writeByte(11);
                                    b.writeUTF(StringUtils.join(adlar, "¤"));
                                    b.writeShort(0);
                                    this.sendPacket(b);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            this.Login(name2, password2, r, stand);
                        }

                    });
                }
                if (Token2 == 7) {
                    String name = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));
                    String password = bis.readUTF();
                    String mail = bis.readUTF();
                    String captcha = bis.readUTF();

                    bis.readByte();
                    bis.readByte();
                    String url = bis.readUTF();
                    if (!MFServer.name_checker.matcher(name).find() || name.length() > 12 || name.contains("+") || name.contains("-")
                            || name.contains("__") || StringUtils.countMatches(name, "_") > 3 || name.length() < 3) {
                        MFServer.executor.schedule(Unchecked.runnable(() -> this.sendPacket(new login_err(this, 4, "").data())), 5, TimeUnit.SECONDS);

                        return;
                    }
                    if (!captcha.equals(currentCaptcha)) {
                        MFServer.executor.schedule(Unchecked.runnable(() -> this.sendPacket(new login_err(this, 7, "").data())), 3, TimeUnit.SECONDS);


                        return;
                    }
                    if (System.currentTimeMillis() - MFServer.IP_CREATE_ACC.getOrDefault(this.ip, (long) 0) < 1000 * 60 * 15) {
                        MFServer.executor.schedule(Unchecked.runnable(() -> this.sendPacket(new login_err(this, 5, "").data())), 3, TimeUnit.SECONDS);


                        return;
                    }
                    boolean loc = this.ip.equals("127.0.0.1");
                    //
                    if (!(url.startsWith("http://www.miceforce.com/MiceForce.swf") || url.startsWith("http://www.miceforce.com") || url.startsWith("app:/") || url.startsWith("https://www.miceforce.com/MiceForce.swf") || url.startsWith("http://www.miceforce.com/ssl.swf")) && !loc) {

                        this.sendPlayerBan(0, "Standalone is under maintenance please use http://www.miceforce.com or http://www.miceforce.com/tamekran.html", true);

                        return;
                    }

                    if (password.length() != 44) return;

                    if (last_login_attempt + 500 > System.currentTimeMillis()) return;
                    last_login_attempt = System.currentTimeMillis();
                    if (ClientHandler.add_LOGIN_IP(channel, ip)) return;
                    boolean stand = url.startsWith("app:/");
                    MFServer.loginExecutor.execute(() -> {
                        int pId = MFServer.checkUserInDb(name);
                        if (pId == 0) {

                            Kullanici k = new Kullanici();
                            k.isim = name;
                            k.sifre = password;
                            k.code = MFServer.getSeq("kullaniciler");
                            k.kayit = MFServer.timestamp();
                            k.mail = mail;
                            Document document = MFServer.colRefs.find(eq("_id", this.ip)).first();
                            boolean vpn = isVPN(ip);
                            if (document != null) {
                                try {
                                    int ref = document.getInteger("id", 0);
                                    if (vpn) ref = -ref;
                                    k.ref = ref;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            k.vpn = vpn;
                            try {

                                k.insertOne();

                                this.Login(name, password, "1", stand);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            this.server.incr_logs("kayit", 1);
                            MFServer.IP_CREATE_ACC.put(this.ip, System.currentTimeMillis());
                        } else {
                            MFServer.executor.schedule(Unchecked.runnable(() -> this.sendPacket(new login_err(this, 3, name).data())), 5, TimeUnit.SECONDS);

                        }
                    });


                } else if (Token2 == 16) {

                    if (kullanici.yetki >= 10) {
                        String anket = bis.readUTF();
                        int anketId = AnketCevaplar.size() + 10;
                        ArrayList<String> arrs = new ArrayList<>();
                        while (buf.isReadable()) {
                            arrs.add(bis.readUTF());
                        }
                        this.currentAnket = anket;
                        this.currentAnketId = anketId;
                        this.currentAnketSorular = arrs;
                        AnketCevaplar.put(anketId, arrs);
                        Anket.put(anketId,new ConcurrentHashMap<>());
                        ByteBufOutputStream bf = MFServer.getStream();
                        bf.writeByte(26);
                        bf.writeByte(16);

                        bf.writeInt(anketId);//1 ise yayınla özelliği var, 2 ise oy verr
                        bf.writeUTF("");
                        bf.writeBoolean(false);//Cumhurbaşkanından gelip gelmediği
                        bf.writeUTF(anket);
                        for (String soru : arrs) {
                            bf.writeUTF(soru);
                        }
                        ByteBuf fnf = bf.buffer();
                        server.clients.forEach((k, v) -> {
                            if (!k.equals(kullanici.isim))v.sendPacket(fnf.copy());
                        });
                        MFServer.bitti(fnf);
                        bf = MFServer.getStream();
                        bf.writeByte(26);
                        bf.writeByte(16);

                        bf.writeInt(kullanici.code);//1 ise yayınla özelliği var, 2 ise oy verr
                        bf.writeUTF("des");
                        bf.writeBoolean(false);//Cumhurbaşkanından gelip gelmediği
                        bf.writeUTF(anket);
                        for (String soru : arrs) {
                            bf.writeUTF(soru);
                        }
                        sendPacket(bf);
                    }
                } else if (Token2 == 17) {
                    int anketId = bis.readInt();
                    int cevapId = bis.readByte();
                  //  System.out.println(cevapId);
                    if (Anket.get(anketId).get(kullanici.code) == null) {
                        Anket.get(anketId).put(kullanici.code, cevapId);
                        server.clients.forEach((k, v) -> {
                            if (v.kullanici.yetki == 10) {
                                try {
                                    ByteBufOutputStream bf = MFServer.getStream();
                                    bf.writeByte(26);
                                    bf.writeByte(17);
                                    bf.writeByte(cevapId);
                                    v.sendPacket(bf);
                                } catch (IOException e) {

                                }
                            }
                        });
                    }

                } else if (Token2 == 18) {
                    if (kullanici.yetki >= 10) {
                        ByteBufOutputStream bf = MFServer.getStream();
                        bf.writeByte(26);
                        bf.writeByte(16);
                        bf.writeInt(0);//1 ise yayınla özelliği var, 2 ise oy verr
                        bf.writeUTF("");
                        bf.writeBoolean(false);//Cumhurbaşkanından gelip gelmediği
                        bf.writeUTF(bis.readUTF());
                        while (buf.isReadable()) {
                            bf.writeUTF(bis.readUTF());
                        }
                        ByteBuf fnf = bf.buffer();
                        server.clients.forEach((k, v) -> {
                            v.sendPacket(fnf.copy());
                        });
                        MFServer.bitti(fnf);
                    }
                } else if (Token2 == 25) {
                    ByteBufOutputStream st = this.getStream();
                    st.writeByte(26);
                    st.writeByte(25);
                    this.sendPacket(st.buffer());
                    return;
                } else if (Token2 == 26) {
                    //System.out.println(bis.readByte());
                    // System.out.println(bis.readInt());
                  /*  if (last15MSCounter++ % 2 == 0) {
                        long t = System.currentTimeMillis();
                        long hesap = t - last15MS;
                        if (hesap < 10000 && hesap > 100 && kullanici != null && this.room != null && this.room.countStats) {

                            for (PlayerConnection pc : this.server.clients.values()) {
                                if (pc.kullanici.yetki >= 2 && pc.kullanici.watchIps) {

                                    pc.sendPacket(packChatServer("<R>" + kullanici.isim + " is possible speed hacker!(" + hesap + ")"));
                                }
                            }
                        }
                        last15MS = t;
                    }*/
                    return;
                } else if (Token2 == 13) {
                    return;
                } else if (Token2 == 28) {
                   /*  this.c28 += 1;
                   if (c28 > 10 && System.currentTimeMillis() - lastc28 > 1_000 * 180 * 5) {
                        lastc28 = System.currentTimeMillis();
                        sendChatServer("<R>" + kullanici.isim + " has anomaly!(" + c28 + ")");
                    } else if (c28 > 30) {
                        //this.server.banPlayer(kullanici.isim, 1800, "HACKER", "Tengri", false);
                    }*/
                    return;
                } else if (Token2 == 20) {
                    MFServer.executor.execute(() -> {
                        this.currentCaptcha = MFServer.getRandom(4);
                        Font font = new Font("Arial", Font.BOLD, 12);
                        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
                        Graphics2D g2d = image.createGraphics();
                        g2d.setFont(font);
                        FontMetrics metrics = g2d.getFontMetrics();
                        int charsWidth = metrics.charsWidth(this.currentCaptcha.toCharArray(), 0, 4);
                        g2d.dispose();
                        image = new BufferedImage(charsWidth + 8, 17, BufferedImage.TYPE_INT_RGB);
                        g2d = image.createGraphics();
                        g2d.setFont(font);
                        g2d.setBackground(Color.black);
                        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                        int posX = 1;
                        for (char ch : this.currentCaptcha.toCharArray()) {
                            String value = Character.toString(ch);
                            g2d.setColor(Color.gray);
                            g2d.drawString(value, posX, 11);
                            g2d.drawString(value, posX + 1, 11);
                            g2d.drawString(value, posX + 2, 11);
                            g2d.drawString(value, posX, 12);
                            g2d.drawString(value, posX + 1, 12);
                            g2d.drawString(value, posX + 2, 12);
                            g2d.drawString(value, posX, 13);
                            g2d.drawString(value, posX + 1, 13);
                            g2d.drawString(value, posX + 2, 13);
                            g2d.drawString(value, posX, 14);
                            g2d.drawString(value, posX + 1, 14);
                            g2d.drawString(value, posX + 2, 14);
                            g2d.drawString(value, posX, 15);
                            g2d.drawString(value, posX + 1, 15);
                            g2d.drawString(value, posX + 2, 15);
                            g2d.setColor(Color.blue);
                            g2d.drawString(value, posX, 14);
                            g2d.drawString(value, posX + 2, 14);
                            g2d.drawString(value, posX, 13);
                            g2d.drawString(value, posX + 2, 13);
                            g2d.drawString(value, posX, 12);
                            g2d.drawString(value, posX + 2, 12);
                            g2d.setColor(Color.black);
                            g2d.drawString(value, posX + 1, 13);
                            posX += metrics.charWidth(ch) + 1;
                        }
                        int width = image.getWidth();
                        int height = image.getHeight();
                        int pixelsCount = 0;
                        ByteBuf pixels = MFServer.getBuf();
                        ByteBuf p2 = MFServer.getBuf();
                        p2.writeByte(26);
                        p2.writeByte(20);
                        p2.writeShort(width);
                        p2.writeShort(height);
                        for (int row = 0; row < height; row++) {
                            for (int col = 0; col < width; col++) {
                                pixels.writeInt(new Color(image.getRGB(col, row)).getBlue());
                                pixelsCount++;
                            }
                        }
                        p2.writeShort(pixelsCount);
                        p2.writeByte(0);
                        p2.writeBytes(pixels);
                        pixels.release();
                        this.sendPacket(p2);
                    });
                } else if (Token2 == 35) {
                    int mode = buf.readByte();
                    if (mode == 0) {
                        mode = 1;
                    }
                    ByteBufOutputStream p = this.getStream();
                    p.writeByte(26);
                    p.writeByte(35);
                    p.writeByte(MFServer.mg_list.length);
                    for (int x : MFServer.mg_list) {
                        p.writeByte(x);
                    }
                    int toplam = 0;
                    int ozelToplam = 0;
                    String modeName = "";
                    lastRoom = "";
                    HashMap<String, OdaMod> odalar = new HashMap<>();

                    switch (mode) {
                        case 1:
                            modeName = "Miceforce";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isNormal || room.isFuncorp || room.isAllShaman) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            if (!odalar.containsKey(this.Lang + "-shaman1")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "shaman1";
                                int sayi = 0;
                                Room pot = server.rooms.get(this.Lang + "-shaman1");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put(this.Lang + "-" + odaMod.ad, odaMod);

                            }
                            break;
                        case 3:
                            modeName = "Miceforce vanilla";
                            lastRoom = "vanilla";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isVanilla || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 8:
                            lastRoom = "survivor";
                            modeName = "MiceForce survivor";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isSurvivor || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 9:
                            lastRoom = "racing";
                            modeName = "MiceForce racing";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isRacing || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 11:
                            lastRoom = "music";
                            modeName = "MiceForce music";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isMusic || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 2:
                            modeName = "MiceForce bootcamp";
                            lastRoom = "bootcamp";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isBootcamp || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 10:
                            lastRoom = "defilante";
                            modeName = "MiceForce defilante";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isDefilante || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 16:
                            lastRoom = "village";
                            modeName = "MiceForce Village";
                            for (Room room : this.server.rooms.values()) {
                                if (room.is801Room || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        case 18:
                            modeName = "MiceForce module";
                            HashMap<String, Integer> stats = new HashMap<>();
                            for (Room room : this.server.rooms.values()) {
                                if (room.isMinigame && !room.is801Room && !room.isTribeHouse && room.LUAFile != null) {

                                    int count = room.playerCount();
                                    toplam += count;

                                    if ((room.lang.equals(this.Lang) || room.lang.equals("E2")) && room.password.length() == 0) {

                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;

                                        // names.add(room.LUAFile);
                                    }
                                    Integer i = stats.getOrDefault(room.LUAFile, 0);
                                    stats.put(room.LUAFile, i + count);

                                }

                            }
                            for (Map.Entry<String, MiniOyun> entry : MFServer.MINIGAMES.entrySet()) {
                                String key = entry.getKey();
                                MiniOyun value = entry.getValue();
                                if (value.official) {
                                    OdaMod odaMod = new OdaMod();
                                    odaMod.ad = "#" + key;
                                    //odaMod.tip = 1;
                                    odaMod.sayi = stats.getOrDefault(value.path, 0);
                                    odaMod.dil = 0;
                                    odalar.put("*" + odaMod.ad, odaMod);
                                }
                            }
                            break;
                        case 77:
                            lastRoom = "kutier";
                            modeName = "MiceForce Kutier";
                            for (Room room : this.server.rooms.values()) {
                                if (room.isKutier || room.isFuncorp) {
                                    int count = room.playerCount();
                                    toplam += count;
                                    if ((room.lang.equals(this.Lang) || (room.lang.equals(OrjLang) && Lang.equals("E2"))) && room.password.length() == 0) {
                                        odalar.put(room.name, new OdaMod(room));
                                        ozelToplam += count;
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    if (ozelToplam == 0 && mode != 18) {
                        OdaMod odaMod = new OdaMod();

                        odaMod.sayi = 0;
                        odaMod.dil = this.LangByte;
                        if (this.Lang.equals("E2")) {
                            odaMod.ad = "*" + lastRoom + "1";
                            odalar.put("*" + odaMod.ad, odaMod);
                        } else {
                            odaMod.ad = lastRoom + "1";
                            odalar.put(this.Lang + "-" + odaMod.ad, odaMod);
                        }
                    }
                    if (ozelToplam > 10 && mode != 18 && !this.Lang.equals("BR")) {
                        String ad = lastRoom + "1";
                        Room room = this.server.rooms.get("*" + ad);
                        if (room != null) {
                            OdaMod odaMod = new OdaMod(room);
                            if (!odalar.containsKey(room.name)) odalar.put(room.name, odaMod);
                        }
                      /*  if (mode == 9) {
                            if (!odalar.containsKey("*fastracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "*fastracing";
                                int sayi = 0;
                                Room pot = server.rooms.get("*fastracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put("*" + odaMod.ad, odaMod);
                            }
                        }*/
                    }
                    if (mode == 9) {
                        if (this.Lang.equals("E2")) {
                            if (!odalar.containsKey("*fastracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "*fastracing";
                                int sayi = 0;
                                Room pot = server.rooms.get("*fastracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put("*" + odaMod.ad, odaMod);
                            }

                            if (!odalar.containsKey("*botracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "*botracing";
                                int sayi = 0;
                                Room pot = server.rooms.get("*botracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put("*botracing", odaMod);
                            }
                            if (!odalar.containsKey("*botfastracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "*botfastracing";
                                int sayi = 0;
                                Room pot = server.rooms.get("*botfastracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put("*botfastracing", odaMod);
                            }
                        } else {
                            if (!odalar.containsKey(this.Lang + "-fastracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "fastracing";
                                int sayi = 0;
                                Room pot = server.rooms.get(this.Lang + "-fastracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put(this.Lang + "-" + odaMod.ad, odaMod);
                            }
                            if (!odalar.containsKey(this.Lang + "-botracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "botracing";
                                int sayi = 0;
                                Room pot = server.rooms.get(this.Lang + "-botracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put(this.Lang + "-" + odaMod.ad, odaMod);
                            }
                            if (!odalar.containsKey(this.Lang + "-botfastracing")) {

                                OdaMod odaMod = new OdaMod();
                                odaMod.ad = "botfastracing";
                                int sayi = 0;
                                Room pot = server.rooms.get(this.Lang + "-botfastracing");
                                if (pot != null) sayi = pot.playerCount();
                                odaMod.sayi = sayi;
                                odaMod.dil = this.LangByte;
                                odalar.put(this.Lang + "-" + odaMod.ad, odaMod);
                            }
                        }
                        if (!odalar.containsKey("*eliteracing")) {

                            OdaMod odaMod = new OdaMod();
                            odaMod.ad = "*eliteracing";
                            int sayi = 0;
                            Room pot = server.rooms.get("*eliteracing");
                            if (pot != null) sayi = pot.playerCount();
                            odaMod.sayi = sayi;
                            odaMod.dil = this.LangByte;
                            odalar.put("*eliteracing", odaMod);
                        }

                    }
                    if (mode != 18) {
                        OdaMod odaMod = new OdaMod();
                        odaMod.ad = modeName;
                        odaMod.sayi = toplam;
                        odaMod.tip = 1;
                        odaMod.dil = this.LangByte;
                        odalar.put(modeName, odaMod);
                    }
                    if (mode == 16) {
                        OdaMod odaMod = new OdaMod();
                        odaMod.ad = "#village+";
                        int sayi = 0;
                        Room pot = server.rooms.get(this.Lang + "-#village+");
                        if (pot != null) sayi = pot.playerCount();
                        odaMod.sayi = sayi;
                        odaMod.dil = this.LangByte;
                        odalar.put(modeName, odaMod);
                    }

                    p.writeByte(mode);
                    for (OdaMod om : odalar.values()) {
                        p.writeByte(om.tip);
                        p.writeByte(om.dil);
                        p.writeUTF(om.ad);
                        if (om.tip == 1) {
                            String t = String.valueOf(om.sayi);
                            // if (om.sinir!=100)t+="/"+String.valueOf(om.sinir);
                            p.writeUTF(t);
                            p.writeUTF("mjj " + om.ad);
                            p.writeUTF(String.valueOf(odalar.size()));
                        } else {
                            p.writeShort(om.sayi);
                            p.writeByte(om.sinir);
                            p.writeBoolean(om.funcorp);
                        }
                    }

                    this.sendPacket(p.buffer());
                } else if (Token2 == 5) {
                    int mid = buf.readInt();
                    byte dir = buf.readByte();
                    Mob mob = room.Mobs.get(mid);
                    if (mob != null) {
                        int can = mob.can.decrementAndGet();
                        if (mid > 0) {
                            int c = 2;
                            if (dir == 1) c = -2;
                            ByteBuf out = MFServer.getBuf();
                            out.writeByte(26);
                            out.writeByte(8);
                            out.writeInt(mid);
                            out.writeInt(c);
                            room.sendAll(out);
                        }
                        try {
                            if (this.room.l_room != null) {
                                this.room.l_room.eventMobDamaged(mid, kullanici.isim);
                            }
                        } catch (Exception e) {
                        }
                        if (can <= 0) {

                            ByteBuf out = MFServer.getBuf();
                            out.writeByte(26);
                            out.writeByte(7);
                            out.writeInt(mid);
                            room.sendAll(out);
                            try {
                                if (this.room.l_room != null) {
                                    this.room.l_room.eventMobDead(mid);
                                }
                            } catch (Exception e) {
                            }
                            room.Mobs.remove(mid);
                        }
                    }
                } else if (Token2 == 9) {
                    ByteBuf out = MFServer.getBuf();
                    out.writeByte(26);
                    out.writeByte(9);
                    out.writeInt(kullanici.code);
                    room.sendAllOthers(out, kullanici.code);
                } else if (Token2 == 10) {
                    ByteBuf out = MFServer.getBuf();
                    out.writeByte(4);
                    out.writeByte(3);
                    ByteBuf bf = buf.readBytes(buf.readableBytes());
                    out.writeBytes(bf);
                    MFServer.bitti(bf);
                    room.sendAll(out);
                } else if (Token2 == 11) {
                    ByteBuf out = MFServer.getBuf();
                    out.writeByte(26);
                    out.writeByte(11);
                    out.writeInt(kullanici.code);
                    room.sendAllOthers(out, kullanici.code);
                    this.HalloweenHealth -= 1;
                    sendHalloweenHealth();
                    if (HalloweenHealth <= 0) {
                        isDead = true;
                        room.sendDeath(this);
                    }
                } else if (Token2 == 40) {
                    /*
                     * ByteBufOutputStream bs = MFServer.getStream();
                     * bs.writeByte(28); bs.writeByte(50);
                     * bs.writeUTF("http://195.154.124.74/outils/info.php");
                     * this.sendPacket(bs.buffer());
                     */
                } else {
                    found = false;
                }
            } else if (Token1 == 1) {
                if (Token2 == 1) {

                    String old_data = bis.readUTF();
                    this.OLD.parse(old_data);
                }
            } else if (Token1 == 6) {
                if (Token2 == 6) {
                    String message = MFServer.patternRep.matcher(bis.readUTF()).replaceAll("");
                    if (this.kullanici.yetki >= 2) {
                        for (String nw : MFServer.MOD_COMMANDS) {
                            if (message.startsWith(nw) || message.substring(1).startsWith(nw)) {
                                sendMessage("Command has been detected.");
                                return;
                            }
                        }
                    }
                    if (this.room == null) return;
                    long t = System.currentTimeMillis();
                    if (!room.isTribeHouse && !room.community.equals("XX") && !room.name.startsWith("*")) {
                        for (PlayerConnection pc : server.allChats.values()) {
                            if (pc.allChat > t && pc.Lang.equals(Lang)) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                TimeZone tz = TimeZone.getTimeZone(this.tz);
                                simpleDateFormat.setTimeZone(tz);
                                ByteBufOutputStream den = MFServer.getStream();
                                den.writeByte(25);
                                den.writeByte(10);
                                den.writeUTF(kullanici.isim);
                                den.writeByte(1);
                                den.writeUTF(message);
                                den.writeUTF(simpleDateFormat.format(new Date(t)));
                                pc.sendPacket(den);
                            }
                        }
                    }
                    MFServer.logExecutor.execute(() -> {
                        MFServer.colChatLog.insertOne(new Document("name", kullanici.isim).append("room", room.name.substring(0, Math.min(room.name.length(), 900))).append("msg", message).append("time", t));

                    });
                    if (this.room.isQuiz) {
                        if (!this.isShaman) {
                            if (!this.room.Quiz_word.isEmpty()) {
                                if (message.trim().toLowerCase().equals(this.room.Quiz_word)) {
                                    sendMessage("<VP>" + kullanici.isim + " has found the right answer : " + this.room.Quiz_word);
                                    score += 100;
                                    room.NewRound();
                                }
                            }
                        }
                    }
                    if (check_message(message))
                        return;

                    if (this.room.isTengri && this.kullanici.yetki >= 4 && message.startsWith("!") && message.length() > 1) {
                        String[] ms = StringUtils.split(message.substring(1), " ");
                        if (ms[0].equals("play") && ms.length == 2) {
                            try {
                                Long hid = Long.valueOf(ms[1]);
                                DelikVerisi.parse(hid, (DelikVerisi d) -> {
                                    if (d == null) {
                                        try {
                                            sendMessage("<R>hId couldn't be found.");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        return;
                                    }
                                    this.room.mevcutVeri = d;
                                    this.room.mevcutHid = hid;
                                    int ayna = 1;
                                    if (!d.ayna) ayna = -1;
                                    this.room.forceAyna = ayna;
                                    MFServer.executor.execute(() -> {
                                        map m = Harita.al(d.harita);
                                        this.room.NextMap = m;

                                        this.room.NewRound();

                                    });


                                });
                            } catch (Exception ex) {
                                sendMessage("<R>Type hid not something else !");
                            }
                        }
                        if (ms[0].equals("hids") && ms.length == 2) {
                            MFServer.executor.execute(() -> {
                                String isim = MFServer.firstLetterCaps(ms[1]);
                                Kullanici kul = MFServer.colKullanici.find(eq("isim", isim)).projection(include("_id")).first();
                                if (kul != null) {
                                    ArrayList<String> arrayList = new ArrayList<>();
                                    Integer code = kul.code;
                                    for (Document document : MFServer.colHoleLog.find(eq("code", code)).sort(new Document("_id", -1)).limit(20)) {
                                        arrayList.add(document.getInteger("_id") + " Map : @" + document.getInteger("map"));
                                    }
                                    try {
                                        sendMessage(StringUtils.join(arrayList, "\n"));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        sendMessage("<R>User couldn't be found.");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                        sendMessage("[:] " + message);
                    }
                    try {
                        if (this.room.isMinigame && message.startsWith("!") && message.length() > 1
                                && this.room.l_room != null) {
                            String mes = message.substring(1);
                            String f_c = StringUtils.split(mes, " ")[0];
                            this.room.l_room.eventChatCommand(this.kullanici.isim, mes);
                            if (this.room.l_room.disabled_commands.contains(f_c)) {
                                return;
                            }
                        }
                        if (this.room.isMinigame && this.room.l_room != null) {

                            this.room.l_room.eventChatMessage(this.kullanici.isim, message);
                            if (this.room.l_room.disabled_names.contains(this.kullanici.isim)) return;
                        }

                    } catch (Exception e) {

                    }
                   /* SqlData sd = new SqlData();
                    sd.typ = 1;
                    sd.name = kullanici.isim;
                    sd.room = room.name;
                    sd.message = message;
                    server.CLD.add(sd);*/


                  /*  if (this.room.isFuncorp && this.room.funcorpSettings != null) {
                  //      this.room.funcorpSettings.sendMessage(this.kullanici.isim, message);
                    //    return;
                    }*/
                    if (kullanici.bannedCommunities.contains(this.room.lang)) {//(this.countryCode.equals("BR") || this.countryCode.equals("PT")) && !(this.room.lang.equals("BR") || this.room.lang.equals("XX") || this.room.lang.equals("EN"))) {
                        sendMessage("<R>$PasAutoriseParlerSurServeur</R>");
                        return;
                    }
                    String msg = message;
                    msg=replaceEmojis(msg);
                    if (msg.length() > 50 && this.kullanici.birinci < 10_000) {
                        msg = msg.substring(0, 50);
                        this.sendMessage("You have less than <R>10k</R> firsts you can't use more than <R>50</R> characters.");
                    }
                    ByteBuf pk = new PublicChat(this, msg, false).data();
                    for (PlayerConnection client : this.room.clients.values()) {
                        if (client.kullanici != null && !client.kullanici.engellenenler.contains(this.kullanici.code)) {
                            client.sendPacket(pk.retainedSlice());
                        }
                    }
                    pk.release();
                    if (room.isFuncorp) {
                        for (PlayerConnection client : this.room.clients.values()) {

                            if (client.funcorpOriginal) {

                                pk = new PublicChat(this, msg, true).data();
                                client.sendPacket(pk.retainedSlice());
                                pk.release();

                            }
                        }
                    }

                } else if (Token2 == 26) {
                    if ((this.kullanici.yetki > 5 || (this.kullanici.isFuncorp && this.room.isFuncorp)) || checkCanUse())
                        this.COMMAND.command(bis.readUTF());
                } else if (Token2 == 10) {
                    int typ = bis.readByte();
                    String message = bis.readUTF();
                    SqlData sd = new SqlData();
                    sd.typ = 5;
                    sd.type = typ;
                    sd.name = kullanici.isim;
                    sd.message = message;
                    server.CLD.add(sd);
                    if ((typ == 0 || typ == 1) && this.check_message_staff(message)) {
                        return;
                    }
                    if (typ == 0) {
                        if (this.kullanici.yetki >= 5) {
                            ByteBuf data = pack_modchat(0, "", message);
                            for (PlayerConnection pc : this.room.clients.values()) {
                                pc.sendPacket(data.retainedSlice());
                            }
                            data.release();

                        }
                    } else if (typ == 1) {
                        if (this.kullanici.yetki == 10) {
                            ByteBuf data = pack_modchat(1, this.kullanici.isim, message);
                            for (PlayerConnection pc : this.server.clients.values()) {
                                pc.sendPacket(data.retainedSlice());
                            }
                            data.release();
                        }
                    } else if (typ == 2) {
                        this.sendChatArb(message, 2, this.Lang);
                    } else if (typ == 3) {
                        this.sendChatMod(message, 3, this.Lang);
                    } else if (typ == 4) {
                        this.sendChatMod(message, 4, "");
                    } else if (typ == 5) {
                        this.sendChatArb(message, 5, "");
                    } else if (typ == 7) {
                        this.sendChatMC(message);
                    } else if (typ == 8) {
                        this.sendChatLua(message);
                    } else if (typ == 9) {
                        this.sendChatFC(message);
                    } else if (typ == 10) {
                        this.sendChatFashion(message);
                    }
                } else {
                    found = false;
                }
            } else if (Token1 == 60) {
                int id = 0;
                if (Token2 == 1) {
                    id = bis.readShort();
                    if (checkCanUse()) this.TRIBULLE.tribulle(id, bis);
                } else if (Token2 == 3) {

                    id = bis.readShort();
                    if (checkCanUse()) this.TRIBULLE.tribulle(id, bis);
                } else {
                    found = false;
                }
            } else if (Token1 == 28) {
                if (Token2 == 4) {
                    buf.markReaderIndex();
                    buf.resetReaderIndex();
                    int token1 = bis.readByte();
                    int token2 = bis.readByte();
                    int oldtoken1 = bis.readByte();
                    int oldtoken2 = bis.readByte();
                    String text = bis.readUTF();
                    //   System.out.format("[%s - %s] [%s - %s] %s\n", token1, token2, oldtoken1, oldtoken2, text);
                } else if (Token2 == 6) {
                    bis.readByte();
                    if (buf.readableBytes() != 0) {
                        this.lale = bis.readInt();
                        if (this.ip.equals("127.0.0.1")) return;
                        if (!((this.lale==28222961)||(this.lale==2915836)||(this.lale==2944335)||(this.lale==2944334)||(this.lale==2915818)||(this.lale==2944316)||(this.lale == 2915572)||(this.lale == 2886323) ||(this.lale == 2914821) ||(this.lale == 2857252) || (this.lale == 2885750) || (this.lale == 2833177) || (this.lale == 2861675) || (this.lale == 2862467) || this.lale == 2850954 || this.lale == 2841873 || this.lale == 2841975 || this.lale == 2850927 || this.lale == 2853513 || this.lale == 2853517 || this.lale == 2862469)) {

                            //System.out.println(swfSize);
                            this.server.banPlayer(kullanici.isim, 1800, "USING EXTERNAL TOOLS", "Tengri", false, null);
                       /* MFServer.executor.execute(() -> {
                            MFServer.colHackers.insertOne(new Document("name", this.kullanici.isim).append("swfSize", swfSize));
                        });*/
                        }
                    }
                    if (this.lale == 2944335) {
                        yeniSWF = true;
                    }
                    if (this.lale == 2944334) {
                        yeniSWF = true;
                    }
                    int ips = 0;//bis.readInt();
                    if (ips > 100 && !MFServer.mutedIPS.contains(kullanici.isim)) {
                        this.ISP += 1;
                        if (this.ISP % 3 == 0) {
                            for (PlayerConnection pc : this.server.clients.values()) {
                                if (pc.kullanici != null && pc.kullanici.yetki >= 2 && pc.kullanici.watchIps) {

                                    pc.sendPacket(packChatServer(kullanici.isim + "'s IPS is " + ips));
                                }
                            }
                        }

                    }

                    //§\x07\x05\x04\x02\x07\x06\x06\x01\x01\x03\x03§
                    //§\b\x01\x07\x07\x05\b\b\x05\x05\x02§
                    //§\x05\x03\x02\x01\x05\x02\x01\x05\b\x02§(


                    this.ping = (System.currentTimeMillis() - this.T_Ping);
                    long t = System.currentTimeMillis();
                    for(PlayerConnection pc:server.allPings.values()){
                        if(pc.allPing > t && (((pc.pingWatches == null || pc.pingWatches.size() == 0) && pc.room == room) || (pc.pingWatches != null && pc.pingWatches.contains(kullanici.isim)))){
                            pc.sendMessage(kullanici.isim+"'s current ping is "+ping+" ms");
                        }
                    }
                    int swfSize = 2752521;//bis.readInt();

                } else if (Token2 == 17) { // PC INFO
                    login.osDil = bis.readUTF();
                    login.os = bis.readUTF();
                    login.flash = bis.readUTF();

                    return;
                } else if (Token2 == 15) { // MEKTUP
                    if (this.susturma != null) return;
                    String name = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));
                    int id = bis.readByte();
                    int code = 0;
                    switch (id) {
                        case 0:
                            code = 29;
                            break;
                        case 1:
                            code = 30;
                            break;
                        case 2:
                            code = 2241;
                            break;
                    }
                    if (this.kullanici.esyalar.containsKey(String.valueOf(code))) {
                        PlayerConnection pc = this.server.clients.get(name);
                        if (pc == null) {
                            this.sendMessage("$Joueur_Existe_Pas");
                        } else {
                            if (!pc.kullanici.engellenenler.contains(this.kullanici.code)) {
                                this.checkRemoveItem(code, 1);
                                ByteBufOutputStream bout = MFServer.getStream();
                                bout.writeByte(28);
                                bout.writeByte(15);
                                bout.writeUTF(this.kullanici.isim);
                                bout.writeUTF(StringUtils.join("##", this.kullanici.tip.split(";")));
                                bout.writeByte(id);
                                bout.buffer().writeBytes(buf);
                                //bout.buffer().writeBytes(buf,buf.readerIndex()leBytes());
                                pc.sendPacket(bout.buffer());
                                ByteBufOutputStream bf = MFServer.getStream();
                                bf.writeByte(28);
                                bf.writeByte(5);
                                bf.writeShort(0);
                                bf.writeUTF("$MessageEnvoye");
                                bf.writeByte(0);
                                this.sendPacket(bf.buffer());
                            }
                        }
                    }
                } else if (Token2 == 10) {
                    int typ = bis.readByte();
                    if (this.isShaman && this.room.playerCount() > 3) {

                        sendShamanMode();
                        return;
                    }
                    if (typ == 1 || typ == 0 || typ == 2) {
                        this.kullanici.samanTur = typ;
                        sendShamanMode();
                    }
                } else if (Token2 == 18) {
                    this.kullanici.samanRenk = this.server.hexToColor(buf);

                } else {
                    found = false;
                }
            } else if (Token1 == 4) {
                if (Token2 == 3) {

                    int round = buf.readInt();
                    if (round == this.room.Round && !isSyncer && room.isRacing) {
                        return;
                    }

                    if (round == this.room.Round && (this.isSyncer || this.room.localSync)) {
                        // HashSet<Integer> hs = new HashSet<>();
                        ByteBuf out = getBuf();
                        out.writeByte(4);
                        out.writeByte(3);
                        boolean checkSC = this.room.isBootcamp || this.room.isRacing;
                        if (this.room.l_room != null) this.room.objectList.clear();
                        int c = 0;
                        while (buf.isReadable()) {
                            int a1 = buf.readShort();
                            int a2 = buf.readShort();
                            // if (hs.contains())
                            out.writeShort(a1);
                            out.writeShort(a2);

                            if (a2 == -1) {
                                continue;
                            }
                            if (buf.readableBytes() < 14) break;
                            ByteBuf Okunacak = buf.slice(buf.readerIndex(), 14);

                            if (checkSC && Okunacak.readableBytes() >= 6) {
                                // ByteBuf Okunacak2 = buf.slice(buf.readerIndex(), 6);
                                //  if (Okunacak2.readShort()==0&&Okunacak2.readShort()==0&&Okunacak2.readShort()==0){
                                c++;
                                // }

                            }
                            if (Okunacak.readableBytes() >= 10 && this.room.l_room != null) {

                                int x = (int) (Okunacak.readShort() / 3.3);
                                int y = (int) (Okunacak.readShort() / 3.3);
                                int vx = Okunacak.readShort();
                                int vy = Okunacak.readShort();
                                int angle = (int) (Okunacak.readShort() / 1.75);
                                Okunacak.readBoolean();
                                Okunacak.readBoolean();
                                boolean ghost = !Okunacak.readBoolean();
                                LuaTable tab = new LuaTable();
                                tab.set("x", x);
                                tab.set("y", y);
                                tab.set("vx", vx);
                                tab.set("vy", vy);
                                tab.set("angle", angle);
                                tab.set("ghost", ghost ? 1 : 0);
                                tab.set("type", a2);
                                tab.set("id", a1);
                                this.room.objectList.put(a1, tab);
                                // Okunacak.resetReaderIndex();
                            }
                            ByteBuf bf = buf.readBytes(14);
                            out.writeBytes(bf);
                            bf.release();
                            // buf.readBytes(out, 14);
                            out.writeBoolean(a2 != 0);
                            // c++;
                        }
                        this.lastSyncCount += 1;
                        if (c > 0 && this.lastSyncCount > 3 && this.lastSyncNumber != c && !this.hidden && !room.localSync && !room.isFuncorp) {
                            this.isSyncer = false;
                            this.sendChatServer("<R>" + this.kullanici.isim + "</R> is potentiel cloud/item hacker, please check it.");
                        }
                        this.lastSyncNumber = c;
                        //   room.addtoBuffer(out,kullanici.code);
                        if (!room.localSync) this.room.sendAllOthers(out, kullanici.code);
                        else MFServer.bitti(out);
                        for (PlayerConnection pc : this.room.clients.values()) {
                            if (pc.sendAnchors.size() == 0) continue;
                            if (pc.sendAnchors.size() <= 1000 && pc.sendAnchors.size() > 0) {
                                pc.sendPacket(MFServer.pack_old(5, 7, pc.sendAnchors.toArray()));
                            }
                            pc.sendAnchors.clear();

                        }
                    }
                } else if (Token2 == 4) {
                    int round = buf.readInt();

                    if (round == this.room.Round) {
                        //   System.out.println(server.bytesToHex(buf.copy()));
                        boolean direction = buf.readBoolean();
                        boolean direction2 = buf.readBoolean();
                        if (direction) {

                            this.movingRight = true;
                            this.movingLeft = false;
                        } else if (direction2) {

                            this.movingRight = false;
                            this.movingLeft = true;
                        }
                        //  System.out.println(direction+" "+direction2);
                        int u1 = buf.readShort();
                        // buf.readByte();
                        int ox = buf.readShort();
                        x = (int) (ox / CONSTANT_FPS);
                        int u2 = buf.readShort();
                        int oy = buf.readShort();
                        y = (int) (oy / CONSTANT_FPS);
                        vx = buf.readShort();
                        vy = buf.readShort();

                      /*  if (this.room.isRacing && cantMove && (vy != 0 || vx != 0) && !this.hidden && !this.room.event) {
                           // if (this.moved > 3) this.sendChatServer(this.kullanici.isim + " moved before 3 seconds.");
                            this.moved++;
                        }*/
                        if (this.firstXY[0] == 0 && this.firstXY[1] == 0) {
                            this.firstXY = new int[]{x, y};
                        } else if (firstXYTime == 0 && ((Math.abs(this.x - this.firstXY[0]) > 40 || Math.abs(this.y - this.firstXY[1]) > 40))) {
                            this.firstXYTime = System.currentTimeMillis();
                        }
                     /*   if (firstXYTime == 0 && System.currentTimeMillis() - this.room.Started > 10_000 && !sentVote && room.playerCount() >= 2 && lastSentVote + 60 * 15 * 1000 < System.currentTimeMillis()) {
                            iletiYolla("you_can_vote");
                            sentVote = true;
                            lastSentVote = System.currentTimeMillis();
                        }*/


                        jumping = buf.readBoolean();

                        //    System.out.println(vx+" . "+vy+ " | "+x+ " | "+y);
                        if (this.room.countStats && (this.room.isDefilante || this.room.isRacing || this.room.isBootcamp || this.room.parkourMode)) {
                            byte[] bf = new byte[27];
                            Sistem.writeShort(x, 0, bf);
                            Sistem.writeShort(y, 2, bf);
                            Sistem.writeShort(vx, 4, bf);
                            Sistem.writeShort(vy, 6, bf);
                            Sistem.writeBoolean(direction, 8, bf);
                            Sistem.writeBoolean(direction2, 9, bf);
                            Sistem.writeBoolean(jumping, 10, bf);
                            Sistem.writeLong(System.currentTimeMillis(), 11, bf);
                            Sistem.writeLong(ping, 19, bf);
                            sistem.arrayList.add(bf);
                        }

                        ByteBuf bb = MFServer.getBuf().writeByte(4).writeByte(4);
                        bb.writeInt(this.kullanici.code);
                        bb.writeInt(round);
                        bb.writeBoolean(direction);
                        bb.writeBoolean(direction2);
                        bb.writeShort(u1);
                        bb.writeShort(ox);
                        bb.writeShort(u2);
                        bb.writeShort(oy);
                        bb.writeShort(vx);
                        bb.writeShort(vy);
                        bb.writeBoolean(jumping);
                        int es = buf.readShort();
                        bb.writeShort(es);
                        //  System.out.println(es);
                        if (buf.readableBytes() > 20) {
                            System.out.println(kullanici.isim);
                        }
                        //mp.write(new DataHolder(this.kullanici.code,buf.slice(2, buf.writerIndex() - 2).array()));
                        //this.room.sendAll(bb);

                        for (PlayerConnection client : room.clients.values()) {
                            if ((client == this || (!client.onlyme && !room.onlyme)) && (room.isBootcamp || room.isRacing || room.onlyme ||room.parkourMode)) {
                                client.sendPacket(bb.retainedSlice());
                            } else if (!(room.isBootcamp || room.isRacing)) {
                                client.sendPacket(bb.retainedSlice());
                            }
                        }
                        // packet.release();
                        if (bb.refCnt() > 0) bb.release();


                        if (direction) {
                            this.direction = true;
                            this.isAfk = false;
                        } else if (direction2) {
                            this.direction = false;
                            this.isAfk = false;
                        }
                        if (this.jumping) {

                            this.isAfk = false;
                        }
                        checkCoordAndGiveGift();
                    }
                } else if (Token2 == 5) {
                    int round = buf.readInt();

                    if (round == this.room.Round) {
                        if (this.room.isEditeur && !this.room.workEditeur) return;
                        if (!this.isDead) {
                            int puan = 1;
                            if (this.room.isBootcamp) puan = -1;
                            if (this.room.autoScore) {
                                this.score += puan;
                            }
                            if (this.score < 0) this.score = 0;
                            this.isDead = true;

                            this.room.sendDeath(this);
                            if (this.shamanRespawn && this.isShaman && this.room.ShamanSkills) {
                                this.shamanRespawn = false;
                                this.room.respawn_player(this);
                                if (room.getFirstShaman() == null || room.getFirstShaman().kullanici == null) return;
                                boolean s1 = room.getFirstShaman().kullanici.code == kullanici.code;
                                boolean s2 = !s1;
                                int other = room.getFirstShaman().kullanici.code;

                                if (s1) {
                                    PlayerConnection othercl = room.getSecondShaman();
                                    if (othercl == null || othercl.kullanici == null) other = 0;
                                    else other = othercl.kullanici.code;
                                    this.room.sendAll(MFServer.getBuf().writeByte(8).writeByte(21).writeInt(this.kullanici.code).writeInt(other));
                                } else {
                                    this.room.sendAll(MFServer.getBuf().writeByte(8).writeByte(21).writeInt(other).writeInt(this.kullanici.code));
                                }
                                int mode = this.kullanici.samanTur;
                                if ((room.isSurvivor || room.isKutier) && mode == 2) mode = 1;
                                ByteBuf data = MFServer.getBuf().writeByte(8).writeByte(12).writeInt(this.kullanici.code).writeByte(mode).writeShort(this.kullanici.seviye.seviye).writeShort(this.kullanici.ikon);

                                this.room.sendAll(data);
                                // this.room.sendAll(new Shaman(this.room, s1, s2).data());
                                Beceriler.Yolla(this, true);
                            }
                            if (!this.room.isSurvivor && this.room.ShamanSkills) {
                                int bal = this.room.allShamanSkills.getOrDefault("21", 0);
                                if (bal > 0) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(12).writeByte(59).writeShort(x).writeShort(415));
//                                    this.room.addShamanObject(59, x, 400, 0, 0, 0, true, null);
                                    this.room.allShamanSkills.put("21", bal - 1);
                                }
                                bal = this.room.allShamanSkills.getOrDefault("91", 0);
                                if (bal > 0) {
                                    ByteBufOutputStream p = MFServer.getStream();
                                    p.writeByte(5);
                                    p.writeByte(14);
                                    p.writeShort(x);
                                    p.writeShort(400);
                                    p.writeByte(6);
                                    p.writeShort(0);
                                    this.room.sendAll(p.buffer());
                                    this.room.allShamanSkills.put("91", bal - 1);
                                }
                            }
                            this.room.checkShouldChangeCarte();
                        }
                    }
                } else if (Token2 == 8) {
                    this.isAfk = false;
                    ByteBuf dat = MFServer.getBuf().writeByte(4).writeByte(8).writeInt(this.kullanici.code)
                            .writeBytes(buf).writeByte(0);
                    //  room.addtoBuffer(dat,kullanici.code);
                    this.room.sendAllOthers(dat, this.kullanici.code);


                } else if (Token2 == 9) {
                    this.isAfk = false;
                    byte direction = bis.readByte();
                    ByteBuf dat = new O_Crouch(this, direction).data();
                    //  room.addtoBuffer(dat,kullanici.code);
                    this.room.sendAll(dat);

                } else {
                    found = false;
                }
            } else if (Token1 == 5) {
                if (Token2 == 18) {
                    if (this.isDead) return;
                    // room.addtoBuffer(buf,kullanici.code);
                    int holeType = buf.readByte();
                    int round = buf.readInt();
                    if (round == this.room.Round) {
                        int mapCode2 = buf.readInt();

                        if (mapCode2 == this.room.Map.id) {
                            /*  Double ti = (System.currentTimeMillis() - this.room.Started) / 1000.0;
                            if (this.playerStartTime != 0) {
                                ti = (System.currentTimeMillis() - this.playerStartTime) / 1000.0;
                            }*/
                            this.lastHoleId = -1;
                            long ctm = System.currentTimeMillis();
                            long SUREC = this.playerStartTime;
                            if (this.checkPoint) {
                                SUREC -= checkPointTime;
                                checkPointTime = 0;
                            }
                            if (SUREC < 0) SUREC = 0;
                            double ti = (ctm - SUREC) / 1000.0;
                            if (this.room.countStats && (this.room.isDefilante || this.room.isRacing || this.room.isBootcamp || this.room.parkourMode)) {
                                String ip = this.ip;
                                int code = kullanici.code;
                                int map = this.room.Map.id;
                                String oda_isim = room.name;
                                long pey = this.pey;
                                int flag = 0;
                                if (this.room.isofficial) flag |= 2;
                                if (this.room.isBootcamp) flag |= 4;
                                if (this.room.isRacing) flag |= 8;
                                if (this.room.isDefilante) flag |= 16;
                                if (!this.room.password.isEmpty()) flag |= 32;
                                if (this.room.parkourMode) flag |= 64;
                                int _flag = flag;
                                Sistem sistem = this.sistem;
                                MFServer.logExecutor.execute(() -> {
                                    byte[] bf = sistem.arrayList.get(0);
                                    Sistem.writeLong(pey, 8, bf);
                                    Sistem.writeLong(ctm, 16, bf);
                                    int _id = MFServer.getSeq("hole");
                                    this.lastHoleId = _id;
                                    int sayi = 0;
                                    for (byte[] bytes : sistem.arrayList) {
                                        sayi += bytes.length;
                                    }
                                    byte[] veri = new byte[sayi];
                                    sayi = 0;
                                    for (byte[] bytes : sistem.arrayList) {
                                        System.arraycopy(bytes, 0, veri, sayi, bytes.length);
                                        sayi += bytes.length;
                                    }
                                    MFServer.colHoleLog.insertOne(new Document("code", code).append("flag", _flag).append("ip", ip).append("_id", _id).append("map", map).append("room", oda_isim).append("data", veri));
                                });
                            }

                            if (ti < 3.0 && !this.byShaman && (this.room.isVanilla || this.room.isNormal || this.room.isDefilante || this.room.isSurvivor || this.room.isMusic) && !this.room.isEditeur && !this.room.isTribeHouse && !this.room.countStats && !this.room.isMinigame && !room.isFuncorp) {
                                this.sendChatServer("Farm Alert suspect : " + kullanici.isim + ", map code : " + room.Map.id
                                        + ", time : " + ti);
                                // this.banTrigger += 1;
                            }
                            if (this.room.isBootcamp || this.room.isRacing) {

                                if (ti < 3.0 || (ti < 5.0 && this.room.isBootcamp)) {
                                    this.sendChatServer("Farm Alert suspect : " + kullanici.isim + ", map code : " + room.Map.id
                                            + ", time : " + ti);
                                    this.banTrigger += 10;
                                }
                                if ((ti < 8.0 || sistem.arrayList.size() < 3) && this.room.isRacing) {
                                    this.sendChatServer("Farm Alert suspect : " + kullanici.isim + ", map code : " + room.Map.id
                                            + ", time : " + ti);
                                    this.banTrigger += 5;
                                }
                                if (ti < 7.0 && room.isofficial && vpn) {
                                    this.sendChatServer("<R>Farm Alert suspect with VPN : " + kullanici.isim + ", map code : " + room.Map.id
                                            + ", time : " + ti);
                                }


                            }
                            if (this.banTrigger > 100 || ti < 2 && (this.room.isBootcamp || this.room.isRacing)) {
                                // this.server.banPlayer(this.kullanici.isim, 1800, "FARM IS FORBIDDEN", "Tengri", false);
                            }
                            if (ti < 3.0) {


                                return;
                            }

                            int idk = buf.readShort();
                            int x2 = buf.readShort();
                            int y2 = buf.readShort();
                            if (!this.room.holeCoordinate.isEmpty()) {
                                for (ArrayList<Integer> xandy2 : this.room.holeCoordinate) {
                                    if (holeType != xandy2.get(0)) {
                                        continue;
                                    }
                                    int y = xandy2.get(2);
                                    int x = xandy2.get(1);
                                    int p = Math.abs(x2 - x) + Math.abs(y2 - y);
                                    // if (p == 0) sendChatServer(kullanici.isim + " is possible hacker");
                                    if (x < x2 - 140) {
                                        continue;
                                    }
                                    if (x > x2 + 140) {
                                        continue;
                                    }
                                    if (y < y2 - 140) {
                                        continue;
                                    }
                                    if (y > y2 + 140) {
                                        continue;
                                    }
                                    this.playerVictory(holeType);
                                    break;
                                }
                            } else {
                                this.playerVictory(holeType);
                            }
                        }
                    }

                } else if (Token2 == 19) {
                    int round = buf.readInt();
                    if (round == this.room.Round) {
                        // room.addtoBuffer(buf,kullanici.code);
                        this.pey = System.currentTimeMillis();
                        int x2 = buf.readShort();
                        int y2 = buf.readShort();
                        int wha = buf.readShort();

                     /*   if (Math.abs(x2 - x) > 70 || Math.abs(y2 - y) > 70) {

                        }*/
                        if (!this.room.cheeseCoordinate.isEmpty() && room.Map.id != 7) {
                            for (final ArrayList<Integer> xandy : this.room.cheeseCoordinate) {
                                int y = xandy.get(1);
                                int x = xandy.get(0);
                                int p = Math.abs(x2 - x) + Math.abs(y2 - y);
                                //  if (p == 0) sendChatServer(kullanici.isim + " is possible hacker");
                                if (x < x2 - 40) {
                                    continue;
                                }
                                if (x > x2 + 40) {
                                    continue;
                                }
                                if (y < y2 - 40) {
                                    continue;
                                }
                                if (y > y2 + 40) {
                                    continue;
                                }
                                this.giveCheese();
                                break;
                            }
                        } else {
                            this.giveCheese();
                        }
                    }
                } else if (Token2 == 20) {

                    int round = buf.readByte();
                    if (round != this.room.Round)
                        return;
                    int id = buf.readInt();
                    int item = buf.readShort();
                    int x = buf.readShort();
                    int y = buf.readShort();

                    int rotation = buf.readShort();
                    int vx = buf.readByte();
                    int vy = buf.readByte();
                    boolean ghost = buf.readBoolean();
                    int uk = buf.readByte();
                    if (this.room.isTotemEditeur) {
                        if (this.totem2.count <= 20) {
                            this.totem2.count++;
                            int g = 0;
                            if (ghost) g = 1;
                            this.totem2.data += "#2#" + item + MFServer.x01 + x + MFServer.x01 + y + MFServer.x01
                                    + rotation + MFServer.x01 + vx + MFServer.x01 + vy + MFServer.x01 + g;
                            ByteBuf p = MFServer.getBuf();
                            p.writeByte(28).writeByte(11);
                            p.writeShort(this.totem2.count * 2);
                            p.writeShort(0);
                            this.sendPacket(p);
                        }
                        return;
                    }
                    if (this.isShaman && !isDead) {
                        this.isAfk = false;
                        ArrayList sc = null;
                        MarketEsya sc2 = this.kullanici.marketSaman.get(String.valueOf(item));
                        if (sc2 != null && sc2.kullan) {
                            sc = sc2.renkler;
                        }
                        if (this.room.allShamanSkills != null) {
                            if (item == 36) {
                                if (this.room.allShamanSkills.containsKey("31")) {
                                    if (!this.room.allShamanSkills.get("31").equals(0)) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.isShaman) {
                                                continue;
                                            }
                                            if (client2.transformice) {
                                                continue;
                                            }
                                            client2.giveTransformice();
                                            this.room.allShamanSkills.replace("31",
                                                    (this.room.allShamanSkills.get("31") - 1));
                                            this.room.sendRemoveShamanSkillCode(36);
                                            break;
                                        }
                                    }
                                }
                            } else if (item == 37) {
                                if (this.room.allShamanSkills.containsKey("9")) {
                                    if (this.room.allShamanSkills.get("9") > 0) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.isShaman) {
                                                continue;
                                            }
                                            this.room.displayParticle(36, client2.x, client2.y);
                                            this.room.displayParticle(37, this.x, this.y);
                                            this.room.movePlayer(client2, this.x, this.y);
                                            this.room.sendRemoveShamanSkillCode(37);
                                            this.room.allShamanSkills.replace("9",
                                                    (this.room.allShamanSkills.get("9") - 1));
                                            break;
                                        }

                                    }
                                }
                            } else if (item == 38) {
                                if (this.room.allShamanSkills.containsKey("6")) {
                                    Room room = this.room;
                                    for (PlayerConnection client2 : room.clients.values()) {
                                        if (this.room.allShamanSkills.get("6") > 0) {
                                            if (client2.isDead) {
                                                if (!client2.isShaman) {
                                                    if (!client2.isComp && !room.arrComp.contains(client2.kullanici.isim) && !client2.isAfk) {
                                                        client2.byShaman = true;
                                                        room.respawn_player(client2);
                                                        client2.revived = true;
                                                        room.movePlayer(client2, x, y);
                                                        room.displayParticle(37, x, y);
                                                        room.sendRemoveShamanSkillCode(38);
                                                        room.allShamanSkills.replace("6",
                                                                (room.allShamanSkills.getOrDefault("6", 0) - 1));
                                                    }
                                                }
                                            }

                                        } else {
                                            break;

                                        }
                                    }

                                }//
                            } else if (item == 42) {
                                if (this.room.allShamanSkills.containsKey("13")) {
                                    this.room.addDefilante(x, y, 3);
                                }
                            } else if (item == 43) {
                                if (this.room.allShamanSkills.containsKey("10")) {
                                    this.room.addDefilante(x, y, 1);
                                }
                            } else if (item == 44) {
                                if (this.room.isSurvivor) return;
                                if (this.room.isKutier) return;
                                if (this.room.isAllShaman) return;
                                if (this.kullanici.samanTur == 1 && !this.sentTotem) {
                                    this.sentTotem = true;
                                    this.sendTotem(x, y);
                                    if (room.unique_playerCount() > 7) gorevIlerleme(13, 1);
                                }
                            } else if (item == 47) {
                                if (this.room.allShamanSkills.containsKey("5")) {
                                    if (!this.room.allShamanSkills.get("5").equals(0)) {
                                        if (this.room.numCompleted.get() > 1) {
                                            ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code,
                                                    x, y);
                                            for (PlayerConnection client2 : nearClients) {
                                                if (client2.isShaman) {
                                                    continue;
                                                }
                                                if (!client2.hasCheese) {
                                                    continue;
                                                }
                                                if (!this.room.isDoubleMap) {
                                                    client2.playerVictory(1);
                                                } else if (this != this.room.codes[0]) {
                                                    if (this == this.room.codes[1]) {
                                                        client2.playerVictory(2);
                                                    }
                                                } else {
                                                    client2.playerVictory(1);
                                                }
                                                try {
                                                    this.room.allShamanSkills.replace("5",
                                                            (this.room.allShamanSkills.get("5") - 1));
                                                } catch (Exception e) {

                                                }
                                                this.room.sendRemoveShamanSkillCode(47);
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else if (item == 55) {
                                if (this.room.allShamanSkills.containsKey("7")) {
                                    if (!this.room.allShamanSkills.get("7").equals(0)) {
                                        if (this.room.numCompleted.get() > 0) {
                                            if (this.hasCheese) {
                                                ArrayList<PlayerConnection> nearClients = this.room
                                                        .findNearMices(this.kullanici.code, x, y);
                                                for (PlayerConnection client2 : nearClients) {
                                                    if (client2.isShaman) {
                                                        continue;
                                                    }
                                                    if (client2.hasCheese) {
                                                        continue;
                                                    }
                                                    this.hasCheese = false;
                                                    client2.byShaman = true;

                                                    this.room.sendAll(MFServer.getBuf().writeByte(8).writeByte(19)
                                                            .writeInt(this.kullanici.code));
                                                    client2.giveCheese();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else if (item == 56) {
                                if (this.room.allShamanSkills.containsKey("29")) {
                                    if (!this.room.allShamanSkills.get("29").equals(0)) {
                                        this.room.displayParticle(36, this.x, this.y);
                                        this.room.displayParticle(37, x, y);
                                        this.room.movePlayer(this, x, y);
                                        this.room.allShamanSkills.replace("29", (this.room.allShamanSkills.getOrDefault("29", 1) - 1));
                                        this.room.sendRemoveShamanSkillCode(56);
                                    }
                                }
                            } else if (item == 57) {
                                if (this.room.cloudId == -1) {
                                    this.room.cloudId = id;
                                } else {
                                    this.room.removeObject(this.room.cloudId);
                                    this.room.cloudId = -1;
                                }
                            } else if (item == 70) {
                                if (this.room.allShamanSkills.containsKey("88")) {
                                    this.room.sendAll(
                                            MFServer.getBuf().writeByte(5).writeByte(36).writeShort(x).writeShort(y));
                                    // this.room.sendRemoveShamanSkillCode(88);

                                }
                            } else if (item == 71) {
                                if (this.room.allShamanSkills.containsKey("69")) {
                                    if (!this.room.allShamanSkills.get("69").equals(0)) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.isShaman) {
                                                continue;
                                            }
                                            this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(30)
                                                    .writeInt(client2.kullanici.code));
                                            this.room.sendRemoveShamanSkillCode(71);
                                            this.room.allShamanSkills.replace("69",
                                                    (this.room.allShamanSkills.get("69") - 1));
                                            break;
                                        }
                                    }
                                }
                            } else if (item == 73) {
                                if (this.room.allShamanSkills.containsKey("71")) {
                                    if (!this.room.allShamanSkills.get("71").equals(0)) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.isShaman) {
                                                continue;
                                            }

                                            this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(31)
                                                    .writeInt(client2.kullanici.code).writeShort(70).writeByte(1));
                                            this.room.sendRemoveShamanSkillCode(73);
                                            this.room.allShamanSkills.replace("71",
                                                    (this.room.allShamanSkills.get("71") - 1));
                                            break;
                                        }
                                    }
                                }
                            } else if (item == 74) {
                                if (this.room.allShamanSkills.containsKey("80")) {
                                    if (!this.room.allShamanSkills.get("80").equals(0)) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.isShaman) {
                                                continue;
                                            }
                                            this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(33)
                                                    .writeBoolean(true).writeInt(client2.kullanici.code));
                                            this.room.sendRemoveShamanSkillCode(74);
                                            this.room.allShamanSkills.replace("80",
                                                    (this.room.allShamanSkills.get("80") - 1));
                                            break;
                                        }
                                    }
                                }
                            } else if (item == 75) {
                                if (this.room.allShamanSkills.containsKey("73")) {
                                    this.room.removeAllObjects();
                                }
                            } else if (item == 76) {
                                if (this.room.allShamanSkills.containsKey("83")) {
                                    this.room.addBoost(x, y, 5, rotation);
                                }
                            } else if (item == 77) {
                                for (PlayerConnection pc : this.room.findNearMices(this.kullanici.code, x, y)) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(35).writeInt(pc.kullanici.code));
                                }
                            } else if (item == 79) {
                                if (this.room.allShamanSkills.containsKey("82")) {
                                    for (PlayerConnection client3 : this.room.clients.values()) {
                                        if (client3.isDead) {
                                            continue;
                                        }
                                        if (client3.isShaman) {
                                            continue;
                                        }
                                        this.room.sendStopPlayer(client3.kullanici.code, true);
                                        MFServer.executor.schedule(() -> {
                                            if (!alive())
                                                return;
                                            if (this.room.Round == round) {
                                                try {
                                                    this.room.sendStopPlayer(client3.kullanici.code, false);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, (long) (this.room.allShamanSkills.get("82") * 2), TimeUnit.SECONDS);
                                    }
                                    this.room.sendRemoveShamanSkillCode(79);
                                }
                            } else if (item == 81) {
                                if (this.room.allShamanSkills.containsKey("63") && !this.room.isSurvivor) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(28)
                                            .writeShort(this.room.allShamanSkills.get("63") * 2).writeInt(0));
                                }
                            } else if (item == 83) {
                                if (this.room.allShamanSkills.containsKey("66")) {
                                    if (!this.room.allShamanSkills.get("66").equals(0)) {
                                        ArrayList<PlayerConnection> nearClients = this.room.findNearMices(this.kullanici.code, x,
                                                y);
                                        for (PlayerConnection client2 : nearClients) {
                                            if (client2.canMeep) {
                                                continue;
                                            }
                                            if (client2.isShaman) {
                                                continue;
                                            }
                                            client2.giveMeep();
                                            this.room.sendRemoveShamanSkillCode(83);
                                            this.room.allShamanSkills.replace("66",
                                                    (this.room.allShamanSkills.get("66") - 1));
                                            break;
                                        }
                                    }
                                }
                            } else if (item == 84) {
                                if (this.room.allShamanSkills.containsKey("74")) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(37).writeInt(this.kullanici.code)
                                            .writeShort(x).writeShort(y));
                                }
                            } else if (item == 86) {
                                if (this.room.allShamanSkills.containsKey("86")) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(45).writeShort(x)
                                            .writeShort(y).writeByte(this.room.allShamanSkills.get(String.valueOf(item)) * 4));
                                }
                            } else if (item == 92) {
                                Beceriler.Yolla(this, true);
                            } /*
                             * else if (item == 93) {
                             * this.room.sendAll(MFServer.getBuf().writeByte
                             * (5).writeByte(42));
                             * Beceriler.Yolla(this,true);
                             *
                             * }
                             */ else if (item == 94) {
                                if (this.room.allShamanSkills.containsKey("94")) {
                                    this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(43).writeInt(this.kullanici.code)
                                            .writeByte(1));
                                }
                            }
                        }
                        if (round == this.room.Round) {
                            ByteBufOutputStream p = MFServer.getStream();
                            p.writeByte(5);
                            p.writeByte(20);
                            p.writeInt(id);
                            p.writeShort(item);
                            p.writeShort(x);
                            p.writeShort(y);
                            p.writeShort(rotation);
                            p.writeByte(vx);
                            p.writeByte(vy);
                            p.writeBoolean(ghost);
                            if (sc != null) {
                                p.writeByte(sc.size());
                                for (Number color : (ArrayList<Number>) sc) {
                                    p.writeInt(color.intValue());
                                }
                            } else {
                                p.writeByte(0);
                            }
                            this.room.sendAllOthers(p.buffer(), this.kullanici.code);
                        }
                    }
                } else if (Token2 == 25) {
                    //       System.out.println(buf.readableBytes());
                    this.defilante += 1;
                } else if (Token2 == 16) {
                    if (this.isShaman && !isDead)
                        this.room.sendAllOthers(MFServer.getBuf().writeByte(5).writeByte(16).writeBytes(buf), this.kullanici.code);

                } else if (Token2 == 14) {
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(13).writeBytes(buf).writeByte(0));

                } else if (Token2 == 15) {
                    if ((this.isShaman) && !isDead) {

                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(15).writeBytes(buf));
                    }

                } else if (Token2 == 26) {
                    ByteBuf bf2 = buf.copy();
                    int uk = bf2.readShort();
                    int uk1 = bf2.readShort();
                    int uk2 = bf2.readShort();
                    int uk3 = bf2.readShort();
                    MFServer.bitti(bf2);
                    if (uk1 == 4) return;
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(26).writeBytes(buf));

                } else if (Token2 == 27) {
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(27).writeBytes(buf));

                } else if (Token2 == 28) {
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(28).writeShort(0).writeBytes(buf));

                } else if (Token2 == 29) {
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(29).writeBytes(buf).writeShort(0));

                } else if (Token2 == 9) {
                    if (this.isShaman && !isDead)
                        this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(9).writeBytes(buf.slice()));
                } else if (Token2 == 32) {
                    int playerCode = buf.readInt();
                    int x = buf.readShort();
                    int y = buf.readShort();
                    if (this.IceCount++ < 3 && this.room.Saves[0] >= 1) {
                        if (this.isShaman && playerCode != kullanici.code && !isDead) {
                            PlayerConnection pc = this.room.clients_p.get(playerCode);
                            if (pc != null) {
                                pc.isDead = true;
                                pc.score++;
                                pc.room.sendDeath(pc);
                                this.room.addShamanObject(54, x, y);
                            }
                        }
                    }
                } else if (Token2 == 35) {
                    if (this.isShaman && !isDead) {
                        int byt = buf.readByte();
                        int ID = buf.readInt();
                        if (this.room.lastHandyMouseID == null) {
                            this.room.lastHandyMouseID = ID;
                            this.room.lastHandyMouseByte = byt;
                        } else {
                            this.sendPacket(MFServer.getBuf().writeByte(5).writeByte(35).writeByte(byt).writeInt(ID)
                                    .writeByte(this.room.lastHandyMouseByte).writeInt(this.room.lastHandyMouseID));
                            this.sendPacket(MFServer.getBuf().writeByte(5).writeByte(40).writeByte(77).writeByte(1));
                            this.room.sendRemoveShamanSkillCode(77);
                            this.room.lastHandyMouseID = null;
                            this.room.lastHandyMouseByte = null;
                        }
                    }

                } else if (Token2 == 38) {
                    int lang = bis.readByte();
                    String name = bis.readUTF();

                    if (name.length() == 0) {
                        name = "1";
                    }
                    if (name.contains(Room.char_3)) {
                        return;
                    }
                    if (lang == 0) {
                        if (Room.Official.matcher(name).matches()) {
                            this.enterRoom(name, true);
                            return;
                        }
                    }
                    String orj = name;
                    if (!name.startsWith("*")) {
                        name = this.Lang + "-" + name;
                    }
                    Room r = this.server.rooms.get(name);
                    if (r != null) {
                        if (!r.password.isEmpty()) {
                            ByteBufOutputStream out = this.getStream();
                            out.writeByte(5);
                            out.writeByte(39);

                            out.writeUTF(orj);
                            this.sendPacket(out);
                            return;
                        }
                    } else {
                        r= new Room(server,name);
                        if (!r.password.isEmpty() && r.outname.startsWith("#amongus") && !r.isofficial) {
                            ByteBufOutputStream out = this.getStream();
                            out.writeByte(5);
                            out.writeByte(39);

                            out.writeUTF(orj);
                            this.sendPacket(out);
                            return;
                        }

                    }
                    this.enterRoom(orj, false);
                } else if (Token2 == 39) {
                    String pw = bis.readUTF();
                    String name = bis.readUTF();
                    String x_name = name;
                    if (!name.startsWith("*"))
                        x_name = this.Lang + "-" + name;
                    Room room = this.server.rooms.get(x_name);
                    if (room != null) {
                        String r_pw = room.password;
                        if (r_pw.equals(pw) || r_pw.isEmpty())
                            this.enterRoom(name, false);
                        else {
                            ByteBufOutputStream out = this.getStream();
                            out.writeByte(5);
                            out.writeByte(34);
                            out.writeUTF(name);
                            this.sendPacket(out);
                        }
                    }
                } else if (Token2 == 61) {
                    if (this.isShaman && !isDead)
                        this.room.sendAllOthers(MFServer.getBuf().writeByte(5).writeByte(16).writeBytes(buf),
                                this.kullanici.code);

                } else if (Token2 == 71) {

                } else if (Token2 == 73) {
                    ByteBufOutputStream bs = MFServer.getStream();
                    bs.writeByte(5);
                    bs.writeByte(73);
                    bs.writeShort(this.room.musics.size());
                    for (Room.MusicVideo musicVideo : this.room.musics) {
                        bs.writeUTF(musicVideo.title);
                        bs.writeUTF(musicVideo.kullanici);
                    }
                    this.sendPacket(bs.buffer());
                } else if (Token2 == 70) {
                    if (this.room.isMusic) {
                        String url = bis.readUTF();
                        Matcher matcher = MFServer.patternYoutube.matcher(url);
                        if (matcher.find()) {
                            String id = matcher.group();
                            MFServer.executor.execute(() -> {
                                try {
                                    String newurl = "https://www.googleapis.com/youtube/v3/videos?id=" + id + "&key=AIzaSyBaasC_VPn1f4fycdmLuCrGZuq2WTVCLyI&part=contentDetails,snippet";
                                    final HttpParams httpParams = new BasicHttpParams();
                                    HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
                                    DefaultHttpClient client = new DefaultHttpClient(httpParams);
                                    HttpUriRequest request = new HttpGet(newurl);
                                    HttpResponse response = client.execute(request);

                                    String body = CharStreams.toString(new InputStreamReader(
                                            response.getEntity().getContent(), Charsets.UTF_8));
                                    JSONObject obj = new JSONObject(body);
                                    JSONArray jsonArray = obj.getJSONArray("items");
                                    if (jsonArray.length() == 0) {

                                        try {
                                            this.sendMessage("$ModeMusic_ErreurVideo");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        return;
                                    }
                                    String title = jsonArray.getJSONObject(0).getJSONObject("snippet").getString("title");
                                    String d = jsonArray.getJSONObject(0).getJSONObject("contentDetails").getString("duration");
                                    Duration duration = Duration.parse(d);
                                    int sec = (int) duration.getSeconds();
                                    if (sec > 300) sec = 300;
                                    for (Iterator<Room.MusicVideo> iterator = this.room.musics.iterator(); iterator.hasNext(); ) {
                                        Room.MusicVideo music = iterator.next();
                                        if (music.vid.equals(id)) {
                                            try {
                                                this.sendMessage("$DejaPlaylist");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            return;
                                        }
                                        if (music.kullanici.equals(this.kullanici.isim)) {

                                            iterator.remove();
                                            break;
                                        }
                                    }

                                    this.room.musics.add(new Room.MusicVideo(this.kullanici.isim, id, sec, title));

                                    try {
                                        this.sendMessage("$ModeMusic_AjoutVideo", "<PT>" + this.room.musics.size());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    if (this.room.musics.size() == 1 && this.room.currentVideo == null) {
                                        this.room.playNext();
                                    }
                                } catch (Exception e) {
                                    // e.printStackTrace();
                                }
                            });

                        } else {
                            this.sendMessage("$ModeMusic_ErreurVideo");
                        }
                    }
                } else {
                    found = false;
                }
            } else if (Token1 == 8) {
                if (Token2 == 2) {
                    LangByte = bis.readByte();
                    this.Lang = this.server.langs.getOrDefault(LangByte, "E2");
                } else if (Token2 == 1) {
                    ByteBuf Okunacak = buf.slice();
                    int Op = Okunacak.readByte();
                    String dil = "";
                    String hedef = "";
                    if (this.isShaman && this.room.ShamanSkills) {
                        if (Op == 0 && room.allShamanSkills != null) {
                            Integer sayi = room.allShamanSkills.get("3");
                            if (sayi != null) {
                                for (PlayerConnection pc : this.room.findNearMices(this.kullanici.code, this.x, this.y)) {
                                    if (sayi-- < 0)
                                        break;
                                    this.playEmote(pc.kullanici.code, 0, false);
                                }
                            }
                        } else if (Op == 4 && room.allShamanSkills != null) {
                            Integer sayi = room.allShamanSkills.get("61");
                            if (sayi != null) {
                                for (PlayerConnection pc : this.room.findNearMices(this.kullanici.code, this.x, this.y)) {
                                    if (sayi-- < 0)
                                        break;
                                    this.playEmote(pc.kullanici.code, 2, false);
                                }
                            }
                        } else if (Op == 8 && room.allShamanSkills != null) {
                            Integer sayi = room.allShamanSkills.get("25");
                            if (sayi != null) {
                                for (PlayerConnection pc : this.room.findNearMices(this.kullanici.code, this.x, this.y)) {
                                    if (sayi-- < 0)
                                        break;
                                    this.playEmote(pc.kullanici.code, 3, false);
                                }
                            }
                        }
                    }
                    if (Op == 10) {
                        bis.readInt();
                        bis.readByte();
                        dil = bis.readUTF();
                        this.playEmote(this.kullanici.code, Op, true, dil);
                    } else if (Op == 14 || Op == 18 || Op == 22) {
                        int hedefId = Okunacak.readInt();
                        ByteBuf bf = MFServer.getBuf();
                        bf.writeByte(8).writeByte(1);
                        bf.writeInt(hedefId);
                        bf.writeByte(Op);
                        bf.writeInt(0);
                        bf.writeInt(0);
                        this.room.sendAll(bf);
                        PlayerConnection hedefPlayer = this.room.clients_p.get(hedefId);
                        if (hedefPlayer != null) {
                            hedef = hedefPlayer.kullanici.isim;

                            if (this.room.l_room != null) {
                                this.room.l_room.eventEmotePlayed(this.kullanici.isim, Op + 1, dil, hedef);
                                this.room.l_room.eventEmotePlayed(hedef, Op + 1, dil, hedef);
                            }
                        }
                        this.playEmote(kullanici.code, Op + 1, false);
                        this.playEmote(hedefId, Op + 1, false);
                    } else if (Op == 26) {

                        ByteBuf bf = MFServer.getBuf();
                        bf.writeByte(8).writeByte(1);
                        bf.writeInt(this.kullanici.code);
                        bf.writeByte(Op);
                        bf.writeInt(0);
                        bf.writeInt(0);
                        this.room.sendAll(bf);
                        int e2 = Okunacak.readInt();
                        ByteBuf bf2 = MFServer.getBuf();
                        bf2.writeByte(8).writeByte(1);
                        bf2.writeInt(e2);
                        bf2.writeByte(Op);
                        bf2.writeInt(0);
                        bf2.writeInt(0);
                        this.room.sendAll(bf2);

                        PlayerConnection hedefPlayer = this.room.clients_p.get(e2);
                        if (hedefPlayer != null) {
                            hedef = hedefPlayer.kullanici.isim;
                        }

                        if (this.room.l_room != null) {
                            this.room.l_room.eventEmotePlayed(this.kullanici.isim, Op, dil, this.kullanici.isim);
                        }
                        ByteBuf bf3 = MFServer.getBuf();
                        bf3.writeByte(100).writeByte(5);
                        bf3.writeInt(this.kullanici.code);
                        this.server.random.setSeed(System.currentTimeMillis());
                        bf3.writeByte(this.server.random.nextInt(3));
                        bf3.writeInt(e2);
                        this.server.random.setSeed(System.currentTimeMillis() + 50);
                        bf3.writeByte(this.server.random.nextInt(3));
                        this.room.sendAll(bf3);
                    } else {
                        this.playEmote(this.kullanici.code, Op, true);
                    }

                    if (this.room.l_room != null) {
                        this.room.l_room.eventEmotePlayed(this.kullanici.isim, Op, dil, hedef);
                    }
                    if (Op == 3 && this.room.is801Room) {
                        int x = 1949;
                        int y = 779;

                        if (x - 50 <= this.x && this.x <= x + 50 && this.y >= y - 50 && this.y <= y + 50
                                && !kullanici.evInfo.titles.containsKey("3061")) {
                            this.add_event_title(3061, "niyazi");
                        }
                    }

                } else if (Token2 == 5) {
                    this.isAfk = false;
                    ByteBuf out = MFServer.getBuf();
                    out.writeByte(8).writeByte(5);
                    out.writeInt(this.kullanici.code);
                    out.writeByte(buf.readByte());
                    this.room.sendAll(out);
                } else if (Token2 == 15) {
                    boolean flying = bis.readBoolean();
                    this.room.sendAll(
                            MFServer.getBuf().writeByte(8).writeByte(15).writeInt(this.kullanici.code).writeBoolean(flying));
                    if (this.room.l_room != null) {
                        this.room.l_room.eventFlyingChange(kullanici.isim, flying);
                    }
                } else if (Token2 == 20) {
                    this.sendShopList();
                } else if (Token2 == 21) {
                    int skillCode = bis.readByte();
                    String sik = String.valueOf(skillCode);
                    //  System.out.println(skillCode);
                    if (skillCode >= 0) {
                        if (skillCode <= 100) {

                            Integer level = kullanici.seviye.seviye;
                            for (Integer d3 : this.kullanici.beceriler.values()) {
                                level -= d3;
                            }
                            if (level > 0) {
                                Boolean set = false;
                                for (String sil : this.kullanici.beceriler.keySet()) {
                                    Integer skill = Integer.valueOf(sil);
                                    if (!skill.equals(skillCode)) {
                                        continue;
                                    }
                                    if (this.kullanici.beceriler.get(sil) > 5) {
                                        continue;
                                    }
                                    this.kullanici.beceriler.replace(sil, this.kullanici.beceriler.get(sil) + 1);
                                    set = true;
                                }
                                if (!set) {
                                    this.kullanici.beceriler.put(sik, 1);
                                }
                                this.sendBeceriler(true);
                            }
                        }
                    }

                } else if (Token2 == 22) {
                    this.kullanici.beceriler.clear();
                    this.sendBeceriler(true);
                } else if (Token2 == 30) {
                    return;
                } else if (Token2 == 66) {
                    if (!this.isVampire) {
                        if (this.room.isKutier || this.room.isSurvivor) {
                            if (this.room.Map.perm != 11)
                                return;
                        } else if (!(this.room.isFuncorp || this.room.l_room != null)) return;
                        this.room.setVampirePlayer(kullanici.isim);
                    }
                } else if (Token2 == 39) {
                    if (this.canMeep || ((this.room.isSurvivor || this.room.isKutier) && this.isShaman)) {
                        ByteBuf bouf = MFServer.getBuf();
                        bouf.writeByte(8);
                        bouf.writeByte(38);
                        bouf.writeInt(kullanici.code);
                        bouf.writeShort(buf.readShort());
                        bouf.writeShort(buf.readShort());
                        // System.out.println(buf.readableBytes());
                        if (this.isShaman)
                            bouf.writeInt(20);
                        else
                            bouf.writeInt(5);
                        this.room.sendAll(bouf);
                    }

                } else if (Token2 == 25) {
                    if (this.kullanici.yetki < 1)
                        return;
                    int t = MFServer.timestamp();
                    if (this.T_report + 60 > t)
                        return;
                    this.T_report = t;
                    String ad = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));
                    int typ = bis.readByte();
                    String sebep = bis.readUTF();
                    PlayerConnection pc = this.server.clients.get(ad);
                    if (pc == null) {
                        this.sendMessage("<R>Offline");
                    } else {
                        Raporlar raporlar = this.server.RaporSistemi.get(ad);
                        if (raporlar == null) {
                            raporlar = new Raporlar();
                            raporlar.surev = t;
                            raporlar.ad = ad;
                            raporlar.oda = pc.room.name.trim();
                            //MFServer.executor.execute(new RunnableRaporlar(raporlar, pc.ip));
                        }
                        for (Rapor rapor : raporlar.arr) {
                            if (rapor.raporlayan.equals(this.kullanici.isim)) return;
                        }
                        raporlar.dil = pc.room.lang;
                        Rapor rapor = new Rapor();
                        rapor.raporlayan = this.kullanici.isim;
                        rapor.sebep = sebep;
                        rapor.typ = typ;
                        rapor.karma = this.karma;
                        raporlar.arr.add(rapor);
                        raporlar.silen = "";
                        this.server.RaporSistemi.put(ad, raporlar);
                        this.sendPacket(MFServer.pack_old(26, 9, new Object[]{"0"}));

                        ByteBuf data = packChatServer("<VP>" + pc.Lang + "</VP><font color=\"#ff00ff\"> " + this.kullanici.isim + "(" + this.karma + ") reported " + ad);
                        for (PlayerConnection client : this.server.clients.values()) {
                            if (client.kullanici.yetki >= 4 && client.kullanici.showReports) {
                                ArrayList<String> arr = this.server.modopwetSetting.get(client.kullanici.isim);
                                if (arr != null) {
                                    for (String prefferedLang : arr) {
                                        if (prefferedLang.equals(client.Lang)) client.sendPacket(data.retainedSlice());
                                    }

                                } else {
                                    client.sendPacket(data.retainedSlice());
                                }
                            }
                        }
                        data.release();

                    }

                } else if (Token2 == 70) {
                    PlayerConnection pc = this.server.clients.get(MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF())));
                    if (pc != null) {
                        ByteBufOutputStream st = this.getStream();
                        String l = pc.kullanici.tip;
                        if (pc.kullanici.tip.split(";")[0].equals("1")) l += ";" + pc.kullanici.kurkRenk;

                        st.writeByte(8);
                        st.writeByte(70);
                        st.writeUTF(pc.kullanici.isim);
                        st.writeUTF(l);
                        st.writeInt(pc.toplamPuan);
                        st.writeShort(pc.kullanici.unvanlar.size());
                        st.writeShort(pc.kullanici.rozetler.size());
                        st.writeShort(MFServer.etkinlikler.size());//LEN EVENT
                        for (Etkinlik etkinlik : Lists.reverse(MFServer.etkinlikler)) {
                            int e_point = 0;
                            st.writeShort(13);
                            st.writeByte(1);
                            st.writeShort(etkinlik.id);
                            st.writeInt(etkinlik.tarih);
                            st.writeShort(pc.etkinlikPuan.getOrDefault(etkinlik, 0));
                            st.writeBoolean(false);
                            st.writeByte(etkinlik.rozetler.size());
                            etkinlik.rozetler.forEach((k, v) -> {
                                try {
                                    st.writeByte(2);
                                    st.writeBoolean(false);
                                    st.writeShort(k);
                                    st.writeShort(v);
                                    st.writeBoolean(pc.kullanici.rozetler.contains(k));
                                    st.writeByte(0);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                            st.writeByte(etkinlik.kalintilar.size());
                            etkinlik.kalintilar.forEach((k) -> {
                                Nesne nesne = pc.kullanici.esyalar.get(String.valueOf(k));
                                int v;
                                if (nesne == null) {
                                    v = 0;
                                } else {
                                    v = nesne.sayi;
                                }
                                try {
                                    st.writeByte(1);
                                    st.writeBoolean(false);
                                    st.writeShort(k);
                                    st.writeShort(v);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                        this.sendPacket(st.buffer());
                    }
                } else {
                    found = false;
                }
            } else if (Token1 == 20) {
                if (Token2 == 15) {
                    ByteBufOutputStream st = this.getStream();
                    st.writeByte(20);
                    st.writeByte(15);
                    st.writeInt(kullanici.marketPey);
                    st.writeInt(kullanici.marketCilek);
                    st.writeShort(2);
                    this.sendPacket(st);

                } else if (Token2 == 18) { // Use
                    buf.readShort();
                    int item = buf.readShort() & 0xffff;
                    int[] xx = MFServer.get_cat_by_id(item);
                    item = MFServer.get_id_by_cat(xx[0], xx[1]);
                    if (this.kullanici.market.containsKey(String.valueOf(item))) {

                        int cat;
                        int id;
                        int[] x = MFServer.get_cat_by_id(item);
                        cat = x[1];
                        id = x[0];
                        item = MFServer.get_id_by_cat(id, cat);
                        String nEquip = id + this.getItemCustomization(item);

                        String[] lookSplited = StringUtils.split(this.kullanici.tip, ";");
                        String lookTopList = lookSplited[0];
                        String[] lookList = new String[11];
                        String[] LP = StringUtils.split(lookSplited[1], ",");
                        for (int i = 0; i < 11; i++) {
                            if (i < LP.length) {
                                lookList[i] = LP[i];
                            } else {
                                lookList[i] = "0";
                            }
                        }

                        if (cat <= 10) {

                            if (lookList[cat].split("_")[0].equals(String.valueOf(id))) {
                                lookList[cat] = "0";
                            } else {
                                lookList[cat] = nEquip;
                            }
                        } else if (cat == 21) {
                            lookTopList = "1";
                            String newcolor = "78583a";
                            if (id == 0 && !this.kullanici.kurkRenk.equals("bd9067")) {
                                newcolor = "bd9067";
                            } else if (id == 1 && !this.kullanici.kurkRenk.equals("593618")) {
                                newcolor = "593618";
                            } else if (id == 2 && !this.kullanici.kurkRenk.equals("8c887f")) {
                                newcolor = "8c887f";
                            } else if (id == 3 && !this.kullanici.kurkRenk.equals("dfd8ce")) {
                                newcolor = "dfd8ce";
                            } else if (id == 4 && !this.kullanici.kurkRenk.equals("4e443a")) {
                                newcolor = "4e443a";
                            } else if (id == 5 && !this.kullanici.kurkRenk.equals("e3c07e")) {
                                newcolor = "e3c07e";
                            } else if (id == 6 && !this.kullanici.kurkRenk.equals("272220")) {
                                newcolor = "272220";
                            }
                            this.kullanici.kurkRenk = newcolor;

                        } else if (cat == 22) {
                            if (lookTopList.equals(String.valueOf(id))) {
                                lookTopList = "1";
                            } else {
                                lookTopList = nEquip;
                            }
                        }

                        String tempLookList = StringUtils.join(lookList, ",");
                        this.kullanici.tip = lookTopList + ";" + tempLookList;
                        fix_tip();
                        this.sendLookChange();
                        //  this.sendShopList();
                    }
                } else if (Token2 == 19) { // Buy

                    int item2 = buf.readShort();
                    int item = buf.readShort();
                    boolean withfra = buf.readBoolean();
                    int item3 = buf.readShort();
                    item = item & 0xffff;
                   // System.out.println(item);
                    int orjitem = item;
                    int cate;
                    int id;
                    int[] x = MFServer.get_cat_by_id(item);
                    cate = x[1];
                    id = x[0];
                 //   System.out.println(cate);
               //     System.out.println(id);
                    item = MFServer.get_id_by_cat(id, cate);
                    int cat = cate;
                    int[] it_data = this.server.ShopList.get(item);
                    boolean ozel = kullanici.ozelKurkler.contains(id);
                    if (it_data != null || ozel) {
                        if (ozel) it_data = new int[]{1, 1, 1};
                        int cash = it_data[1];
                        int my_money = kullanici.marketPey;
                        if (withfra) {
                            cash = it_data[2];
                            my_money = kullanici.marketCilek;
                        }
                        if (cash < my_money) {
                            if (withfra) {
                                kullanici.marketCilek -= cash;
                            } else {
                                kullanici.marketPey -= cash;
                            }
                            this.kullanici.market.put(String.valueOf(item), new MarketEsya(cat));
                            if (cat == 22) {
                                try {
                                    this.checkBadges();
                                } catch (Exception e) {

                                }

                                if (id == 207) {
                                    MFServer.logExecutor.execute(() -> MFServer.getSeq("minister"));
                                }
                                if (id == 213) {
                                    add_event_title(3525, "stpatrick");
                                }
                            }
                            this.room.sendAll(new AnimZelda(this, orjitem).data());
                            // this.room.sendAll(new
                            // ItemBought(this,packet.item).data());
                            this.sendPacket(MFServer.getBuf().writeByte(20).writeByte(2).writeShort(item).writeByte(1));
                            this.sendShopList();
                        }
                    }
                } else if (Token2 == 6) { // Use Outfit
                    int index = buf.readByte();
                    if (index < kullanici.tipler.size()) {
                        String clothe = this.kullanici.tipler.get(index);
                        String[] sl = clothe.split(";");
                        this.kullanici.tip = sl[0] + ";" + sl[1];
                        fix_tip();
                        if (sl.length >= 3)
                            this.kullanici.kurkRenk = sl[2];
                        if (sl.length == 4)
                            this.kullanici.samanRenk = sl[3];
                        this.sendLookChange();
                        this.sendShopList();
                    }
                } else if (Token2 == 7) { // Save Outfit
                    int index = buf.readByte();
                    if (this.kullanici.tipler.size() > index) {
                        Object[] data = {this.kullanici.tip, this.kullanici.kurkRenk, this.kullanici.samanRenk};
                        this.kullanici.tipler.set(index, StringUtils.join(data, ";"));
                        this.sendShopList();
                    }
                } else if (Token2 == 22) { // Buy Outfit
                    int index = buf.readByte();
                    boolean withfra = buf.readBoolean();
                    if (kullanici.tipler.size() >= 65 || index >= 65) {
                        return;
                    }
                    int cash_c = 50;
                    int cash_f = 5;
                    if (index == 2) {
                        cash_c = 1000;
                        cash_f = 50;
                    } else {
                        cash_c = 4000;
                        cash_f = 100;
                    }
                    int cash = cash_c;
                    int my_money = kullanici.marketPey;
                    if (withfra) {
                        cash = cash_f;
                        my_money = kullanici.marketCilek;
                    }
                    if (cash < my_money) {
                        if (withfra) {
                            kullanici.marketCilek -= cash;
                        } else {
                            kullanici.marketPey -= cash;
                        }
                        Object[] data = {this.kullanici.tip, this.kullanici.kurkRenk, this.kullanici.samanRenk};
                        this.kullanici.tipler.add(StringUtils.join(data, ";"));
                        this.sendShopList();
                    }
                } else if (Token2 == 20) { // Buy Customize
                    buf.readShort();
                    int item = buf.readShort();
                    boolean withfra = buf.readBoolean();
                    int cat;
                    int id;
                    int[] x = MFServer.get_cat_by_id(item);
                    cat = x[1];
                    id = x[0];

                    int[] it_data = this.server.ShopList.get(MFServer.get_id_by_cat(id, cat));
                    if (it_data != null && it_data[0] != 0) {
                        int cash = 2000;
                        int cus = it_data[1];
                        if (cus > 0) {
                            int my_money = kullanici.marketPey;
                            if (withfra) {
                                cash = 20;
                                my_money = kullanici.marketCilek;
                            }
                            if (cash < my_money) {
                                if (withfra) {
                                    kullanici.marketCilek -= cash;
                                } else {
                                    kullanici.marketPey -= cash;
                                }
                                String sit = String.valueOf(item);
                                MarketEsya val = this.kullanici.market.get(sit);
                                if (val != null) {
                                    val.kisisellestirme = true;
                                    this.kullanici.market.put(sit, val);
                                } else {
                                    sendMessage("<R>First buy the item.");
                                }
                                // this.room.sendAll(new AnimZelda(this, cat,
                                // id, 0).data());
                                // this.room.sendAll(new
                                // ItemBought(this,packet.item).data());
                                this.sendShopList();
                            }
                        }
                    }
                } else if (Token2 == 21) { // Customize
                    buf.readShort();
                    int item = buf.readShort();
                    byte size = buf.readByte();
                    ArrayList<Integer> colors = new ArrayList<>();
                    for (int i = 0; size > i; i++) {
                        colors.add(buf.readInt());
                    }
                    String sit = String.valueOf(item);
                    MarketEsya val = this.kullanici.market.get(sit);
                    if (val != null) {


                        if (val.kisisellestirme) {
                            int cat;
                            int id;
                            int[] x = MFServer.get_cat_by_id(item);
                            cat = x[1];
                            id = x[0];

                            String[] lookSplited = StringUtils.split(this.kullanici.tip, ";");
                            String lookTopList = lookSplited[0];
                            String[] lookList = new String[11];
                            String[] LP = StringUtils.split(lookSplited[1], ",");
                            for (int i = 0; i < 11; i++) {
                                if (i < LP.length) {
                                    lookList[i] = LP[i];
                                } else {
                                    lookList[i] = "";
                                }
                            }
                            val.renkler = colors;
                            this.kullanici.market.put(sit, val);

                            String nEquip = id + this.getItemCustomization(item);
                            String look = lookList[cat];
                            String dress;
                            if (look.contains("_")) {
                                String[] itemSplited = StringUtils.split(look, "_");
                                dress = itemSplited[0];

                            } else {
                                dress = look;
                            }
                            if (dress.isEmpty()) dress = "0";

                            if (Integer.valueOf(dress).equals(id)) {
                                lookList[cat] = nEquip;
                            }

                            String tempLookList = StringUtils.join(lookList, ",");
                            this.kullanici.tip = lookTopList + ";" + tempLookList;
                            fix_tip();
                            this.sendLookChange();
                            //  this.sendShopList();
                        }
                    }

                } else if (Token2 == 23) { // Buy Shaman item
                    int item = buf.readShort();
                    boolean withfra = buf.readBoolean();
                    int[] it_data = MFServer.ShamanShopList.get(item);
                    if (it_data != null) {
                        int cash = it_data[3];
                        int my_money = kullanici.marketPey;
                        if (withfra) {
                            cash = it_data[4];
                            my_money = kullanici.marketCilek;
                        }

                        if (cash < my_money) {
                            if (withfra) {
                                kullanici.marketCilek -= cash;
                            } else {
                                kullanici.marketPey -= cash;
                            }
                            this.kullanici.marketSaman.put(String.valueOf(item), new MarketEsya());
                            this.sendShopList();
                        }
                    }

                } else if (Token2 == 24) { // Use shaman
                    buf.readShort();
                    int item = buf.readShort();
                    String sit = String.valueOf(item);
                    MarketEsya al = this.kullanici.marketSaman.get(sit);
                    if (al != null) {
                        al.kullan = !al.kullan;
                        this.kullanici.marketSaman.put(sit, al);
                        this.sendShopList();
                    }
                } else if (Token2 == 25) { // Buy shaman customize

                    int item = buf.readShort();
                    boolean withfra = buf.readBoolean();
                    String sit = String.valueOf(item);

                    MarketEsya val = this.kullanici.marketSaman.get(sit);
                    int cash = 4000;
                    int my_money = kullanici.marketPey;
                    if (withfra) {
                        cash = 150;
                        my_money = kullanici.marketCilek;
                    }

                    if (cash < my_money) {
                        if (withfra) {
                            kullanici.marketCilek -= cash;
                        } else {
                            kullanici.marketPey -= cash;
                        }
                        val.kisisellestirme = true;
                        this.kullanici.marketSaman.put(sit, val);

                        this.sendShopList();
                    }
                } else if (Token2 == 26) { // shaman customize
                    int item = buf.readShort();
                    String sit = String.valueOf(item);
                    byte size = buf.readByte();
                    ArrayList<Integer> colors = new ArrayList<>();
                    for (int i = 0; size > i; i++) {
                        colors.add(buf.readInt());
                    }
                    MarketEsya val = this.kullanici.marketSaman.get(sit);
                    if (val.kisisellestirme) {
                        val.renkler = colors;
                        this.kullanici.marketSaman.put(sit, val);

                        this.sendShopList();
                    }

                } else if (Token2 == 27) {
                    //this.kullanici.market.clear();

                    int item = buf.readShort();
                    if (MFServer.fashion.containsKey(item) && this.kullanici.marketCilek >= this.promMoney) {
                        String look = MFServer.fashion.get(item)[1];
                        String[] items = look.split(";");
                        if (!items[0].equals("1"))
                            this.kullanici.market.putIfAbsent(String.valueOf(MFServer.get_id_by_cat(Integer.valueOf(items[0]), 22)), new MarketEsya(22, true));
                        if (items.length == 3) {
                            this.kullanici.kurkRenk = items[2];
                        }
                        int[] cat = new int[]{0};
                        for (String Item : items[1].split(",")) {
                            String pitem = Item;
                            ArrayList<Integer> pcustoms = new ArrayList<>();
                            if (pitem.contains("_")) {
                                String[] la = pitem.split("_");
                                pitem = la[0];
                                try {
                                    for (String l : la[1].split("\\+")) {
                                        pcustoms.add(Integer.parseInt(l, 16));
                                    }
                                } catch (Exception e) {

                                }
                                //

                            }
                            if (!pitem.equals("0")) {
                                this.kullanici.market.put(String.valueOf(MFServer.get_id_by_cat(Integer.valueOf(pitem), cat[0])), new MarketEsya(cat[0], true, pcustoms));
                            }
                            cat[0]++;
                        }


                        this.kullanici.tip = items[0] + ";" + items[1];
                        fix_tip();
                        this.kullanici.marketCilek -= this.promMoney;
                        this.sendLookChange();
                        this.sendShopList();

                    } else {
                        this.sendMessage("$Pas_Assez_Fromage");
                    }
                } else {
                    found = false;
                }
            } else if (Token1 == 27) {
                if (Token2 == 11) {
                    if (this.isDead) return;
                    if (!this.transformice) return;
                    this.room.sendAll(MFServer.getBuf().writeByte(27).writeByte(11).writeInt(this.kullanici.code)
                            .writeShort(buf.readShort()));
                }
            } else if (Token1 == 29) {
                if (Token2 == 1) {
                    String lua_code = ((BIS) bis).readUTFMedium();
                    if (kullanici.luaBan > System.currentTimeMillis() && kullanici.yetki != 10) {
                        sendMessage("<R>You have lua ban for 2 minutes.");
                        return;
                    }
                    boolean canExecute = false;
                    if (this.room.isTribeHouse && this.tribe == this.room.tribe
                            && this.tribe.checkPermissions("can_load_map", this) && System.currentTimeMillis() - this.lastExecuteLUA > 2) {
                        this.lastExecuteLUA = System.currentTimeMillis();
                        canExecute = true;
                    }
                    if (!this.room.isofficial && (this.kullanici.yetki == 10 || this.kullanici.isLuacrew) && this.room.LUAFile == null) {
                        canExecute = true;
                    }
                    if ((this.kullanici.isLuacrew && this.kullanici.yetki != 10) && !this.room.outname.startsWith("#"))
                        canExecute = false;
                    if (this.kullanici.isFuncorp && this.room.isFuncorp && !this.room.isofficial) canExecute = true;
                    if (!canExecute) {
                        this.sendMessage("<R>Forbidden");
                        return;
                    }
                    if (this.room.l_room != null) {
                        this.room.closeLua();
                    }
                    for (PlayerConnection pc : this.room.clients.values()) {
                        pc.LuaGame();
                    }
                    Future[] waiter = new Future[1];
                    ScheduledFuture<?> timer = MFServer.executor.schedule(() ->
                    {
                        if (waiter[0] != null) {
                            System.out.println("not null");
                        }
                        if (!waiter[0].isDone()) waiter[0].cancel(true);
                    }, 6, TimeUnit.SECONDS);
                    if (MFServer.debug) {
                        System.out.println(this.kullanici.isim + " Working Lua");
                    }
                    if ((this.kullanici.yetki == 10 || this.kullanici.isLuacrew)) {
                        MFServer.logExecutor.execute(() -> {
                            Document doc = new Document("room", room.name);
                            doc.append("lua_code", lua_code);
                            doc.append("time", System.currentTimeMillis());
                            doc.append("ad", kullanici.isim);
                            doc.append("op", "lua command");
                            MFServer.colLuaLog.insertOne(doc);
                        });
                    } else {
                        MFServer.logExecutor.execute(() -> {
                            Document doc = new Document("room", room.name);
                            doc.append("lua_code", lua_code);
                            doc.append("time", System.currentTimeMillis());
                            doc.append("ad", kullanici.isim);
                            doc.append("op", "lua command");
                            MFServer.colLuaLog2.insertOne(doc);
                        });
                    }

                    waiter[0] = MFServer.executor.submit(() -> {
                        if (this.room != null && this.room.l_room == null) {
                            timer.cancel(false);
                            this.room.l_room = new L_Room(this.room, true);
                            this.room.l_room.developer = this.kullanici.isim;
                            this.room.isMinigame = true;
                            LUA_THREAD t = new LUA_THREAD2(this.room.l_room, lua_code);
                            t.start();
                            this.server.threads.add(t);

                            if (this.room.l_room != null) this.room.l_room.t = t;
                            // else this.sendMessage("<R>Room is not ready to run lua.");

                        }
                    });

                } else if (Token2 == 2) {
                    int key = bis.readShort();
                    boolean down = bis.readBoolean();
                    x = bis.readShort();
                    y = bis.readShort();
                    if (this.room.l_room != null) {
                        try {
                            this.room.l_room.eventKeyboard(kullanici.isim, key, down, x, y);
                        } catch (Exception e) {

                        }
                    }
                    if(room.isBootcamp && (key == 88 || key == 120) && !isDead) {
                        room.sendDeath(this);
                    }
                    checkCoordAndGiveGift();

                } else if (Token2 == 3) {

                    int x = bis.readShort();
                    int y = bis.readShort();
                    if (room.isTengri && kullanici.yetki >= 4) {
                        this.room.movePlayer(this, x, y);
                        return;
                    }
                    try {
                        if (this.room.l_room != null) {
                            this.room.l_room.eventMouse(kullanici.isim, x, y);
                        }
                    } catch (Exception e) {

                    }
                } else if (Token2 == 20) {

                    int popupId = bis.readInt();
                    String answer = bis.readUTF();
                    boolean fr = false;
                    if (popupId < 0 && answer.equals("yes")) {

                        int pId = -popupId;
                        if (reqs.contains(pId)) {
                            fr = true;
                            if (this.friends.size() > 500 || friends.contains(pId)) {
                                sendMessage("<R>You can't add the person.");
                            } else {
                                kullanici.engellenenler.remove(pId);
                                executor.execute(() -> {
                                    try {
                                        this.add_friend(pId);
                                        TRIBULLE.arkadasiYolla();
                                    } catch (NoSuchFieldException e) {
                                        e.printStackTrace();
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                });
                            }
                        }
                    }
                    if (this.room.l_room != null && !fr) {
                        this.room.l_room.eventPopupAnswer(popupId, kullanici.isim, answer);
                    }
                    if (popupId == 7779) {
                        sendMessage("Please use /tag 9999");
                    }
                    if (popupId == 7778) {
                        if (answer.equals("yes")) {
                            Lang = "E2";
                            LangByte = MFServer.tersLangs.getOrDefault(Lang, 0);
                            enterRoom(this.server.recommendRoom(Lang), false);

                        }
                    }
                    if (popupId == 7775) {
                        if (answer.equals("yes")) {
                            this.kullanici.tip = "1;0,0,0,0,0,0,0,0,0";
                            if (this.kullanici.market.size() > 0) {
                                MFServer.BadgeList.forEach((k, v) -> {
                                    if (this.kullanici.market.containsKey(String.valueOf(k))) {
                                        this.kullanici.rozetler.remove(v);
                                    }
                                });
                            }
                            this.kullanici.market.clear();
                            this.kullanici.marketSaman.clear();

                            this.kullanici.marketPey = 500_000;
                            this.kullanici.marketCilek = 10_000;
                            // this.client.kullanici.unvanlar.clear();
                            this.kullanici.tipler.clear();
                            this.checkAndRebuildTitleList("all");
                        }
                    }
                } else if (Token2 == 21) {

                    int code = bis.readInt();
                    String event = bis.readUTF();
                    if (event.equals("closeAnan2")) {
                        if (this.kullanici.yetki >= 5) {
                            this.sendPacket(L_Room.packRemoveTextArea(341));
                            this.sendPacket(L_Room.packRemoveTextArea(342));
                            for (PlayerConnection pc : this.room.clients.values()) pc.watchVelocity = false;
                        }
                    }
                    if (event.equals("closeAnan")) {

                        this.sendPacket(L_Room.packRemoveTextArea(990));
                        this.sendPacket(L_Room.packRemoveTextArea(991));
                        this.sendPacket(L_Room.packRemoveTextArea(992));
                        this.sendPacket(L_Room.packRemoveTextArea(995));
                    }
                    if (event.startsWith("tasks-")) {
                        String sub = event.substring(6);
                        if (sub.startsWith("sayfa")) {
                            int sayfa = Integer.valueOf(sub.substring(5));
                            MFServer.executor.execute(Unchecked.runnable(() -> {
                                gorevEkrani(sayfa, 0);
                            }));
                        }
                        if (sub.startsWith("sil")) {
                            String _id = sub.substring(3);
                            if (ObjectId.isValid(_id)) {
                                MFServer.executor.execute(Unchecked.runnable(() -> {
                                    MFServer.colGorev.deleteOne(and(eq("_id", new ObjectId(_id)), eq("pId", kullanici.code)));
                                    gorevEkrani(gorevSayfa, 0);
                                }));

                            }

                        }
                        if (sub.startsWith("viewTask")) {
                            int index = Integer.valueOf(sub.substring(8));
                            gorevEkrani(gorevSayfa, index);

                        }
                        if (sub.equals("close")) {
                            this.gorevKapat();
                        }

                        return;
                    }

                    if (event.equals("mf_closeAnnouncement")) {
                        this.sendPacket(L_Room.packRemoveTextArea(-7072));
                        this.sendPacket(L_Room.packRemoveTextArea(-7074));
                        this.sendPacket(L_Room.packRemoveImage(-7073));
                        return;
                    }
                    if (event.equals("mf_showAnnouncement")) {
                        this.sendPacket(L_Room.packRemoveTextArea(-7071));
                        this.sendPacket(L_Room.packRemoveImage(-7070));
                        this.sendPacket(L_Room.packAddTextArea(-7072, "<a href='event:mf_closeAnnouncement'>\n\n\n\n\n\n\n\n\n\n", 200, 160, 400, 100, 0, 0x000000, 0, false));

                        this.sendPacket(L_Room.packImage("announcement_window.19425", -7073, 7, -998, 190, 150));
                        this.sendPacket(L_Room.packAddTextArea(-7074, "<font size='12'><textformat tabstops='[15]'>\t" + MFServer.announcement, 218, 179, 364, 62, 0, 0x000000, 0, false));


                        return;
                    }

                    if (event.startsWith("g_settings_changeSet")) {
                        int ayar = Integer.parseInt(event.substring(20));
                        if (ayar == 1) {
                            sendMessage("<R>/tag TAG</R> eg: <VP>/tag 777</VP>");
                        } else if (ayar == 2) {

                            if (this.refs.size() >= 10 || this.kullanici.yetki > 2 || kullanici.isFuncorp || kullanici.isTester || this.kullanici.isFashion || kullanici.isMapcrew || kullanici.isArtist || kullanici.isLuacrew || kullanici.isSentinel || (kullanici.fikicoinAlmis && arti)) {

                                room.showColorPicker(777779, kullanici.isim, kullanici.chatColor, "Chat Color");
                            } else {
                                sendMessage("You can't use this please check https://mforum.ist/threads/renewal-of-miceforce.7860 and use <ROSE>/ref</ROSE> to invite.");
                            }
                        } else if (ayar == 3) {
                            kullanici.disableKurkRozet = !kullanici.disableKurkRozet;
                        } else if (ayar == 4) {
                            kullanici.optAnnounce = !kullanici.optAnnounce;
                        } else if (ayar == 5) {
                            kullanici.disableBeceriler = !kullanici.disableBeceriler;
                            sendMessage("<VP>Please reload game :)");
                            sendBeceriler(false);
                            sendSeviye();
                        } else if (ayar == 6) {
                            kullanici.disableInventoryPopup = !kullanici.disableInventoryPopup;
                        } else if (ayar == 7) {
                            kullanici.disableOnScreen = !kullanici.disableOnScreen;
                        } else if (ayar == 8) {
                            kullanici.disableSettings = !kullanici.disableSettings;
                        } else if (ayar == 9) {
                            kullanici.disableFriendReqs = !kullanici.disableFriendReqs;
                        }
                        ayarEkrani();
                      /*  if (sub.equals("sec1")) {
                            kullanici.optAnnounce = !kullanici.optAnnounce;

                            ayarEkrani();
                        } else if (sub.equals("sec2")) {
                            sendMessage("<R>/tag TAG</R> eg: <VP>/tag 777</VP>");
                        } else if (sub.equals("sec3")) {
                            if (this.refs.size() >= 10 || this.kullanici.yetki > 2 || kullanici.isFuncorp || kullanici.isMapcrew || kullanici.isArtist || kullanici.isLuacrew || kullanici.isSentinel || (kullanici.fikicoinAlmis && kullanici.isim.startsWith("+"))) {

                                room.showColorPicker(777779, kullanici.isim, kullanici.chatColor, "Chat Color");
                            } else {
                                sendMessage("You can't use this please check https://mforum.ist/threads/renewal-of-miceforce.7860 and use <ROSE>/ref</ROSE> to invite.");
                            }
                        } else if (sub.equals("sec4")) {
                            kullanici.disableSettings = true;
                        } else if (sub.equals("kapat")) {
                            ayarKapat();
                        } else if (sub.equals("ac")) {
                            ayarEkrani();
                        }*/
                        return;
                    } else if (event.startsWith("g_settings_close")) {
                        ayarKapat();
                        return;
                    } else if (event.startsWith("g_settings_ac")) {
                        ayarEkrani();
                        return;
                    }

                    if (this.room.l_room != null) {

                        this.room.l_room.eventTextAreaCallback(kullanici.isim, code, event);
                    }
                } else if (Token2 == 32) {

                    int code = bis.readInt();
                    int color = bis.readInt();
                    if (code == 777777) {
                        this.kullanici.kurkRenk = String.format("%06X", (0xFFFFFF & color));
                    } else if (code == 777778) {
                        if (color == 16777215) color = 0;
                        this.kullanici.isimRenk = color;
                        this.send_isim_renk();
                    } else if (code == 777779) {
                        boolean isStaff = (kullanici.yetki > 2 && kullanici.yetki != 6) || kullanici.isTester || this.kullanici.isFashion || kullanici.isFuncorp || kullanici.isMapcrew || kullanici.isArtist || kullanici.isLuacrew || kullanici.isSentinel;

                        if (color == 16777215) {
                            this.kullanici.chatColor = -1;
                            sendMessage("<CH>Done.");
                        }
                        if (this.refs.size() >= 10 || isStaff || (kullanici.fikicoinAlmis && arti)) {
                            Color c = new Color(color);
                            //  System.out.println(c.getRed()>(c.getBlue()+c.getGreen())*1.7);
                            if (!(isStaff ||(kullanici.fikicoinAlmis && arti)) && c.getRed() > (c.getBlue() + c.getGreen()) * 1.7) {
                                sendMessage("<R>THIS COLOR IS RED WE CAN NOT ACCEPT IT");
                            } else {
                                this.kullanici.chatColor = color;
                                sendMessage("<VP>Done.");
                            }
                        }
                    }
                    if (this.room.l_room != null) {
                        this.room.l_room.eventColorPicked(kullanici.isim, code, color);
                    }
                }
            } else if (Token1 == 30) {
                if (Token2 == 40) {

                    this.sendPacket(this.getBuf().writeByte(30).writeByte(42).writeBoolean(kullanici.peynir > 25000 && !kullanici.cafeBan));
                    Kafe.konular(this);
                    /*if (!kullanici.unvanlar.contains(3557)) {
                        kullanici.unvanlar.add(3557);
                        ByteBuf packet = MFServer.pack_old(8, 14, new Object[]{kullanici.code, 3557});
                        room.sendAll(packet);
                    }*/
                } else if (Token2 == 41) {
                    int tid = bis.readInt();
                    MFServer.executor.execute(new BenchMark(() -> {
                        try {
                            Kafe.konuyu_ac(tid, this);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }, "KONUYU_AC"));
                } else if (Token2 == 43) {

                    if (kullanici.cafeBan) return;
                    int t = MFServer.timestamp() - this.T_message;
                    if (t < 3) {
                        this.sendMessage("$AttendreNouveauMessage", String.valueOf(3 - t));
                        return;
                    }
                    if (this.kullanici.peynir < 25_000) return;
                    int tid = bis.readInt();
                    String ileti = bis.readUTF();
                    if (this.check_message(ileti))
                        return;
                    this.T_message = MFServer.timestamp();
                    Kafe.ileti_yolla(ileti, tid, this);
                    this.server.checkForCafes.put(this.kullanici.code, this);

                } else if (Token2 == 44) {

                    if (kullanici.cafeBan) return;
                    int t = MFServer.timestamp() - this.T_topic;
                    if (t < 60) {
                        this.sendMessage("$AttendreNouveauSujet", String.valueOf((60 - t) / 60));
                        return;
                    }
                    if (this.kullanici.peynir < 25_000) return;

                    String baslik = bis.readUTF();
                    int maxLength = (baslik.length() < 40) ? baslik.length() : 40;
                    baslik = baslik.substring(0, maxLength);
                    String ileti = bis.readUTF();
                    if (this.check_message(ileti))
                        return;
                    this.T_topic = MFServer.timestamp();
                    Kafe.konu_ac(baslik, ileti, this);
                    this.server.checkForCafes.put(this.kullanici.code, this);
                } else if (Token2 == 45) {
                    boolean isCafe = buf.readBoolean();
                    this.konular.clear();
                    if (isCafe)
                        this.server.Cafers.put(kullanici.isim, this);
                    else
                        this.server.Cafers.remove(kullanici.isim);
                } else if (Token2 == 46) {
                    if (last_Vote + 1000 > System.currentTimeMillis()) return;
                    last_Vote = System.currentTimeMillis();
                    if (kullanici.cafeBan) return;
                    if (this.kullanici.peynir < 10_000) return;
                    int t_id = buf.readInt();
                    int c_id = buf.readInt();
                    int point = buf.readByte();
                    if (!(point == 0 || point == 1))
                        return;
                    MFServer.executor.execute(() -> {
                        try (Connection connection = MFServer.DS.getConnection()) {
                            int p = point;
                            if (p == 0) p = -1;
                            if (this.kullanici.yetki >= 5) p *= 15000;
                            PreparedStatement statement = connection
                                    .prepareStatement("select id from cafe_vote where post_id=? and pid=?");
                            statement.setInt(1, c_id);
                            statement.setInt(2, this.kullanici.code);
                            ResultSet rs = statement.executeQuery();
                            boolean exist = rs.next();
                            statement.close();
                            if (!exist) {

                                statement = connection.prepareStatement("select player_id from cafe_post where id=?");
                                statement.setInt(1, c_id);
                                rs = statement.executeQuery();
                                exist = rs.next();
                                int vpid;
                                if (exist)
                                    vpid = rs.getInt("player_id");
                                else
                                    return;
                                statement.close();

                                statement = connection.prepareStatement(
                                        "INSERT INTO cafe_vote(`voted_pid`,`post_id`,`pid`,`puan`)VALUES (?,?,?,?)");
                                statement.setInt(1, vpid);
                                statement.setInt(2, c_id);
                                statement.setInt(3, this.kullanici.code);
                                statement.setInt(4, p);
                                statement.execute();
                                statement.close();
                                String sql = "UPDATE cafe_post SET vote = vote + ?  WHERE id = ?";

                                statement = connection.prepareStatement(sql);
                                statement.setInt(1, p);
                                statement.setInt(2, c_id);

                                statement.execute();
                                statement.close();
                                MFServer.executor.execute(Unchecked.runnable(() -> {
                                    Kafe.konuyu_ac(t_id, this);
                                }));
                            }

                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    });
                } else if (Token2 == 47) {
                    if (this.kullanici.yetki >= 4) {
                        int topic = buf.readInt();
                        int mes = buf.readInt();
                        MFServer.executor.execute(Unchecked.runnable(() -> {
                            try (Connection connection = MFServer.DS.getConnection()) {
                                PreparedStatement statement = connection.prepareStatement(
                                        "select id from cafe_post where thread_id=? order BY time ASC LIMIT 1");
                                statement.setInt(1, topic);
                                ResultSet rs = statement.executeQuery();
                                boolean exist = rs.next();
                                if (exist) {
                                    int id = rs.getInt(1);

                                    statement.close();
                                    if (id == mes) {
                                        statement = connection
                                                .prepareStatement("delete from cafe_post where thread_id=?");
                                        statement.setInt(1, topic);
                                        statement.execute();
                                        statement.close();

                                        statement = connection.prepareStatement("delete from cafe_thread where id=?");
                                        statement.setInt(1, topic);
                                        statement.execute();
                                        statement.close();
                                        Kafe.konular(this);
                                    } else {
                                        statement = connection.prepareStatement("delete from cafe_post where id=?");
                                        statement.setInt(1, mes);
                                        statement.execute();
                                        statement.close();

                                    }
                                } else {

                                    statement.close();
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }));
                    }
                } else if (Token2 == 48) {
                    if (this.kullanici.yetki >= 4) {
                        int topic = bis.readInt();
                        String name = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));

                        MFServer.executor.execute(() -> {
                            int pid = MFServer.checkUserInDb(name);
                            try (Connection connection = MFServer.DS.getConnection()) {
                                PreparedStatement statement = connection
                                        .prepareStatement("select id from cafe_thread where player_id=?");
                                statement.setInt(1, pid);
                                ResultSet rs = statement.executeQuery();
                                while (rs.next()) {
                                    PreparedStatement st = connection
                                            .prepareStatement("delete from cafe_post where thread_id=?");
                                    st.setInt(1, rs.getInt(1));
                                    st.execute();
                                    st.close();
                                }
                                PreparedStatement st = connection
                                        .prepareStatement("delete from cafe_post where player_id=?");
                                st.setInt(1, pid);
                                st.execute();
                                st.close();
                                st = connection.prepareStatement("delete from cafe_thread where player_id=?");
                                st.setInt(1, pid);
                                st.execute();
                                st.close();
                                statement.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                        });
                    }
                } else if (Token2 == 20) {//Start Mulodrome
                    if (!this.room.isMulodrome) return;
                    int[] teams = new int[]{0, 0};
                    for (MulodromePlayer mulodromePlayer : this.room.mulodrome.values()) {
                        teams[mulodromePlayer.team] += 1;
                    }
                    if (teams[0] < 1 || teams[1] < 1) {
                        this.room.isMulodrome = false;
                        this.room.mulodrome.clear();
                        this.room.sendAll(this.room.mulodromeIptal());
                        return;
                    }
                    if (this.room.boss.equals(this.kullanici.isim)) {
                        this.room.sendAll(this.room.mulodromeIptal());
                        this.room.mulodromeStart = true;
                        this.room.roundTime = 60;
                        this.room.orjroundTime = 60;
                        this.room.never20secTimer = true;
                        this.room.Round = 0;
                        this.room.autoShaman = false;
                        this.room.mulodromeStats[0] = 0;
                        this.room.mulodromeStats[1] = 0;
                        this.room.NewRound();
                    }

                } else if (Token2 == 15) {//Join
                    if (!this.room.isMulodrome) return;

                    int team = buf.readByte();
                    int pos = buf.readByte();
                    if (this.room.mulodromeLock.get() == 0) {
                        this.room.mulodromeLock.set(1);
                        try {
                            this.room.mulodromeLeft(this.kullanici.code);
                            boolean taken = false;
                            for (MulodromePlayer mulodromePlayer : this.room.mulodrome.values()) {
                                if (mulodromePlayer.team == team && mulodromePlayer.pos == pos) {
                                    taken = true;
                                    break;
                                }
                            }
                            if (!taken) {
                                MulodromePlayer mulodromePlayer = new MulodromePlayer();
                                mulodromePlayer.pos = pos;
                                mulodromePlayer.team = team;
                                mulodromePlayer.code = this.kullanici.code;
                                mulodromePlayer.name = this.kullanici.isim;

                                if (this.tribe != null) mulodromePlayer.tribeName = this.tribe.kabile.isim;
                                this.room.mulodrome.put(this.kullanici.code, mulodromePlayer);
                                this.room.mulodromeJoin(this.kullanici.code);
                            }
                        } finally {

                            this.room.mulodromeLock.set(0);
                        }
                    }
                } else if (Token2 == 13) {//Cancel
                    if (this.room.boss.equals(this.kullanici.isim) && (this.room.mulodrome != null)) {
                        this.room.mulodrome.clear();
                        this.room.isMulodrome = false;
                        this.room.mulodromeStart = false;
                        this.room.sendAll(this.room.mulodromeIptal());
                    }
                } else if (Token2 == 17) {//Leave

                    if (!this.room.isMulodrome) return;

                    int team = buf.readByte();
                    int pos = buf.readByte();
                    MulodromePlayer bulundu = null;
                    for (MulodromePlayer mulodromePlayer : this.room.mulodrome.values()) {
                        if (mulodromePlayer.team == team && mulodromePlayer.pos == pos) {
                            bulundu = mulodromePlayer;
                            break;
                        }
                    }
                    if (bulundu != null) {
                        this.room.mulodromeLeft(bulundu.code);
                    }
                }
            } else if (Token1 == 16) {
                if (Token2 == 1) {
                    if (this.tribe != null) {
                        this.enterRoom(s_tribe + this.tribe.kabile.isim, false);
                        Room room = this.server.rooms.get(s_tribe + this.tribe.kabile.isim);

                        if (room != null)
                            room.tribe = this.tribe;
                    }
                } else if (Token2 == 2) {
                    String name = MFServer.noTag(bis.readUTF());
                    String room = this.inv.get(name);
                    if (room != null) {
                        Room rm = this.server.rooms.get(s_tribe + room);
                        if (rm != null) {
                            this.enterRoom(s_tribe + room, false);

                            this.inv.remove(name);
                        }
                    }
                } else if (Token2 == 4) {
                    // Run for mayor
                    String message = bis.readUTF();
                    if (this.kullanici.yetki != 1 || kullanici.isArtist || kullanici.isMapcrew || kullanici.isFuncorp || kullanici.isLuacrew || kullanici.isSentinel) {
                        this.sendMessage("<R>Staff members can't run for mayor");
                        return;

                    } else if (MFServer.kazananlar.containsKey(this.kullanici.isim)) {
                        this.sendMessage("<R>You can't join to this election");
                        return;
                    } else {
                        MFServer.executor.execute(() -> {
                            if (canmayor()) {
                                try (Connection connection = MFServer.DS.getConnection();
                                     PreparedStatement statement = connection.prepareStatement(
                                             "INSERT INTO `log`.`mayor`(`playerCode`,`name`,`look`,`message`,`vote_count`,`dil`,`time_stamp`,`message2`,renk)VALUES(?,?,?,?,0,?,?,?,?)")) {
                                    statement.setInt(1, this.kullanici.code);
                                    statement.setString(2, this.kullanici.isim);
                                    statement.setString(3, this.kullanici.tip);
                                    statement.setString(4, message);
                                    statement.setString(5, this.Lang);
                                    statement.setInt(6, MFServer.timestamp());
                                    statement.setString(7, "");
                                    statement.setInt(8, Integer.valueOf(this.kullanici.kurkRenk, 16));
                                    statement.execute();
                                    this.sendOylar();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } else if (Token2 == 5) {
                    // Vote
                    String name = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));
                    if (this.kullanici.kayit > MFServer.timestamp() - 1296000 || this.kullanici.peynir < 30000) {
                        this.sendMessage("<R>New users can't vote");
                        return;
                    } else {
                        MFServer.executor.execute(() -> {
                            if (canvote() && !this.kullanici.isim.equals(name)) {
                                try (Connection connection = MFServer.DS.getConnection();
                                     PreparedStatement statement = connection
                                             .prepareStatement("select dil from log.mayor where name=?")) {

                                    statement.setString(1, name);
                                    ResultSet rs = statement.executeQuery();
                                    if (rs.next() && rs.getString(1).equals(this.Lang)) {
                                        PreparedStatement st = connection.prepareStatement(
                                                "update log.mayor set vote_count=vote_count+1 where name=?");
                                        st.setString(1, name);
                                        st.execute();
                                        st.close();
                                        st = connection.prepareStatement(
                                                "insert log.mayor_vote(playerCode,votedPlayer) values (?,?)");
                                        st.setInt(1, kullanici.code);
                                        st.setString(2, name);
                                        st.execute();
                                        st.close();
                                        this.sendOylar();
                                        this.sendMessage("$ElectionVoteOk");
                                        return;
                                    } else {
                                        this.sendMessage("$ElectionJoueurPasCandidat");
                                        return;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } else if (Token2 == 6) {
                    // Change slogan
                    int typ = bis.readByte();
                    String message = bis.readUTF();
                    MFServer.executor.execute(() -> {

                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection
                                     .prepareStatement("select dil from log.mayor where name=?")) {

                            statement.setString(1, kullanici.isim);
                            ResultSet rs = statement.executeQuery();
                            if (rs.next()) {
                                PreparedStatement st;
                                if (typ == 3)
                                    st = connection.prepareStatement("update log.mayor set message2=? where name=?");
                                else
                                    st = connection.prepareStatement("update log.mayor set message=? where name=?");
                                st.setString(1, message);
                                st.setString(2, kullanici.isim);
                                st.execute();
                                st.close();
                                this.sendOylar();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });

                } else if (Token2 == 7) {
                    // Moderate
                    if (this.kullanici.yetki < 5)
                        return;
                    String name = MFServer.noTag(bis.readUTF());
                    MFServer.executor.execute(() -> {

                        try (Connection connection = MFServer.DS.getConnection();
                             PreparedStatement statement = connection.prepareStatement(
                                     "update log.mayor set message2='Moderated',message='Moderated' where name=?")) {
                            statement.setString(1, name);
                            statement.execute();
                            this.sendOylar();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });

                }/* else if (Token2 == 10) {
                    if (true) return;
                    //Drag = 1
                    //Ölüm = 2
                    //Örs = 3
                    //Peynir = 4
                    //Şaman == 5
                    //Ruh = 6
                    //Mavi Küpe = 7
                    //Lolipop=8
                    //Top=9
                    //Balon = 10
                    //Balkabağı =11
                    //Yıldız = 12
                    //Bulut = 13
                    //Elma = 14
                    //Yıldızlı Şeker = 15
                    //Kiraz = 16
                    //Yarasa = 17
                    //Havuç = 18
                    //Armut = 19
                    //Turuncu Şeker = 0
                    if (this.isDead) return;
                    ByteBufOutputStream bf = MFServer.getStream();
                    int unkown = buf.readShort();
                    buf.readByte();
                    String machine = bis.readUTF();
                    switch (machine) {
                        case "1":
                            if (!checkRemoveItem(2202, 1)) return;
                            MFServer.candyCount.addAndGet(1);
                            break;
                        case "2":
                            if (!checkRemoveItem(2202, 2)) return;
                            MFServer.candyCount.addAndGet(2);
                            break;
                        case "3":
                            if (!checkRemoveItem(2202, 3)) return;
                            MFServer.candyCount.addAndGet(3);
                            break;
                        default:
                            return;
                    }
                    if (slots.contains(machine)) {
                        this.sendMessage("Wait");
                        return;
                    }
                    //¤T_3087=Pumpkaboo¤T_3088=Hellhound¤T_3089=Sinister¤T_3090=Devilish¤T_3091=Grim Reaper¤T_3092=Pennywise"""
//¤T_3083=Bloody Mary¤T_3084=Count Dracula¤T_3085=Hellstorm T_3086=Extraterrestial
                    slots.add(machine);
                    MFServer.candChecker.set(true);
                    ArrayList<String> col = new ArrayList<>(Arrays.asList("0", "6", "5", "2", "10", "13", "12", "17"));
                    switch (machine) {
                        case "1":
                            col.add("19");
                            col.add("18");
                            col.add("2");
                            break;
                        case "2":
                            col.add("18");
                            col.add("2");
                            if (!this.kullanici.rozetler.contains(228)) col.add("14");
                            if (!this.kullanici.unvanlar.contains(3086)) col.add("8");
                            break;
                        case "3":
                            if (!this.kullanici.rozetler.contains(229)) col.add("11");
                            if (!this.kullanici.unvanlar.contains(3092)) col.add("8");
                            break;
                    }
                    ArrayList<String> bulunabilir = new ArrayList<>();
                    for (int i = 0; i < 7; i++) {
                        bulunabilir.addAll(col);
                        if (i == 2) bulunabilir.add("15");
                    }
                    this.server.random.setSeed((System.currentTimeMillis() % 10000) * this.kullanici.code);
                    int tuttu = this.server.random.nextInt(5);
                    ArrayList<String> tutanlar = new ArrayList<>();
                    if (tuttu == 3) {

                        String netuttu = bulunabilir.get(this.server.random.nextInt(bulunabilir.size()));
                        for (int i = 0; i < 3; i++) {
                            tutanlar.add(netuttu);
                        }
                        MFServer.executor.schedule(() -> {
                            slots.remove(machine);
                            switch (netuttu) {
                                case "0":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(14, 5);
                                            this.addInventoryItem(20, 5);
                                            break;
                                        case "2":
                                            this.addInventoryItem(2202, 10);

                                            break;
                                        case "3":
                                            this.addInventoryItem(2202, 15);

                                            break;
                                    }
                                    break;
                                case "8":
                                    switch (machine) {
                                        case "2":
                                            try {
                                                if (!this.kullanici.unvanlar.contains(3083)) {
                                                    this.add_event_title(3083);
                                                } else if (!this.kullanici.unvanlar.contains(3084)) {
                                                    this.add_event_title(3084);
                                                } else if (!this.kullanici.unvanlar.contains(3085)) {
                                                    this.add_event_title(3085);
                                                } else if (!this.kullanici.unvanlar.contains(3086)) {
                                                    this.add_event_title(3086);
                                                }
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        case "3":
                                            try {
                                                if (!this.kullanici.unvanlar.contains(3087)) {
                                                    this.add_event_title(3087);
                                                } else if (!this.kullanici.unvanlar.contains(3088)) {
                                                    this.add_event_title(3088);
                                                } else if (!this.kullanici.unvanlar.contains(3089)) {
                                                    this.add_event_title(3089);
                                                } else if (!this.kullanici.unvanlar.contains(3090)) {
                                                    this.add_event_title(3090);
                                                } else if (!this.kullanici.unvanlar.contains(3091)) {
                                                    this.add_event_title(3091);
                                                } else if (!this.kullanici.unvanlar.contains(3092)) {
                                                    this.add_event_title(3092);
                                                }
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                    }
                                    break;
                                case "11":
                                    if (!this.kullanici.rozetler.contains(229)) this.kullanici.rozetler.add(229);
                                    break;
                                case "14":
                                    if (this.kullanici.rozetler.contains(227) && !this.kullanici.rozetler.contains(228))
                                        this.kullanici.rozetler.add(228);
                                    else if (!this.kullanici.rozetler.contains(227)) this.kullanici.rozetler.add(227);
                                    break;
                                case "19":
                                    if (machine.equals("1")) this.addInventoryItem(4, 5);
                                    break;
                                case "15":
                                    this.addInventoryItem(2202, 250);
                                    this.addInventoryItem(12, 250);
                                    this.addInventoryItem(9, 250);
                                    this.addInventoryItem(13, 250);
                                    this.addInventoryItem(17, 50);
                                    this.addInventoryItem(19, 50);
                                    this.addInventoryItem(22, 50);
                                    this.addInventoryItem(27, 50);
                                    this.addInventoryItem(407, 50);
                                    this.addInventoryItem(2251, 50);
                                    this.addInventoryItem(2258, 50);
                                    this.addInventoryItem(2308, 50);
                                    this.addInventoryItem(29, 50);
                                    this.addInventoryItem(777, 50);
                                    for (int i = 3083; i < 3093; i++) {
                                        try {
                                            this.add_event_title(i,"halloween");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    this.kullanici.rozetler.add(227);
                                    this.kullanici.rozetler.add(228);
                                    this.kullanici.rozetler.add(229);
                                    for (PlayerConnection playerConnection : this.server.clients.values()) {
                                        try {
                                            playerConnection.sendMessage("$JackpotHalloween", this.kullanici.isim);
                                        } catch (IOException e) {
                                        }
                                    }
                                    MFServer.candyCount.set(0);
                                    MFServer.candChecker.set(true);
                                    //JACKPOT
                                    break;
                                case "18":
                                    switch (machine) {
                                        case "1":
                                            try {
                                                this.add_event_title(372);
                                            } catch (IOException e) {
                                            }
                                            break;
                                        case "2":

                                            try {
                                                this.add_event_title(373);
                                            } catch (IOException e) {
                                            }
                                            break;
                                    }
                                    break;
                                case "6":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(3, 5);
                                            break;
                                        case "2":
                                            this.addInventoryItem(2, 5);
                                            break;
                                        case "3":
                                            this.addInventoryItem(23, 5);
                                            break;
                                    }
                                    break;
                                case "5":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(29, 1);
                                            break;
                                        case "2":
                                            this.addInventoryItem(29, 8);
                                            break;
                                        case "3":
                                            this.addInventoryItem(29, 10);
                                            break;
                                    }
                                    break;
                                case "2":
                                    switch (machine) {
                                        case "2":
                                            this.addInventoryItem(1, 5);
                                            break;
                                        case "3":
                                            this.addInventoryItem(9, 5);
                                            break;
                                    }
                                    break;
                                case "10":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(28, 5);
                                            break;
                                        case "2":
                                            this.addInventoryItem(28, 5);
                                            break;
                                        case "3":
                                            this.addInventoryItem(28, 5);
                                            break;
                                    }
                                    break;
                                case "12":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(21, 2);
                                            break;
                                        case "2":
                                            this.addInventoryItem(21, 4);
                                            break;
                                        case "3":
                                            this.addInventoryItem(21, 10);
                                            break;
                                    }
                                    break;
                                case "17":
                                    switch (machine) {
                                        case "1":
                                            this.addInventoryItem(4, 2);
                                            break;
                                        case "2":
                                            this.addInventoryItem(4, 5);
                                            break;
                                        case "3":
                                            this.addInventoryItem(4, 10);
                                            break;
                                    }
                                    break;
                            }


                        }, 6, TimeUnit.SECONDS);

                    } else {

                        MFServer.executor.schedule(() -> slots.remove(machine), 6, TimeUnit.SECONDS);
                        for (int i = 0; i < 3; i++) {
                            tutanlar.add(bulunabilir.get(this.server.random.nextInt(bulunabilir.size())));
                        }
                    }


                    bf.writeByte(16);
                    bf.writeByte(10);

                    bf.writeByte(23);
                    bf.writeByte(1);
                    bf.writeByte(4);
                    bf.writeUTF(machine);
                    for (String tut : tutanlar) bf.writeUTF(tut);

                    // bf.writeByte(0);
                    // bf.writeShort(machineId).writeByte(0).writeByte(2).writeShort(12592).writeByte(0).writeByte(1).writeByte(50).writeByte(0).writeByte(1).writeShort(11000);
                    this.sendPacket(bf.buffer());
                }*/ else {
                    found = false;
                }
            } else if (Token1 == 31) {
                if (Token2 == 1) {
                    yollaNesneleri();
                } else if (Token2 == 4) {
                    if (hidden) return;
                    int item = bis.readShort();
                    boolean equiped = bis.readBoolean();
                    Nesne x = this.kullanici.esyalar.get(String.valueOf(item));
                    if (x != null) {
                        if (!equiped)
                            this.kullanilan_esya--;

                        if (equiped && this.kullanilan_esya > 3) {

                            this.kullanilan_esya++;
                            return;
                        }

                        x.kullan = equiped;

                        this.kullanici.esyalar.put(String.valueOf(item), x);
                    }
                } else if (Token2 == 3) {
                    int itemCode = bis.readShort();
                    if (lll.contains(itemCode)) return;
                    if (itemCode == 785) return;
                    if (itemCode == 830) return;
                    if (itemCode == 831) return;
                    if (itemCode == 2469) return;
                    if (itemCode == 2336) return;
                    if (itemCode == 776) {
                        if (checkRemoveItem(itemCode, 1)) {
                            MFServer.executor.execute(() -> {
                                ArrayList<Integer> secilemez = new ArrayList<>();
                                ArrayList<Integer> var = new ArrayList<>();
                                for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code))) {
                                    int hash = Arrays.hashCode(new int[]{G.tip, G.bitis, G.son});
                                    if (G.bitti && G.ilerleme >= G.bitis) {
                                        secilemez.add(hash);
                                        var.add(G.odul.hashCode());
                                    } else if (!G.bitti) {
                                        secilemez.add(hash);
                                        var.add(G.odul.hashCode());
                                    } else if (kullanici.kurtarma < 3000 && G.tip == 7) secilemez.add(hash);
                                    else if (kullanici.kurtarma < 1000 && G.tip == 8) secilemez.add(hash);
                                    if (G.tip >= 30 && (G.tip < 33 || G.tip > 38)) secilemez.add(hash);

                                }
                                ArrayList<Gorev> secilebilir = new ArrayList<>();
                                for (Map.Entry<Integer, Gorev> x : MFServer.GorevListesi.entrySet()) {
                                    Gorev gorev = x.getValue();
                                    int lale = x.getKey();
                                    if (!secilemez.contains(lale) && !var.contains(gorev.odul.hashCode()) && (gorev.tip < 30 || (gorev.tip > 33 && gorev.tip < 38))) {
                                        secilebilir.add(gorev);
                                    }

                                }
                                if (secilebilir.size() == 0) {
                                    try {
                                        this.sendMessage("<R>We can't find a task for you.");
                                        addInventoryItem(776, 1);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }

                                int rand = betweenRandom(0, secilebilir.size());
                                Gorev gorev = secilebilir.get(rand);
                                gorevEkle(gorev.tip, gorev.bitis, gorev.odul, gorev.aciklama, gorev.son);
                                try {
                                    gorevEkrani(1, 0);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });


                        }
                        return;
                    }
                    if (this.room.isDefilante) return;
                    if ((this.room.isBootcamp || this.room.isRacing || this.room.physicalConsumables)
                            && (!MFServer.useAnyWay.contains(itemCode) || MFServer.dont.contains(itemCode))) {
                        return;
                    }
                    if (this.room.is801Room && this.esyaKullanimi++ > 5) return;

                    if (this.room.isofficial && (itemCode == 2252 || itemCode == 2256 || itemCode == 2349 || itemCode == 2379)) return;

                    if (this.room.isofficial && this.esyaKullanimi++ > 5) {
                        return;
                    }
                    if (MFServer.dont2.contains(itemCode) && this.pet != null) {
                        return;
                    }
                    if (this.room.is801Room) {
                        for (int checks : MFServer.badForVillage) {
                            if (checks == itemCode) return;
                        }
                        Nesne nesne = this.kullanici.esyalar.get(String.valueOf(itemCode));
                        if (nesne == null) {
                            return;
                        }
                    }
                    if (this.room.Saves[0] < 1 && this.room.isNormal && this.room.isofficial) {
                        for (int checks : MFServer.beforeFirst) {
                            if (checks == itemCode) return;
                        }
                    }
                    for (int checks : MFServer.beforeFirst) {
                        if (checks == itemCode && this.collidableConsumableUsage++ > 2 && !room.isTribeHouse) return;
                    }
                    if (this.consumableUsage++ > 15 && !room.isTribeHouse) return;
                    if (this.room.Map.perm == 14 && itemCode == 24) {
                        return;
                    }

                    if (!checkRemoveItem(itemCode, 1)) return;
                    ByteBuf packet = MFServer.getBuf().writeByte(31).writeByte(3).writeInt(this.kullanici.code)
                            .writeShort(itemCode);
                    this.room.sendAll(packet);
                    this.movingRight = this.direction || this.movingRight;
                    //     System.out.println(this.movingRight+" "+this.movingLeft+ " "+this.direction);
                    if (itemCode == 1) {
                        this.room.addShamanObject(33, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 5) {
                        this.room.addShamanObject(6, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 6) {
                        this.room.addShamanObject(34, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 7) {
                        this.openGift(-1);
                    } else if (itemCode == 8) {
                        this.room.addShamanObject(89, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 9) {
                        this.itemSkin = 10;
                    } else if (itemCode == 11) {
                        this.room.addShamanObject(90, this.x, this.y, 0, 0, 0, true, null);
                        this.room.sendDeath(this);
                    } else if (itemCode == 12) {
                        this.itemSkin = 33;
                    } else if (itemCode == 13) {
                        this.itemSkin = 35;
                    } else if (itemCode == 17) {
                        this.itemSkin = 37;
                    } else if (itemCode == 18) {
                        this.itemSkin = 16;
                    } else if (itemCode == 19) {
                        this.itemSkin = 42;
                    } else if (itemCode == 20) {
                        this.room.addShamanObject(33, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 21) {
                        this.playEmote(this.kullanici.code, 12, false);
                    } else if (itemCode == 22) {
                        this.itemSkin = 45;
                    } else if (itemCode == 24) {
                        this.room.addShamanObject(63, this.x, this.y, 0, 0, 0, true, null);
                    } else if (itemCode == 25) {
                        this.room.addShamanObject(80, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 26) {
                        this.room.addShamanObject(95, this.movingRight ? this.x + 20 : this.x - 20, this.y, 0,
                                this.movingRight ? 10 : -10, -3, true, null);
                    } else if (itemCode == 27) {
                        this.itemSkin = 51;
                    } else if (itemCode == 2258) {
                        this.itemSkin = 75;
                    } else if (itemCode == 2259) {
                        int t = this.kullanici.tur * 120;
                        int d = t / 60 / 60 / 24;
                        int h = (t / 60 / 60) % 24;
                        this.room.sendAll(MFServer.getBuf().writeByte(100).writeByte(40).writeByte(5).writeInt(this.kullanici.code)
                                .writeByte(0).writeByte(d).writeByte(h));
                    } else if (itemCode == 28) {
                        packet = MFServer.getBuf().writeByte(5).writeByte(45).writeShort(this.x + 25)
                                .writeShort(this.y - 10).writeByte(10);
                        this.room.sendAll(packet);
                    } else if (itemCode == 31) {
                        this.sendPlayerPet(2);
                        this.pet = new ArrayList();
                        this.pet.add(2);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 33) {
                        this.playEmote(this.kullanici.code, 16, false);
                    } else if (itemCode == 34) {
                        this.sendPlayerPet(3);
                        this.pet = new ArrayList();
                        this.pet.add(3);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 800) {
                        this.kullanici.marketPey += 50;
                        packet = MFServer.getBuf().writeByte(8).writeByte(2).writeByte(0).writeByte(50);
                        this.sendPacket(packet);
                        this.sendNewItemAnim(2, 0);
                        if (this.firstShop) {
                            this.sendShopList();
                        }
                    } else if (itemCode == 801) {
                        this.kullanici.marketCilek = this.kullanici.marketCilek + 50;
                        this.sendNewItemAnim(2, 2);
                        if (this.firstShop) {
                            this.sendShopList();
                        }

                    } else if (itemCode == 2210) {
                        this.openGift(0);
                    } else if (itemCode == 2211) {
                        this.openGift(1);
                    } else if (itemCode == 2234) {
                        this.playEmote(this.kullanici.code, 20, false);
                    } else if (itemCode == 2240) {
                        this.sendPlayerPet(4);
                        this.pet = new ArrayList();
                        this.pet.add(4);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2246) {
                        this.playEmote(this.kullanici.code, 24, false);
                    } else if (itemCode == 2247) {
                        this.sendPlayerPet(5);
                        this.pet = new ArrayList();
                        this.pet.add(5);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2262) {
                        this.sendPlayerPet(6);
                        this.pet = new ArrayList();
                        this.pet.add(6);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2332) {
                        this.sendPlayerPet(7);
                        this.pet = new ArrayList();
                        this.pet.add(7);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2340) {
                        this.sendPlayerPet(8);
                        this.pet = new ArrayList();
                        this.pet.add(8);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2437) {
                        this.sendPlayerPet(9);
                        this.pet = new ArrayList();
                        this.pet.add(9);
                        this.pet.add(System.currentTimeMillis() + (60 * 60 * 1000));
                    } else if (itemCode == 2252) {
                        this.fircaYolla(5687614);
                    } else if (itemCode == 2256) {
                        this.fircaYolla(13188682);
                    } else if (itemCode == 2349) {
                        this.fircaYolla(5422075);
                    } else if (itemCode == 2379) {
                        this.fircaYolla(16745472);
                    }  else if (itemCode == 777) {
                        int[] rand = new int[]{2262, 2258, 33, 2234, 2246, 800, 801, 2252, 2256, 2349, 2332, 2340, 2247};
                        int[] chances = new int[]{0, 1, 1, 1, 1, 2, 3, 4, 5};
                        int chance = chances[this.server.random.nextInt(chances.length)];
                        if (chance == 0) {
                            this.sendMessage("<R>Chest is empty");
                            return;
                        }

                        for (int i = 0; i < chance; i++) {
                            int newitem = rand[this.server.random.nextInt(rand.length)];
                            if (newitem == 2257) chance = 1;
                            this.addInventoryItem(newitem, chance);
                        }
                    } else if (itemCode == 2481){
                        try {
                            add_event_title(4136, "valentine2021");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else if (Token2 == 5) {//Invite to trade
                    String ad = MFServer.firstLetterCaps(bis.readUTF());
                    String ad2 = MFServer.noTag(ad);
                    PlayerConnection pc = this.room.clients.get(ad2);

                    if (pc != null) {

                        if (!pc.inTrade && !pc.kullanici.engellenenler.contains(this.kullanici.code) && !pc.ip.equals(this.ip)) {//) {
                            if (pc.currentTrade == this.kullanici.code) {
                                pc.sendTradeStart(this.kullanici.code);
                                this.sendTradeStart(pc.kullanici.code);
                                this.inTrade = true;
                                pc.inTrade = true;
                            } else {
                                ByteBuf bf = MFServer.getBuf();
                                pc.sendPacket(bf.writeByte(31).writeByte(5).writeInt(this.kullanici.code));
                            }
                            this.currentTrade = pc.kullanici.code;
                        }
                    } else {
                        this.sendMessageTrade(ad2, 3);
                    }
                } else if (Token2 == 6) {//Trade over
                    String ad = MFServer.noTag(MFServer.firstLetterCaps(bis.readUTF()));

                    if (this.inTrade) {
                        PlayerConnection pc = this.room.clients.get(ad);
                        if (pc != null) {
                            pc.clearTrade();
                            tradeNotOk();
                        }
                        this.clearTrade();

                    }

                } else if (Token2 == 8) {//Put item
                    int item_code = bis.readShort();
                    boolean put = bis.readBoolean();
                    if (item_code == 776) return;
                    if (item_code == 805) return;
                    if (item_code == 910) return;
                    if (item_code == 2210) return;
                    if (item_code == 2481) return;
                    if (item_code >= 780 && item_code < 786 && item_code != 784) return;
                    if (item_code == 901) {
                        if (this.kullanici.ruhesi == 0 || this.currentTrade == kullanici.ruhesi) {
                            putItem(item_code, put);
                        } else {
                            MFServer.executor.execute(() -> {
                                Kullanici kul = MFServer.colKullanici.find(eq("_id", kullanici.ruhesi)).projection(fields(include("cevrimici"))).first();
                                if (kul == null || kul.cevrimici < System.currentTimeMillis() - (3600 * 24 * 3)) {
                                    try {
                                        putItem(item_code, put);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            });
                        }
                    } else {
                        putItem(item_code, put);
                    }

                } else if (Token2 == 9) {//Trade OK
                    boolean ok = bis.readBoolean();
                    if (ok) {
                        PlayerConnection pc = this.room.clients_p.get(this.currentTrade);
                        if (pc != null) {
                            this.tradeOK = true;
                            this.sendPacket(MFServer.getBuf().writeByte(31).writeByte(9).writeByte(1).writeByte(1));
                            pc.sendPacket(MFServer.getBuf().writeByte(31).writeByte(9).writeByte(0).writeByte(1));
                            if (pc.tradeOK && pc.currentTrade == this.kullanici.code) {
                                tradeItems.forEach((k, v) -> {
                                    String K = String.valueOf(k);
                                    Nesne nesne = this.kullanici.esyalar.get(K);
                                    if (nesne != null && nesne.sayi >= v) {
                                        checkRemoveItem(k, v);
                                        if (k == 901) {
                                            k = 910;
                                            try {
                                                add_event_title(3504, "valentine2019");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        pc.addInventoryItem(k, v);
                                    }
                                });
                                pc.tradeItems.forEach((k, v) -> {
                                    String K = String.valueOf(k);
                                    Nesne nesne = pc.kullanici.esyalar.get(K);
                                    if (nesne != null && nesne.sayi >= v) {
                                        pc.checkRemoveItem(k, v);
                                        if (k == 901) {
                                            k = 910;
                                            try {
                                                pc.add_event_title(3504, "valentine2019");
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        addInventoryItem(k, v);


                                    }
                                });
                                this.sendMessageTrade(MFServer.plusReady(pc.kullanici), 4);
                                pc.sendMessageTrade(MFServer.plusReady(this.kullanici), 4);
                                this.clearTrade();
                                pc.clearTrade();

                            }

                        } else {
                            this.sendMessageTrade("", 3);
                        }
                    } else {
                        this.tradeNotOk();
                    }

                } else {
                    found = false;
                }
            } else if (Token1 == 25) {
                if (Token2 == 26) {// Yolla
                    if (this.kullanici.yetki < 4)
                        return;
                    String dil = bis.readUTF();
                    boolean only = bis.readBoolean();
                    boolean time = bis.readBoolean();
                    this.openMP(dil, time, !only);
                } else if (Token2 == 12) {//Chat bildirimleri
                    if (this.kullanici.yetki < 4)
                        return;
                    int enabled = bis.readByte();
                    int len = bis.readByte();
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int i = 0; i < len; i++) {
                        arrayList.add(bis.readUTF());
                    }
                    this.server.modopwetSetting.put(this.kullanici.isim, arrayList);
                } else if (Token2 == 23) {// Sil
                    if (this.kullanici.yetki < 4)
                        return;
                    String ad = bis.readUTF();
                    boolean handled = bis.readBoolean();

                    Raporlar raporlar = this.server.RaporSistemi.get(ad);
                    if (raporlar != null) {
                        int puan = -1;
                        if (handled) puan = 2;
                        this.server.giveKarma(raporlar, puan);
                        raporlar.silen = this.kullanici.isim;
                        raporlar.arr.clear();
                        silindi(raporlar);
                    }
                    //this.openMP("ALL");
                } else if (Token2 == 24) {// İzle
                    if (this.kullanici.yetki < 4)
                        return;
                    String ad = bis.readUTF();
                    PlayerConnection pc = this.server.clients.get(ad);
                    if (pc == null) {
                        this.sendMessage("<R>Offline.");
                    } else {
                        this.hidden = true;
                        this.enterRoom(pc.room.name, false);
                        this.watch(ad);
                    }
                } else if (Token2 == 27) {// İleti kayıtları
                    if (this.kullanici.yetki < 4) return;
                    String ad = MFServer.firstLetterCaps(bis.readUTF());
                    processChatLog(ad);
                }
            } else if (Token1 == 100) {
                if (Token2 == 10) {
                    this.checkPoint = true;
                } else if (Token2 == 80) {
                    // LEN BYTE > SHORT
                    // LEN BYTE > SHORT
                    if (this.isSyncer && this.room.localSync) {
                        return;
                    }
                    if (this.isSyncer && room.cheeseCoordinate.size() == 0) {
                        ArrayList<ArrayList<Integer>> arr1 = new ArrayList<>();
                        ArrayList<ArrayList<Integer>> arr2 = new ArrayList<>();
                        int lenCheese = bis.readByte();
                        ArrayList<Integer> array = new ArrayList<>();
                        for (int i = 0; i < lenCheese; ++i) {
                            array.add((int) bis.readShort());
                            if (array.size() >= 2) {
                                arr1.add((ArrayList<Integer>) array.clone());
                                array.clear();
                            }
                        }
                        this.room.cheeseCoordinate = arr1;
                        int lenHole = bis.readByte();
                        for (int j = 0; j < lenHole; ++j) {
                            array.add((int) bis.readShort());
                            if (array.size() >= 3) {
                                arr2.add((ArrayList<Integer>) array.clone());
                                array.clear();
                            }
                        }
                        this.room.holeCoordinate = arr2;
                    }

                } else if (Token2 == 31) {
                    int item = buf.readShort();
                    try {
                        if (MFServer.fashion.containsKey(item)) {
                            ByteBufOutputStream bf = MFServer.getStream();
                            bf.writeByte(100);
                            bf.writeByte(31);

                            bf.writeShort(item);
                            bf.writeByte(20);
                            String l = MFServer.fashion.get(item)[1];
                            ArrayList<Integer> lItems = new ArrayList<>();
                            String[] lit = l.split(";");
                            if (lit.length == 3) {
                                l = lit[0] + ";" + lit[1];
                            }
                            int fur = Integer.valueOf(lit[0]);
                            if (fur != 1) lItems.add(MFServer.get_id_by_cat(fur, 22));
                            int catI = 0;
                            for (String li : lit[1].split(",")) {
                                if (!li.equals("0")) {
                                    if (li.contains("_")) {
//                                    lCustom.add(MFServer.get_id_by_cat(Integer.valueOf(li.split("_")[1]),catI));
                                        li = li.split("_")[0];
                                    }
                                    lItems.add(MFServer.get_id_by_cat(Integer.valueOf(li), catI));
                                }
                                catI++;
                            }
                            bf.writeUTF(l);
                            bf.writeByte(lItems.size() + 1);

                            bf.writeInt(-1);
                            bf.writeByte(2);
                            bf.writeShort(50);
                            bf.writeShort(0);
                            bf.writeByte(3);
                            bf.writeShort(0);
                            bf.writeShort(0);
                            int prom = 2;
                            normalMon = 0;
                            promMoney = 0;
                            for (Integer it : lItems) {
                                int[] data = MFServer.get_cat_by_id(it);

                                int[] data_in_shop = this.server.ShopList.get(MFServer.get_id_by_cat(data[0], data[1]));
                                String sit = String.valueOf(it);
                                MarketEsya val = this.kullanici.market.get(sit);
                                int custom = 3;
                                int actual = 1;
                                if (data_in_shop[0] == 1) {
                                    custom = 1;
                                }
                                if (val == null) {
                                    custom = 1;
                                } else {
                                    if (val.kisisellestirme) custom = 0;
                                    actual = 0;
                                }
                                int cus = 20;
                                int cusProm = cus / prom;
                                int mon = data_in_shop[2];
                                int monProm = mon / prom;
                                bf.writeInt(it);
                                bf.writeByte(actual);
                                bf.writeShort(mon);
                                bf.writeShort(monProm);
                                bf.writeByte(custom);
                                bf.writeShort(cus);
                                bf.writeShort(cusProm);
                                if (custom != 0) {
                                    normalMon += cus;
                                    promMoney += cusProm;
                                }
                                if (actual != 0) {
                                    normalMon += mon;
                                    promMoney += monProm;
                                }

                            }


                            bf.writeShort(normalMon);
                            bf.writeShort(promMoney);

                            this.sendPacket(bf.buffer());
                            //this.sendPacket(MFServer.getBuf().writeBytes(this.server.hexStringToByteArray("641f008114006a38333b3134355f6665633439612b6661353634322b6130626433352c395f6661353634322c32365f6130626433352b6130626433352b6130626433352b6661353634322b6661353634322b6661353634322c355f6532356434322c32385f6130626433352c302c302c3007ffff0200320000030000000027a10100320028010014001001310000000000020014000000e20100280020020014000008eb01015e0118030000000001ac01002800200100140010006d00000000000100140010027601b0")));
                        }
                    }
                    catch (Exception e){}
                } else if (Token2 == 40) {
                    if (!this.fircaKullan) {
                        this.fircaSure = System.currentTimeMillis();
                        this.fircaKullan = true;
                        ByteBufOutputStream bs = MFServer.getStream();
                        bs.writeByte(100);
                        bs.writeByte(40);
                        bs.writeByte(3);
                        bs.writeUTF(this.kullanici.isim);
                        ByteBuf bf = bs.buffer();
                        Room r = this.room;
                        MFServer.executor.schedule(() -> {

                            try {
                                r.sendAll(bf);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }, 10_000, TimeUnit.MILLISECONDS);
                    }
                    if (this.fircaSure + 10_000 > System.currentTimeMillis()) {
                        ByteBuf out = getBuf();
                        out.writeByte(100);
                        out.writeByte(40);
                        out.writeByte(2);
                        out.writeInt(this.kullanici.code);
                        out.writeInt(this.fircaBoya);
                        buf.readByte();
                        out.writeBytes(buf.slice());
                        this.room.sendAllOthers(out, this.kullanici.code);
                    }
                } else if (Token2 == 2) {
                    if (this.isShaman && !isDead) {
                        ByteBuf out = getBuf();
                        out.writeByte(100);
                        out.writeByte(2);
                        out.writeInt(this.kullanici.code);
                        int objectType = buf.readShort();
                        int x = buf.readShort();
                        int y = buf.readShort();
                        int angle = buf.readShort();
                        int bom = buf.readShort();
                        int starting = buf.readByte();

                        out.writeShort(objectType);
                        out.writeShort(x);
                        out.writeShort(y);
                        out.writeShort(angle);
                        out.writeShort(bom);
                        out.writeByte(starting);
                        this.summoningObject.put("x", x);
                        this.summoningObject.put("y", y);
                        this.summoningObject.put("angle", angle);
                        this.summoningObject.put("objectType", objectType);
                        if (this.room.l_room != null && this.room.l_room.t != null) {
                            if (this.summoning == 1 && starting == 0) {
                                this.room.l_room.eventSummoningCancel(this.kullanici.isim);
                            } else {
                                this.room.l_room.eventSummoningStart(this.kullanici.isim, this.summoningObject.get("objectType"), this.summoningObject.get("x"), this.summoningObject.get("y"), this.summoningObject.get("angle"));
                            }
                        }

                        this.summoning = starting;
                        this.room.sendAllOthers(out, this.kullanici.code);
                    }

                } else if (Token2 == 3) {
                    if (this.isShaman && !isDead) {
                        ByteBuf out = getBuf();
                        out.writeByte(100);
                        out.writeByte(3);
                        out.writeInt(this.kullanici.code);
                        out.writeBytes(buf.slice());
                        L_Room l_room = this.room.l_room;
                        if (l_room != null && l_room != null)
                            l_room.eventSummoningEnd(this.kullanici.isim, this.summoningObject.get("objectType"), this.summoningObject.get("x"), this.summoningObject.get("y"), this.summoningObject.get("angle"));
                        this.room.sendAllOthers(out, this.kullanici.code);
                    }

                } else if (Token2 == 79) {
                    int cart = buf.readByte();
                    if (this.kullanici.ikonlar.contains(cart) || cart == 0) {
                        this.kullanici.ikon = cart;
                        MFServer.executor.execute(Unchecked.runnable(() -> this.sendProfile(kullanici.isim)));
                    }
                } else if (Token2 == 75) {
                    int t = bis.readByte();
                    if (t == 4) {
                        String isim = bis.readUTF();
                        sendBotShop(isim);

                    } else if (t == 10) {
                        int i = bis.readByte();
                        ArrayList veri = this.server.bots.get(this.son_bot);
                        if (veri != null) {
                            ArrayList<ArrayList> nesneler = (ArrayList) veri.get(6);
                            ArrayList nesne = nesneler.get(i);
                            int nesne_turu = (int) nesne.get(2);
                            int kod = (int) nesne.get(3);
                            int para_turu = (int) nesne.get(4);
                            int para = (int) nesne.get(5);
                            int parca = (int) nesne.get(6);
                            if (alabilir(para_turu, para)) {
                                checkRemoveItem(para_turu, para);
                                if (nesne_turu == 1) {
                                    this.kullanici.rozetler.add(kod);
                                } else if (nesne_turu == 2) {
                                    this.kullanici.ikonlar.add(kod);
                                } else if (nesne_turu == 3) {
                                    this.add_event_title(kod, "köy");
                                } else if (nesne_turu == 4) {
                                    this.addInventoryItem(kod, parca);
                                }
                                sendBotShop(this.son_bot);
                            }
                        }
                    }
                } else {
                    found = false;
                }
            } else if (Token1 == -107) {

                if (Token2 == 1) {
                    this.gorevEkrani(1, 0);
                } else {
                    found = false;
                }
            } else {
                found = false;
            }
           // if (!found) System.out.println(Token1 + " - " + Token2 + " " + MFServer.bytesToHex(buf.copy()));

        }
    }

    private void checkVersion(int version, String key, String QS) throws IOException {

        if (key.equals("ItPBZA") || (key.equals("ItPBZ"))) {
//class_702.var_2.loaderInfo.bytes
            List<String> ar = splitQuery(QS).get("L");
            if (ar.size() == 1) {
                String LANG = ar.get(0).toUpperCase();
                if (MFServer.tersLangs.get(LANG) != null) this.Lang = LANG;
                //System.out.println(this.Lang);
            }
            this.incID = MFServer.random.nextInt(70) + 5;
            this.sendPacket(new correct_version(this).data());
                           /*    ByteBufOutputStream ab = MFServer.getStream();
                                ab.writeByte(20);
                                ab.writeByte(4);

                                ab.writeShort(1);
                                ab.writeInt(1);
                                ab.writeInt(20);
                                ab.writeByte(1);
                                byte[] datak = Files.readAllBytes(Paths.get("kurk200.swf"));
                                // ab.writeUTF(new String(datak,"UTF-8"));
                                ab.writeShort(datak.length);
                                ab.write(datak);
                                this.sendPacket(ab.buffer());*/
            event_background = "artwork6.png";
            event_id =7;
            this.sendPacket(MFServer.getBuf().writeByte(14).writeByte(4).writeByte(0).writeByte(0));
            if (event_id != 0) {
                this.sendPacket(MFServer.getBuf().writeByte(16).writeByte(9).writeByte(1).writeByte(event_id).writeByte(1).writeByte(0));


            }
            if (event_background != null && !event_background.isEmpty()) {
                ByteBufOutputStream bs = MFServer.getStream();
                bs.writeByte(100);
                bs.writeByte(99);
                bs.writeUTF(event_background);

                this.sendPacket(bs.buffer());
            }
        /*

            this.sendPacket(MFServer.getBuf().writeByte(16).writeByte(9).writeByte(1).writeByte(23).writeByte(1).writeByte(0));
            ByteBufOutputStream bs = MFServer.getStream();
            bs.writeByte(100);
            bs.writeByte(99);
            bs.writeUTF("Horsemen1.png");

            this.sendPacket(bs.buffer());

            this.sendPacket(bs.buffer());*/
         /*   ByteBufOutputStream bs = MFServer.getStream();
            bs.writeByte(100);
            bs.writeByte(100);
            bs.writeUTF("2xpromo.jpg");
            this.sendPacket(bs.buffer());*/
            //https://cdn.discordapp.com/attachments/323951534578597889/569211739401355267/banner.png

            state = ConnectionState.VERIFIED;

            this.OLD = new OldData(this);
        } else {
            this.channel.close();
        }
    }

    public static String escapeHTML(String s) {
        StringBuilder out = new StringBuilder(Math.max(16, s.length()));
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c > 127 || c == '"' || c == '<' || c == '>' || c == '&') {
                out.append("&#");
                out.append((int) c);
                out.append(';');
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }

    private void putItem(int item_code, boolean put) throws IOException {
        PlayerConnection pc = this.room.clients_p.get(this.currentTrade);
        if (pc != null) {


            if (put) {
                Nesne nesne = this.kullanici.esyalar.get(String.valueOf(item_code));
                Integer cur = this.tradeItems.getOrDefault(item_code, 0);
                if (nesne != null && nesne.sayi > cur) {

                    this.sendPacket(MFServer.getBuf().writeByte(31).writeByte(8).writeByte(1).writeShort(item_code).writeBoolean(put).writeByte(1).writeByte(0));
                    pc.sendPacket(MFServer.getBuf().writeByte(31).writeByte(8).writeByte(0).writeShort(item_code).writeBoolean(put).writeByte(1).writeByte(0));
                    this.tradeItems.put(item_code, cur + 1);
                    this.tradeNotOk();
                    pc.tradeNotOk();

                }
            } else {
                Integer cur = this.tradeItems.getOrDefault(item_code, 0);
                if (cur > 0) {

                    this.sendPacket(MFServer.getBuf().writeByte(31).writeByte(8).writeByte(1).writeShort(item_code).writeBoolean(put).writeByte(1).writeByte(0));
                    pc.sendPacket(MFServer.getBuf().writeByte(31).writeByte(8).writeByte(0).writeShort(item_code).writeBoolean(put).writeByte(1).writeByte(0));
                    this.tradeItems.put(item_code, cur - 1);
                    this.tradeNotOk();
                    pc.tradeNotOk();

                }
            }

        } else {
            this.sendMessageTrade("", 3);
        }
    }

    private void gorevKapat() throws IOException {
        for (int txt : gorevUI[0]) {
            if (txt != 0) {

                this.sendPacket(L_Room.packRemoveTextArea(txt));
            }
        }
        for (int img : gorevUI[1]) {
            if (img != 0) {

                this.sendPacket(L_Room.packRemoveImage(img));
            }
        }
        gorevUI = new int[2][100];

    }

    public static int betweenRandom(int i1, int i2) {
        return (int) (Math.random() * (i2 - i1)) + i1;
    }


    public void watch(String ad) throws IOException {
        ad = MFServer.noTag(ad);
        PlayerConnection pc = this.server.clients.get(ad);
        if (pc != null) ad = MFServer.plusReady(pc.kullanici);
        ByteBufOutputStream bf = MFServer.getStream();
        bf.writeByte(25);
        bf.writeByte(11);
        bf.writeUTF(ad);
        MFServer.executor.schedule(() -> this.sendPacket(bf), 1, TimeUnit.SECONDS);
    }

    public boolean checkCanUse() throws IOException {

        if (limitArr.size() > 10) {
            limitArr.remove(0);
        }
        long ort = 0;
        long cur = System.currentTimeMillis();
        limitArr.add(cur);
        for (Long x : limitArr) {
            ort += cur - x;
        }

        if (limitArr.size() < 5) return true;
        return ort / limitArr.size() > 1_000;

    }

    public void processChatLog(String ad) {
        MFServer.executor.execute(() -> {

            try {
                List<Document> l = new ArrayList(30);
                l = MFServer.colChatLog.find(and(eq("name", ad), gte("time", System.currentTimeMillis() - 2_000_000))).projection(new Document("time", 1).append("room", 1).append("msg", 1).append("_id", -1)).sort(new Document("time", -1)).limit(30).into(l);
                Collections.reverse(l);


                ByteBufOutputStream den = MFServer.getStream();
                den.writeByte(25);
                den.writeByte(10);
                den.writeUTF(ad);
                den.writeByte(l.size() + 1);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                TimeZone tz = TimeZone.getTimeZone(this.tz);
                simpleDateFormat.setTimeZone(tz);
                den.writeUTF("\n\n\n");
                den.writeUTF(simpleDateFormat.format(new Date()));
                // ArrayList<String> text = new ArrayList<>();
                String room = "";
                for (Document doc : l) {
                    String _room = doc.getString("room");
                    String msg = doc.getString("msg");
                    if (!room.equals(_room)) {
                        msg = "Room change: " + room + " to " + _room + "\n" + msg;
                        room = _room;
                    }
                    den.writeUTF(msg);
                    den.writeUTF(simpleDateFormat.format(new Date(doc.getLong("time"))));
                    //  text.add("<v>[" + simpleDateFormat.format(new Date((long) result.getInt(1) * 1000)) + "]</v> <n>[" + ad + "]</n> " + result.getString(2));

                }

                // den.writeUTF("Altan");
                //  den.writeByte(1);
                this.sendPacket(den.buffer());


                this.sendChatServer(this.kullanici.isim + " checked " + ad + "'s chatlog");
            } catch (Exception e) {
                e.printStackTrace();
            }
                /*this.LuaGame(true);
                this.sendPacket(L_Room.packAddTextArea(990, "", 112, 35, 596, 353, 0x050505, 0x050505, 60, false));
                this.sendPacket(L_Room.packAddTextArea(991, "<p align='center'><font face=\"Lucida Console\" size='20'><V>C H A T L O G</p>", 120, 43, 576, 37, 0x050505, 0x050505, 0, false));
                this.sendPacket(L_Room.packAddTextArea(992, StringUtils.join(text, "\n"), 121, 79, 579, 303, 0x050505, 0x050505, 60, false));
                this.sendPacket(L_Room.packAddTextArea(995, "<p align='center'><a href=\"event:closeAnan\"><r>X</a></p>", 684, 34, 23, 21, 0x000000, 0x000000, 0, false));
*/

        });
    }

    private void sendTradeStart(int code) {
        ByteBuf bf = MFServer.getBuf();
        this.sendPacket(bf.writeByte(31).writeByte(7).writeInt(code));
    }

    private void sendMessageTrade(String name, int op) throws IOException {
        ByteBufOutputStream bs = MFServer.getStream();
        bs.writeByte(31);
        bs.writeByte(6);
        bs.writeUTF(name);
        bs.writeByte(op);
        this.sendPacket(bs.buffer());
    }

    private void clearTrade() {
        this.currentTrade = 0;
        this.inTrade = false;
        this.tradeOK = false;
        this.tradeItems.clear();
    }

    private void tradeNotOk() throws IOException {
        PlayerConnection pc = this.room.clients_p.get(this.currentTrade);
        if (pc != null) {
            this.sendPacket(MFServer.getBuf().writeByte(31).writeByte(9).writeByte(1).writeShort(0));
            pc.sendPacket(MFServer.getBuf().writeByte(31).writeByte(9).writeByte(0).writeShort(0));
            this.tradeOK = false;
        } else {
            this.sendMessageTrade("", 3);
        }
    }

    public void sendShamanMode() {
        if (this.kullanici.kurtarma >= 500) {
            ByteBuf bf = MFServer.getBuf();
            bf.writeByte(28).writeByte(10).writeByte(this.kullanici.samanTur);
            if (this.kullanici.kurtarma >= 1500 && this.kullanici.hardKurtarma >= 500)
                bf.writeByte(1);
            else
                bf.writeByte(0);
            bf.writeInt(Integer.parseInt(this.kullanici.samanRenk, 16));
            this.sendPacket(bf);
        }
    }

    public void add_event_title(int title, String from) throws IOException {
        this.kullanici.unvanlar.add(title);
        String t = String.valueOf(title);
        if (!this.kullanici.evInfo.titles.containsKey(t)) {
            this.kullanici.evInfo.titles.put(t, from);
            ByteBuf packet = MFServer.pack_old(8, 14, new Object[]{this.kullanici.code, title});
            this.room.sendAll(packet);
        }
        if (!this.kullanici.unvanlar.contains(3457) && this.kullanici.unvanlar.containsAll(Arrays.asList(3462, 3461, 3460, 3459, 3458))) {
            this.add_event_title(3457, "event");
        }

        if (!this.kullanici.ozelKurkler.contains(205) && this.kullanici.unvanlar.containsAll(Arrays.asList(3610, 3611, 3612))) {

            this.kullanici.ozelKurkler.add(205);
            this.sentShop = false;
            this.sendShopList();
        }
        //  MFServer.bitti(packet);
    }

    public void sendPlayerPet(int petCode) throws IOException {
        ByteBuf packet = MFServer.getBuf().writeByte(100).writeByte(70).writeInt(this.kullanici.code).writeByte(petCode);
        this.room.sendAll(packet);
    }

    private boolean checkRemoveItem(int item, int miktar) {
        String sit = String.valueOf(item);
        Nesne nesne = this.kullanici.esyalar.get(sit);
        if (nesne == null) return false;
        int count = nesne.sayi;

        int yeni = count - miktar;
        if (yeni < 0) return false;
        if (yeni < 1) {
            this.kullanici.esyalar.remove(sit);
            yeni = 0;
        } else {
            nesne.sayi = yeni;
        }
        count = yeni;
        if (count > 250)
            count = 250;

        this.sendPacket(MFServer.getBuf().writeByte(31).writeByte(2).writeShort(item).writeByte(count));
        return true;
    }

    public void send_isim_renk() throws IOException {
        Integer renk = room.NickColors.getOrDefault(kullanici.isim, 0);
        if (renk == 0) {
            if (this.kullanici != null && this.kullanici.isimRenk != 0) {
                this.room.setNameColor(this, this.kullanici.isimRenk);
            }
        } else {
            this.room.setNameColor(this, renk);
        }
    }

    public void addInventoryItem(int itemCode, int count) {

        int showCount = 0;
        int _count;
        String sit = String.valueOf(itemCode);
        Nesne nesne = this.kullanici.esyalar.get(sit);

        if (nesne != null) {
            _count = nesne.sayi + count;
            if (_count > 4096) {
                _count = 4096;
            }
            showCount = _count;
            nesne.sayi = _count;
        } else {
            if (count > 4096) {
                count = 4096;
            }
            showCount = count;
            nesne = new Nesne(count, false);
            this.kullanici.esyalar.put(sit, nesne);
        }
        ByteBuf packet = MFServer.getBuf().writeByte(31).writeByte(2).writeShort(itemCode).writeByte(showCount);
        this.sendPacket(packet);
        if (!kullanici.disableInventoryPopup) {

            packet = MFServer.getBuf().writeByte(100).writeByte(67).writeByte(0).writeShort(itemCode).writeShort(count);
            this.sendPacket(packet);
        }
    }

    private void openGift(Integer giftType) throws IOException {
        if (giftType.equals(-1)) {
            Integer itemCode = MFServer.random.nextInt(30) + 1;
            this.addInventoryItem(MFServer.random.nextInt(30) + 1, 2);
            this.sendNewItemAnim(4, itemCode);
        } else if (giftType.equals(0) || giftType.equals(1)) {
            Integer chance = MFServer.random.nextInt(3);
            if (chance.equals(2)) {
                Integer itemCode = 0;
                if (giftType.equals(0)) {
                    int[] fragments = new int[]{2113, 2114, 2116, 2119, 2121, 2122, 2123, 2125, 2126, 2127, 2128, 2129,
                            2132, 2134, 2135, 2136, 2139, 2141, 2142, 2144, 2146, 2148, 2149, 2150, 2151, 2153, 2154,
                            2157, 2158, 2161, 2162, 2167, 2170, 2171, 2172, 2174, 2175, 2176, 2178, 2179};
                    itemCode = fragments[this.server.random.nextInt(fragments.length)];
                } else if (giftType.equals(1)) {
                    int[] fragments = new int[]{2111, 2112, 2115, 2118, 2120, 2130, 2133, 2137, 2140, 2143, 2147, 2155,
                            2156, 2160, 2163, 2164, 2165, 2168, 2169, 2177};
                    itemCode = fragments[this.server.random.nextInt(fragments.length)];
                }
                this.addInventoryItem(itemCode, 1);
                this.sendNewItemAnim(4, itemCode);
            } else {
                Integer itemCode = 0;
                if (giftType.equals(0)) {
                    int[] randList = new int[]{1, 2, 3, 4, 5, 6, 7, 10, 11, 13, 14, 15, 2233};
                    itemCode = randList[this.server.random.nextInt(randList.length)];
                } else if (giftType.equals(1)) {
                    int[] randList = new int[]{16, 17, 20, 21, 25, 26, 30, 31, 33, 2232, 2234, 2240, 2246, 2257};
                    itemCode = randList[this.server.random.nextInt(randList.length)];
                }
                this.addInventoryItem(itemCode, 3);
                this.sendNewItemAnim(4, itemCode);
            }
        }
    }

    public void playEmote(int playerCode, int emoteId, boolean others, String flag) throws IOException {
        ByteBufOutputStream packet = MFServer.getStream();
        packet.writeByte(8);
        packet.writeByte(1);

        packet.writeInt(playerCode);
        packet.writeByte(emoteId);
        if (!flag.isEmpty()) {
            packet.writeUTF(flag);
        }
        packet.writeByte(0);
        if (others) {
            this.room.sendAllOthers(packet.buffer(), this.kullanici.code);
        } else {
            this.room.sendAll(packet.buffer());
        }
    }

    public void playEmote(Integer playerCode, Integer emoteId, Boolean others) throws IOException {
        this.playEmote(playerCode, emoteId, others, "");
    }

    private void sendNewItemAnim(int pcode, int code, int code2) throws IOException {
        ByteBuf packet = MFServer.getBuf();
        packet.writeByte(8).writeByte(44).writeInt(pcode).writeByte(code).writeInt(code2);
        this.room.sendAll(packet);
    }

    private void sendNewItemAnim(int code, int code2) throws IOException {
        this.sendNewItemAnim(this.kullanici.code, code, code2);
    }

    public void sendNewItemAnim(int code) throws IOException {
        this.sendNewItemAnim(this.kullanici.code, 0, code);
    }

    public void checkBalliPeynir() {

        Nesne bal = this.kullanici.esyalar.get("780");
        Nesne peynir = this.kullanici.esyalar.get("783");
        while (bal != null && peynir != null && bal.sayi >= 8 && peynir.sayi >= 6) {
            checkRemoveItem(780, 8);
            checkRemoveItem(783, 6);
            addInventoryItem(784, 1);
            bal = this.kullanici.esyalar.get("780");
            peynir = this.kullanici.esyalar.get("783");
        }
    }

    public void checkValentine2021() {
        int l = 0;
        for (int j = 0; j < 12; j++) {
            int fragKey = 2212 + j;
            Nesne frag = this.kullanici.esyalar.get(String.valueOf(fragKey));
            if (frag != null && frag.sayi > 0) l++;
        }
        if (l == 12) {
            addInventoryItem(2481, 1);
            /*try {
                add_event_title(4136, "valentine2021");
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            for (int m = 0; m < 10; m++) {
                int fragKey =  2212 + m;
                checkRemoveItem(fragKey, 1);
            }
        }
    }


    public void checkSummer() {
        int r = 0;
        int[] titles = new int[]{3672, 3673, 3671, 3674};
        for (int i = 0; i < 4; i++) {
            int l = 0;
            int relicKey = 2262 + (i * 11) + 10 + 1;

            for (int j = 0; j < 10; j++) {
                int fragKey = 2262 + (i * 11) + j + 1;
                Nesne frag = this.kullanici.esyalar.get(String.valueOf(fragKey));
                if (frag != null && frag.sayi > 0) l++;
            }
            if (l == 10) {
                addInventoryItem(2257, 5);
                try {
                    add_event_title(titles[i], "summer2019");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                addInventoryItem(relicKey, 1);
                for (int m = 0; m < 10; m++) {
                    int fragKey = 2262 + (i * 11) + m + 1;
                    checkRemoveItem(fragKey, 1);
                }
            }

            Nesne relic = this.kullanici.esyalar.get(String.valueOf(relicKey));
            if (relic != null && relic.sayi > 0) r++;
        }
        if (r == 4) {
            addInventoryItem(2257, 10);
            try {
                add_event_title(3675, "summer2019");
            } catch (IOException e) {
                e.printStackTrace();
            }
            kullanici.rozetler.add(1007);
            for (int i = 0; i < 5; i++) {
                int relicKey = 2262 + (i * 11) + 10 + 1;
                checkRemoveItem(relicKey, 1);
            }
        }
    }


    public void checkValentine() {
        int r = 0;
        int[] titles = new int[]{3502, 3498, 3499, 3500, 3501};
        for (int i = 0; i < 5; i++) {
            int l = 0;
            int relicKey = 2190 + i;

            for (int j = 0; j < 6; j++) {
                int fragKey = 2110 + (i * 7) + j + 1;
                Nesne frag = this.kullanici.esyalar.get(String.valueOf(fragKey));
                if (frag != null && frag.sayi > 0) l++;
            }
            if (l == 6) {
                try {
                    add_event_title(titles[i], "valentine2019");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                addInventoryItem(relicKey, 1);
                for (int m = 0; m < 6; m++) {
                    int fragKey = 2110 + (i * 7) + m + 1;
                    checkRemoveItem(fragKey, 1);
                }
            }

            Nesne relic = this.kullanici.esyalar.get(String.valueOf(relicKey));
            if (relic != null && relic.sayi > 0) r++;
        }
        if (r == 5) {
            addInventoryItem(901, 1);
            for (int i = 0; i < 5; i++) {
                int relicKey = 2190 + i;
                checkRemoveItem(relicKey, 1);
            }
        }
    }

    public void yollaNesneleri() throws IOException {

        //checkBalliPeynir();
        //    checkValentine();
        //checkSummer();
        checkValentine2021();
        ByteBufOutputStream p = MFServer.getStream();
        kullanilan_esya = 0;
        int t = 0;
        int i = 1;
        for (Map.Entry<String, Nesne> esya : this.kullanici.esyalar.entrySet()) {
            Nesne veri = esya.getValue();
            Integer id = Integer.valueOf(esya.getKey());
            int sayi = veri.sayi;
            for (int ew = 0; ew < sayi / 255; ew++) {
                t++;
                int type = 3;
                if (MFServer.types0.contains(id)) {
                    type = 0;
                }
                if (MFServer.types1.contains(id)) {
                    type = 1;
                }
                if (MFServer.types2.contains(id)) {
                    type = 4;
                }
                if (MFServer.types3.contains(id)) {
                    type = 5;
                }

                p.writeShort(id);
                p.writeByte(255);
                p.writeByte(type);
                if (MFServer.forbiddens.contains(id)) {
                    p.writeByte(1);
                    p.writeByte(0);
                    p.writeByte(0);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(0);
                    p.writeByte(0);
                } else if (id.equals(4)) {
                    p.writeByte(0);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(0);
                    p.writeByte(1);
                    p.writeUTF("tag_cadeau");
                } else {
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(1);
                    p.writeByte(0);
                }
                int a = 0;
                if (veri.kullan && i < 3) {
                    a = i++;
                    kullanilan_esya++;
                }
                p.writeByte(a);
            }
            t++;
            int type = 3;
            if (MFServer.types0.contains(id)) {
                type = 0;
            }
            if (MFServer.types1.contains(id)) {
                type = 1;
            }
            if (MFServer.types2.contains(id)) {
                type = 4;
            }
            if (MFServer.types3.contains(id)) {
                type = 5;
            }

            p.writeShort(id);
            p.writeByte(sayi % 255);
            p.writeByte(type);
            if (MFServer.forbiddens.contains(id)) {
                p.writeByte(1);
                p.writeByte(0);
                p.writeByte(0);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(0);
                p.writeByte(0);
            } else if (id.equals(4)) {
                p.writeByte(0);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(0);
                p.writeByte(1);
                p.writeUTF("tag_cadeau");
            } else {
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(1);
                p.writeByte(0);
            }
            int a = 0;
            if (veri.kullan && i < 3) {
                a = i++;
                kullanilan_esya++;
            }
            p.writeByte(a);
        }

        ByteBufOutputStream p2 = MFServer.getStream();
        p2.writeByte(31);
        p2.writeByte(1);
        p2.writeShort(t);
        p2.buffer().writeBytes(p.buffer());
        MFServer.bitti(p.buffer());
        this.sendPacket(p2.buffer());
    }

    private boolean alabilir(int kod, int gerekli) {
        Nesne nesne = this.kullanici.esyalar.get(String.valueOf(kod));
        if (nesne != null) {
            if (nesne.sayi >= gerekli) {
                return true;
            }
        }
        return false;
    }

    private void sendBotShop(String isim) throws IOException {
        ArrayList veri = this.server.bots.get(isim);
        if (veri != null) {
            ArrayList<ArrayList> nesneler = (ArrayList) veri.get(6);
            ByteBufOutputStream out = MFServer.getStream();
            out.writeByte(26);
            out.writeByte(38);
            out.writeUTF(isim);
            out.writeByte(nesneler.size());
            for (ArrayList nesne : nesneler) {
                boolean h = false;
                int nesne_turu = (int) nesne.get(2);
                int kod = (int) nesne.get(3);
                int para_turu = (int) nesne.get(4);
                int para = (int) nesne.get(5);
                int parca = (int) nesne.get(6);
                if (nesne_turu == 1) {
                    if (this.kullanici.rozetler.contains(kod))
                        h = true;
                } else if (nesne_turu == 2) {
                    if (this.kullanici.ikonlar.contains(kod))
                        h = true;
                } else if (nesne_turu == 3) {
                    if (this.kullanici.unvanlar.contains(kod))
                        h = true;
                }
                if (h)
                    out.writeByte(2);
                else {
                    out.writeBoolean(!alabilir(para_turu, para));
                }
                out.writeByte(nesne_turu);
                out.writeShort(kod);
                out.writeShort(parca);
                out.writeByte(4);
                out.writeShort(para_turu);
                out.writeShort(para);
            }
            this.son_bot = isim;
            this.sendPacket(out.buffer());
        }
    }

    public int birOnceki() {
        int seviye = kullanici.seviye.seviye;
        if (seviye == 256) return 0;
    /*    if (seviye >= 200) {
            seviye = seviye - 50;
        }*/
        seviye = seviye - 255;
        return kullanici.seviye.sonrakiXp - ((seviye - 1) * (seviye + 2));
    }

    public void sendSeviye() {
        ByteBuf buf = MFServer.getBuf();
        buf.writeByte(8);
        buf.writeByte(8);
        int s = kullanici.seviye.seviye;
        if (kullanici.disableBeceriler) {
            s = kullanici.beceriler.size();
            s = 0;
        }
        buf.writeShort(s);
        int bironceki = birOnceki();

        if (kullanici.seviye.xp - bironceki < 0) {
            kullanici.seviye.xp = bironceki + 1;
        }
        buf.writeInt(kullanici.seviye.xp - bironceki);
        buf.writeInt(kullanici.seviye.sonrakiXp - bironceki);
        this.sendPacket(buf);
        try {
            this.sendMessage(String.format("<CH>XP </CH>: <V>%,d</V> <N>/</N> <VP>%,d</VP>  <BV>(%,d)</BV>", this.kullanici.seviye.xp, this.kullanici.seviye.sonrakiXp, this.kullanici.seviye.sonrakiXp - this.kullanici.seviye.xp));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendBeceriler(boolean add) throws IOException {
        ByteBuf buf = MFServer.getBuf();
        buf.writeByte(8);
        buf.writeByte(22);
        //   kullanici.disableBeceriler = true;
        int s = kullanici.beceriler.size();
        if (kullanici.disableBeceriler) {
            s = kullanici.seviye.seviye;
            iletiYolla("disabled_skills");
            s = 0;
        }
        buf.writeByte(s);
        for (Map.Entry<String, Integer> entry : kullanici.beceriler.entrySet()) {
            int val = entry.getValue();
            if (val > 5 || val < 0) {
                val = 0;
            }
            buf.writeByte(Integer.valueOf(entry.getKey()));
            buf.writeByte(val);
        }
        buf.writeBoolean(add);
        this.sendPacket(buf);

    }

    public void sendStartMap(boolean dontStart) {
        this.cantMove = dontStart;
        this.sendPacket(MFServer.getBuf().writeByte(5).writeByte(10).writeBoolean(dontStart));
    }

    public void giveCheese() throws IOException {
        this.room.sendAll(new GetCheese(this).data());
        if (this.room.isTutorial) {
            ByteBufOutputStream st = this.getStream();
            st.writeByte(5);
            st.writeByte(90);
            st.writeByte(1);
            this.sendPacket(st);
        }
        this.hasCheese = true;

        if (this.room.l_room != null) {
            this.room.l_room.eventPlayerGetCheese(this.kullanici.isim);

        }
    }

    public void playerVictory(int holeType) throws IOException {
        long SUREC = (System.currentTimeMillis() - this.playerStartTime);
        long SUREC2 = (System.currentTimeMillis() - this.room.Started);

        if (this.checkPoint) {
            SUREC += checkPointTime;
            SUREC2 += checkPointTime;
            checkPointTime = 0;
        }
        int ti = (int) SUREC2 / 10;
        int t2 = ti;
        int t1 = ti;
        Double _rek = SUREC2 / 1000.0;

        if (this.playerStartTime != 0 && !this.revived) {

            _rek = SUREC / 1000.0;
            ti = (int) SUREC / 10;
            t2 = ti;
        }
        final Double rek = _rek;
        if (this.room.l_room != null) {
            this.room.l_room.eventPlayerWon(this.kullanici.isim, t1, t2);
        }
        if (this.room.isTutorial) {
            ByteBufOutputStream st = this.getStream();
            st.writeByte(5);
            st.writeByte(90);
            st.writeByte(2);
            this.sendPacket(st);
            MFServer.executor.schedule(() -> {
                if (!this.alive())
                    return;
                try {
                    this.enterRoom(this.server.recommendRoom(this.Lang), false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 5, TimeUnit.SECONDS);
        } else if (this.room.isEditeur) {

            this.sendPacket(MFServer.pack_old(14, 17, new Object[0]));
        } else {

            if (this.isShaman) {
                boolean all = false;
                for (PlayerConnection pc : this.room.clients.values()) {
                    if (!pc.isDead && !pc.isShaman) {
                        all = true;
                        break;
                    }
                }
                if (all) {

                    this.sendPacket(this.server.pack_old(8, 18));
                    return;
                }
            }

            int place = this.room.numCompleted.incrementAndGet();
            this.isDead = true;
            if (place == 1 && (this.room.isFastRacing || this.room.isEliteRacing)) {
                this.room.set_time(5);
                this.room.sendMessage("<R>" + this.kullanici.isim + "</R> is winner");
            }
            if (this.room.autoScore) {
                switch (place) {
                    case 1:
                        this.score += 16;
                        break;
                    case 2:
                        this.score += 14;
                        break;
                    case 3:
                        this.score += 12;
                        break;
                    default:
                        this.score += 10;
                        break;
                }
            }
            String Rmode = "";
            int count = this.room.unique_playerCount();
            if (this.room.isDefilante) {
                this.score += this.defilante;
                if (count > 3) {
                    this.kullanici.extraStats.defilante += defilante * genel_carpan;
                    build_defilante();
                    gorevIlerleme(9, defilante * genel_carpan);
                    Rmode = "defilante";
                }
            }

            if (this.room.countStats && count > 3 && !this.room.isFuncorp) {
                firstList.add(place == 1);
                if (firstList.size() > 20) firstList.remove(0);
                if (firstList.stream().filter(k -> k).count() > 14) {
                    sendChatServer("<R>" + kullanici.isim + "</R> has a lot first recently. Room has password: " + !room.password.isEmpty());
                    firstList.clear();
                }
                avgList.add(ti);
                if (avgList.size() > 30) avgList.remove(0);
                if (avgList.size() > 5) {
                    double avg = avgList.stream().mapToInt(Integer::intValue).average().orElse(5_000);
                    if (avg < 300 && ((this.room.isBootcamp || this.room.isRacing || this.room.isDefilante) || (this.room.isNormal && this.vpn)) && !room.isFuncorp) {
                        this.server.banPlayer(this.kullanici.isim, 1800, "FARM IS FORBIDDEN(3r) " + avg, "Tengri", false, null);
                    }
                }
            }
           /* if (this.room.countStats && count > 3 && (this.room.isBootcamp || this.room.isRacing || this.room.isDefilante) && !this.room.isFuncorp && ti < 300) {//3 Saniye
                this.server.banPlayer(this.kullanici.isim, 1800, "FARM IS FORBIDDEN", "Tengri", false);

                return;
            }*/
            boolean kontrol = false;
            if (count > 3 && (!this.room.isBootcamp || this.room.odaBaslangic > 3)) {
                if (this.room.countStats) {
                    if (place == 1) {
                        if (this.room.isDefilante) kontrol = true;
                        if (this.room.parkourMode && this.room.isofficial) kontrol = true;
                    }
                    if (this.room.isDoubleMap) {
                        if (holeType == 0) {
                            this.room.Saves[1] += 1;
                            this.room.Saves[2] += 1;
                        } else {
                            this.room.Saves[holeType] += 1;
                        }
                    } else {

                        this.room.Saves[1] += 1;
                    }
                    if (this.room.isVanilla) {
                        kullanici.extraStats.vanilla_rounds += 1 * MFServer.genel_carpan;
                        this.checkAndRebuildTitleList("vanilla");
                    }
                    this.room.Saves[0] += 1;
                    if (this.isShaman) {
                        this.kullanici.samanPeynir += 1;
                    } else {
                        this.kullanici.peynir += 272 * MFServer.genel_carpan;
                        this.giveExp(20 * MFServer.carpan * MFServer.genel_carpan, false, 0);
                        this.kullanici.marketPey += 272 * MFServer.genel_carpan;
                        this.gorevIlerleme(0, 272 * MFServer.genel_carpan);
                        this.checkAndRebuildTitleList("cheese");
                        if (place == 1) {
                            this.kullanici.marketCilek += 10 * MFServer.genel_carpan;
                            this.kullanici.birinci += 272 * MFServer.genel_carpan;
                            this.checkAndRebuildTitleList("first");
                            this.gorevIlerleme(1, 272 * MFServer.genel_carpan);
                        }
                        if (this.room.isRacing) {
                            if (place == 1) {
                                kullanici.extraStats.racing_first += 1 * MFServer.genel_carpan;
                                kullanici.extraStats.racing_podium += 1 * MFServer.genel_carpan;
                                kontrol = true;
                                if (count > 7) gorevIlerleme(12, 1 * MFServer.genel_carpan);
                            } else if (place == 2 || place == 3) {
                                kullanici.extraStats.racing_podium += 1 * MFServer.genel_carpan;
                            }
                            Rmode = "racing";
                        } else if (this.room.isBootcamp) {
                            this.kullanici.bootcamp += 1 * MFServer.genel_carpan;
                            this.gorevIlerleme(2, 1 * MFServer.genel_carpan);
                            this.checkAndRebuildTitleList("bootcamp");
                            this.addInventoryItem(2261, 1 * MFServer.genel_carpan);
                            kontrol = true;
                            Rmode = "bootcamp";
                        }
                    }
                }
            }
            if (!Rmode.isEmpty()) {
                if (place == 1) {
                    MFServer.updateToRanking(kullanici.code, Rmode, 5, 1, 1);

                } else if (place == 2 || place == 3) {
                    if (place == 2)
                        MFServer.updateToRanking(kullanici.code, Rmode, 3, 2, 1);
                    else
                        MFServer.updateToRanking(kullanici.code, Rmode, 2, 3, 1);
                } else
                    MFServer.updateToRanking(kullanici.code, Rmode, 1, 0, 0);
            }
            if (count >= 10 && this.room.countStats && this.room.isofficial) {
                int sans = 100;
                if (place == 1) sans = 4;
                if (place == 2 || place == 3) sans = 16;
                int ver = this.server.random.nextInt(sans);
                if (ver == 2) this.addInventoryItem(777, 1 * MFServer.genel_carpan);


            }
            if (kontrol && !this.room.isFuncorp && this.room.password.isEmpty() && this.kullanici.birinci > 100_000 && (!room.parkourMode || getRecord)) {
                MFServer.rekorKontrol(this, this.room.Map.id, rek, (Rekor rekor) -> {
                    if (rekor != null) {
                        try {
                            this.rekorTemp += 1;
                            this.room.iletiYolla("record_new", this.kullanici.isim, rek);
                            if (this.rekorTemp >= 5 && this.rekorTemp % 5 == 0) {
                                this.sendChatServer("<R>" + this.kullanici.isim + "</R> made a lot new records check with <V>/records " + this.kullanici.isim + "</V>  (" + rek + "s).");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            this.isComp = true;
            this.room.arrComp.add(this.kullanici.isim);
            this.sendPlayerGotCheese(this.kullanici.code, score, place, ti);
            if (!this.room.isTutorial) {
                this.room.checkShouldChangeCarte();
            }
            if (this.room.isMulodrome && this.room.mulodromeStart) {
                this.room.mulodromeAddPoint(this.kullanici.code, place);
            }
            if (hediyeAldi && this.room.countStats && this.room.isofficial) {
                this.addInventoryItem(830, 1 * MFServer.genel_carpan);
            }
        }
    }


    public void sendTotem(Integer x, Integer y) throws IOException {
        if (kullanici == null) return;
        if (this.room.isTotemEditeur)
            this.sendPacket(MFServer.pack_old(22, 22,
                    new Object[]{String.valueOf(this.kullanici.code) + "#" + x.toString() + "#" + y.toString() + "#701;12961221,460551,263172,328965" + totem2.data}));
        else if (kullanici.totem != null) {
            this.room.sendAllOld(22, 22, new Object[]{});
            this.sendPacket(MFServer.pack_old(22, 22,
                    new Object[]{String.valueOf(this.kullanici.code) + "#" + x.toString() + "#" + y.toString() + "#701;12961221,460551,263172,328965" + kullanici.totem.data}));

        }

    }

    public void bindKeyboard(int code, boolean down, boolean yes) throws IOException {

        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(2);
        p.writeShort(code);
        p.writeBoolean(down);
        p.writeBoolean(yes);
        this.sendPacket(p.buffer());
    }

    public void giveMeep() {
        this.canMeep = true;
        ByteBuf p = this.getBuf();
        p.writeByte(8);
        p.writeByte(39);
        p.writeByte(1);
        this.sendPacket(p);
    }

    public void move(int x, int y, boolean positionOffset, int xSpeed, int ySpeed, boolean speedOffset) {

        ByteBuf p = this.getBuf();
        p.writeByte(8);
        p.writeByte(3);
        p.writeShort(x);
        p.writeShort(y);
        p.writeBoolean(positionOffset);
        p.writeShort(xSpeed);
        p.writeShort(ySpeed);
        p.writeBoolean(speedOffset);
        //p.writeInt(0);
        this.sendPacket(p);
    }

    private boolean alive() {
        return channel.isActive();
    }

    public void sendPing() throws IOException {

        if (!alive())
            return;
        S_ileti = 0;
        c28 = 0;
        // genel = 0.0;
        ByteBufOutputStream st = this.getStream();
        st.writeByte(28);
        st.writeByte(6);
        st.writeInt(this.Ping++);
        this.sendPacket(st.buffer());
        this.T_Ping = System.currentTimeMillis();

    /*    Room.iptal(this.pingTimer);
        this.pingTimer = MFServer.executor.schedule(() -> {
            try {
                this.sendPing();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, 5, TimeUnit.SECONDS);*/
    }

    private void sendPlayerGotCheese(int playerCode, int score, int place, int timeTaken) throws IOException {
        int deathCount = this.room.death_alive()[0];
        if (timeTaken > 32767) {
            timeTaken = 32767;
        }
        if (deathCount > 125) {
            deathCount = 125;
        }
        if (place > 125) {
            place = 125;
        }
        ByteBufOutputStream st = this.getStream();
        st.writeByte(8);
        st.writeByte(6);
        st.writeByte(deathCount);
        st.writeInt(playerCode);
        st.writeShort(score);
        st.writeByte(place);
        st.writeShort(timeTaken);
        this.room.sendAll(st.buffer());
        this.isComp = true;
        this.hasCheese = false;
    }

    public void resetPlay() {
        sendAnchors.clear();
        sentSyncData = false;
        this.isDead = false;
        this.sentTotem = false;
        this.IceCount = 0;
        this.defilante = 0;
        this.hasCheese = false;
        this.isShaman = false;
        this.isAfk = true;
        this.isSyncer = false;
        this.isVampire = false;
        this.esyaKullanimi = 0;
        this.playerStartTime = 0;
        isComp = false;
        revived = false;
        transformice = false;
        firstObjects = false;
        shamanRespawn = false;
        byShaman = false;
        motor = false;
        daires.clear();
        lastSyncCount = 0;
        lastSyncNumber = 0;
        firstXY = new int[2];
        firstXYTime = 0;
        sistem = new Sistem();
        //  koordinatProtos = KoordinatProtos.Bitirme.newBuilder();
        slots.clear();
        checkPoint = false;
        checkPointTime = 0;
        sentVote = false;
        collidableConsumableUsage = 0;
        consumableUsage = 0;
        HalloweenHealth = 4;
        if (event_coords.length != 0) {
            try {
                sendPacket(L_Room.packRemoveImage(-98750));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        event_coords = new int[0];
        hediyeAldi = false;
        wings = false;

    }


    public void sendHalloweenHealth() {
        this.sendPacket(MFServer.getBuf().writeByte(26).writeByte(4).writeByte(HalloweenHealth));
    }


    public void giveTransformice() {
        this.sendPacket(MFServer.getBuf().writeByte(27).writeByte(10).writeByte(1));
        this.transformice = true;
    }


    public void giveExp(int xp, boolean shaman, int amount) throws IOException {
        this.kullanici.seviye.xp += xp;
        int kalanxp = kullanici.seviye.xp - kullanici.seviye.sonrakiXp;
        //  System.out.println(kalanxp);
        if (shaman) {
            if (kalanxp > 0) {
                this.sendPacket(MFServer.getBuf().writeByte(8).writeByte(9).writeInt(xp - kalanxp));
            } else {

                this.sendPacket(MFServer.getBuf().writeByte(8).writeByte(9).writeInt(xp));
            }

            if (room.unique_playerCount() > 5) gorevIlerleme(14, xp);
            if (amount > 0)
                this.sendPacket(MFServer.getBuf().writeByte(24).writeByte(1).writeShort(xp).writeShort(amount));
        }
        boolean yolla = false;
        while (this.kullanici.seviye.seviye < 20_000 && this.kullanici.seviye.xp >= kullanici.seviye.sonrakiXp) {
            this.kullanici.seviye.sonrakiXp = calculateNextExp(kullanici.seviye.seviye + 1, this.kullanici.seviye.sonrakiXp);
            kullanici.seviye.seviye++;
            checkAndRebuildTitleList("level");
            if (this.kullanici.seviye.seviye > 120) {
                yolla = true;
            }
        }
        if (yolla) {
            for (PlayerConnection pc : room.clients.values()) {
                ByteBufOutputStream st = MFServer.getStream();
                st.writeByte(24);
                st.writeByte(2);
                st.writeUTF(this.kullanici.isim);
                st.writeShort(kullanici.seviye.seviye - 1);
                pc.sendPacket(st);
            }
        }
        this.sendSeviye();
        if (shaman) {
            if (kalanxp >= 0) {
                this.sendPacket(MFServer.getBuf().writeByte(8).writeByte(9).writeInt(kalanxp));
            }
        }

    }

    public static int calculateNextExp(int seviye, int gerekenXp) {
        seviye = seviye - 100;
        //seviye = seviye % 100;
        return gerekenXp + ((seviye - 1) * (seviye + 2));
    }

    private void sendShopList() throws IOException {//725

        ByteBufOutputStream st = this.getStream();
        st.writeByte(8);
        st.writeByte(20);
        st.writeInt(kullanici.marketPey);
        st.writeInt(kullanici.marketCilek);
        st.writeUTF(kullanici.tip);
        st.writeInt(kullanici.market.size());
        for (Map.Entry<String, MarketEsya> entry : kullanici.market.entrySet()) {
            String key = entry.getKey();
            MarketEsya value = entry.getValue();
            int opened = 0;
            if (value.kisisellestirme) {
                opened = value.renkler.size() + 1;
            }
            st.writeByte(opened);
            st.writeInt(Integer.valueOf(key));
            for (Number color : value.renkler) {
                st.writeInt(color.intValue());
            }
        }
        if (!this.sentShop) {
            if (kullanici.ozelKurkler.size() > 0) {
                st.writeInt(this.server.ShopList.size() + kullanici.ozelKurkler.size());
                for (int kurk : kullanici.ozelKurkler) {

                    st.writeShort(22);
                    st.writeShort(kurk);
                    st.writeByte(0);
                    st.writeByte(12);
                    st.writeByte(0);
                    st.writeInt(1);
                    st.writeInt(1);
                    st.writeShort(0);
                }
                for (Map.Entry<Integer, int[]> x : this.server.ShopList.entrySet()) {
                    int[] val = MFServer.get_cat_by_id(x.getKey());
                    int[] valu = x.getValue();
                    int cheese = valu[1];
                    st.writeShort(val[1]);
                    st.writeShort(val[0]);
                    st.writeByte(valu[0]);
                    st.writeByte(valu[3]);
                    st.writeByte(0);
                    st.writeInt(cheese);
                    st.writeInt(valu[2]);
                    st.writeShort(0);
                }

            } else st.write(this.server.shop_byte);
        } else {
            st.writeInt(0);
            sentShop = true;
        }
        st.writeByte(MFServer.fashion.size());
        //
        int lel = 0;
        for (Map.Entry<Integer, String[]> entry : MFServer.fashion.entrySet()) {
            st.writeShort(entry.getKey());
            st.writeUTF(entry.getValue()[1]);
            st.writeByte(lel++);
        }

        st.writeShort(kullanici.tipler.size());
        for (String clothe : kullanici.tipler) {
            ArrayList<String> cloth = new ArrayList<>(Arrays.asList(clothe.split(";")));
            int s = cloth.size();
            if (!cloth.get(0).equals("1")) {
                if (s > 2) {
                    cloth.set(2, "78583a");
                } else {
                    cloth.add("78583a");
                }
                if (s > 3) {
                    cloth.set(3, "9f9f9f");
                } else {

                    cloth.add("9f9f9f");
                }


            }
            st.writeUTF(StringUtils.join(cloth, ";"));
        }
        ShamanItems(st);
        if (!this.sentShop) {
            st.writeShort(MFServer.ShamanShopList.size());
            for (Map.Entry<Integer, int[]> entry : MFServer.ShamanShopList.entrySet()) {
                Integer key = entry.getKey();
                int[] value = entry.getValue();

                st.writeInt(key);
                //st.writeShort(value[0]);
                st.writeByte(value[0]);
                st.writeByte(value[1]);
                st.writeByte(value[2]);
                st.writeInt(value[3]);
                st.writeShort(value[4]);
            }
        } else {
            st.writeShort(0);
        }
        //   st.writeShort(0);
        /* st.writeByte(1);
        st.writeByte(1);
        st.writeByte(1);
        st.writeInt(1);
        st.writeShort(1);*/
        this.sendPacket(st);
    }

    private void ShamanItems(ByteBufOutputStream st) throws IOException {

        st.writeShort(kullanici.marketSaman.size());
        for (Map.Entry<String, MarketEsya> entry : kullanici.marketSaman.entrySet()) {
            Integer key = Integer.valueOf(entry.getKey());
            MarketEsya value = entry.getValue();

            int opened = 0;
            if (value.kisisellestirme) {
                opened = value.renkler.size() + 1;
            }
            st.writeShort(key);
            st.writeBoolean(value.kullan);
            st.writeByte(opened);
            for (Number color : value.renkler) {
                st.writeInt(color.intValue());
            }
        }
    }


    public void Umay() throws Exception {

        ByteBufOutputStream st = this.getStream();
        st.writeByte(2);
        st.writeInt(this.kullanici.code);
        st.buffer().retain();
        Kayra(st);
        st.writeShort(kullanici.engellenenler.size());
        for (Integer yasakli : kullanici.engellenenler) {
            st.writeUTF(MFServer.getIsim(yasakli)[0]);
        }
        // Kabile
        String kabile_adi = "";
        int kabile_id = 0;
        String kabile_ileti = "";
        if (this.tribe != null) {
            kabile_adi = this.tribe.kabile.isim;
            kabile_id = this.tribe.Code;
            kabile_ileti = this.tribe.kabile.ileti;
        }
        st.writeUTF(kabile_adi);
        st.writeInt(kabile_id);
        st.writeUTF(kabile_ileti);

        st.writeInt(0);

        Kul(st, "", 0);
        this.sendTriPacket2(st.buffer(), 3);
        MFServer.bitti(st.buffer());

    }

    private void Kayra(ByteBufOutputStream st) throws Exception {

        boolean evli = false;
        ArrayList<Arkadas> arkadaslar = new ArrayList<>();
        List<Integer> x = MFServer.colKullanici.find(eq("_id", this.kullanici.code)).projection(fields(include("arkadaslar"))).first().arkadaslar.stream().filter(Objects::nonNull).mapToInt(k -> k.getInteger("pid")).boxed().collect(Collectors.toList());
        for (Kullanici _arkadas : MFServer.colKullanici.find(new Document("_id", new Document("$in", x))).projection(fields(include("avatar", "isim", "ruhesi", "cinsiyet", "cevrimici", "tag"), new Document("arkadaslar", new Document("$elemMatch", new Document("pid", this.kullanici.code)))))) {
            if (_arkadas == null) continue;
            if (kullanici == null) return;
            if (_arkadas.code == kullanici.code) continue;
            if (_arkadas.isim == null) continue;
            Arkadas arkadas = new Arkadas(_arkadas, this.kullanici);
            arkadas.oda = this.server.getRoom(_arkadas.isim, kullanici.yetki > 1);
            if (!evli && _arkadas.ruhesi == kullanici.code && kullanici.ruhesi == _arkadas.code) {
                evli = true;
                arkadas.evli = true;
            }
            arkadaslar.add(arkadas);
        }

        if (!evli && this.kullanici.ruhesi != 0) this.kullanici.ruhesi = 0;
        Collections.sort(arkadaslar);
        int rows = 0;
        ByteBufOutputStream st2 = MFServer.getStream();
        String adi = "";
        int cinsiyeti = 0;
        int idsi = 0;
        int avatari = 0;
        String oda, ruhesi, isim;
        boolean isfriend, acik;
        int cevrimici, cinsiyet, f_id, cins;
        evli = false;
        for (Arkadas arkadas : arkadaslar) {

            isfriend = false;
            cins = arkadas.kullanici.cinsiyet * 4;
            if (arkadas.kullanici.arkadaslar.size() != 0) {
                isfriend = true;
            }
            ByteBufOutputStream lst = st2;
            if (!evli && arkadas.kullanici.ruhesi == kullanici.code && kullanici.ruhesi == arkadas.kullanici.code) {
                isfriend = true;
                cins += 2;
                evli = true;
                lst = st;
                rows -= 1;

            }
            if (isfriend)
                cins += 1;
            String i = arkadas.kullanici.isim;
            if (!arkadas.kullanici.tag.isEmpty())
                i = MFServer.plusReady(i, arkadas.kullanici.tag);
            lst.writeInt(arkadas.kullanici.code);
            lst.writeUTF(i);
            lst.writeByte(cins);
            lst.writeInt(arkadas.kullanici.avatar);
            // st.writeInt(f_id);

            lst.writeBoolean(isfriend);
            // st.writeInt(cins);

            // st.writeInt(4);
            acik = false;
            int durum = 1;

            if (!arkadas.oda.isEmpty()) {
                acik = true;
                durum = 4;
            }
          /*  com.datastax.driver.core.ResultSet resultSet = MFServer.session.execute("select arena from online where  id = ?;", arkadas.kullanici.code);
            Row row = resultSet.one();
            if (row != null && row.getBool("arena")) {
                durum = 6;
                acik = true;
            }*/
            lst.writeBoolean(acik);
            Tigin(lst, arkadas.oda, durum);
            // st.writeUTF(oda);
            if (isfriend) {
                lst.writeInt(arkadas.kullanici.cevrimici / 60);
            } else {
                lst.writeInt(0);
            }
            rows++;
            this.friends.add(arkadas.kullanici.code);
        }
        if (!evli) {

            st.writeInt(0);
            st.writeUTF("");
            st.writeByte(0);
            st.writeInt(0);
            st.writeByte(0);
            st.writeByte(0);
            Tigin(st, "", 1);
            st.writeInt(0);
        }
        st.writeShort(rows);
        st.buffer().writeBytes(st2.buffer());
        MFServer.bitti(st2.buffer());


    }

    public void checkCoordAndGiveGift() {
        if (!hediyeAldi) {
            if (event_coords.length != 0) {
                if (Math.abs(this.x - event_coords[0]) < 30 && Math.abs(this.y - event_coords[1]) < 30) {
                    hediyeAldi = true;
                    try {
                        sendPacket(L_Room.packRemoveImage(-98750));

                        sendPacket(L_Room.packImage("emptytransparent-png.24487", -98750, 7, -1, event_coords[0], event_coords[1]));
                        ByteBufOutputStream ab = MFServer.getStream();
                        ab.writeByte(100);
                        ab.writeByte(101);
                        ab.writeByte(2);
                        ab.writeInt(kullanici.code);//Id
                        ab.writeUTF("x_transformice/x_aventure/empty.png");
                        ab.writeShort(-34);//x
                        ab.writeShort(-35);//y
                        ab.writeBoolean(false);
                        ab.writeShort(100);//Scale
                        ab.writeShort(0);//Rotation
                        room.sendAll(ab.buffer());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    public void Umay2() throws Exception {
        ByteBufOutputStream st = this.getStream();
        // st.writeByte(2);
        // st.writeInt(this.kullanici.code);
        // Boşluk

        // Arkadaşlar
        Kayra(st);
        this.sendTriPacket2(st.buffer(), 34);
        MFServer.bitti(st.buffer());

    }

    public void gorevEkle(int tip, int bitis, Document odul, String aciklama, int son) {
        Gorev gorev = new Gorev();
        gorev.pId = kullanici.code;
        gorev.aciklama = aciklama;
        gorev.odul = odul;
        gorev.tip = tip;
        gorev.bitis = bitis;
        gorev.son = MFServer.timestamp() + son;
        MFServer.colGorev.insertOne(gorev);
    }

    public void gorevEkle(int tip, int bitis, List<Document> oduller, String aciklama, int son) {
        Gorev gorev = new Gorev();
        gorev.pId = kullanici.code;
        gorev.aciklama = aciklama;
        gorev.oduller = oduller;
        gorev.tip = tip;
        gorev.bitis = bitis;
        gorev.son = MFServer.timestamp() + son;
        MFServer.colGorev.insertOne(gorev);
    }

    public boolean gorevSonaErdi(Gorev gorev) {
        if (gorev.son < MFServer.timestamp() && !gorev.bitti) {
            MFServer.colGorev.updateOne(eq("_id", gorev._id), set("bitti", true));
            return true;
        }
        return false;
    }


    private void task_80() {
        int son = 0;
        int dvm = 0;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", 80))) {
            if (G.ilerleme >= G.bitis) {
                son = G.bitis;
            } else if (!G.bitti) {
                dvm = G.bitis;
            }
        }
        int[] svs = {20, 40, 60};
        List<List<Document>> arr = new ArrayList<>();

        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3767)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3765)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3766)));
        //   arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3428),new Document("odulTip", 0).append("unvan", 3430)));
        int i = 0;
        for (int sv : svs) {
            if ((son < sv) && dvm == 0) {
                List<Document> odul = arr.get(i);
                this.gorevEkle(80, sv, odul, "task80", 60 * 60 * 24 * 21);
                break;
            }
            i++;
        }
    }

    private void task_82() {
        int son = 0;
        int dvm = 0;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", 82))) {
            if (G.ilerleme >= G.bitis) {
                son = G.bitis;
            } else if (!G.bitti) {
                dvm = G.bitis;
            }
        }
        int[] svs = {50, 100, 150, 200, 250};
        List<List<Document>> arr = new ArrayList<>();

        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3760)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3761)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3762)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3763)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3764)));
        int i = 0;
        for (int sv : svs) {
            if ((son < sv) && dvm == 0) {
                List<Document> odul = arr.get(i);
                this.gorevEkle(82, sv, odul, "task82", 60 * 60 * 24 * 21);
                break;
            }
            i++;
        }
    }

    private void task_81() {
        int son = 0;
        int dvm = 0;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", 81))) {
            if (G.ilerleme >= G.bitis) {
                son = G.bitis;
            } else if (!G.bitti) {
                dvm = G.bitis;
            }
        }
        int[] svs = {25, 50, 75, 100, 125};
        List<List<Document>> arr = new ArrayList<>();

        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3755)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3756)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3757)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3758)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3759)));
        int i = 0;
        for (int sv : svs) {
            if ((son < sv) && dvm == 0) {
                List<Document> odul = arr.get(i);
                this.gorevEkle(81, sv, odul, "task81", 60 * 60 * 24 * 21);
                break;
            }
            i++;
        }
    }


    public void bittiyseOdulVer(Gorev gorev) throws IOException {
        int bitis = gorev.bitis;
        if (gorev.tip == 5) bitis = bitis * 3600;

        if (gorev.ilerleme >= bitis && !gorev.bitti) {

            MFServer.colGorev.updateOne(eq("_id", gorev._id), set("bitti", true));
            if (gorev.tip >= 30 && gorev.tip < 33) {
                gorevIlerleme(33, 1);
            }
            if (gorev.odul != null) {
                int odulTip = gorev.odul.getInteger("odulTip");
                if (odulTip == 0) {//Ünvan ver
                    int unvan = gorev.odul.getInteger("unvan");
                    this.add_event_title(unvan, "görev");
                } else if (odulTip == 1) {//Rozet veriniz efenim
                    int rozet = gorev.odul.getInteger("rozet");
                    this.kullanici.rozetler.add(rozet);
                } else if (odulTip == 2) {//Kürk veriniz efenim
                    int kurk = gorev.odul.getInteger("kurk");
                    this.kullanici.ozelKurkler.add(kurk);
                    this.sentShop = false;
                    this.sendShopList();
                }
            } else if (gorev.oduller != null) {
                for (Document odul : gorev.oduller) {
                    int odulTip = odul.getInteger("odulTip");
                    if (odulTip == 0) {//Ünvan ver
                        int unvan = odul.getInteger("unvan");
                        this.add_event_title(unvan, "görev");
                    } else if (odulTip == 1) {//Rozet veriniz efenim
                        int rozet = odul.getInteger("rozet");
                        this.kullanici.rozetler.add(rozet);
                    } else if (odulTip == 2) {//Kürk veriniz efenim
                        int kurk = odul.getInteger("kurk");
                        this.kullanici.ozelKurkler.add(kurk);
                        this.sentShop = false;
                        this.sendShopList();
                    }
                }
            }
            if (gorev.tip == 60) {
                cadilar();
            } else if (gorev.tip == 80) {
                task_80();
            } else if (gorev.tip == 81) {
                task_81();
            } else if (gorev.tip == 82) {
                task_82();
            } else if (gorev.tip == 85) {
                nisanEvent();
            }
        }
    }

    public void gorevIlerleme(int tip, int ilerleme) throws IOException {
        MFServer.executor.execute(() -> {

            for (Gorev gorev : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", tip).append("bitti", false))) {
                if (!gorevSonaErdi(gorev)) {
                    int bitis = gorev.bitis;
                    if (gorev.tip == 5) bitis = bitis * 3600;
                    gorev.ilerleme = Math.min(gorev.ilerleme + ilerleme, bitis);
                    if (gorev.ilerleme < 0) gorev.ilerleme = 0;

                    MFServer.colGorev.updateOne(eq("_id", gorev._id), inc("ilerleme", ilerleme));
                    try {
                        bittiyseOdulVer(gorev);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
            // MFServer.colOplog.insertOne(new Document("pId",kullanici.code).append("tip",tip).append("ilerleme",ilerleme).append("t",System.currentTimeMillis()));
        });

    }


    private void ayarKapat() throws IOException {
        this.sendPacket(L_Room.packRemoveTextArea(-7000));
        this.sendPacket(L_Room.packRemoveTextArea(-7001));
        this.sendPacket(L_Room.packRemoveTextArea(-7002));
        this.sendPacket(L_Room.packRemoveImage(-68));
        this.sendPacket(L_Room.packRemoveImage(-69));
        for (int i = 1; i < 10; i++) {
            this.sendPacket(L_Room.packRemoveImage(-100 - i));
            this.sendPacket(L_Room.packRemoveTextArea(-7100 - i));

        }
    }

    public void ayarEkrani() throws IOException {

        this.LuaGame();
        ayarKapat();
        this.sendPacket(L_Room.packImage("prophunt_helpbg.5927", -68, 7, -998, 140, 40));
        this.sendPacket(L_Room.packImage("prophunt_helpbg.4921", -69, 7, -999, 629, 43));
        this.sendPacket(L_Room.packAddTextArea(-7000, "<p align=\"center\"><font size=\"14\" color=\"#ffd991\">" + Dil.yazi(this.Lang, "settings") + "</font></p>", 170, 42, 460, 34, 0x324650, 0x000000, 0, false));
        this.sendPacket(L_Room.packAddTextArea(-7002, "<a href=\"event:g_settings_close\">\n</a>", 629, 42, 20, 24, 0, 0x000000, 0, false));
        int extraLine = 0;
        for (int i = 1; i < 10; i++) {
            String img = "";
            if (i == 3) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableKurkRozet) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 4) {
                img = "checkbox_filled.19230";
                if (kullanici.optAnnounce) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 5) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableBeceriler) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 6) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableInventoryPopup) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 7) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableOnScreen) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 8) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableSettings) {
                    img = "checkbox_empty.19229";
                }
            } else if (i == 9) {
                img = "checkbox_filled.19230";
                if (!kullanici.disableFriendReqs) {
                    img = "checkbox_empty.19229";
                }
            }
            int newLineLimit = 9;
            if (this.Lang.equals("RU")) {
                newLineLimit = 7;
            }

            ArrayList<String> texts = new ArrayList<>(Arrays.asList(StringUtils.split(Dil.yazi(this.Lang, "settings_" + i), " ")));
            //  System.out.println(i + " " + texts.size());
            if (texts.size() > newLineLimit) {
                texts.add(newLineLimit, "\n\t" + texts.get(newLineLimit));
            }
            String yazi = StringUtils.join(texts, " ");
            //  System.out.println(yazi);
            this.sendPacket(L_Room.packImage(img, -100 - i, 7, -998, 160, 58 + (i * 20) - (extraLine * 7)));

            this.sendPacket(L_Room.packAddTextArea(-7100 - i, "<a href='event:g_settings_changeSet" + i + "'><textformat tabstops='[25]'><font size='12'>\t" + yazi, 155, 55 + (i * 20) - (extraLine * 7), 500, 20, 0, 0x000000, 0, false));


        }

        /* String text = "";
        text += "<a href=\"event:ayar-sec1\"><v>[•]</v> <BL>" + Dil.yazi(this.Lang, "enable_messages") + "</BL></a>";
        if (!kullanici.optAnnounce) {
            text = text.replace("BL", "r");
        }

        this.sendPacket(L_Room.packAddTextArea(-7001, text + "\n<a href=\"event:ayar-sec2\"><v>[•]</v> <bl>" + Dil.yazi(this.Lang, "change_tag") + "</bl></a>" + "\n<a href=\"event:ayar-sec3\"><v>[•]</v> <bl>" + Dil.yazi(this.Lang, "change_chat_color") + "</bl></a>" + "\n<a href=\"event:ayar-sec4\"><v>[•]</v> <bl>" + Dil.yazi(this.Lang, "disable_settings") + "</bl></a>", 150, 75, 495, 290, 0x324650, 0x000000, 0, false));

        this.sendPacket(L_Room.packAddTextArea(-7002, "<a href=\"event:ayar-kapat\">\n</a>", 615, 43, 24, 22, 0x324650, 0x000000, 0, false));
  */
    }

    public void gorevEkrani(int sayfa, int index) throws IOException {
        gorevSayfa = sayfa;
        this.LuaGame();
        this.gorevKapat();
        ArrayList<Gorev> gorevler = new ArrayList<>();
        boolean hepsi = true;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code)).sort(new Document("bitti", 1).append("ilerleme", -1))) {
            if (gorevSonaErdi(G)) G.bitti = true;
            if (!G.bitti) hepsi = false;
            gorevler.add(G);
        }
        this.sendPacket(MFServer.getBuf().writeByte(144).writeByte(5).writeBoolean(!hepsi));


        int son = (int) Math.ceil((double) gorevler.size() / 3.0);
        if (son == 0) son = 1;
        String t1 = "";
        String prev = "";
        if (sayfa > 1) {
            prev += "<a href='event:tasks-sayfa" + (sayfa - 1) + "'><CH>";
        } else {
            prev += "<BL>";
        }
        prev += "<p align='center'>" + Dil.yazi(this.Lang, "prev") + "\n";
        String next = "";
        if (sayfa < son) {
            next += "<a href='event:tasks-sayfa" + (sayfa + 1) + "'><CH>";
        } else {
            next += "<BL>";
        }
        next += "<p align='center'>" + Dil.yazi(this.Lang, "next") + "\n";
        t1 = "<p align='center'><font size='12'><J>" + Dil.yazi(this.Lang, "page") + " <BL>" + (sayfa) + "/" + (son);
        gorevUI[1][0] = -1;
        gorevUI[1][1] = -2;
        gorevUI[1][2] = -3;
        gorevUI[1][3] = -4;
        gorevUI[0][0] = 100000;
        gorevUI[0][1] = 100001;
        gorevUI[0][2] = 100002;
        gorevUI[0][3] = 100003;
        gorevUI[0][4] = 100004;
        this.sendPacket(L_Room.packImage("tasks_background.18647", -1, 7, -1, 50, 40));
        this.sendPacket(L_Room.packImage("close_button.4921", -2, 7, -1, 719, 44));
        this.sendPacket(L_Room.packAddTextArea(100000, "<p align='center'><font face='Verdana' color='#ffd991' size='14'>" + Dil.yazi(this.Lang, "tasks"), 170, 43, 460, 0, 0, 1, 0, false));
        this.sendPacket(L_Room.packAddTextArea(100001, "<a href='event:tasks-close'>\n\n\n", 719, 43, 20, 20, 0, 1, 0, false));
        this.sendPacket(L_Room.packAddTextArea(100002, prev, 75, 341, 120, 20, 0, 1, 0, false));
        this.sendPacket(L_Room.packAddTextArea(100003, next, 375, 341, 120, 20, 0, 1, 0, false));
        this.sendPacket(L_Room.packAddTextArea(100004, t1, 225, 340, 120, 20, 0, 1, 0, false));
        int index2 = 3;
        int index1 = 4;
        int x = (sayfa - 1) * 3;
        for (int i = 0; i < gorevler.size(); i++) {
            if (i >= x && i < x + 3) {
                int i2 = i % 3 + 1;

                gorevUI[1][++index2] = -300 * -(i2 + 1);
                this.sendPacket(L_Room.packImage("task_window.5402", -300 * -(i2 + 1), 7, -1, 70, (i2 * 85) - 5));
                Gorev gorev = gorevler.get(i);
                Document odul = gorev.odul;
                if (odul == null) odul = gorev.oduller.get(MFServer.random.nextInt(gorev.oduller.size()));
                int odulTip = odul.getInteger("odulTip");
                if (odulTip == 2 || odulTip == 1 || odulTip == 0) {
                    String img = odul.getString("img");
                    if (img == null) img = "icon_title.18787";
                    gorevUI[1][++index2] = -177 * -(i2 + 1);
                    this.sendPacket(L_Room.packImage(img, -177 * -(i2 + 1), 7, 0, 85, (i2 * 85) + 10));
                }
                int bitis = gorev.bitis;

                if (gorev.tip == 5) bitis = bitis * 3600;
                double d = (double) gorev.ilerleme / (double) bitis;
                double durum = Math.min(d * 100.0, 100.0);

                if (i2 == index) {

                    gorevUI[1][++index2] = -97 * -(i2 + 1);
                    gorevUI[1][++index2] = -83 * -(i2 + 1);
                    gorevUI[0][++index1] = 130004 + i2;
                    String highlight = "task_highlight.18778";
                    this.sendPacket(L_Room.packImage(highlight, -83 * -(i2 + 1), 7, 0, 65, (i2 * 85) - 10));

                    if (odulTip == 2) {
                        this.sendPacket(L_Room.packImage(MFServer.KurkResim.getOrDefault(odul.getInteger("kurk"), odul.getString("img")), -97 * -(i2 + 1), 7, 0, 600, 178));
                    }
                    int T = (gorev.son - MFServer.timestamp());
                    int saat = T / 3600;
                    int dakika = (T % 3600) / 60;
                    String yazi = "";
                    if (!gorev.bitti) {
                        if (saat == 0) {
                            yazi += Dil.yazi(this.Lang, "minutes", dakika);
                        } else {
                            yazi += Dil.yazi(this.Lang, "hours", saat);

                        }
                    }


                    String t4 = "";
                    if (gorev.bitti && durum < 100) {
                        t4 += "<R>" + Dil.yazi(this.Lang, "failed");
                    } else if (durum >= 100) {
                        t4 += "<a ><CH>" + Dil.yazi(this.Lang, "prizeClaimed") + "\n";
                    } else {
                        t4 += gorev.ilerleme + "/" + bitis;
                    }
                    int fontsize = 11;
                    if (yazi.length() > 65) fontsize = 8;
                    yazi = "<font size='" + fontsize + "'>" + yazi;
                    this.sendPacket(L_Room.packAddTextArea(130004 + i2, "<p align='center'><BL>Progress: <N>" + t4 + "\n<ROSE>" + yazi, 540, 264, 180, 35, 0, 1, 0, false));

                }

                gorevUI[0][++index1] = 100004 + i2;
                String yazi = "<J>" + Dil.yazi(this.Lang, gorev.aciklama, gorev.bitis) + "<G> ";
                String img = "tasks_profull.5446";
                double or = durum * 3.32d;
                if (!gorev.bitti && durum < 100) {
                    img = "tasks_progressbar.5404";
                }

                this.sendPacket(L_Room.packAddTextArea(100004 + i2, yazi, 140, (i2 * 85) + 10, 330, 20, 0, 1, 0, false));

                //ui.addTextArea(100004 + i, v.desc or "$MISSING_DESC$", n, 260, (i * 80) + 25, 330, 20, 0, 1, 0)
                //
                gorevUI[1][++index2] = -13 * -(i2 + 1);
                this.sendPacket(L_Room.packImage(img, -13 * -(i2 + 1), 7, -1, 136, (i2 * 85) + 30));
                if (durum < 100 && !gorev.bitti) {
                    gorevUI[0][++index1] = 110010 + i2;
                    if (durum >= 1)
                        this.sendPacket(L_Room.packAddTextArea(110010 + i2, "", 144, (i2 * 85) + 39, (int) or, 2, 0x333333, 0x333333, 100, false));
                }
                String t3 = "<p align='center'><font size='11'>";
                if (gorev.bitti && durum < 100) {
                    t3 += "<R>" + Dil.yazi(this.Lang, "failed");
                    String t5 = "<R><p align='center'><a href='event:tasks-sil" + gorev._id.toHexString() + "'>" + Dil.yazi(this.Lang, "failed") + "</font><p/>";

                    gorevUI[0][++index1] = 190007 + i2;
                    this.sendPacket(L_Room.packAddTextArea(190007 + i2, t5, 540, 304, 180, 20, 0, 1, 0, false));

                } else if (durum >= 100) {
                    t3 += "<a ><CH>" + Dil.yazi(this.Lang, "prizeClaimed") + "\n";
                } else {
                    t3 += String.format("%.3f", durum) + "%";
                }
                gorevUI[0][++index1] = 100007 + i2;
                this.sendPacket(L_Room.packAddTextArea(100007 + i2, t3, 135, (i2 * 85) + 31, 350, 20, 0, 1, 0, false));

                gorevUI[0][++index1] = 110007 + i2;
                this.sendPacket(L_Room.packAddTextArea(110007 + i2, "<a href='event:tasks-viewTask" + (i2) + "'>\n\n\n\n\n\n\n", 70, (i2 * 85) + -5, 430, 70, 0, 1, 0, false));

//	ui.addTextArea(100007 + i, "<p align='center'><font size='11'>"..(v.perc == 100 and "<a href='event:tasks-claim'><CH>Claim prize\n" or "<BL>"..(v.perc or 0).."%"), n, 250, (i * 80) + 46, 350, 20, 0, 1, 0)
//

            }
        }
    }

    public static boolean isVPN(String ip) {
        return isVPN(ip, false);
    }

    public static boolean isVPN(String ip, boolean allowPuffin) {

        Document Ip = MFServer.colIP.find(new Document("ip", ip)).first();
        if (Ip == null) {

            try {
                final HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
                DefaultHttpClient client = new DefaultHttpClient(httpParams);
                HttpUriRequest request = new HttpGet("http://v2.api.iphub.info/ip/" + ip);
                request.addHeader("X-Key", "MzgxMTpSMUpPSVg2NGxRRGo5TXliVjlkZ0VWMDFqMHZoOFd0cQ==");
                HttpResponse response = client.execute(request);
                String body = CharStreams.toString(new InputStreamReader(
                        response.getEntity().getContent(), Charsets.UTF_8));
                //  HashMap<String, String> hm = gson2.fromJson(new String(b, 0, i), type2);
                Ip = Document.parse(body);
                MFServer.colIP.insertOne(Ip);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Ip == null) {
            Ip = new Document("block", 0).append("isp", "");
        }
        boolean block = Ip.getInteger("block", 0) != 0;
        if (block && allowPuffin && Ip.getString("isp").equals("HURRICANE")) return false;

        return block;
    }

    private void Login(String name2, String password, String room, boolean stand) {
        try {


            String name = MFServer.firstLetterCaps(name2);
            if (this.server.clients.containsKey(name)) {
                MFServer.executor.schedule(() -> {
                    if (!alive())
                        return;
                    try {
                        this.sendPacket(new login_err(this, 1, name).data());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }, 1, TimeUnit.SECONDS);
                return;
            }

            // this.oldTribulle=true;
            String ip = this.ip;
            if (ip.equals("127.0.0.1")) {
                ip = "62.248.36.169";
            }
            boolean block = false;
            String ip2 = ip;
            Document Ip = MFServer.colIP.find(new Document("query", ip2)).first();
            Document rek = new Document();
            if (Ip == null) {
                try {
                    final HttpParams httpParams = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
                    DefaultHttpClient client = new DefaultHttpClient(httpParams);
                    HttpUriRequest request = new HttpGet("http://ip-api.com/json/" + ip2 + "?fields=country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,reverse,mobile,proxy,query,status,message");
                    HttpResponse response = client.execute(request);
                       /* Gson gson2 = MFServer.get_Gson();
                        Type type2 = new TypeToken<HashMap<String, String>>() {
                        }.getType();*/
                    String body = CharStreams.toString(new InputStreamReader(
                            response.getEntity().getContent(), Charsets.UTF_8));
                    //  HashMap<String, String> hm = gson2.fromJson(new String(b, 0, i), type2);
                    rek = Document.parse(body);
                    if (rek.containsKey("lat") && rek.containsKey("lon")) {
                        rek.append("coordinates", new Document("lat", rek.get("lat")).append("lon", rek.get("lon")));
                        rek.remove("lat");
                        rek.remove("lon");
                    }

                    MFServer.colIP.insertOne(rek);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(ip2 + " a");
                }
            } else {
                rek = Ip;
            }
            this.countryCode = rek.get("countryCode", defDil);
            this.tz = rek.get("timezone", defTz);
            this.country = rek.get("country", defCountry);
            this.city = rek.get("city", defCity);


            this.isp = rek.get("as", "not sure").split(" ", 2)[0];
            if (this.server.BAD_ISP.contains(this.isp) || block) {
                try {
                    this.sendPlayerBan(0,
                            "VPN/Proxy USAGE IS FORBIDDEN.You can contact with us via forum http://mforum.ist", false);
                    this.channel.close();
                    //System.out.println(this.isp);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Kullanici temp = MFServer.colKullanici.find(and(eq("isim", name), eq("sifre", password))).first();
            if (wrongPW > 5) {
                sendPlayerBan(0, "Too many login try", false);
            }
            if (temp == null && !name.startsWith("-")) {
                ArrayList<String> adlar = new ArrayList<>();
                long count = MFServer.colKullanici.countDocuments(eq("isim", "-" + name.toLowerCase()));
                if (count > 0) {
                    adlar.add("-" + name2);
                    count = MFServer.colKullanici.countDocuments(eq("isim", name));
                    if (count > 0) {
                        adlar.add(name2);
                    }
                    ByteBufOutputStream b = MFServer.getStream();
                    try {
                        b.writeByte(26);
                        b.writeByte(12);
                        b.writeByte(11);
                        b.writeUTF(StringUtils.join(adlar, "¤"));
                        b.writeShort(0);
                        this.sendPacket(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }

            if (temp == null || temp.yetki < 1) {
                MFServer.executor.schedule(Unchecked.runnable(() -> {
                    this.sendPacket(new login_err(this, 2, name).data());
                }), 3, TimeUnit.SECONDS);
                wrongPW++;
                return;
            } else if (temp.ban != null) {

                if ((temp.ban.saat * 3600) + temp.ban.when >= MFServer.timestamp() || temp.ban.saat >= 1800) {
                    int saat;
                    if (temp.ban.saat >= 1800) {
                        saat = 1800;
                    } else {
                        saat = temp.ban.saat;//((temp.ban.saat * 3600 + temp.ban.when) - MFServer.timestamp()) / 3600;
                    }
                    this.sendPlayerBan(saat, temp.ban.sebep, false);
                    return;
                } else {
                    temp.ban = null;
                    temp.updateOne("code", "ban");
                }
            }

            this.kullanici = temp;
            fix_tip();
            if (kullanici.birinci > kullanici.peynir) kullanici.peynir = kullanici.birinci;
            kullanici.unvanlar.remove(3438);
            vpn = isVPN(ip, true);
            if (vpn) {
                sendChatServer("<R>WARNING " + name + " logged in with VPN/PROXY IP.");
            }

            if (!kullanici.vpn && vpn) kullanici.vpn = true;
            if (this.kullanici.evInfo == null) this.kullanici.evInfo = new EvInfo();
            this.kullanici.stand = stand;
            int[] a = new int[kullanici.karma.size()];
            for (int i = 0; i < kullanici.karma.size(); i++) {
                a[i] = kullanici.karma.get(i);
            }
            boolean isStaff = (kullanici.yetki > 2 && kullanici.yetki != 6) || kullanici.isTester || this.kullanici.isFashion || kullanici.isFashion || kullanici.isArtist || kullanici.isFuncorp || kullanici.isMapcrew || kullanici.isLuacrew || kullanici.isSentinel;
            if (isStaff || kullanici.fikicoinAlmis) {
                this.kullanici.tagUsage = 100;
            } else {
                this.kullanici.tagUsage = 0;
            }
            if(kullanici.fikicoinAlmis){
                kullanici.rozetler.add(1024);
            }

            this.karma = IntStream.of(a).sum();
            this.checkForCafePoints();
            this.sendPacket(new login_1(this).data());
            if (kullanici.birinci > 10_000 && kullanici.evInfo.tag == 0) {
                this.addInventoryItem(805, 2);
                kullanici.evInfo.tag = 1;
            }
            if (kullanici.birinci > 10_000 && kullanici.evInfo.tag == 1) {
                this.addInventoryItem(805, 2);
                kullanici.evInfo.tag = 2;
            }
            // pc.sendPacket(new login_4(pc).data());
            ByteBuf packet = MFServer.getBuf().writeByte(28).writeByte(2).writeByte(4)
                    .writeInt((int) (System.currentTimeMillis() / 1000L));
            this.sendPacket(packet);
            packet = MFServer.getBuf().writeByte(28).writeByte(3).writeByte(22).writeByte(80).writeByte(10)
                    .writeShort(175);
            this.sendPacket(packet);
            this.sendPacket(MFServer.getBuf().writeByte(99).writeByte(4).writeByte(1));
            this.sendPacket(MFServer.getBuf().writeByte(20).writeByte(27).writeShort(0));
            this.sendPacket(MFServer.getBuf().writeByte(60).writeByte(4).writeBoolean(!this.oldTribulle));// Yeni


            // tribulle
            // için
            // pc.sendPacket(MFServer.getBuf().writeByte(60).writeByte(4).writeByte(0));
            // ByteBuf st=MFServer.getBuf();
            // st.writeByte(26).writeByte(41).writeByte(0);
            // String bb="1dHKGAN2XpAcvbHgC3oxg2hjiFNN3H6chC3s84hTRysRTWpaaT";
            // st.writeBytes(bb.getBytes());
            // pc.sendPacket(st);
            // packet =
     /*   ByteBufOutputStream p1 = MFServer.getStream();
        p1.writeByte(100);
        p1.writeByte(100);
        p1.writeUTF("23-disizhalloween2.jpg");
        this.sendPacket(p1.buffer());*/
            // packet =
            // MFServer.getBuf().writeByte(16).writeByte(9).writeByte(52).writeByte(0);
            // pc.sendPacket(packet);

            if (this.kullanici.evInfo.becerilerSifirlandi == 0) {
                String inval = "{ \"1\" : 5, \"4\" : 1, \"0\" : 5, \"7\" : 1, \"5\" : 5, \"6\" : 5, \"13\" : 1, \"9\" : 5, \"10\" : 1, \"20\" : 5, \"24\" : 1, \"23\" : 5, \"27\" : 1, \"26\" : 5, \"30\" : 1, \"29\" : 5, \"33\" : 1, \"32\" : 5, \"34\" : 1, \"40\" : 5, \"44\" : 1, \"43\" : 5, \"42\" : 5, \"45\" : 5, \"48\" : 5, \"86\" : 5, \"68\" : 5, \"88\" : 5, \"73\" : 1, \"92\" : 1, \"62\" : 5, \"67\" : 1, \"65\" : 5, \"82\" : 5, \"60\" : 5, \"64\" : 1, \"74\" : 5, \"90\" : 1, \"84\" : 1, \"71\" : 5, \"70\" : 1, \"54\" : 1, \"49\" : 5, \"51\" : 5, \"52\" : 5, \"47\" : 1, \"12\" : 5, \"87\" : 1, \"83\" : 5, \"11\" : 5, \"31\" : 5, \"28\" : 5, \"46\" : 5, \"53\" : 1, \"80\" : 5, \"91\" : 1, \"94\" : 1, \"69\" : 5, \"93\" : 5, \"85\" : 5, \"63\" : 5, \"66\" : 5, \"2\" : 5, \"3\" : 5, \"8\" : 5, \"14\" : 1, \"22\" : 5, \"25\" : 5, \"21\" : 5, \"41\" : 5, \"50\" : 1, \"89\" : 5, \"72\" : 5, \"61\" : 5, \"81\" : 5 } ";
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String, Integer>>() {
                }.getType();
                HashMap<String, Integer> hm = gson.fromJson(inval, type);
                hm.forEach((k, v) -> {
                    kullanici.beceriler.put(k, v);
                });
                if (this.kullanici.seviye.seviye != 256) {
                    this.kullanici.seviye.seviye += 155;
                    this.kullanici.evInfo.becerilerSifirlandi = 1;
                }
            }
            for (Map.Entry<String, Integer> entry : kullanici.beceriler.entrySet()) {
                if (entry.getValue() > 5) {
                    kullanici.beceriler.put(entry.getKey(), 5);
                }
            }
            if (this.kullanici.seviye.seviye > 350 && this.kullanici.kayit < 1541364570 && this.kullanici.kayit > 1541278149) {
                this.kullanici.seviye.seviye -= 155;
            }
            this.sendPacket(new login_3(this).data());
            // this.sendBeceriler(false);
            this.sendSeviye();

            // pc.sendPacket(new login_4(pc).data());

            // pc.enterRoom("village");
            this.COMMAND = new Command(this);
            this.state = ConnectionState.LOGINED;
            String prefRoom = room;
            if (prefRoom.length() == 0 || prefRoom.contains(Room.char_3)) {
                prefRoom = "1";
            }
            if (prefRoom.equals("1")) {
                prefRoom = this.server.recommendRoom(this.Lang);
            }
            this.server.clients.put(this.kullanici.isim, this);
            this.server.clients2.put(this.kullanici.code, this);
            String odaAd = prefRoom;
            if (!odaAd.startsWith("*")) {

                if (this.Lang.equals("E2")) odaAd = "*" + odaAd;
                else {
                    odaAd = this.Lang + "-" + odaAd;
                }
            }
            Room ro = this.server.rooms.get(odaAd);
            if (ro != null) {
                if (!ro.password.isEmpty()) {
                    prefRoom = "1";
                }
            }
            int[] cezaList = new int[]{360, 720, 1440, 1800};
            this.enterRoom(prefRoom, false);
            boolean sifir = true;
            int surec = 0;
            for (Ceza ceza : MFServer.colCezaLog.find(new Document("ad", name).append("tip", 0).append("surev", new Document("$gt", 1557618446000L))).sort(new Document("surev", -1)).limit(20)) {
                if (ceza.surec == 0) sifir = true;
                else {
                    if (sifir) {
                        sifir = false;
                        continue;
                    }
                    if (!ceza.sebep.toLowerCase().contains("hack")) continue;
                    for (int s : cezaList) {
                        if (ceza.surec < s) {
                            surec = s;
                        }
                    }
                }
            }
            if (surec >= 720) {
                siciliKotu = true;
            }
            this.TRIBULLE = new Tribulle(this);
            this.sendPing();
            if (this.kullanici.tribeCode != 0) {
                this.server.addClient2Tribe(this, this.kullanici.tribeCode);

            } else {
                this.noTribe();
            }


            int code = this.kullanici.code;
            ArrayList<Document> arkadaslar = this.kullanici.arkadaslar;

            String wt = MFServer.plusReady(this.kullanici);
            MFServer.executor.schedule(Unchecked.runnable(() -> {


                for (Document doc : arkadaslar) {
                    Integer pid = doc.getInteger("pid");
                    PlayerConnection cl = this.server.clients2.get(pid);
                    if (cl != null) {
                        if (cl.friends.contains(code)) {
                            ByteBufOutputStream bs = MFServer.getStream();
                            bs.writeUTF(wt);
                            ByteBuf data = bs.buffer();
                            cl.sendTriPacket2(data, 32);
                        }
                    }
                }
            }), 2, TimeUnit.SECONDS);
            // data.release();
            // pc.sendIgnores();
            //gorevEkle(7,1_000,new Document("img","tasks_teushan.5419").append("odulTip",2).append("kurk",200),"task7",72*3600);

            //  gorevEkle(0,77_777,new Document("img","tasks_teushan.5420").append("odulTip",2).append("kurk",201),"task0",24*3600);

            //  long tt = System.currentTimeMillis();
            Umay();
            ByteBuf data = MFServer.getBuf();
            data.writeByte(kullanici.cinsiyet);
            this.sendTriPacket2(data, 12);
            //  new BenchMark(Unchecked.runnable(this::Umay), "UMAY").run();
            // System.out.println(System.currentTimeMillis()-tt);
            if (this.kullanici.yetki > 1 || this.kullanici.isFuncorp || kullanici.isFashion || this.kullanici.isMapcrew || this.kullanici.isLuacrew || this.kullanici.isSentinel) {
                this.server.addClient2Channel(this, this.Lang, 0);
                this.server.addClient2Channel(this, "GL", 0);
                if (this.kullanici.yetki >= 4) {
                    for (PlayerConnection pc : server.clients.values()) {
                        if (pc.kullanici.yetki >= 4) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#C565FE", pc.Lang, this.kullanici.isim));
                        } else if (pc.kullanici.yetki >= 2) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#B993CA", pc.Lang, this.kullanici.isim));
                        } else if (pc.kullanici.isMapcrew) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#52C6FF", pc.Lang, this.kullanici.isim));
                        } else if (pc.kullanici.isFuncorp) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#ff8547", pc.Lang, this.kullanici.isim));
                        } else if (pc.kullanici.isLuacrew) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#98E2EB", pc.Lang, this.kullanici.isim));
                        } else if (pc.kullanici.isFashion) {
                            this.sendPacket(MFServer.pack_messagewithcolor(pc.kullanici.isim + " : " + pc.room.name, "#F0A78E", pc.Lang, this.kullanici.isim));
                        }
                    }
                }


            }
            this.kullanici.unvanlar.remove(444);
            this.kullanici.unvanlar.remove(440);
            checkStaffTitles();

            if (kullanici.evInfo.sifirla2257 == 0) {
                kullanici.evInfo.sifirla2257 = 1;
                this.kullanici.esyalar.remove("2257");
            }
            if (kullanici.evInfo.sifirla2210 == 0) {
                kullanici.evInfo.sifirla2210 = 1;
                this.kullanici.esyalar.remove("2210");
            }

            this.OrjLang = this.Lang;

            this.kullanici.unvanlar.remove(441);
            this.kullanici.unvanlar.remove(443);
            this.kullanici.unvanlar.remove(455);

            this.kullanici.evInfo.titles.remove("3217");
            this.kullanici.evInfo.titles.remove("3309");
            this.kullanici.evInfo.titles.remove("3234");
            Document r = new Document("target", this.kullanici.isim).append("dev", "Kaanade").append("title", 3303);
            if (MFServer.colMG.find(r).first() != null) {
                this.kullanici.evInfo.titles.remove("3303");
                MFServer.colMG.deleteOne(r);
            }
            if (this.kullanici.fikicoinAlmis) {
                this.kullanici.unvanlar.add(3217);
                    this.kullanici.unvanlar.add(3234);
                this.add_event_title(3609, "fikicoin");
            }
            this.server.incr_logs("giris", 1);
            this.server.rekor("TOTAL", this.server.clients.size());
            // this.server.yukselt_logs("YUK-" + this.OrjLang,
            // this.server.incr_logs(this.OrjLang, 1));
            // this.server.yukselt_logs("YUKSEK",
            // this.server.incr_logs("online", 1));
            SqlData sd = new SqlData();
            sd.typ = 2;
            sd.dil = this.OrjLang;
            sd.name = this.kullanici.isim;
            sd.ip = this.ip;
            sd.country = this.country;
            sd.city = this.city;
            sd.tz = this.tz;
            this.server.CLD.add(sd);
            login.ad = kullanici.isim;
            login.ip = ip;
            login.country = country;
            login.city = city;
            login.tz = tz;
            colGiris.insertOne(login);
            try {
                this.checkBadges();
            } catch (Exception e) {

            }
            if (colHackers.count(and(eq("ip", ip), gt("epoch", MFServer.timestamp() - 60 * 15))) > 0) {
                sendChatServer("<R>WARNING " + name + "</R> logged with Fake Client probably a hack !!!");
            }
            if (colHackers.count(and(eq("ip", ip), gt("epoch", MFServer.timestamp() - 60 * 5))) > 0) {
                server.banPlayer(kullanici.isim, 1800, "Using hack tools is forbidden", "Tengri", false, this.ip);
                sendChatServer("Tengri banned " + kullanici.isim + " for 1800 hours. Reason : Using hack tools is forbidden");
                channel.close();
                return;

            }

            this.sendShamanMode();
            this.sendPacket(MFServer.getBuf().writeByte(28).writeByte(13).writeByte(1));
            long sayi = MFServer.colGorev.count(new Document("pId", kullanici.code).append("bitti", false));
            this.sendPacket(MFServer.getBuf().writeByte(144).writeByte(5).writeByte(sayi > 0 ? 1 : 0));
            this.rekor = MFServer.rekorStats.getOrDefault(this.kullanici.isim, 0);
            if (MFServer.kazananlar.containsKey(this.kullanici.isim) && !this.kullanici.unvanlar.contains(416)) {
                this.add_event_title(416, "election");
                this.add_event_title(417, "election");
                this.add_event_title(418, "election");
                this.add_event_title(419, "election");
                this.add_event_title(420, "election");
                this.add_event_title(421, "election");

            }
            Integer Title = MFServer.kazananlar.get(this.kullanici.isim);
            if (Title != null) {
                add_event_title(Title, "election");
            }
           /* if (System.currentTimeMillis() < 1544146970000L + (60L * 10_000L)) {
                this.add_event_title(3415, "seven");
            }*/
            if (!kullanici.evInfo.sessizSifirlandi) {

                kullanici.sessiz = null;
                kullanici.evInfo.sessizSifirlandi = true;
            }
            if (kullanici.evInfo.fircalar == 0) {
                this.addInventoryItem(2252, 15);
                this.addInventoryItem(2256, 15);
                this.addInventoryItem(2349, 15);
                kullanici.evInfo.fircalar = 1;
            }
            if (kullanici.evInfo.gorevBileti == 0) {
                this.addInventoryItem(776, 2);
                kullanici.evInfo.gorevBileti = 1;
            }
            if (kullanici.evInfo.gorevBileti == 1) {
                this.addInventoryItem(776, 2);
                kullanici.evInfo.gorevBileti = 2;
            }
            if (kullanici.evInfo.gorevBileti == 2) {
                this.addInventoryItem(776, 1);
                kullanici.evInfo.gorevBileti = 3;
                //    gorevEkrani(1);
            }

            if (kullanici.evInfo.gorevBileti == 3) {
                this.addInventoryItem(776, 3);
                kullanici.evInfo.gorevBileti = 4;
                //    gorevEkrani(1);
            }
            if (kullanici.evInfo.gorevBileti == 4) {
                this.addInventoryItem(776, 1);
                kullanici.evInfo.gorevBileti = 5;
                //    gorevEkrani(1);
            }

            if (kullanici.evInfo.sifirlaYilbasi < 2) {
                kullanici.esyalar = kullanici.esyalar.entrySet()
                        .stream()
                        .filter((v) -> {
                            int key = Integer.valueOf(v.getKey());
                            return key < 2110 || key > 2210;
                        })
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (u, v) -> {
                                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                                },
                                LinkedHashMap::new));
                kullanici.evInfo.sifirlaYilbasi = 2;
            }/*if (kullanici.evInfo.sifirlaSevgililerGunu == 0) {
                kullanici.esyalar = kullanici.esyalar.entrySet()
                        .stream()
                        .filter((v) -> {
                            int key = Integer.valueOf(v.getKey());
                            return !((key >= 2212 && key <= 2236) || key == 2481);
                        })
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (u, v) -> {
                                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                                },
                                LinkedHashMap::new));
                kullanici.evInfo.sifirlaSevgililerGunu = 1;
            }*/
           /* if (kullanici.evInfo.sifirlaSummer == 0) {
                kullanici.esyalar = kullanici.esyalar.entrySet()
                        .stream()
                        .filter((v) -> !lll.contains(Integer.valueOf(v.getKey())))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (u, v) -> {
                                    throw new IllegalStateException(String.format("Duplicate key %s", u));
                                },
                                LinkedHashMap::new));
                kullanici.evInfo.sifirlaSummer = 1;
            }*/
            // cadilar();
           /* if(this.kullanici.evInfo.dragonEvent2019Gorevleri==0){
                this.gorevEkle(70,5, new Document("odulTip", 0).append("unvan", 3457),"task70",60*60*24*21);
                this.kullanici.evInfo.dragonEvent2019Gorevleri=1;
            }*/

          /*  if(this.kullanici.evInfo.cadilarHediye==0){
                this.addInventoryItem(2437,3);
                this.sendMessage("<VP>We gave u three Bat pet.</VP>");
                this.kullanici.evInfo.cadilarHediye=1;
            }
            if (this.kullanici.evInfo.yilbasi2018Gorevleri2 == 0) {
                this.kullanici.rozetler.remove(210);
                this.kullanici.rozetler.remove(211);
                this.kullanici.rozetler.remove(212);
                this.kullanici.evInfo.yilbasi2018Gorevleri2 = 1;
            }*/
           /* if (System.currentTimeMillis() < 15457824001000L) {
                this.add_event_title(3434, "xmas");
            }*/
           /* if (kullanici.evInfo.nisan == 0) {

                nisanEvent();
                kullanici.evInfo.nisan = 1;
            }*/
            if (kullanici.evInfo.eksiler == 0) {

                for (Kullanici kullanici2 : MFServer.colKullanici.find(new Document("ref", -kullanici.code)).projection(include("_id", "isim", "birinci", "peynir"))) {
                    MFServer.colKullanici.updateOne(eq("_id", kullanici2.code), set("ref", kullanici.code));
                }
                kullanici.evInfo.eksiler = 1;
            }

            try {
                ArrayList<String> arrayList = new ArrayList<>();
                try (Connection connection = MFServer.DS.getConnection(); PreparedStatement statement = connection.prepareStatement(
                        "select * from log.login where name=? order by time_stamp DESC limit 10;");) {
                    statement.setString(1, kullanici.isim);
                    ResultSet result = statement.executeQuery();
                    while (result.next()) {
                        arrayList.add(result.getString("ip"));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                int[] t = new int[]{0};
                for (Kullanici kul : MFServer.colKullanici.find(new Document("ref", kullanici.code).append("peynir", new Document("$gt", 50_000))).projection(Projections.include("_id", "isim", "birinci", "peynir"))) {
                    boolean found = false;
                    if (kul.refComp) {
                        this.refs.add(kul);
                        continue;
                    }
                    try (Connection connection = MFServer.DS.getConnection(); PreparedStatement statement = connection.prepareStatement(
                            "select * from log.login where name=? order by time_stamp asc limit 10;");) {
                        statement.setString(1, kul.isim);
                        ResultSet result = statement.executeQuery();
                        while (result.next()) {
                            found = arrayList.contains(result.getString("ip"));
                            if (found) break;
                        }
                        if (!found) {

                            MFServer.colKullanici.updateOne(eq("_id", kul.code), set("refComp", true));
                            this.refs.add(kul);
                        } else {
                            if (t[0]++ > 4 || isVPN(result.getString("ip"), true)) {

                                MFServer.colKullanici.updateOne(eq("_id", kul.code), set("ref", -kullanici.code));
                            }
                        }

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            if (refs.size() >= 15) {
                add_event_title(3693, "stats");
            }

            if (karma >= 200) {
                add_event_title(3694, "stats");
            }
            if (karma < -10) {

                add_event_title(3695, "stats");
            }

            if (refs.size() >= 10) {
                this.kullanici.tagUsage = 100;
                this.sendMessage("<VP>You can use your tag command without using a number because you have invited more than 20 player to our game!");
            } else if ((kullanici.chatColor != -1 || kullanici.tagUsage == 100) && !(isStaff || kullanici.fikicoinAlmis)) {
                kullanici.chatColor = -1;
                kullanici.tagUsage = 0;
                this.sendMessage("<R>PAPI SEVEN IS MAD AT YOU :rage:");
            }
            kullanici.tag = safeTag(kullanici.tag);
            kullanici.tag = kullanici.tag.substring(0, Math.min(8, kullanici.tag.length()));

            if (sayi < 2) {
                ArrayList<Integer> secilemez = new ArrayList<>();
                ArrayList<Integer> var = new ArrayList<>();
                for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code))) {
                    int hash = Arrays.hashCode(new int[]{G.tip, G.bitis, G.son});
                    if (G.bitti && G.ilerleme >= G.bitis) {
                        secilemez.add(hash);
                        if (G.odul != null) {
                            var.add(G.odul.hashCode());
                        }
                    } else if (!G.bitti) {
                        secilemez.add(hash);
                        if (G.odul != null) {
                            secilemez.add(G.odul.hashCode());
                        }
                    } else if (kullanici.kurtarma < 3000 && G.tip == 7) secilemez.add(hash);
                    else if (kullanici.kurtarma < 1000 && G.tip == 8) secilemez.add(hash);
                    if (G.tip >= 30 && (G.tip < 33 || G.tip > 38)) secilemez.add(hash);

                }
                ArrayList<Gorev> secilebilir = new ArrayList<>();
                for (Map.Entry<Integer, Gorev> x : MFServer.GorevListesi.entrySet()) {
                    Gorev gorev = x.getValue();
                    int lale = x.getKey();
                    if (!secilemez.contains(lale) && !var.contains(gorev.odul.hashCode()) && (gorev.tip < 30 || (gorev.tip > 33 && gorev.tip < 38))) {
                        secilebilir.add(gorev);
                    }

                }
                if (secilebilir.size() > 0) {
                    int rand = betweenRandom(0, secilebilir.size());
                    Gorev gorev = secilebilir.get(rand);
                    secilebilir.remove(rand);
                    gorevEkle(gorev.tip, gorev.bitis, gorev.odul, gorev.aciklama, gorev.son);
                    if (secilebilir.size() > 0) {
                        rand = betweenRandom(0, secilebilir.size());
                        gorev = secilebilir.get(rand);
                        secilebilir.remove(rand);
                        gorevEkle(gorev.tip, gorev.bitis, gorev.odul, gorev.aciklama, gorev.son);

                    }
                    try {
                        gorevEkrani(1, 0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }


            }




      /*  if (kullanici.evInfo.paskalyaGorevleri != 2) {
            MFServer.GorevListesi.values().stream().filter(k -> k.tip >= 30).forEach(gorev -> {
                gorevEkle(gorev.tip, gorev.bitis, gorev.odul, gorev.aciklama, gorev.son);
            });
            MFServer.executor.schedule(Unchecked.runnable(() -> gorevEkrani(1)), 3, TimeUnit.SECONDS);
            kullanici.evInfo.paskalyaGorevleri = 2;
        }*/
            if (kullanici.evInfo.seviyeSifirlandi == 0) {
                kullanici.beceriler.clear();
                sendBeceriler(false);
                kullanici.evInfo.seviyeSifirlandi = 1;
            }
            int level = 0;
            for (Integer d3 : this.kullanici.beceriler.values()) {
                level += d3;
            }
            if (level > kullanici.seviye.seviye) kullanici.beceriler.clear();
            if (kullanici.evInfo.unvanYenilemesi == 0) {
                this.kullanici.evInfo.titles.remove("3077");
                this.kullanici.evInfo.titles.remove("3152");
                this.kullanici.evInfo.titles.remove("3151");
                this.kullanici.evInfo.titles.remove("3150");
                this.kullanici.evInfo.titles.remove("3036");
                this.kullanici.evInfo.titles.remove("3161");
                this.kullanici.evInfo.titles.remove("3162");
                this.kullanici.evInfo.titles.remove("3163");
                kullanici.evInfo.unvanYenilemesi = 1;
            }
            this.checkAndRebuildTitleList("all");
            this.susturma = MFServer.colCezaLog.find(new Document("ad", this.kullanici.isim).append("tip", 1)).sort(new Document("surev", -1)).first();
            if (MFServer.colBotVerify.countDocuments(eq("gId", kullanici.code)) == 1) {
                add_event_title(3607, "discord");
            } else {
                kullanici.unvanlar.remove(3607);
                kullanici.evInfo.titles.remove("3607");
            }

            if (this.susturma == null || (susturma.surev + (susturma.surec * 3600_000) < System.currentTimeMillis())) {
                this.susturma = MFServer.colCezaLog.find(new Document("ip", this.ip).append("tip", 1)).sort(new Document("surev", -1)).first();
            }
            if (this.susturma == null || (susturma.surev + (susturma.surec * 3600_000) < System.currentTimeMillis())) {
                this.susturma = null;
            }
            if (!this.kullanici.unvanlar.contains(this.kullanici.unvan)) {
                this.kullanici.unvan = 0;
            }
            this.kullanici.ikonlar.add(25);
            this.kullanici.ikonlar.add(26);
            this.kullanici.ikonlar.add(27);
            this.kullanici.ikonlar.add(28);
            if (this.kullanici.rozetler.contains(779) && this.kullanici.rozetler.contains(778)) {
                this.kullanici.ikonlar.add(29);
            }
            //  this.kullanici.rozetler.add(222);
            for (Etkinlik etkinlik : MFServer.etkinlikler) {
                int[] etk = new int[1];
                etkinlik.rozetler.forEach((k, v) -> {
                            if (this.kullanici.rozetler.contains(k)) {
                                this.toplamPuan += v;
                                etk[0] += v;
                            }
                        }
                );

                this.etkinlikPuan.put(etkinlik, etk[0]);
            }
            if (this.kullanici.peynir < 100_000) {
                iletiYolla("helpinfo");
            }

            if (MFServer.Forbidden_names.contains(this.kullanici.isim) && MFServer.mylord) {
                MFServer.executor.schedule(() -> {
                    try {
                        for (PlayerConnection PC : this.server.clients.values()) {
                            PC.sendPacket(MFServer.pack_old(26, 12, new Object[]{"http://mp3.miceforce.com/LBLhGBTS0xg"}));
                            PC.sendMessage("<R>Sith Lord " + this.kullanici.isim + " is back.");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }, 1, TimeUnit.SECONDS);
            }
            if (kullanici.fikicoin >= 3) arti = true;
            if (kullanici.eskiIsimler.size() - 1 + kullanici.fikicoin >= 3) arti = true;
            if (kullanici.isim.contains("+")) arti = true;
            if (kullanici.isim.contains("*")) arti = true;
            long fikidoc = colFikicoinler.count(new Document("consumedById", kullanici.code).append("productId", "3fikicoin"));
            if (fikidoc > 0) {
                arti = true;
            }
            for (String eskiIsim : kullanici.eskiIsimler) {
                if (eskiIsim.equals(kullanici.isim)) continue;
                if (eskiIsim.contains("+")) arti = true;
                if (eskiIsim.contains("*")) arti = true;
                for (Rekorlar rekorlar : MFServer.colRekorlar.find(new Document("rekorlar.ad", eskiIsim))) {
                    for (Rekor rekor : rekorlar.rekorlar) {
                        if (rekor.ad.equals(eskiIsim)) {
                            rekor.ad = kullanici.isim;
                        }
                    }
                    MFServer.colRekorlar.updateOne(eq("_id", rekorlar._id), set("rekorlar", rekorlar.rekorlar), new UpdateOptions().upsert(true));

                }
            }
            this.LuaGame();
            String rastgele = UUID.randomUUID().toString();
            String img = "tasks_progressbars." + rastgele;
            this.sendPacket(L_Room.packImage(img, -4400, 7, -1, -9000, -9000));
            colRandomUUID.insertOne(new Document("ip", this.ip).append("rand", rastgele).append("time", System.currentTimeMillis()));
            this.rand = rastgele;
            MFServer.executor.schedule(() -> {
                try {
                    this.sendPacket(L_Room.packRemoveImage(-4400));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 5, TimeUnit.SECONDS);
            if (kullanici.evInfo.fikicoin200k == 0 && kullanici.fikicoinAlmis) {
                kullanici.evInfo.fikicoin200k = 1;
                this.kullanici.marketCilek+=200_000;
                ByteBufOutputStream bf = MFServer.getStream();
                bf.writeByte(8);
                bf.writeByte(50);
                bf.writeByte(1);
                bf.writeInt(200_000);
                sendPacket(bf);
                sendMessage("As a supporter of the game you have earned 200k fraises");
            }

        } catch (NullPointerException ne) {
            System.out.println("hata2");
            ne.printStackTrace();
            System.out.println(ne.getCause().getMessage());
            MFServer.log.warning(MFServer.stackTraceToString(ne));
        } catch (Exception e) {
            System.out.println("hata");
            e.printStackTrace();
            MFServer.log.warning(MFServer.stackTraceToString(e));
        }


    }

    private void fix_tip() {
        this.kullanici.tip = kullanici.tip.replace("_;", ";");
    }

    private void checkStaffTitles() throws IOException {
        if (this.kullanici.isFuncorp) {

            this.kullanici.unvanlar.add(3312);
            this.kullanici.yildizlar.put("3312", 8);
            sendFC(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#ff8547", this.Lang, this.kullanici.isim));

        } else {

            this.kullanici.unvanlar.remove(3312);
            this.kullanici.yildizlar.remove("3312");
        }
        if (this.kullanici.isLuacrew) {
            sendLua(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#98E2EB", this.Lang, this.kullanici.isim));

        }

        if (this.kullanici.isFashion) {
            sendFashion(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#F0A78E", this.Lang, this.kullanici.isim));
        }
        if (this.kullanici.isMapcrew) {
            sendMC(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#52C6FF", this.Lang, this.kullanici.isim));
            this.kullanici.unvanlar.add(247);
            this.kullanici.yildizlar.put("247", 8);
            this.kullanici.unvanlar.add(3313);
            this.kullanici.yildizlar.put("3313", 8);
        } else {
            this.kullanici.unvanlar.remove(247);
            this.kullanici.unvanlar.remove(3313);
            this.kullanici.yildizlar.remove("247");
            this.kullanici.yildizlar.remove("3313");
        }
        if (this.kullanici.yetki == 2) {
            sendArb(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#B993CA", this.Lang, this.kullanici.isim), "");
        }
        if (this.kullanici.yetki >= 4 && !this.kullanici.isim.equals("Tengri")) {
            sendMod(MFServer.pack_messagewithcolor("$chat.tribu.signaleConnexionMembre", "#C565FE", this.Lang, this.kullanici.isim), "");
        }
        if (this.kullanici.yetki >= 5 && this.kullanici.yetki != 6) {
            this.kullanici.unvanlar.add(247);
            this.kullanici.unvanlar.add(442);
            this.kullanici.yildizlar.put("247", 8);
            this.kullanici.yildizlar.put("442", 8);

            this.kullanici.unvanlar.add(448);
            this.kullanici.yildizlar.put("448", 8);
            this.kullanici.unvanlar.add(445);
            this.kullanici.yildizlar.put("445", 8);

        }

        if (this.kullanici.yetki < 5 || this.kullanici.yetki == 6) {
            this.kullanici.unvanlar.remove(445);
            this.kullanici.yildizlar.remove("445");

            this.kullanici.unvanlar.remove(448);
            this.kullanici.yildizlar.remove("448");

            this.kullanici.unvanlar.remove(247);
            this.kullanici.unvanlar.remove(442);
            this.kullanici.yildizlar.remove("247");
            this.kullanici.yildizlar.remove("442");
        }

        if (this.kullanici.yetki >= 9) {
            this.kullanici.unvanlar.add(440);
            this.kullanici.yildizlar.put("440", 8);
            this.kullanici.unvanlar.add(444);
            this.kullanici.yildizlar.put("444", 8);
            this.kullanici.unvanlar.add(445);
            this.kullanici.yildizlar.put("445", 8);
            this.kullanici.unvanlar.add(446);
            this.kullanici.yildizlar.put("446", 8);
            this.kullanici.unvanlar.add(448);
            this.kullanici.yildizlar.put("448", 8);
        } else {


            this.kullanici.unvanlar.remove(449);
            this.kullanici.unvanlar.remove(446);
            this.kullanici.yildizlar.remove("449");
            this.kullanici.yildizlar.remove("446");
        }

        if (this.kullanici.yetki == 10) {
            this.kullanici.unvanlar.add(442);
            this.kullanici.yildizlar.put("442", 8);
            this.kullanici.unvanlar.add(447);
            this.kullanici.yildizlar.put("447", 8);
            this.kullanici.unvanlar.add(448);
            this.kullanici.yildizlar.put("448", 8);
            this.kullanici.unvanlar.add(449);
            this.kullanici.yildizlar.put("449", 8);
            this.kullanici.unvanlar.add(450);
            this.kullanici.yildizlar.put("450", 8);
            this.kullanici.unvanlar.add(451);
            this.kullanici.yildizlar.put("451", 8);
            this.kullanici.unvanlar.add(452);
            this.kullanici.yildizlar.put("452", 8);
            this.kullanici.unvanlar.add(453);
            this.kullanici.yildizlar.put("453", 8);
            this.kullanici.unvanlar.add(454);
            this.kullanici.yildizlar.put("454", 8);
        } else {
            if (this.kullanici.yetki < 9) {
                this.kullanici.unvanlar.remove(449);
                this.kullanici.yildizlar.remove("449");
                this.kullanici.yildizlar.remove("446");
                this.kullanici.unvanlar.remove(446);
            }

            this.kullanici.unvanlar.remove(449);
            this.kullanici.unvanlar.remove(450);
            this.kullanici.unvanlar.remove(451);
            this.kullanici.unvanlar.remove(452);
            this.kullanici.unvanlar.remove(453);
            this.kullanici.unvanlar.remove(454);
            this.kullanici.yildizlar.remove("451");
            this.kullanici.yildizlar.remove("449");
            this.kullanici.yildizlar.remove("452");
            this.kullanici.yildizlar.remove("453");
            this.kullanici.yildizlar.remove("454");
        }
    }

    public static String safeTag(String tag) {
        return tag.replaceAll("[^A-Za-z0-9+]", "");
//        return tag.replace("'", "").replace("<","").replace(">","").replace("#","");
    }

    private void nisanEvent() {
        int son = 0;
        int dvm = 0;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", 85))) {
            if (G.ilerleme >= G.bitis) {
                son = G.bitis;
            } else if (!G.bitti) {
                dvm = G.bitis;
            }
        }
        int[] svs = {25, 50, 75, 100, 125, 150};
        List<List<Document>> arr = new ArrayList<>();
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3817)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3818)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3819)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3820)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3821)));
        arr.add(Arrays.asList(new Document("img", "chomar_2-png.5502").append("odulTip", 2).append("kurk", 205), new Document("odulTip", 0).append("unvan", 3777), new Document("odulTip", 1).append("rozet", 1008)));
        int i = 0;
        for (int sv : svs) {
            if ((son < sv) && dvm == 0) {
                List<Document> odul = arr.get(i);
                this.gorevEkle(85, sv, odul, "task85", 60 * 60 * 24 * 30);
                break;
            }
            i++;
        }

    }

    private void cadilar() {
        int son = 0;
        int dvm = 0;
        for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code).append("tip", 60))) {
            if (G.ilerleme >= G.bitis) {
                son = G.bitis;
            } else if (!G.bitti) {
                dvm = G.bitis;
            }
        }
        int[] svs = {5, 10, 15, 20, 24, 25};
        List<List<Document>> arr = new ArrayList<>();
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3428), new Document("odulTip", 1).append("rozet", 212).append("img", "altankod202.14117")));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3429)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3430), new Document("odulTip", 1).append("rozet", 211).append("img", "altankod201.14116")));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3431)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3432)));
        arr.add(Arrays.asList(new Document("odulTip", 0).append("unvan", 3433), new Document("odulTip", 1).append("rozet", 210).append("img", "altankod200.14115")));
        int i = 0;
        for (int sv : svs) {
            if ((son < sv) && dvm == 0) {
                List<Document> odul = arr.get(i);
                this.gorevEkle(60, sv, odul, "task60", 60 * 60 * 24 * 30);
                break;
            }
            i++;
        }
    }

    public void giveTitleforMusic(int Title, int Duration) {

        if (!this.kullanici.unvanlar.contains(Title)) {
            if (this.T_Music != null) {
                this.T_Music.cancel(false);
                this.T_Music = null;
            }
            this.T_Music = MFServer.executor.schedule(() -> {

                try {
                    this.add_event_title(Title, "mp3");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }, Duration, TimeUnit.SECONDS);
        }


    }

    private String getItemCustomization(int item) {
        MarketEsya al = this.kullanici.market.get(String.valueOf(item));
        if (al != null) {
            if (al.kisisellestirme) {
                String[] newCustoms = new String[al.renkler.size()];
                int x = 0;
                for (Number custom : al.renkler) {
                    newCustoms[x] = String.format("%06X", (0xFFFFFF & custom.intValue()));
                    x += 1;
                }
                return "_" + StringUtils.join(newCustoms, "+");
            }
        }

        return "";
    }

    public void enterRoom(String r_name, boolean bypass) throws Exception {
        if (last_Room + 150 > System.currentTimeMillis()) return;
        last_Room = System.currentTimeMillis();
        if (!alive())
            return;
        this.isNew = true;
        if (this.room != null) {
            room.removeClient(this);
            MFServer.executor.execute(Unchecked.runnable(this::SaveUser));
        }
        if (this.kullanici.yetki >= 4 && r_name.length() >= 3) {
            if (r_name.charAt(2) == Room.tire) {
                this.server.addClient2Room(this, r_name);
                return;
            }
        }
        if (bypass)
            r_name = "EN-" + r_name;
        if (!r_name.startsWith("*") && !bypass) {

            if (this.Lang.equals("E2")) r_name = "*" + r_name;
            else {
                r_name = this.Lang + "-" + r_name;
            }
        }
        if (r_name.contains("eliteracing") && !r_name.contains(s_tribe)) {
            if ((kullanici.peynir > 1_000_000 && !siciliKotu) || kullanici.yetki >= 4) {
                r_name = "*eliteracing";
            } else {
                r_name = "*notelite";
            }
        }
        this.server.addClient2Room(this, r_name);

    }

    public void startPlay(boolean yeni) throws Exception {
        if (this.room.countStats && (this.room.isDefilante || this.room.isRacing || this.room.isBootcamp || this.room.parkourMode)) {


            byte[] bf = sistem.arrayList.get(0);
            Sistem.writeLong(System.currentTimeMillis(), 0, bf);
            Sistem.writeBoolean(this.room.ayna, 24, bf);
        }
        //koordinatProtos.setAyna(this.room.ayna);
        // koordinatProtos.setMap(this.room.Map.id);
        // koordinatProtos.setStart(System.currentTimeMillis());
        if (yeni) {
            this.sendPacket(new MapData(this.room, this.room.Map).data());
            if (this.room.shamanSkillsData != null) {
                for (ByteBuf buf : this.room.shamanSkillsData) {
                    this.sendPacket(buf);
                }
            }
            MFServer.executor.schedule(() -> {
                try {
                    int t = (int) (this.room.roundTime - (System.currentTimeMillis() - this.room.Started) / 1000);
                    this.sendPacket(new RoundTime(this.room, t).data());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 700, TimeUnit.MILLISECONDS);

            this.sendPacket(room.getPlayerBuf());
            this.sendPacket(new Shaman(this.room).data());
            this.sendPacket(new MapData2(this.room).data());
            this.sendSync();
            for (PlayerConnection pc : this.room.clients.values()) {
                pc.send_isim_renk();
            }

            if (this.room.isMusic) {
                this.room.sendVideo(this);
            }

        } else {
            if (this.pet != null && !this.pet.isEmpty()) {
                if ((long) this.pet.get(1) > System.currentTimeMillis()) {
                    this.sendPlayerPet((int) this.pet.get(0));
                }
            }
        }
        this.sendPacket(MFServer.getBuf().writeByte(5).writeByte(22).writeByte(0).writeByte(120));
        if (this.room.isMulodrome && this.room.mulodromeStart) {
            this.sendPacket(this.room.mulodromeRoundData());
            for (MulodromePlayer mulodromePlayer : this.room.mulodrome.values()) {
                int color = 3689924;
                if (mulodromePlayer.team == 1) color = 16078438;
                this.room.sendAll(MFServer.getBuf().writeByte(29).writeByte(4).writeInt(mulodromePlayer.code).writeInt(color));
            }
        }
//if(this.room.is801Room)this.LuaGame(false,true);

        if (this.room.isFuncorp && this.room.funcorpSettings != null) {

            this.room.funcorpSettings.sendSize(this.kullanici.code);
            this.room.funcorpSettings.sendColor(this);
        }
        room.sendEmoticons(this);

    }

    private void fircaYolla(int renk) throws IOException {
        if (this.susturma != null && this.susturma.surec > 0) {
            return;
        }
        this.fircaBoya = renk;
        this.fircaKullan = false;
        this.sendPacket(MFServer.getBuf().writeByte(100).writeByte(40).writeByte(1).writeShort(3000).writeInt(this.fircaBoya));

    }

    private void sendSync() throws IOException {
        if (this.room.Syncer != -1) {
            this.sendPacket(MFServer.pack_old(8, 21, new Object[]{this.room.Syncer, ""}));
        }
    }

    public void SaveUser() {
        if (this.kullanici.kurkRenk.length() != 6)
            this.kullanici.kurkRenk = "78583a";
        this.kullanici.cevrimici = MFServer.timestamp();
        this.kullanici.unvanlar = this.kullanici.unvanlar.stream().filter((k) -> k >= 0).collect(Collectors.toSet());
        try {
            this.kullanici.updateOne("code", "unvan", "cevrimici", "peynir", "birinci", "bootcamp", "kurtarma", "samanPeynir", "hardKurtarma", "divineKurtarma", "tur", "marketPey", "marketCilek", "ikon", "samanTur", "tip", "samanRenk", "kurkRenk", "ikonlar", "engellenenler", "market", "beceriler", "marketSaman", "esyalar", "tipler", "rozetler", "extraStats", "totem", "karma", "sessiz", "evInfo", "ozelKurkler", "isimRenk", "seviye", "watchIps", "showReports", "tag", "mail", "stand", "cafeBan", "tagUsage", "watch_pw", "unvanlar", "yildizlar", "chatColor", "vpn", "rareTitles", "disableSettings", "disableBeceriler", "optAnnounce", "disableKurkRozet", "disableInventoryPopup", "bestFriends", "luaBan", "disableOnScreen", "disableFriendReqs");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void noTribe() throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeInt(this.kullanici.code);
        p.writeByte(0);
        this.sendTriPacket("ET_ErreurInformationsTribu", p.buffer());
    }

    public void sendProfile(String name) throws Exception {
        if (System.currentTimeMillis() - this.lastProfile < 500) return;
        this.lastProfile = System.currentTimeMillis();
        PlayerConnection cl = this.server.clients.get(name);
        Kullanici ck = null;
        if ((cl != null && !cl.topsecret && !cl.hidden) || name.equals(kullanici.isim)) {
            ck = cl.kullanici;
        } else if (!name.equals(kullanici.isim)) {
            ck = MFServer.colKullanici.find(eq("isim", name)).first();
            if (ck != null) sendMessage("<R>" + name + "</R> is offline.");
        }
        if (ck != null) {
            ByteBufOutputStream p = this.getStream();
            p.writeByte(8);
            p.writeByte(16);
            p.writeUTF(MFServer.plusReady(ck));
            p.writeInt(MFServer.get_avatar(ck.code));
            p.writeInt(ck.kayit);
            int priv = ck.yetki;
            boolean sent = false;
            boolean cont = profileNameCache.contains(ck.code);
            if (priv == 9 || priv == 10) {
                priv = 10;
                if (!cont) {
                    sent = true;
                    iletiYolla("info_mod", ck.isim);
                }
            } else if (priv == 5 || priv == 8) {
                priv = 5;
                if (!cont) {
                    sent = true;
                    iletiYolla("info_mod", ck.isim);
                }
            } else if (ck.isMapcrew) {
                priv = 11;
                if (!cont) {
                    sent = true;
                    iletiYolla("info_mc", ck.isim);
                }
            } else if (ck.isFuncorp) {
                priv = 13;
                if (!cont) {
                    sent = true;
                    iletiYolla("info_fc", ck.isim);
                }
            } /*else if (cl.isHelper) {
                priv = 61;
            }*/
           /* else if (cl.isArtist){
                priv=60;
            }*/
            else if (priv == 2) {
                priv = 1;
            }
            if (priv == 6) priv = 1;
            //<font color='#

            if (yeniSWF) {
                if (priv == 10) {
                    priv = 15408465;
                } else if (priv == 5) {
                    priv = 12238127;//51283 Sentinel
                } else if (priv == 11) {
                    priv = 3044043;
                } else if (priv == 13) {
                    priv = 16745799;
                } else if (ck.isSentinel) {
                    priv = 51283;
                    if (!cont) {
                        sent = true;
                        iletiYolla("info_sentinel", ck.isim);
                    }
                } else if (ck.isArtist) {
                    priv = 10181046;
                    if (!cont) {
                        sent = true;
                        iletiYolla("info_art", ck.isim);
                    }
                } else if (ck.isFashion) {
                    priv = 15771534;
                    if (!cont) {
                        sent = true;
                        iletiYolla("info_fashion", ck.isim);
                    }
                } else if (ck.isTester) {
                    priv = 7832526;
                    if (!cont) {
                        sent = true;
                        iletiYolla("info_tester", ck.isim);
                    }
                } else {
                    priv = 4561820;
                }
                if (cl != null && !sent) {
                    if (cl.pink) {
                        priv = 16758465;
                        if (!cont) {
                            sent = true;
                            iletiYolla("info_pink", ck.isim);
                        }
                    } else if (cl.white) {
                        priv = 11466996;
                        if (!cont) {
                            sent = true;
                            String tip;
                            if (cl.kullanici.cinsiyet == 1) tip = "she";
                            else if (cl.kullanici.cinsiyet == 2) tip = "he";
                            else tip = "they";
                            iletiYolla("info_white", ck.isim, tip);
                        }
                    }

                }
                p.writeInt(priv);
            } else {
                p.writeByte(priv);
            }
            if (sent) {
                profileNameCache.add(ck.code);
            }
            p.writeByte(ck.cinsiyet);
            String _tri = "";
            if (cl != null && cl.tribe != null) {
                _tri = cl.tribe.kabile.isim;
            }
            p.writeUTF(_tri);
            String ruhesi = "";
            if (ck.ruhesi != 0) ruhesi = MFServer.plusReady(MFServer.getIsim(ck.ruhesi));
            p.writeUTF(ruhesi);

            p.writeInt(ck.kurtarma);
            p.writeInt(ck.samanPeynir);
            p.writeInt(ck.birinci);
            p.writeInt(ck.peynir);
            p.writeInt(ck.hardKurtarma);
            p.writeInt(ck.bootcamp);
            p.writeInt(ck.divineKurtarma);
            p.writeShort(ck.unvan);
            Set<Integer> unvanlar = ck.unvanlar;
            if (ck.rareTitles) {
                unvanlar = ck.unvanlar.stream().map(Integer::new).filter(k -> RareTitleList.containsKey(k)).collect(Collectors.toSet());
            }
            p.writeShort(unvanlar.size());
            for (Integer t : unvanlar) {
                p.writeShort(t);
                int y = ck.yildizlar.getOrDefault(String.valueOf(t), 0) + 1;
                p.writeByte(y);
            }
            //String l ="11;48_0+e0a81c,8,0,4,5";//cl.look+";"+ck.samanRenk;
            String l = ck.tip;
            if (ck.tip.split(";")[0].equals("1")) l += ";" + ck.kurkRenk;
            p.writeUTF(l);
            p.writeShort(ck.seviye.seviye);
            ArrayList<Integer> rozetler = sort_desc(ck.rozetler);
            if (kullanici.disableKurkRozet) {
                rozetler = rozetler.stream().filter(x -> !MFServer.BadgeList.containsValue(x)).collect(Collectors.toCollection(ArrayList::new));
            }

            p.writeShort(rozetler.size() * 2);
            for (Integer badg : rozetler) {
             /*   if (badg > 255) {
                    badg = 0;
                }*/
                p.writeShort(badg);

                if (cl != null) {
                    p.writeShort(cl.rozetler2.getOrDefault(badg, 1));
                } else {
                    p.writeShort(0);
                }
            }
            //p.writeShort(0);
            Field[] fields = ck.extraStats.getClass().getFields();
            p.writeByte(fields.length - 3);
            for (Field field : fields) {
                String ad = field.getName();
                if (ad.equals("defilante")) continue;
                if (ad.equals("vanilla_rounds")) continue;
                if (ad.equals("allSham")) continue;
                int[] data = MFServer.LogList.get(ad);
                p.writeByte(data[0]);
                p.writeInt((int) field.get(ck.extraStats));
                p.writeInt(data[1]);
                p.writeByte(data[3]);
            }
            //Map<String, Integer> treeMap = new TreeMap<>(cl.log);

           /* for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
                String key = entry.getKey();
                Integer val = entry.getValue();
                int[] data = MFServer.LogList.get(key);
                /*if (data == null) {
                    continue;
                }
                p.writeByte(data[0]);
                p.writeInt(val);
                p.writeInt(data[1]);
                p.writeByte(data[3]);
            }*/
            p.writeByte(ck.ikon);
            p.writeByte(ck.ikonlar.size());
            for (Integer ik : ck.ikonlar) {
                p.writeByte(ik & 0xFF);
            }
            p.writeByte(1);
            if (cl != null) {
                p.writeInt(cl.toplamPuan);
            } else {
                p.writeInt(0);
            }
            this.sendPacket(p.buffer());
        }
    }

    public void sendMessage(String message) throws IOException {
        //System.out.println(message);
        this.sendPacket(MFServer.pack_message(message));
    }

    public void iletiYolla(String anahtar, Object... args) throws IOException {
        this.sendPacket(MFServer.pack_message(Dil.yazi(this.Lang, anahtar, args)));
    }

    public void sendMessage(String message, String... args) throws IOException {
        this.sendPacket(MFServer.pack_message(message, args));
    }

    private void sendLookChange() throws IOException {
        ByteBufOutputStream p = this.getStream();
        p.writeByte(20);
        p.writeByte(17);
        String[] lookSplited = StringUtils.split(this.kullanici.tip, ";");
        String lookTopList = lookSplited[0].replace("_", "");
        String[] lookList = new String[12];
        String[] LP = lookSplited[1].split(",");

       /* while (LP.length<11){
            List<String> x=Arrays.asList(LP);
            x.add("0");
            LP=x.toArray(new String[LP.length+1]);
        }*/
        // System.out.println(LP.length);
        p.writeByte(Integer.valueOf(lookTopList));
        for (String look : LP) {
            if (look.contains("_")) {
                String[] itemSplited = StringUtils.split(look, "_");
                String dress = itemSplited[0];
                String custom = "";

                try {
                    custom = itemSplited[1];
                } catch (ArrayIndexOutOfBoundsException error) {

                }
                String[] customSplited;
                if (custom.contains("+")) {
                    customSplited = StringUtils.split(custom, "+");
                } else if (!custom.equals("")) {
                    customSplited = new String[1];
                    customSplited[0] = custom;
                } else {
                    customSplited = new String[0];
                }
                p.writeInt(Integer.valueOf(dress)); // Dress
                p.writeByte(customSplited.length); // Customs Lenght

                for (String customSplited1 : customSplited) {
                    try {
                        p.writeInt(Integer.valueOf(customSplited1, 16)); // Custom
                        // Color
                    } catch (Exception e) {
                        p.writeInt(Integer.valueOf("78583a", 16));
                    }
                }
            } else {
                p.writeInt(Integer.valueOf(look)); // Dress
                p.writeByte(0); // Customs Lenght
            }

        }
        int color = 0;
        String MC = this.kullanici.kurkRenk;
        if (!lookTopList.equals("1")) {
            MC = "78583a";
        }
        try {
            color = Integer.valueOf(MC, 16);
        } catch (Exception e) {
            this.kullanici.kurkRenk = "78583a";
        }
        p.writeInt(color); // Mouse Color

        this.sendPacket(p);
    }

    private void build_cheese() {
        build_new(this.kullanici.peynir, MFServer.CheeseTitleList);
    }

    @Override
    public String toString() {
        return this.kullanici.isim;
    }

    private void build_first() {
        build_new(this.kullanici.birinci, MFServer.FirstTitleList);
    }

    private void build_level() {
        build_new(this.kullanici.seviye.seviye, MFServer.LevelTitleList);
    }

    private void build_save() {

        build_new(this.kullanici.kurtarma, MFServer.SaveTitleList);
    }

    private void build_shaman() {

        build_new(this.kullanici.extraStats.allSham, MFServer.ShamanTitleList);
    }

    private void build_shop() {
        final int count = this.kullanici.market.size();
        build_new(count, MFServer.ShopTitleList);
    }

    private void build_new(int count, Map<Integer, Integer> dict) {
        ArrayList<Map.Entry<Integer, Integer>> stream = dict.entrySet().stream().filter((kv) -> (kv.getKey() <= count)).collect(Collectors.toCollection(ArrayList::new));
        boolean over = stream.size() == dict.size();
        stream.forEach((kv) -> {
            this.kullanici.unvanlar.add(kv.getValue());
            if (over) {
                double val = Math.floor((double) count / kv.getKey());
                this.kullanici.yildizlar.put(kv.getValue().toString(), (int) Math.min(val, 8));
            }
        });
    }

    private void build_bootcamp() {
        build_new(this.kullanici.bootcamp, MFServer.BootTitleList);

    }

    private void build_divine() {

        build_new(this.kullanici.divineKurtarma, MFServer.DivineTitleList);
    }

    private void build_defilante() {

        build_new(this.kullanici.extraStats.defilante, MFServer.DefilanteTitleList);
    }

    private void build_hard() {

        build_new(this.kullanici.hardKurtarma, MFServer.HardTitleList);
    }

    private void build_racing() {

        if (kullanici.extraStats.racing_rounds >= 3000) {
            this.kullanici.unvanlar.add(3005);
        }
        if (kullanici.extraStats.racing_podium >= 1000) {
            this.kullanici.unvanlar.add(3007);
        }
        if (kullanici.extraStats.racing_podium >= 25_000) {
            this.kullanici.unvanlar.add(3533);
        }
        if (kullanici.extraStats.racing_podium >= 10_000) {
            this.kullanici.unvanlar.add(3538);
        }
        if (kullanici.extraStats.racing_first >= 5_000) {
            this.kullanici.unvanlar.add(3534);
        }
        if (kullanici.extraStats.racing_first >= 1_000) {
            this.kullanici.unvanlar.add(3583);
        }
        if (kullanici.extraStats.racing_first >= 2000) {
            this.kullanici.unvanlar.add(3584);
        }
        if (kullanici.extraStats.racing_comp_rounds >= 1500) {
            this.kullanici.unvanlar.add(3585);
        }
        if (kullanici.extraStats.racing_rounds >= 1000) {
            this.kullanici.unvanlar.add(3581);
        }
        if (kullanici.extraStats.racing_podium >= 700) {
            this.kullanici.unvanlar.add(3582);
        }

    }

    private void build_vanilla() {
        if (kullanici.extraStats.vanilla_rounds >= 300) {
            this.kullanici.unvanlar.add(3532);
        }
    }

    private void build_kutier() {

        if (kullanici.extraStats.kutier_killed >= 50) {
            this.kullanici.unvanlar.add(3035);
        }
        if (kullanici.extraStats.kutier_killed >= 500) {
            this.kullanici.unvanlar.add(3033);
        }
        if (kullanici.extraStats.kutier_killed >= 1000) {
            this.kullanici.unvanlar.add(3037);
        }
        if (kullanici.extraStats.kutier_survivor >= 50) {
            this.kullanici.unvanlar.add(3034);
        }
        if (kullanici.extraStats.kutier_survivor >= 1000) {
            this.kullanici.unvanlar.add(3055);
        }
        if (kullanici.extraStats.kutier_survivor >= 5000) {
            this.kullanici.unvanlar.add(3042);
        }
        if (kullanici.extraStats.kutier_survivor >= 100) {
            this.kullanici.unvanlar.add(3548);
        }

        if (kullanici.extraStats.kutier_survivor >= 100) {
            this.kullanici.unvanlar.add(3529);
        }
        if (kullanici.extraStats.kutier_killed >= 777) {
            this.kullanici.unvanlar.add(3551);
        }
        if (kullanici.extraStats.kutier_killed >= 555) {
            this.kullanici.unvanlar.add(3549);
        }
        if (kullanici.extraStats.kutier_survivor >= 2000) {
            this.kullanici.unvanlar.add(3547);
        }
        if (kullanici.extraStats.kutier_killed >= 300) {
            this.kullanici.unvanlar.add(3595);
        }
        if (kullanici.extraStats.kutier_killed >= 500) {
            this.kullanici.unvanlar.add(3596);
        }
        if (kullanici.extraStats.kutier_killed >= 700) {
            this.kullanici.unvanlar.add(3599);
        }
        if (kullanici.extraStats.kutier_survivor >= 500) {
            this.kullanici.unvanlar.add(3597);
        }
        if (kullanici.extraStats.kutier_survivor >= 500) {
            this.kullanici.unvanlar.add(3598);
        }


    }

    private void build_survivor() {

        if (kullanici.extraStats.survivor >= 5000) {
            this.kullanici.unvanlar.add(3017);
        }
        if (kullanici.extraStats.survivor >= 500) {
            this.kullanici.unvanlar.add(3018);
        }
        if (kullanici.extraStats.survivor >= 1000) {
            this.kullanici.unvanlar.add(3019);
        }
        if (kullanici.extraStats.survivor_killed >= 2666) {
            this.kullanici.unvanlar.add(3528);
        }

        if (kullanici.extraStats.survivor >= 5000) {
            this.kullanici.unvanlar.add(3539);
        }
        if (kullanici.extraStats.survivor >= 3000) {
            this.kullanici.unvanlar.add(3541);
        }
        if (kullanici.extraStats.survivor_killed >= 2500) {
            this.kullanici.unvanlar.add(3544);
        }
        if (kullanici.extraStats.survivor_killed >= 4000) {
            this.kullanici.unvanlar.add(3545);
        }
        if (kullanici.extraStats.survivor >= 2000) {
            this.kullanici.unvanlar.add(3546);
        }
        if (kullanici.extraStats.survivor_rounds >= 5000) {
            this.kullanici.unvanlar.add(3577);
        }
        if (kullanici.extraStats.survivor >= 3000) {
            this.kullanici.unvanlar.add(3578);
        }
        if (kullanici.extraStats.survivor_killed >= 2500) {
            this.kullanici.unvanlar.add(3579);
        }
        if (kullanici.extraStats.survivor_killed >= 5000) {
            this.kullanici.unvanlar.add(3580);
        }
        if (kullanici.extraStats.survivor_shaman >= 1000) {
            this.kullanici.unvanlar.add(3576);
        }
    }

    private void BurcUnvanlari(long t) {
        // Calendar cal = Calendar.getInstance();
        // cal.setTimeInMillis(this.kullanici.kayit*1000);

        Date tarih = new Date(t);
        int month = tarih.getMonth() + 1;//cal.get(Calendar.MONTH);
        int day = tarih.getDate();//cal.get(Calendar.DAY_OF_MONTH);

        if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 1 && day <= 19))
            this.kullanici.unvanlar.add(3185);
        else if ((month == 1 && day >= 20 && day <= 31) || (month == 2 && day >= 1 && day <= 17))
            this.kullanici.unvanlar.add(3186);
        else if ((month == 2 && day >= 18 && day <= 29) || (month == 3 && day >= 1 && day <= 19))
            this.kullanici.unvanlar.add(3187);
        else if ((month == 3 && day >= 20 && day <= 31) || (month == 4 && day >= 1 && day <= 19))
            this.kullanici.unvanlar.add(3176);
        else if ((month == 4 && day >= 20 && day <= 30) || (month == 5 && day >= 1 && day <= 20))
            this.kullanici.unvanlar.add(3177);
        else if ((month == 5 && day >= 21 && day <= 31) || (month == 6 && day >= 1 && day <= 20))
            this.kullanici.unvanlar.add(3178);
        else if ((month == 6 && day >= 21 && day <= 30) || (month == 7 && day >= 1 && day <= 22))
            this.kullanici.unvanlar.add(3179);
        else if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 1 && day <= 22))
            this.kullanici.unvanlar.add(3180);
        else if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 1 && day <= 22))
            this.kullanici.unvanlar.add(3181);
        else if ((month == 9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22))
            this.kullanici.unvanlar.add(3182);
        else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21))
            this.kullanici.unvanlar.add(3183);
        else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21))
            this.kullanici.unvanlar.add(3184);
    }

    private void only_login() {

        if (this.kullanici.tur >= 1000)
            this.kullanici.unvanlar.add(3006);

        if (this.kullanici.cinsiyet == 1)
            this.kullanici.unvanlar.add(3003);
        if (this.friends.size() >= 120) {
            this.kullanici.unvanlar.add(3029);
        } else if (this.friends.size() < 50) {
            this.kullanici.unvanlar.add(3047);
        }

        int t = MFServer.timestamp();
        if (this.kullanici.kayit < t - 31536000) {
            this.kullanici.unvanlar.add(3030);
        }
        this.kullanici.unvanlar.removeAll(MFServer.zodiacTitles);
        long tt = ((long) this.kullanici.kayit) * 1000;
        this.BurcUnvanlari(tt);
        if (forumDogumGunu != 0) {
            this.BurcUnvanlari(this.forumDogumGunu);
        }
       /* if (this.kullanici.kayit< t-2592000){
            this.BurcUnvanlari();
        }*/
        if (this.kullanici.kayit < t - 63072000) {
            this.kullanici.unvanlar.add(3045);
        }
        if (this.kullanici.kayit < 1438646400) {//Deleted User 2014-2015 Users only
            this.kullanici.unvanlar.add(3741);
        }
        if (this.kullanici.kayit < 1470268800) {//Deleted User 2014-2016 Users only
            this.kullanici.unvanlar.add(3742);
        }
        if (this.kullanici.kayit < t - 2592000) {
            this.kullanici.unvanlar.add(3059);
        }
        if (this.kullanici.kayit < (1469698635) + 60 * 60 * 24 * 365) {
            this.kullanici.unvanlar.add(3031);
        }
        if (kullanici.ruhesi != 0) {
            this.kullanici.unvanlar.add(3041);
        }
        this.kullanici.unvanlar.add(3044);
        this.kullanici.unvanlar.add(3082);
        this.kullanici.unvanlar.add(3062);
        if (this.Oy >= 150) {
            this.kullanici.unvanlar.add(3053);
        }
        if (this.Oy >= 100) {
            this.kullanici.unvanlar.add(3054);
        }
        if (this.Oy >= 30) {
            this.kullanici.unvanlar.add(3056);
        }
        if (this.Oy >= 10) {
            this.kullanici.unvanlar.add(3057);
        }
        if (this.Oy >= 1) {
            this.kullanici.unvanlar.add(3049);
        }
        if (this.Oy >= 1) {
            this.kullanici.unvanlar.add(3531);
        }

        if (this.kullanici.evInfo.n23 >= 20) {
            this.kullanici.unvanlar.add(323);
        }
        if (this.kullanici.evInfo.e31 >= 1) {
            this.kullanici.unvanlar.add(287);
        }

        this.kullanici.evInfo.titles.keySet().forEach((k) -> this.kullanici.unvanlar.add(Integer.valueOf(k)));


        if (refs.size() >= 7) {
            this.kullanici.unvanlar.add(3536);
        }
        this.kullanici.unvanlar.add(3537);
    }

    public void checkBadges() {
        kullanici.unvanlar.remove(2268);
        kullanici.evInfo.titles.remove("2268");
        if (this.kullanici.market.size() > 0) {
            MFServer.BadgeList.forEach((k, v) -> {
                if (this.kullanici.market.containsKey(String.valueOf(k))) {
                    this.kullanici.rozetler.add(v);
                    if (k == 2268) {
                        try {
                            add_event_title(3559, "leaf_fur");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
        if (kullanici.extraStats.racing_rounds >= 1500) {
            this.kullanici.rozetler.add(124);
            rozetler2.put(124, Math.floorDiv(kullanici.extraStats.racing_rounds, 1500));
        }
        //rozetler2.put(124, Math.floorDiv(kullanici.extraStats.racing_rounds, 1500));

        if (kullanici.extraStats.racing_comp_rounds >= 10000) {
            this.kullanici.rozetler.add(125);
            rozetler2.put(125, Math.floorDiv(kullanici.extraStats.racing_comp_rounds, 10_000));
        }
        if (kullanici.extraStats.racing_podium >= 10000) {
            this.kullanici.rozetler.add(127);
            rozetler2.put(127, Math.floorDiv(kullanici.extraStats.racing_podium, 10_000));
        }
        if (kullanici.extraStats.racing_first >= 10000) {
            this.kullanici.rozetler.add(126);
            rozetler2.put(126, Math.floorDiv(kullanici.extraStats.racing_first, 10000));
        }
        if (kullanici.extraStats.survivor_rounds >= 1000) {
            this.kullanici.rozetler.add(120);
            rozetler2.put(120, Math.floorDiv(kullanici.extraStats.survivor_rounds, 1000));
        }
        if (kullanici.extraStats.survivor_shaman >= 800) {
            this.kullanici.rozetler.add(121);
            rozetler2.put(121, Math.floorDiv(kullanici.extraStats.survivor_shaman, 800));
        }
        if (kullanici.extraStats.survivor_killed >= 10000) {
            this.kullanici.rozetler.add(122);
            rozetler2.put(122, Math.floorDiv(kullanici.extraStats.survivor_killed, 10000));
        }
        if (kullanici.extraStats.survivor >= 10000) {
            this.kullanici.rozetler.add(123);
            rozetler2.put(123, Math.floorDiv(kullanici.extraStats.survivor, 10000));
        }
        if (kullanici.extraStats.kutier_survivor >= 10000) {
            this.kullanici.rozetler.add(230);
        }

        rozetler2.put(230, Math.floorDiv(Math.max(kullanici.extraStats.survivor, kullanici.extraStats.kutier_killed), 10000));
        if (kullanici.extraStats.kutier_killed >= 10000) {
            this.kullanici.rozetler.add(230);
        }
        if (kullanici.extraStats.kutier_killed >= 5_000) {
            this.kullanici.rozetler.add(242);
            rozetler2.put(242, Math.floorDiv(kullanici.extraStats.kutier_killed, 5_000));
        }
        if (kullanici.extraStats.kutier_survivor >= 5_000) {
            this.kullanici.rozetler.add(243);
            rozetler2.put(243, Math.floorDiv(kullanici.extraStats.kutier_survivor, 5_000));
        }
        // yeniler

        if (kullanici.extraStats.racing_podium >= 5_000) {
            this.kullanici.rozetler.add(1009);
        }
        if (kullanici.extraStats.racing_first >= 5_000) {
            this.kullanici.rozetler.add(1010);
        }
        if (kullanici.extraStats.racing_first >= 15_000) {
            this.kullanici.rozetler.add(1011);
        }
        //
        if (kullanici.extraStats.survivor >= 5_000) {
            this.kullanici.rozetler.add(1012);
        }
        if (kullanici.extraStats.survivor_shaman >= 2_000) {
            this.kullanici.rozetler.add(1013);
        }
        if (kullanici.extraStats.survivor_killed >= 1_500) {
            this.kullanici.rozetler.add(1014);
        }
        if (kullanici.extraStats.survivor_rounds >= 5_000) {
            this.kullanici.rozetler.add(1015);
        }
        //
        if (kullanici.kurtarma >= 50_000) {
            this.kullanici.rozetler.add(1016);
        }
        if (kullanici.hardKurtarma >= 5_000) {
            this.kullanici.rozetler.add(1026);
        }
        if (kullanici.divineKurtarma >= 5_000) {
            this.kullanici.rozetler.add(1027);
        }
        //
        if (kullanici.extraStats.defilante >= 10_000) {
            this.kullanici.rozetler.add(1017);
        }
        if (kullanici.extraStats.defilante >= 2_000) {
            this.kullanici.rozetler.add(1018);
        }
        if (kullanici.extraStats.defilante >= 500) {
            this.kullanici.rozetler.add(1019);
        }
    }

    public void FriendReq(int code) throws IOException {
        if (kullanici.disableFriendReqs) return;
        if (friends.contains(code)) {
            sendMessage("<VP>" + MFServer.plusReady(MFServer.getIsim(code)) + " has added you back.");

        } else {
            LuaGame();
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(23);
            p.writeInt(-code);
            p.writeByte(1);
            p.writeUTF("<font color='#58dd94'>" + MFServer.plusReady(MFServer.getIsim(code)) + "</font> has sent you friend request\nDo you want to accept?");
            p.writeShort(300);
            p.writeShort(200);
            p.writeShort(230);
            p.writeBoolean(true);
            sendPacket(p);
            reqs.add(code);
        }
    }

    public void checkAndRebuildTitleList(String type) {
        Set<Integer> orj = new HashSet<>();
        orj.addAll(this.kullanici.unvanlar);
        switch (type) {
            case "cheese":
                this.build_cheese();
                break;

            case "first":
                this.build_first();
                break;
            case "save":
                this.build_save();
                break;
            case "shaman":
                this.build_shaman();
                break;

            case "shop":
                this.build_shop();
                break;

            case "bootcamp":
                this.build_bootcamp();
                break;

            case "hard":
                this.build_hard();
                break;
            case "defilante":
                this.build_defilante();
                break;

            case "divine":
                this.build_divine();
                break;
            case "level":
                this.build_level();
                break;
            case "all":
                this.build_cheese();
                this.build_first();
                this.build_level();
                this.build_save();
                this.build_shop();
                this.build_bootcamp();
                this.build_hard();
                this.build_divine();
                this.build_kutier();
                this.build_survivor();
                this.build_racing();
                this.only_login();
                build_vanilla();

                break;
            case "racing":
                this.build_racing();
                break;
            case "survivor":
                this.build_survivor();
                break;
            case "kutier":
                this.build_kutier();
                break;
            case "vanilla":
                this.build_vanilla();
                break;
        }
        if (!type.equals("all")) {
            final Integer[] sum = new Integer[1];
            sum[0] = 0;
            int kc = this.kullanici.code;
            this.kullanici.unvanlar.stream().filter(item -> !orj.contains(item)).forEach((Integer title) -> {

                try {
                    if (MFServer.CheeseTitleList.containsValue(title)) {
                        nextTitle("cheese");
                    } else if (MFServer.FirstTitleList.containsValue(title)) {
                        nextTitle("first");
                    } else if (MFServer.DefilanteTitleList.containsValue(title)) {
                        nextTitle("defilante");
                    } else if (MFServer.BootTitleList.containsValue(title)) {
                        nextTitle("bootcamp");
                    }
                    if (sum[0] < 1) {
                        ByteBuf packet = MFServer.pack_old(8, 14, new Object[]{kc, title});

                        this.room.sendAll(packet);
                    }/* else {
                        this.sendPacket(packet);
                    }*/
                    sum[0] = sum[0] + 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void writePlayerData(ByteBufOutputStream byteBuf) throws IOException {
        String name = this.kullanici.isim;
        if (room.isFuncorp && room.funcorpSettings != null) {
            name = this.room.funcorpSettings.miceNames.getOrDefault(name, name);
        }
        if (room.isMinigame) {
            name = this.room.miceNames.getOrDefault(name, name);
        }
        String _look = this.kullanici.tip;
        String _color = this.kullanici.kurkRenk;
        if (this.room.isBootcamp || this.room.playerCount() > 50) {
            _look = "1;0,0,0,0,0,0,0";
        } else if (this.room.isTutorial) {
            _look = "1;0,0,0,0,0,0,0";
            _color = "ffffff";
        }
        if (this.tempTip != null) {
            _look = this.tempTip;
            this.tempTip = null;
        }
        if (this.tempKurkRenk != null) {
            _color = this.tempKurkRenk;
            this.tempKurkRenk = null;
        }

        int dead = 0;
        if (this.isDead) {
            dead = 1;
        }
        Integer yildiz = this.kullanici.yildizlar.getOrDefault(String.valueOf(this.kullanici.unvan), 0) + 1;

        if (room.TagsEnabled) name = MFServer.plusReady(name, kullanici.tag);
        byteBuf.writeUTF(name);//İSİM
        byteBuf.writeInt(kullanici.code);//PID
        byteBuf.writeBoolean(isShaman);
        byteBuf.writeByte(dead);
        byteBuf.writeShort(Math.min(30_000, score));
        byteBuf.writeBoolean(hasCheese);
        byteBuf.writeShort(kullanici.unvan);//ÜNVAN
        byteBuf.writeByte(yildiz);
        byteBuf.writeByte(kullanici.cinsiyet);
        byteBuf.writeUTF("0");
        byteBuf.writeUTF(_look);
        byteBuf.writeBoolean(true);
        byteBuf.writeInt(Integer.parseInt(_color, 16));
        byteBuf.writeInt(Integer.parseInt(kullanici.samanRenk, 16));
        byteBuf.writeInt(0);
        byteBuf.writeInt(kullanici.chatColor);//İsim renk 3906460
    }


    public void LuaGame() throws IOException {
        LuaGame(true, false);
    }

    private void LuaGame(boolean open, boolean force) throws IOException {
        if ((!open && this.room.isMinigame) && !force) return;
        if (!open) return;
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeBoolean(open);
        this.sendPacket(p.buffer());
    }

    public void LuaGame(boolean open) throws IOException {
        LuaGame(open, false);
    }


    public void bindMouse(boolean yes) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(3);
        p.writeBoolean(yes);
        this.sendPacket(p.buffer());
    }

    public void sendPlayerBan(int hours, String reason, boolean hidden) throws IOException {
        ByteBuf data = MFServer.pack_old(26, 17, new Object[]{(long) hours * 3600000L, reason});

        channel.writeAndFlush(data).addListener(ChannelFutureListener.CLOSE);
        if (this.room != null) {
            if (!hidden) {
                sendPlayerBanMessage(this.kullanici.isim, hours, reason);
            }
            this.server.disconnectIPaddress(this.ip);
        }
    }

    private void sendPlayerBanMessage(String name, int hours, String reason) throws IOException {
        if (hours == 1800) {
            this.room.sendAll(MFServer.pack_message("<ROSE>[$Moderation] $MessageBanDefSalon",
                    name, reason));
        } else {
            this.room.sendAll(MFServer.pack_message("<ROSE>[$Moderation] $Message_Ban",
                    name, String.valueOf(hours), reason));

        }
    }

    public void add_friend(Integer code) throws NoSuchFieldException, IllegalAccessException, IOException {
        if (code == null || code == -1) return;
        // this.kullanici.arkadaslar.removeIf(p -> p.getInteger("pid").equals(code));
        //  this.kullanici.arkadaslar.add(new Document("pid", code).append("tarih", MFServer.timestamp()));
        //  this.kullanici.updateOne("code", "arkadaslar");
        if (friends.size() > 300) iletiYolla("how_many_friends", friends.size() + 1);
        MFServer.colKullanici.updateOne(eq("_id", this.kullanici.code), new Document("$push", new Document("arkadaslar", new Document("pid", code).append("tarih", MFServer.timestamp()))));

    }

    public void remove_friend(Integer code) throws NoSuchFieldException, IllegalAccessException, IOException {
        //  db.kullanicilar.update({"isim":"Sevenops"},{"$pull":{"arkadaslar":{"pid":1}}})
        if (friends.size() > 300) iletiYolla("how_many_friends", friends.size() - 1);
        MFServer.colKullanici.updateOne(eq("_id", this.kullanici.code), new Document("$pull", new Document("arkadaslar", new Document("pid", code))));
        // this.kullanici.arkadaslar.removeIf(p -> p.getInteger("pid").equals(code));
        //  this.kullanici.updateOne("code", "arkadaslar");
    }

    public void sendLama(int num) throws IOException {
        if (this.kullanici.stand) {
            sendMessage("<R>This event doesn't work on standalone!");
            return;
        }
        if (this.room.isLama) {
            if (num != 0) {
                lamaCount += 1;

                sendMessage("<V>" + lamaCount + " / 3");
                if (lamaCount >= 3) {
                    lamaCount = 0;

                    ArrayList<Integer> secilemez = new ArrayList<>();
                    ArrayList<Integer> var = new ArrayList<>();
                    for (Gorev G : MFServer.colGorev.find(new Document("pId", kullanici.code))) {
                        int hash = Arrays.hashCode(new int[]{G.tip, G.bitis, G.son});
                        if (G.bitti && G.ilerleme >= G.bitis) {
                            secilemez.add(hash);
                            if (G.odul != null) var.add(G.odul.hashCode());
                        } else if (!G.bitti) {
                            secilemez.add(hash);
                            if (G.odul != null) var.add(G.odul.hashCode());
                        }

                    }
                    ArrayList<Gorev> secilebilir = new ArrayList<>();
                    for (Map.Entry<Integer, Gorev> x : MFServer.GorevListesiLama.entrySet()) {
                        Gorev gorev = x.getValue();
                        int lale = x.getKey();
                        if (!secilemez.contains(lale) && !var.contains(gorev.odul.hashCode())) {
                            secilebilir.add(gorev);
                        }

                    }
                    if (secilebilir.size() > 0) {
                        int rand = betweenRandom(0, secilebilir.size());
                        Gorev gorev = secilebilir.get(rand);
                        MFServer.executor.execute(() -> {
                            gorevEkle(gorev.tip, gorev.bitis, gorev.odul, gorev.aciklama, gorev.son);
                        });

                        try {
                            gorevEkrani(1, 0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        sendMessage("<CP>You have got all the tasks in this event.");
                    }
                }
            }
            sendPacket(MFServer.pack_old(26, 12, new Object[]{"https://www.youtube.com/watch?v=DHjQk4oh-x0"}));
            lamaExec = MFServer.executor.schedule(() -> {
                try {
                    sendLama(num + 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 20, TimeUnit.SECONDS);
        }
    }

    public void nextTitle(String type) throws IOException {
        TreeMap<Integer, Integer> dict = null;
        int p = 0;
        if (type.equals("defilante")) {

            p = kullanici.extraStats.defilante;
            dict = MFServer.DefilanteTitleList;
        } else if (type.equals("first")) {

            p = kullanici.birinci;
            dict = MFServer.FirstTitleList;

        } else if (type.equals("cheese")) {


            p = kullanici.peynir;
            dict = MFServer.CheeseTitleList;
        } else if (type.equals("bootcamp")) {


            p = kullanici.bootcamp;
            dict = MFServer.BootTitleList;
        }

        Map.Entry<Integer, Integer> entry = dict.higherEntry(p);
        if (entry == null) {
            iletiYolla("completed_all", type);
        } else {
            iletiYolla("next_title_" + type, "$T_" + entry.getValue(), entry.getKey() - p);
        }
    }

    public enum ConnectionState {
        VERIFY,
        VERIFIED,
        LOGINED
    }
}

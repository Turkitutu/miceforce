/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class I_ShamanObject extends Packet {

    public byte direction;
    public int id;
    public short x;
    public short item;
    public short y;
    public short rotation;
    public short vx;
    public short vy;
    public boolean ghost;
    public byte uk;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        buf.readByte();
        id = buf.readInt();
        item = buf.readShort();
        x = buf.readShort();
        y = buf.readShort();

        rotation = buf.readShort();
        vx = buf.readByte();
        vy = buf.readByte();
        ghost = buf.readBoolean();
        uk = buf.readByte();

    }

}

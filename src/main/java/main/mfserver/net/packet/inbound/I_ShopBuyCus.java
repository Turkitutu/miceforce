/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class I_ShopBuyCus extends Packet {

    public int item;
    public boolean withfra;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        item = buf.readShort();
        withfra = buf.readBoolean();

    }

}

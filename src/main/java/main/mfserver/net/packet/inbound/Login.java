/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.main.MFServer;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class Login extends Packet {

    public String name;
    public String password;
    public String url;
    public String room;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        decode(this.BUF);
        name = buf.readUTF();
        password = buf.readUTF();
        url = buf.readUTF();
        room = buf.readUTF();
        // if
        // (!client.server.Name_Regex.matcher(name).find()||name.length()>12){
        // return;
        // }
        // System.out.println(name);

        name = MFServer.firstLetterCaps(name);
    }

}

// client.Login(name, password);

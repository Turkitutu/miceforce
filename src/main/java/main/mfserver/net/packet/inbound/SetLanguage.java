/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class SetLanguage extends Packet {
    public int lang_code;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        lang_code = buf.readByte();

        // System.err.println("set lang");
    }
}

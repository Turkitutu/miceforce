package mfserver.util;

import veritabani.Kullanici;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sevendr on 22.04.2017.
 */
public class Arkadas implements Comparable<Arkadas> {
    public Kullanici kullanici;
    public String oda = "";
    public boolean evli = false;
    public Kullanici owner;
    public Arkadas(Kullanici rs,Kullanici owner) throws SQLException {
        kullanici=rs;
        this.owner=owner;
    }

    @Override
    public int compareTo(Arkadas o) {
        int puan1 = 0;
        int puan2 = 0;
        if (o.evli)
            puan2 += 30;
        if (this.evli)
            puan1 += 30;
        if(owner.bestFriends.contains(o.kullanici.code)){
            puan2+=40;
        }
        if(owner.bestFriends.contains(this.kullanici.code)){
            puan1+=40;
        }
        if (!this.oda.isEmpty()) {
            puan1 += 4;
        }
        if (!o.oda.isEmpty()) {
            puan2 += 4;
        }
        if (this.kullanici.arkadaslar.size() != 0) {
            puan1 += 2;
        }
        if (o.kullanici.arkadaslar.size() != 0) {
            puan2 += 2;
        }
        if (this.kullanici.isim.compareTo(o.kullanici.isim) < 0) {
            puan1 += 1;
        } else {
            puan2 += 1;
        }
        // if(o.cevrimici>this.cevrimici)return 1;
        // else return -1;
        return Integer.compare(puan2, puan1);
    }
}

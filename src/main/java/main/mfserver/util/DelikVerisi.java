package mfserver.util;

import mfserver.main.MFServer;
import org.bson.Document;
import veritabani.Kullanici;
import veritabani.SRC;

import java.io.*;
import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;

public class DelikVerisi {

    public long start;
    public long pey;
    public long delik;
    public boolean ayna;
    public ArrayList<byte[]> arrs=new ArrayList<>();
    public ArrayList<Long> vakitler=new ArrayList<>();
    public ArrayList<Long> pingler=new ArrayList<>();
    public  double fp = 10/3;
    public int harita;
    public int code;
    public boolean isAyna;
    public Long hId;
    public String ad;

    public DelikVerisi(int harita,Long hId,String ad) {
        this.harita = harita;
        this.hId = hId;
        this.ad=ad;
    }
    public static void parse(Long hid, SRC<DelikVerisi> cb){
        MFServer.executor.execute(()->{
            Document del=MFServer.colHoleLog.find(eq("_id",hid)).first();
            if (del==null){
                cb.onResult(null);
            }
            else {
                String ad="";
                Kullanici av=MFServer.colKullanici.find(eq("_id",del.getInteger("code"))).projection(include("isim")).first();
                if(av!=null)ad=av.isim;
                DelikVerisi delikVerisi=new DelikVerisi(del.getInteger("map"),hid,ad);
                try {
                    delikVerisi.parse(del.get("data",org.bson.types.Binary.class).getData());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cb.onResult(delikVerisi);
            }
        });
    }

    public void parse(byte[] st) throws IOException {
      //  System.out.println(st.length);
        ByteArrayInputStream arr = new ByteArrayInputStream(st);
        boolean hasping=(arr.available()-25)%27==0;
        DataInputStream data = new DataInputStream(arr);
        start=data.readLong();
        pey=data.readLong()-start;
        delik=data.readLong()-start;
        this.isAyna=data.readBoolean();
        ayna = isAyna;
        while (arr.available()>0){
            ByteArrayOutputStream bot = new ByteArrayOutputStream();
            DataOutputStream dot=new DataOutputStream(bot);
            Number x=data.readShort()*3.3;
            Number y=data.readShort()*3.3;
            int vx=data.readShort();
            int vy=data.readShort();
            boolean direct1=data.readBoolean();
            boolean direct2=data.readBoolean();
            boolean jump=data.readBoolean();
            long vakit=data.readLong();
            long ping=0;
            if(hasping)ping=data.readLong();
            dot.writeBoolean(direct1);
            dot.writeBoolean(direct2);
            dot.writeShort(0);
            dot.writeShort(x.intValue());
            dot.writeShort(0);
            dot.writeShort(y.intValue());
            dot.writeShort(vx);
            dot.writeShort(vy);
            dot.writeBoolean(jump);
            dot.writeShort(0);
            vakitler.add(vakit-start);
            pingler.add(ping);
            arrs.add(bot.toByteArray());
        }
    }
}

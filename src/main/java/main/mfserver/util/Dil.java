package mfserver.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sevendr on 10.05.2017.
 */
public class Dil {
    static Map<String, String> EN = new HashMap<>();
    static Map<String, String> HE = new HashMap<>();
    static Map<String, String> ES = new HashMap<>();
    static Map<String, String> TR = new HashMap<>();
    static Map<String, String> BR = new HashMap<>();
    static Map<String, String> PL = new HashMap<>();
    static Map<String, String> HU = new HashMap<>();
    static Map<String, String> RO = new HashMap<>();
    static Map<String, String> SK = new HashMap<>();
    static Map<String, String> RU = new HashMap<>();
    static Map<String, String> IT = new HashMap<>();
    static Map<String, String> CZ = new HashMap<>();
    static Map<String, String> CN = new HashMap<>();
    static Map<String, String> LV = new HashMap<>();
    static Map<String, String> HR = new HashMap<>();
    static Map<String, String> NL = new HashMap<>();
    static Map<String, String> BG = new HashMap<>();
    static Map<String, String> DE = new HashMap<>();
    static Map<String, String> AR = new HashMap<>();
    static Map<String, String> PH = new HashMap<>();
    static Map<String, String> VK = new HashMap<>();
    static Map<String, String> LT = new HashMap<>();
    static Map<String, String> ID = new HashMap<>();
    static Map<String, String> FI = new HashMap<>();
    static Map<String, String> JP = new HashMap<>();
    static Map<String, String> FR = new HashMap<>();

    static Map<String, Map<String, String>> diller = new HashMap<>();
    static {
        EN.put("task85","Collect %,d eggs (2020)");
        PL.put("task85","Zbierz %,d jajek (2020)");
        BR.put("task85", "Colete %,d ovos (2020)");
        ES.put("task85","Colecta %,d huevos. (2020)");
        RO.put("task85","Colectează %,d ouă (2020)");
        FR.put("task85","Collecter %,d œufs (2020)");
        HU.put("task85","Gyűjts össze %,d tojást (2020)");
        RU.put("task85","Собери %,d яичек (2020)");
        AR.put("task85","جمع %,d بيضة");
        CZ.put("task85", "Vesta %,d vajíčka (2020)");
        TR.put("task85", "%,d tane yumurta topla (2020)");
    }

    static {
        //Command descriptions.
        EN.put("command_ayshe", "It unlocks <font color='#626ca8'>MiceForce &lt;3</font> title.");
        EN.put("command_mp3", "It lets you play youtube links as mp3 e.g. /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        EN.put("command_music", "It lets you play videos from youtube in tribe house e.g. /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        EN.put("command_crown", "It enables/disables crown picture in racing/bootcamp/defilante rooms.");
        EN.put("command_ping", "It shows your ping.");
        EN.put("command_task", "It shows your tasks.");
        EN.put("command_isimrenk", "It lets you change your name color.");
        EN.put("command_color", "It lets you change your mouse color.");
        EN.put("command_mapcrew", "It shows online mapcrew members.");
        EN.put("command_mod", "It shows online moderators.");
        EN.put("command_sentinel", "It shows online sentinels(forum moderators).");
        EN.put("command_ping2", "It shows your ping | /ping <username> shows player's ping.");
        EN.put("helpinfo", "<VP>Would you like to learn about our commands ?</VP> Use <R>/help</R>");
        EN.put("command_ref", "Helpful command for referral system.");
        EN.put("command_etiket", "You can change your tag with this command eg: /tag 777");
        EN.put("command_karma", "It shows your karma.");
        EN.put("command_karma2", "It shows your karma | /karma <username> shows player's karma.");
        EN.put("command_bf", "You can add them as your best friend !");
        EN.put("command_emoticon", "You can change your emoticon with this command eg: /emoticon 7 also you can close your emoticon by just /emoticon");
        EN.put("lama_event", "New Event! /room lama");
        EN.put("ads", "Please click the ads to support our game");
        EN.put("facebook", "Did you like our Facebook page ? <font color='#76ffef'><a href='https://www.facebook.com/MiceForce/' target='_blank'>click</a></font>");
        EN.put("new_forum", "Did you see our forum ? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>click</a></font>");
        EN.put("record_new", "<J>%s</J> set a new record for this map in <V>%.3f</V> sec");
        EN.put("record", "The record of this map was beaten by <J>%s</J>(<V>%.3f</V>s)");
        EN.put("close", "Close");
        EN.put("gameModes", "Game Modes");
        EN.put("helpcik", "Unlike other games, MiceForce is innovative and developed well.There are unlimited cheese and strawberries in your market, various mini games, colorful titles, different events and has a management that values its players.You can browse the topic MiceForce Staff on the forum to reach the other team members who have been lead by Sevenops. Or you can go directly by clicking on this link » <a href=\"https://goo.gl/0TEZPu\" target=\"_blank\">https://goo.gl/0TEZPu</a>. You can get to list of miceforce colorful titles in forum topic made by <v>Glow<n> <a href=\"https://goo.gl/O9N2Tu\" target=\"_blank\">https://goo.gl/O9N2Tu</a>");
        EN.put("next", "Next");
        EN.put("prev", "Previous");
        EN.put("tasks", "Tasks");
        EN.put("page", "Page");
        EN.put("task0", "Gather %,d cheese.");
        EN.put("task1", "Gather %,d first.");
        EN.put("task2", "Gather %,d bootcamp.");
        EN.put("task3", "Survive %,d rounds in Survivor rooms.");
        EN.put("task4", "Survive %,d rounds in Kutier rooms.");
        EN.put("task5", "Play the game for %s hour(s).");
        EN.put("task6", "Save %,d mice in normal mode.");
        EN.put("task7", "Save %,d mice in divine mode.");
        EN.put("task8", "Save %,d mice in hard mode.");
        EN.put("task9", "Get %,d points in defilante.");
        EN.put("task10", "Kill %,d mice in Kutier.");
        EN.put("task11", "Kill %,d mice in Survivor.");
        EN.put("task12", "Complete %,d rounds as first in Racing.");
        EN.put("task13", "Use your totem %,d times.");
        EN.put("task14", "Get %,d xp as shaman.");
        EN.put("task15", "Earn %,d karma points by reporting bad people.");
        EN.put("hours", "(%d hours remaining)");
        EN.put("minutes", "(%d minutes remaining)");
        EN.put("prizeClaimed", "Prize Claimed");
        EN.put("failed", "Failed");
        EN.put("task30", "Collect %,d eggs");
        EN.put("task31", "Catch %,d rabbits");
        EN.put("task32", "Keep the eggs away from mice for %,d times");
        EN.put("task33", "Complete all Easter tasks.");
        EN.put("task34", "Be the cheese %,d times on the cheesehunter minigame.");
        EN.put("task35", "Catch the cheese %,d times on the cheesehunter minigame.");
        EN.put("task36", "Kill %,d mice as a moldy cheese on cheesehunter minigame.");
        EN.put("task37", "As a cheese, win the round %,d times on the cheesehunter minigame.");
        EN.put("task60", "Help others decorate the Christmas tree %,d times");

        EN.put("task38", "Get revived by the hellhound %,d times");
        EN.put("task39", "Survive the netherworld %,d times. ");
        EN.put("task40", "Die %,d times on the netherworld map.");
        EN.put("fca", "<J>%s</J><FC> has started Funcorp in /room %s");
        EN.put("bot_warning", "Bots has <VP>*</VP> in front of their usernames. They are imitating their previous rounds played. Moreover, their scores on the leaderboard do not count as official ranks.");
        EN.put("announcement_ranking", "{username} is now {title} in {type} ranking.");
        EN.put("daily", "daily");
        EN.put("weekly", "weekly");
        EN.put("monthly", "monthly");
        EN.put("general", "general");
        EN.put("title10000", "(<font color=\"#FFD700\">Emperor of Racing</font>|<font color=\"#B86B77\">Empress of Racing</font>)");
        EN.put("title10001", "<font color=\"#c0c0c0\">(Duke|Duchess) of Racing</font>");
        EN.put("title10002", "<font color=\"#995500\">(Count|Countess) of Racing</font>");
        EN.put("title10003", "(<font color=\"#FFD700\">Emperor of Bootcamp</font>|<font color=\"#B86B77\">Empress of Bootcamp</font>)");
        EN.put("title10004", "<font color=\"#c0c0c0\">(Duke|Duchess) of Bootcamp</font>");
        EN.put("title10005", "<font color=\"#995500\">(Count|Countess) of Bootcamp</font>");
        EN.put("title10006", "(<font color=\"#FFD700\">Emperor of Defilante</font>|<font color=\"#B86B77\">Empress of Defilante</font>)");
        EN.put("title10007", "<font color=\"#c0c0c0\">(Duke|Duchess) of Defilante</font>");
        EN.put("title10008", "<font color=\"#995500\">(Count|Countess) of Defilante</font>");
        EN.put("title10009", "<font color=\"#aa4b6b\">D</font><font color=\"#a0506f\">i</font><font color=\"#955673\">v</font><font color=\"#8b5b77\">i</font><font color=\"#80607b\">n</font><font color=\"#76667f\">e</font><font color=\"#6b6b83\"> </font><font color=\"#637187\">S</font><font color=\"#5b768a\">h</font><font color=\"#537c8e\">a</font><font color=\"#4b8292\">m</font><font color=\"#438795\">a</font><font color=\"#3b8d99\">n</font>");
        EN.put("title10010", "<font color=\"#b0af4a\">H</font><font color=\"#b9b842\">o</font><font color=\"#c2c03a\">l</font><font color=\"#cac931\">y</font><font color=\"#d3d129\"> </font><font color=\"#dcda21\">M</font><font color=\"#e5e219\">o</font><font color=\"#edeb10\">u</font><font color=\"#f6f308\">s</font><font color=\"#fffc00\">e</font>");
        EN.put("title10011", "<font color=\"#c0392b\">P</font><font color=\"#bc3a35\">r</font><font color=\"#b83b3f\">o</font><font color=\"#b43c49\">f</font><font color=\"#b13c53\">a</font><font color=\"#ad3d5d\">n</font><font color=\"#a93e67\">e</font><font color=\"#a53f71\">d</font><font color=\"#a1407b\"> </font><font color=\"#9d4185\">M</font><font color=\"#9a418f\">o</font><font color=\"#964299\">u</font><font color=\"#9243a3\">s</font><font color=\"#8e44ad\">e</font>");
        EN.put("recommend", "Would you like to go to a community with more people");

        RU.put("change_tag", "Изменить тэг");
        RU.put("enable_messages", "Включить/Выключить рейтинговые уведомления.");
        RU.put("change_chat_color", "Изменить цвет ника в чате");
        HU.put("change_tag", "Változtasd meg a taged");
        HU.put("enable_messages", "Engedélyezd/tiltsd a rangsori bejelentéseket.");
        HU.put("change_chat_color", "Változtasd meg a chat színedet");

        TR.put("change_tag", "Etiketi değiştir");
        TR.put("enable_messages", "Sıralama duyurularını kapat/aç");
        TR.put("change_chat_color", "Konuşma rengini değiştir");

        EN.put("how_many_friends", "You have %,d friends in your friend list (limit : <R>500</R>).");
        EN.put("you_can_vote", "You can vote to skip the round.");
        EN.put("vote_stats", "Vote stats %,d / %,d <R>(/skip)</R>");
        EN.put("room_is_unlocked", "<VP>Room owner %s has unlocked the room.");
        EN.put("room_is_locked", "<R>Room owner %s has locked the room.");
        EN.put("cant_set_password", "<R>You can't use it in this room.");
        EN.put("karma_message", "[•] Your report regarding the player %s has been handled. (karma: %,d) ");
        EN.put("command_warn", "Command will choose hours for you eg: /warn username reason");
        EN.put("event_ads", "Click on ads at http://www.miceforce.com and there will be event in %,d minutes.");
        TR.put("event_ads", "http://www.miceforce.com sitedeki reklamlara tıklarsanız %,d dakika içinde event olacak.");

        EN.put("info_mod", "[%s] is a member of the Moderation team. Their job is to enforce the in-game rules and to act upon those who do not!");
        EN.put("info_sentinel", "[%s] is a member of the Sentinel team. The Sentinel's job is to moderate the forums and ensure that everyone is abiding the rules!");
        EN.put("info_fc", "[%s] is a member of the FunCorp team. Within this position, they create rooms of controlled chaos for the entertainment of other players!");
        EN.put("info_mc", "[%s] is a member of the MapCrew team. This position helps to filter the maps within the game an ensure that they are in the best condition possible!");
        EN.put("info_art", "[%s] is a member of the Artists team. Within this position are artists who draw pieces of artwork that will be used for different events, furs and much more!");
        EN.put("info_fashion", "[%s] is a member of the Fashion Squad. Their job is to create outfits for shop!");
        EN.put("info_tester", "[%s] is a member of the Tester Team. Their job is to test minigames/events before they came out!");
        EN.put("info_pink", "[%s] is not a staff member. I believe he/she just likes pink.");
        EN.put("info_white", "[%s] is not a staff member. I believe %s just likes white or he/she is on the side of the white walkers !!!!!!");

        EN.put("change_tag", "Change tag");
        EN.put("enable_messages", "Enable/Disable the ranking announcements.");
        EN.put("change_chat_color", "Change chat color");
        EN.put("disable_settings", "Disable settings button (you can use /settings to enable it back).");
        EN.put("command_settings", "Enable/Disable settings button.");
        EN.put("next_title_first", "<J>The next title for First is « %s » you need to gather %,d more first.");
        EN.put("next_title_cheese", "<J>The next title for Cheese is « %s » you need to gather %,d more cheese.");
        EN.put("next_title_defilante", "<J>The next title for Defilante is « %s » you need to gather %,d more defilante points.");
        EN.put("next_title_bootcamp", "<J>The next title for Bootcamp is « %s » you need to gather %,d more bootcamp.");
        EN.put("completed_all", "<VP>You have completed all %s titles.");
        EN.put("command_skip", "Allows you to collectively vote on skipping rounds.");
        EN.put("disabled_skills", "<JP>You have disabled skills button if you want to upgrade skills please enable it in settings.");
        EN.put("settings", "Settings");
        EN.put("settings_1", "<CH>Change user tag.");
        EN.put("settings_2", "<CH>Change user chat color.");
        EN.put("settings_3", "Disable fur badges in profiles.");
        EN.put("settings_4", "Disable ranking announcements.");
        EN.put("settings_5", "Disable the unspent level points popup.");
        EN.put("settings_6", "Disable popups for earned inventory items.");
        EN.put("settings_7", "Disable on-screen announcements.");
        EN.put("settings_8", "Disable the settings button.");
        EN.put("settings_9", "Disable friend request popups.");

    }

    static {
        ES.put("info_mod", "[%s] es un miembro del equipo de Moderación. ¡Su trabajo es aplicar las reglas en el juego y actuar sobre quienes no las sigan!");
        ES.put("info_sentinel", "[%s] es un miembro del equipo de Sentinelas. ¡Su trabajo es moderar los foros y asegurarse que todos y cada uno sigan las reglas allí!");
        ES.put("info_fc", "[%s] es un miembro del equipo FunCorp. ¡Ellos crean salas de caos, obviamente controlado para el divertimento de los demás jugadores!");
        ES.put("info_mc", "[%s] es un miembro del equipo MapCrew. ¡Ésta posición ayuda a filtrar los mapas en el juego y asegurarse que están en la mejor condición posible!");
        ES.put("info_art", "[%s] es un miembro del equipo de Artistas. ¡En esta posición hay artistas que diseñan piezas de arte que serán usadas en eventos, pieles y mucho más!");
        ES.put("info_pink", "[%s] no es un miembro del Staff. Pensamos que a el/ella simplemente le gusta el rosa.");
        ES.put("event_ads", "Haz click sobre los anuncios en http://www.miceforce.com/ y así habrá un evento en %,d minutos.");
        ES.put("lama_event", "Nuevo Evento! /room lama");
        ES.put("change_tag", "Cambiar tag");
        ES.put("enable_messages", "Habilitar/Deshabilitar los anuncios de ranking.");
        ES.put("change_chat_color", "Cambiar el color de chat.");
        ES.put("how_many_friends", "Tienes %,d amigos en tu lista de amigos. (Límite : <R>500</R>).");
        ES.put("command_ayshe", "Desbloquea el título <font color='#626ca8'>MiceForce &lt;3</font>.");
        ES.put("command_mp3", "Te permitirá reproducir enlaces de YouTube, ej: /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        ES.put("command_music", "Te permitirá reproducir vídeos de YouTube en la casa de tribu, ej: /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        ES.put("command_crown", "Habilita/deshabilita la corona en las salas defilante, racing y bootcamp.");
        ES.put("command_ping", "Para ver tu ping.");
        ES.put("command_task", "Para ver tus tareas.");
        ES.put("command_isimrenk", "Para cambiar el color en tu nombre.");
        ES.put("command_color", "Para cambiar el color de tu ratón.");
        ES.put("command_mapcrew", "Para ver los miembros del mapcrew conectados.");
        ES.put("command_mod", "Para ver los moderadores conectados.");
        ES.put("command_sentinel", "Para ver los centinelas (moderadores del foro) conectados.");
        ES.put("command_ping2", "Para ver tu ping | /ping <username>  para ver el ping del jugador.");
        ES.put("helpinfo", "<VP>¿Te gustaría aprender los comandos que hay?</VP> Usa<R>/help</R>");
        ES.put("ads", "Haz clic en los anuncios para ayudar a mantener el servidor en línea");
        ES.put("next", "Siguiente");
        ES.put("prev", "Anterior");
        ES.put("tasks", "Tareas");
        ES.put("page", "Página");
        ES.put("task0", "Recolectar %,d queso(s)");
        ES.put("task1", "Recolectar %,d queso(s) de primero.");
        ES.put("task2", "Recolectar %,d bootcamp.");
        ES.put("task3", "Sobrevivir %,d rondas en Survivor.");
        ES.put("task4", "Sobrevivir %,d rondas en las salas Kutier.");
        ES.put("task5", "Jugar al juego por %s horas.");
        ES.put("task6", "Salvar %,d ratones en modo normal.");
        ES.put("task7", "Salvar %,d ratone en  modo divino.");
        ES.put("task8", "Salvar %,d ratones en modo difícil.");
        ES.put("task9", "Conseguir %,d puntos en defilante.");
        ES.put("task10", "Matar a %,d ratones en Kutier.");
        ES.put("task11", "Matar a %,d ratones en Survivor.");
        ES.put("task12", "Completa %,d rondas en Racing.");
        ES.put("task13", "Usa tu tótem %,d veces.");
        ES.put("task14", "Consigue %,d puntos de experiencia como chamán.");
        ES.put("task15", "Gana %,d puntos de karma reportando jugadores con mal comportamiento.");
        ES.put("hours", "(%d horas restantes)");
        ES.put("minutes", "(%d minutos restantes)");
        ES.put("prizeClaimed", "Premio reclamado");
        ES.put("failed", "Tarea no completada");
        ES.put("task30", "Colecta %,d huevos.");
        ES.put("task31", "Atrapa %,d conejos.");
        ES.put("task32", "Mantén los huevos a salvo de los ratones %,d veces.");
        ES.put("task33", "Completa todas las tareas de pascua.");
        ES.put("task34", "Se el queso %,d veces en el minijuego cheesehunter.");
        ES.put("task35", "Atrapa el queso %,d veces en el minijuego cheesehunter.");
        ES.put("task36", "Mata %,d ratones siendo un queso podrido en el minijuego cheesehunter.");
        ES.put("task37", "Siendo un queso, gana la ronda %,d veces en el minijuego cheesehunter.");
        ES.put("task38", "Se revivido por el hellhound %,d veces.");
        ES.put("task39", "Sobrevive al inframundo %,d veces. ");
        ES.put("task40", "Muere %,d veces en el mapa del inframundo.");
        ES.put("facebook", "¿Te gustó nuestra página de Facebook? <font color='#76ffef'><a href='https://www.facebook.com/MiceForce/' target='_blank'>Clic</a></font>");
        ES.put("new_forum", "¿Has visto nuestro nuevo foro? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>Clic</a></font>");
        ES.put("record_new", "<J>%s</J> ha batido un nuevo record para este mapa en <V>%.3f</V> segundos");
        ES.put("record", "El record de este mapa fue hecho por<J>%s</J>(<V>%.3f</V> segundos)");
        ES.put("fca", "<J>%s</J><FC> ha comenzado un Funcorp en /sala %s");
        ES.put("announcement_ranking", "{username} es ahora {title} en la clasificación {type}.");
        ES.put("daily", "diaria");
        ES.put("weekly", "semanal");
        ES.put("monthly", "mensual");
        ES.put("general", "general");
        ES.put("you_can_vote", "Puedes votar para saltar ésta ronda.");
        ES.put("vote_stats", "Votación %,d / %,d <R>(/saltar)</R>");
        ES.put("room_is_unlocked", "<VP>El propietario de la sala %s la ha desbloqueado.");
        ES.put("room_is_locked", "<R>El propietario de la sala %s la ha bloqueado.");
        ES.put("cant_set_password", "<R>No puedes usarlo en ésta sala.");
        ES.put("karma_message", "[•] Tu reporte concerniente al jugador %s ha sido resuelto. (karma: %,d) ");

        ES.put("next_title_first", "<J> El siguiente título por First es « %s » Necesitas reunir %,d más First.");

        ES.put("next_title_cheese", "<J> El siguiente título por queso es « %s » Necesitas reunir %,d más queso.");

        ES.put("next_title_defilante", "<J> El siguiente título para Defilante es « %s » Necesitas reunir %,d más puntos de defilante.");

        ES.put("next_title_bootcamp", "<J> El siguiente título para Bootcamp es « %s » Necesitas para reunir %,d más bootcamp.");

        ES.put("completed_all", "<VP> Tú tienes completo todos %s los títulos.");

        ES.put("settings", "Configuración");
        ES.put("settings_1", "<CH>Cambiar el Tag de usuario.");
        ES.put("settings_2", "<CH>Cambiar el color del chat de usuario.");
        ES.put("settings_3", "Desactivar insignias de piel en los perfiles.");
        ES.put("settings_4", "Desactivar los anuncios de ranking.");
        ES.put("settings_5", "Desactivar las ventanas emergentes de los puntos de nivel no gastados.");
        ES.put("settings_6", "Desactivar las ventanas emergentes para los artículos de inventario obtenidos.");
        ES.put("settings_7", "Desactivar los anuncios en pantalla.");
        ES.put("settings_8", "Desactivar el botón de configuración.");
        ES.put("bot_warning", "Los bots tienen <VP>*</VP> delante de su nombre de usuario. Ellos imitan las rondas jugadas por otros jugadores anteriormente. Además, su puntaje no cuenta de manera oficial.");
    }

    static {
        TR.put("info_mod", "[%s] Moderatör ekibinin bir üyesi. Görevi oyun kurallarının uygulandığından emin olmak.");
        TR.put("info_sentinel", "[%s] Sentinel ekibinin bir üyesi. Görevi forum içi düzeni sağlamak!");
        TR.put("info_fc", "[%s] Funcorp ekibinin bir üyesi. Görevi eğlence amaçlı kaotik odalar yaratmak eğer eğlenmek istiyorsan ona mesaj atman yeterli :)");
        TR.put("info_mc", "[%s] Harita ekibinin bir üyesi. Görevi oyundaki haritaların düzenini olmasını sağlamak.");
        TR.put("info_art", "[%s] Sanat ekibinin bir üyesi. Görevi etkinlikler için sanat eserleri ortaya çıkarmak.");
        TR.put("info_pink", "[%s] ekibin bir üyesi değil sanırım sadece pembe rengini seviyor.");
        //Command descriptions.
        TR.put("task60", "Diğerlerine yılbaşı ağacını süslemede %,d kez yardım et.");
        TR.put("command_ayshe", "<font color='#626ca8'>MiceForce &lt;3</font> ünvanını açar.");
        TR.put("command_mp3", "Youtube linklerini mp3 olarak dinlemenize olanak sağlar örnek: /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        TR.put("command_music", "Youtube videolarını kabile evinde oynatmanızı sağlar örnek: /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        TR.put("command_crown", "Taç resmini racing/bootcamp/defilante odalarında açar/kapatır.");
        TR.put("command_ping", "Pinginizi gösterir.");
        TR.put("command_task", "Görevlerinizi gösterir.");
        TR.put("command_isimrenk", "İsim renginizi değiştirmenizi sağlar.");
        TR.put("command_color", "Kürk renginizi değiştirmenizi sağlar.");
        TR.put("command_mapcrew", "Çevrimiçi mapcrew üyelerini gösterir.");
        TR.put("command_mod", "Çevrimiçi yetkilileri gösterir");
        TR.put("command_sentinel", "Çevrimiçi sentinelleri(forum yetkilileri) gösterir");
        TR.put("command_ping2", "Pinginizi gösterir | /ping <isim> kişinin pingini gösterir.");
        TR.put("helpinfo", "<VP>Komutlarımız hakkında bilgi edinmek ister misin ?</VP> <R>/yardım</R> komutunu kullan.");


        TR.put("ads", "Lütfen oyunumuza destek olmak için reklamlara tıklayın");
        TR.put("next", "İleri");
        TR.put("prev", "Geri");
        TR.put("tasks", "Görevler");
        TR.put("page", "Sayfa");
        TR.put("task0", "%,d peynir kazan.");
        TR.put("task1", "%,d tane birincilik kazan.");
        TR.put("task2", "%,d tane bootcamp kazan.");
        TR.put("task3", "Survivor odasında %,d el hayatta kal.");
        TR.put("task4", "Kutier odasında %,d el hayatta kal.");
        TR.put("task5", "Oyunu %s saat boyunca oyna.");
        TR.put("task6", "Normal modda iken %,d fare kurtar.");
        TR.put("task7", "Zor modda iken %,d fare kurtar.");
        TR.put("task8", "Kutsal modda iken %,d fare kurtar.");
        TR.put("task9", "Defilante modunda %,d puan kazan.");
        TR.put("task10", "Kutier odasında %,d fare öldür.");
        TR.put("task11", "Survivor odasında %,d fare öldür.");
        TR.put("task12", "Racing'te %,d turu birinci olarak tamamla.");
        TR.put("task13", "Totemini %,d kez kullan.");
        TR.put("task14", "Şaman olarak %,d XP topla.");
        TR.put("task15", "Kötü insanları rapor ederek %,d karma puanı topla.");
        TR.put("hours", "(%d saat kaldı)");
        TR.put("minutes", "(%d dakika kaldı)");
        TR.put("prizeClaimed", "Ödül alındı");
        TR.put("failed", "Başarısız olundu");
        TR.put("task30", "%,d tane yumurta topla.");
        TR.put("task31", "%,d tane tavşan yakala.");
        TR.put("task32", "Fareleri %,d kez yumurtalardan uzak tut.");
        TR.put("task33", "Bütün Paskalya görevlerini tamamla.");
        TR.put("task34", "cheesehunter minioyununda küflü peynir olarak %,d fare öldür.");
        TR.put("task35", "cheesehunter minioyununda %,d tane peynir yakala.");
        TR.put("task36", "cheesehunter minioyununda küflü peynir olarak %,d fare öldür.");
        TR.put("task37", "cheesehunter minioyununda peynir olarak %,d tur kazan.");
        TR.put("facebook", "Facebook sayfamızı beğendiniz mi ? <font color='#76ffef'><a href='https://www.facebook.com/MiceForce/' target='_blank'>tıklayın</a></font>");
        TR.put("new_forum", "Yeni forumu gördünüz mü ? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>tıklayın</a></font>");
        TR.put("record_new", "<J>%s</J> yeni bir rekor kırarak bu haritayı <V>%.3f</V> saniye içinde bitirdi");
        TR.put("record", "Bu haritanın rekoru <J>%s</J> tarafından kırılmıştır(<V>%.3f</V>s)");
        TR.put("close", "Kapat");
        TR.put("gameModes", "Oyun Modları");
        TR.put("helpcik", "MiceForce, diğer oyunların aksine gelişmiş ve yeniliklere açık bir oyundur. Marketinizde sınırsız peynir ve çilek mevcuttur, çeşitli minioyunları, rengarenk unvanları, değişik etkinlikleri ve gelişmiş oyun modlarıyla oyuncularına değer veren bir yönetime sahiptir. Liderliğini Sevenops'un yaptığı diğer ekip üyelerine ulaşmak için forumdaki MiceForce Staff adlı başlığa göz atabilirsiniz. Veya bu linke tıklayarak direkt gidebilirsiniz » <a href=\"https://goo.gl/0TEZPu\" target=\"_blank\">https://goo.gl/0TEZPu</a>. \n\n<vp><font size=\"15\"><b># Renkli Unvanlar</b></font>\n<bl>«<font color=\"#FF0000\">Illuminati</font>»<bl> - 666 zor mod puanı yapın.\n«<font color=\"#00E2FF\">Tiryaki</font>»<bl> - MiceForce'u 1 yıl oynayın.\n«<font color=\"#7E3A5E\">Amor</font>»<bl> - Bir ruh ikizi yapın.\n\n<n>Tabii ki renkli unvanlarımız bu kadar değil! MiceForce'a ait diğer tüm renkli-renksiz unvanların listesini forumumuzdaki <v>Glow<n> adlı faremiz tarafından açılmış konuda bulabilirsin! <a href=\"https://goo.gl/O9N2Tu\" target=\"_blank\">https://goo.gl/O9N2Tu</a>");

        TR.put("fca", "<J>%s</J><FC> /room %s odasında Funcorp başlattı.");

        TR.put("announcement_ranking", "{username}, {type} sıralamada artık {title}.");
        TR.put("daily", "günlük");
        TR.put("weekly", "haftalık");
        TR.put("monthly", "aylık");
        TR.put("general", "genel");
        TR.put("recommend", "Daha fazla kişinin olduğu bir topluluğa gitmek ister misiniz");
        TR.put("bot_warning", "Botların adlarının başında <VP>*</VP> işareti vardır. Onlar geçmiş oynayışlarını taklit ederler. Bu yüzden skorları ve rekorları sayılmaz.");


        TR.put("settings", "Ayarlar");
        TR.put("settings_1", "<CH>Kullanıcı etiketini değiştir.");
        TR.put("settings_2", "<CH>Sohbet isim rengini değiştir.");
        TR.put("settings_3", "Profildeki kürk rozetlerini gizle.");
        TR.put("settings_4", "Sıralama duyurularını gizle.");
        TR.put("settings_5", "Kullanılmamış seviye göstergesini gizle.");
        TR.put("settings_6", "Kazanılan envanter eşyası simgelerini gizle.");
        TR.put("settings_7", "Oyun ekranı duyurularını etkisizleştir.");
        TR.put("settings_8", "Ayarlar butonunu gizle.");
        TR.put("settings_9", "Arkadaş isteği bildirimlerini kapa.");
    }

    static {
        BR.put("bot_warning", "Os bots tem <VP>*</VP> na frente dos nicknames. Eles estão repetindo rodadas jogadas anteriormente por outros jogadores. Além do mais, as pontuações feitas por eles nas tabelas de rankings não são contadas oficialmente.");
        BR.put("settings", "Configurações");
        BR.put("settings_1", "<CH>Alterar Tag do usuário.");
        BR.put("settings_2", "<CH>Alterar cor do Chat do usuário.");
        BR.put("settings_3", "Desabilitar todas as medalhas de pelos no perfil.");
        BR.put("settings_4", "Desabilitar os anúncios do Ranking.");
        BR.put("settings_5", "Desabilitar as notificações de pontuação de níveis não gastos.");
        BR.put("settings_6", "Desabilitar as notificações de itens recebidos pra o inventário.");
        BR.put("settings_7", "Desabilitar os anúncios na tela.");
        BR.put("settings_8", "Desabilitar o botão de configurações.");
        BR.put("info_mod", "[%s] é um membro da equipe de Moderadores. Seu trabalho é cumprir com as regras do jogo e punir aqueles que não as seguem!");
        BR.put("info_sentinel", "[%s] é um membro da equipe de Sentinelas. O trabalho de um Sentinela é moderar o fórum e assegurar que todos estão seguindo as regras!");
        BR.put("info_fc", "[%s] é um membro da equipe FunCorp. Com esse cargo, eles podem criar salas com caos controlado para o entretenimento dos jogadores!");
        BR.put("info_mc", "[%s] é um membro da equipe MapCrew. Membros com essa posição ajudam a filtrar mapas do jogo e garantir sempre que eles estejam nas melhores condições possíveis!");
        BR.put("info_art", "[%s] é um membro da equipe dos Artistas. Dentro desse cargo estão os artistas, eles que desenham as artes, itens e peles que são usadas para diferentes eventos e além disso, muitas outras coisas!");
        BR.put("info_pink", "[%s] não faz parte de nenhuma equipe. Nós acreditamos que ele/ela só gosta de rosa.");
        BR.put("event_ads", "Clique nos anúncios contidos no http://www.miceforce.com/ e teremos um evento em %,d minutos.");
        BR.put("task60", "Ajude os demais ratinhos a decorar à Árvore de Natal %,d vezes");
        BR.put("change_tag", "Mudar tag");
        BR.put("enable_messages", "Habilitar/Desabilitar anúncios do ranking.");
        BR.put("change_chat_color", "Mudar a cor do chat");
        BR.put("how_many_friends", "Você tem %,d amigos adicionados em sua lista. (limite máximo : <R>500</R>).");

        BR.put("lama_event", "Novo Evento! /sala lama");
        BR.put("ads", "Por favor, clique nos anúncios para apoiar nosso jogo.");
        BR.put("next", "Próximo");
        BR.put("prev", "Anterior");
        BR.put("tasks", "Tarefas");
        BR.put("page", "Página");
        BR.put("task0", "Colete %,d queijo(s)");
        BR.put("task1", "Colete %,d queijo(s) em primeiro.");
        BR.put("task2", "Colete %,d bootcamp.");
        BR.put("task3", "Sobreviva %,d rodadas no Survivor.");
        BR.put("task4", "Sobreviva %,d rodadas nas salas Kutier.");
        BR.put("task5", "Jogue o jogo por %s horas.");
        BR.put("task6", "Salve %,d ratos no modo normal.");
        BR.put("task7", "Salve %,d ratos no modo divino.");
        BR.put("task8", "Salve %,d ratos no modo hard.");
        BR.put("task9", "Pegue %,d pontos na defilante.");
        BR.put("task10", "Mate %,d ratos nas salas Kutier.");
        BR.put("task11", "Mate %,d ratos nas salas Kutier.");
        BR.put("task12", "Complete %,d rodadas como primeiro na Racing.");
        BR.put("task13", "Use seu totem %,d vezes.");
        BR.put("task14", "Obtenha %,d pontos de experiência como shaman.");
        BR.put("task15", "Ganhe %,d pontos de karma reportando jogadores com mau comportamento.");
        BR.put("hours", "(%d horas restantes)");
        BR.put("minutes", "(%d minutos restantes)");
        BR.put("prizeClaimed", "Prêmio Requerido");
        BR.put("failed", "Falhou");
        BR.put("task30", "Colete %,d ovos");
        BR.put("task31", "Pegue %,d coelhos");
        BR.put("task32", "Mantenha os ovos longe dos ratos por %,d vezes");
        BR.put("task33", "Complete todas as tarefas da Páscoa");
        BR.put("task34", "Seja o queijo %,d vezes no minigame cheesehunter.");
        BR.put("task35", "Pegue o queijo %,d vezes no minigame cheesehunter.");
        BR.put("task36", "Mate %,d rato como um queijo mofado no minigame cheesehunter.");
        BR.put("task37", "Como um queijo, ganhe a rodada %,d vezes no minigame cheesehunter.");
        BR.put("task38", "Seja revivido pelo Cão Infernal %,d vezes");
        BR.put("task39", "Sobreviva o mundo dos mortos %,d vezes.");
        BR.put("task40", "Morra %,d vezes no mapa do Inferno.");

        BR.put("new_forum", "Você já viu nosso novo fórum ? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>Clique aqui</a></font>");
        BR.put("novo_recorde", "<J>%s</J> Fez um novo recorde neste mapa em <V>%.3f</V> sec");
        BR.put("record", "O recorde deste mapa foi batido por<J>%s</J>(<V>%.3f</V>s)");
        BR.put("fca", "<J>%s</J><FC> iniciou  Funcorp na /sala %s");
        BR.put("command_mp3", "Permite que você reproduza links do youtube como mp3, por exemplo /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        BR.put("command_ayshe", "Ele desbloqueia o título <font color='#626ca8'>MiceForce &lt;3</font>");
        BR.put("command_music", "Permite que você reproduza vídeos no seu cafofo de tribo, por exemplo /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        BR.put("command_crown", "Ativa/desativa a imagem da coroa nas salas racing/bootcamp/defilante");
        BR.put("command_ping", "Mostra seu ping.");
        BR.put("command_task", "Mostra suas tarefas.");
        BR.put("command_isimrenk", "Permite alterar a cor do seu nickname.");
        BR.put("command_color", "Permite alterar a cor do seu rato.");
        BR.put("command_mapcrew", "Mostra os membros da mapcrew online.");
        BR.put("command_mod", "Mostra os moderadores online.");
        BR.put("command_sentinel", "Mostra os sentinelas online (moderadores do fórum).");
        BR.put("command_ping2", "Mostra seu ping | /ping <nickname> mostra o ping do jogador.");
        BR.put("helpinfo", "<VP>Você gostaria de aprender sobre nossos comandos?</VP> Use <R>/help</R>");
        BR.put("announcement_ranking", "{username} agora é {title} no ranking {type}.");
        BR.put("daily", "diário");
        BR.put("weekly", "semanal");
        BR.put("monthly", "mensal");
        BR.put("general", "geral");
        BR.put("you_can_vote", "Você pode votar para pular a partida");
        BR.put("vote_stats", "Quantidade de votos %,d / %,d <R>(/skip)</R>");
        BR.put("room_is_unlocked", "<VP>O dono da sala %s acaba de remover senha da sala.");
        BR.put("room_is_locked", "<R>O dono da sala %s trancou a sala com uma senha.");
        BR.put("cant_set_password", "<R>Você não pode usá-lo nesta sala.");

        BR.put("karma_message", "[•] Sua denúncia sobre o jogador %s foi atendida. (karma: %,d) ");

        BR.put("next_title_first", "<J>O próximo título por first é « %s » você precisa coletar mais %,d firsts.");

        BR.put("next_title_cheese", "<J>O próximo título por queijo é « %s » você precisa coletar para mais %,d queijos.");

        BR.put("next_title_defilante", "<J>O próximo título por Defilante é « %s » você precisa coletar mais  %,d para pontos de Defilante.");

        BR.put("next_title_bootcamp", "<J>O próximo título por Bootcamp é « %s » você precisa coletar mais  %,d bootcamp.");

        BR.put("completed_all", "<VP>Você completou todos %s títulos.");
    }

    static {
        PL.put("settings", "Ustawienia");
        PL.put("settings_1", "<CH>Zmień tag.");
        PL.put("settings_2", "<CH>Zmień kolor nazwy użytkownika na czacie.");
        PL.put("settings_3", "Wyłącz odznaki za futra w profilu.");
        PL.put("settings_4", "Wyłącz ogłoszenia rankingowe.");
        PL.put("settings_5", "Wyłącz powiadomienia o niewykorzystanych punktach poziomu.");
        PL.put("settings_6", "Wyłacz powiadomienia o zdobytych itemach.");
        PL.put("settings_7", "Wyłącz komunikaty na ekranie.");
        PL.put("settings_8", "Wyłącz przycisk ustawień.");
        PL.put("lama_event", "Nowy Event! /room lama");
        PL.put("change_tag", "Zmień swój tag");
        PL.put("enable_messages", "Włącz / Wyłącz wiadomości rankingowe");
        PL.put("change_chat_color", "Zmień swój kolor nicku na czacie");
        PL.put("how_many_friends", "Masz %,d znajomych na swojej liście (Limit : <R>500</R>).");
        PL.put("command_ayshe", "Odblokowuje tytuł <font color='#626ca8'>MiceForce &lt;3</font>.");
        PL.put("command_mp3", "Odtwarza piosenkę np. /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        PL.put("command_music", "Odtwarza film w ekranach w chatkach ppemiennych np. /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        PL.put("command_crown", "Włącza lub wyłącza koronę w pokojach Racing/Bootcamp/Defilante.");
        PL.put("command_ping", "Wyświetla twój ping.");
        PL.put("command_task", "Wyświetla listę zadań.");
        PL.put("command_isimrenk", "Zmienia kolor twojej nazwy.");
        PL.put("command_color", "Zmienia kolor twojej myszki");
        PL.put("command_mapcrew", "Wyświetla listę członków MapCrew online w grze.");
        PL.put("command_mod", "Wyświetla listę moderatorów online w grze.");
        PL.put("command_sentinel", "Wyświetla listę Sentineli online w grze.");
        PL.put("command_ping2", "Wyświetla twój ping. Użycie /ping <nick> wyświetla ping podanego gracza.");
        PL.put("helpinfo", "<VP>Chcesz się dowiedzieć jakie mamy tu komendy? </VP>Wpisz <R>/help</R>");
        PL.put("ads", "Klikając w reklamy wspierasz naszą grę");
        PL.put("next", "Następna");
        PL.put("prev", "Poprzednia");
        PL.put("tasks", "Zadania");
        PL.put("page", "Strona");
        PL.put("task0", "Zbierz następującą ilość serków: %,d.");
        PL.put("task1", "Zbierz następującą ilość pierwszych miejsc: %,d.");
        PL.put("task2", "Ukończ następującą ilość map typu Bootcamp: %,d.");
        PL.put("task3", "Przetrwaj następującą ilość rund w trybie Survivor: %,d");
        PL.put("task4", "Przetrwaj następującą ilość rund w trybie Kutier: %,d.");
        PL.put("task5", "Graj w MiceForce przez następującą ilość godzin: %s.");
        PL.put("task6", "Uratuj następującą ilość myszek jako szaman: %,d.");
        PL.put("task7", "Uratuj następującą ilość myszek w trybie boskim: %,d.");
        PL.put("task8", "Uratuj następującą ilość myszek w trybie trudnym: %,d.");
        PL.put("task9", "Zbierz następującą ilość punktów w Defilante: %,d.");
        PL.put("task10", "Zabij następującą ilość myszy w trybie Kutier: %,d.");
        PL.put("task11", "Zabij następującą ilość myszy w trybie Survivor: %,d");
        PL.put("task12", "Ukończ następującą liczbą rund Racing jako pierwszy: %,d");
        PL.put("task13", "Użyj swojego totemu następującą ilość razy: %,d");
        PL.put("task14", "Zdobądź następującą ilość PD: %,d");
        PL.put("task15", "Zdobądź następującą ilość karmy: %,d.");
        PL.put("hours", "(Pozostały czas: %d godzin)");
        PL.put("minutes", "(Pozostały czas: %d minut)");
        PL.put("prizeClaimed", "Otrzymano nagrodę");
        PL.put("failed", "Nie ukończono");
        PL.put("task30", "Zbierz %,d jajek");
        PL.put("task31", "Złap %,d zajączków");
        PL.put("task32", "Trzymaj jajka z dala od myszy %,d razy");
        PL.put("task33", "Ukończ wszystkie zadania związane z Wielkanocą");
        PL.put("task34", "Bądź serem %,d razy w minigrze cheesehunter.");
        PL.put("task35", "Złap ser %,d razy w minigrze cheesehunter.");
        PL.put("task36", "Zabij %,d myszek jako spleśniały ser w minigrze cheesehunter.");
        PL.put("task37", "Jako ser, wygraj rundę %,d razy w minigrze cheesehunter.");
        PL.put("task38", "Zostań ożywionym(-oną) przez Piekielnego Ogara następującą ilosć razy: %,d");
        PL.put("task39", "Przetrwaj w piekle następującą ilość razy: %,d");
        PL.put("task40", "Zgiń w piekle następującą ilość razy: %,d.");
        PL.put("new_forum", "Czy widziałeś nasze forum? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>Kliknij</a></font>");
        PL.put("fca", "<J>%s</J> <FC>aktywował tryb FunCorp na /room %s");
        PL.put("record_new", "Gracz <J>%s</J> ustanowił nowy rekord dla tej mapy (<V>%.3f</V>s)");
        PL.put("record", "Rekord tej mapy został pobity przez <J>%s</J>(<V>%.3f</V>s)");
        PL.put("announcement_ranking", "Gracz {username} osiągnął tytuł {title} w rankingu {type}.");
        PL.put("daily", "dziennym");
        PL.put("weekly", "tygodniowym");
        PL.put("monthly", "miesięcznym");
        PL.put("general", "ogólnym");
        PL.put("task60", "Pomóż w udekorowaniu choinki następującą ilość razy: %,d");
        PL.put("you_can_vote", "Możesz zagłosować, aby pominąć rundę.");
        PL.put("vote_stats", "Statystyki głosowania %,d / %,d <R>(/skip)</R>");
        PL.put("room_is_unlocked", "<VP>Właściciel pokoju %s zdjął hasło.");
        PL.put("room_is_locked", "<R>Właściciel pokoju %s założył hasło.");
        PL.put("cant_set_password", "<R>Nie możesz tego użyć w tym pokoju.");

        PL.put("karma_message", "[•] Twoje zgłoszenie dotyczące gracza %s zostało rozpatrzone. (karma: %,d) ");
        PL.put("info_mod", "[%s] jest członkiem zespołu moderacyjnego. Zadaniem moderatorów jest egzekwowanie zasad w grze i podejmowanie działań w stosunku do tych, którzy ich nie przestrzegają!");
        PL.put("info_sentinel", "[%s] jest członkiem zespołu moderacyjnego Forum. Pracą Sentineli jest moderowanie forum i upewnianie się, że wszyscy przestrzegają zasad!");
        PL.put("info_fc", "[%s] jest członkiem zespołu FunCorp. Osoby na tej pozycji tworzą pokoje rozrywki dla innych graczy!");
        PL.put("info_mc", "[%s] jest członkiem zespołu Mapcrew. Ta pozycja pomaga filtrować mapy w grze i zapewnia, że ​​są one w najlepszym stanie!");
        PL.put("info_art", "[%s] jest członkiem zespołu Artystów. Tę pozycję zajmują artyści, których dzieła sztuki będą wykorzystywane do różnych wydarzeń. Zajmują się również tworzeniem futer i wielu innych rzeczy.");
        PL.put("info_pink", "[%s] nie jest członkiem zespołu. Myślę, że po prostu lubi różowy kolor.");

        PL.put("next_title_first", "<J>Następny tytuł za zdobyte pierwsze miejsca to « %s » potrzebujesz jeszcze %,d zdobytych pierwszych miejsc.");

        PL.put("next_title_cheese", "<J>Nastepny tytuł za zebrany ser to « %s » potrzebujesz jeszcze %,d sera.");

        PL.put("next_title_defilante", "<J>Następny tytuł za Defilante to « %s » potrzebujesz jeszcze %,d punktów.");

        PL.put("next_title_bootcamp", "<J>Następny tytuł za Bootcamp to « %s » potrzebujesz jeszcze %,d bootcampów.");

        PL.put("completed_all", "<VP>Zdobyłeś/aś wszystkie %s tytułów.");
        PL.put("bot_warning", "Boty mają  <VP>*</VP> przed swoimi nazwami. Naśladują swoje poprzednie rozegrane rundy. Co więcej, ich noty w tabeli wyników nie liczą się jako oficjalne.");
    }

    static {
        HU.put("bot_warning", "A botoknak a nevük előtt egy <VP>*</VP> található. Az előző játszott körüket utánozzák. A pontszámuk nem számít bele a ranglistába.");
        HU.put("info_mod", "[%s] tagja a Moderátor csapatnak. A feladatuk az, hogy betartassák a játék szabályait és cselekedjenek azok ellen akik nem!");
        HU.put("info_sentinel", "[%s] tagja a Sentinel csapatnak. A Sentinelek feladata az, hogy moderálják a forumot és biztosítsák, hogy mindenki betartsa a szabályokat!");
        HU.put("info_fc", "[%s] tagja a FunCorp csapatnak. Ők ebben a pozícióban szobákat tudnak létrehozni egy kezelhető káosszal, ahol együtt szórakozhatnak a játékosokkal.");
        HU.put("info_mc", "[%s] tagja a MapCrew csapatnak. Ez a pozíció segít abban, hogy a csapat tagjai kiszűrjék a pályákat és a legjobb állapotban biztosítsák azokat a játékban!");
        HU.put("info_art", "[%s] tagja a Művész csapatnak. Ebben a pozícióban művészek vannak, akik olyan alkotásokat készítenek, amelyeket különféle eseményekre, bundákra és még sok másra használhatunk fel!");
        HU.put("info_pink", "[%s] nem tagja a staffnak. Úgy véljük, ő csak szereti a pink színt.");
        HU.put("event_ads", "Kattolj a hírdetésekre a http://www.miceforce.com/ -on és %,d percen belül event lesz.");
        HU.put("command_ayshe", "Megszerezheted a <font color='#626ca8'>MiceForce &lt;3</font> címet");
        HU.put("command_mp3", "Lejátszhatsz YouTube linkeket mp3-ként p.l.: /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        HU.put("command_music", "Lejátszhatsz YouTube videókat a törzsházban p.l.: /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        HU.put("command_crown", "Megjelenítheted/eltűntetheted a koronát racing/bootcamp/defilante szobákban");
        HU.put("command_ping", "Megmutatja a pinged");
        HU.put("command_task", "Megmutatja a feladataidat");
        HU.put("command_isimrenk", "Megváltoztathatod a neved színét.");
        HU.put("command_color", "Megváltoztathatod az egered színét");
        HU.put("command_mapcrew", "Megmutatja az online pályalegénységi tagokat");
        HU.put("command_mod", "Megmutatja az online moderátorokat");
        HU.put("command_sentinel", "Megmutatja az online sentineleket (fórum moderátorok)");
        HU.put("command_ping2", "Megmutatja a pinged | /ping <username> megmutatja a játékos ping-jét");
        HU.put("helpinfo", "<VP>Meg akarod ismerni a parancsainkat?</VP> Use <R>/help</R>");
        HU.put("ads", "Kérlek kattints a hirdetésekre a játékunk támogatásához");
        HU.put("next", "Következő");
        HU.put("prev", "Előző");
        HU.put("tasks", "Feladatok");
        HU.put("page", "Oldal");
        HU.put("task0", "Gyűjts %,d sajtot.");
        HU.put("task1", "Gyűjts %,d wint.");
        HU.put("task2", "Gyűjts %,d bootcamp-et.");
        HU.put("task3", "Élj túl %,d kört a Túlélő szobában");
        HU.put("task4", "Élj túl %,d kört Kutier módban");
        HU.put("task5", "Játssz a játékkal %s órát.");
        HU.put("task6", "Ments meg %,d egeret normál módban.");
        HU.put("task7", "Ments meg %,d egeret felséges módban.");
        HU.put("task8", "Ments meg %,d egeret nehéz módban.");
        HU.put("task9", "Szerezz %,d pontot a Defilante szobában.");
        HU.put("task10", "Ölj meg %,d egeret a Kutier szobában.");
        HU.put("task11", "Ölj meg %,d egeret a Túlélő szobában.");
        HU.put("task12", "Racingon érj be %,d alkalommal elsőnek.");
        HU.put("task13", "Használd a totemed %,d alkalommal.");
        HU.put("task14", "Szerezz meg %,d szintet sámánként.");
        HU.put("task15", "Szerezz %,d karma pontot rossz emberek jelentésével.");
        HU.put("hours", "(%d óra maradt)");
        HU.put("minutes", "(%d perc maradt)");
        HU.put("prizeClaimed", "Jutalom begyűjtve");
        HU.put("failed", "Sikertelen");
        HU.put("task30", "Gyűjts össze %,d tojást");
        HU.put("task31", "Kapj el %,d nyulat");
        HU.put("task32", "Tartsd távol a tojásokat az egerektől %,d alkalommal");
        HU.put("task33", "Teljesítsd az összes húsvéti feladatot");
        HU.put("task34", "Legyél a sajt %,d alkalommal a cheesehunter mini játékban.");
        HU.put("task35", "Fogd el a sajtot %,d alkalommal a cheesehunter mini játékban.");
        HU.put("task36", "Ölj meg %,d egeret penészes sajtként a cheesehunter mini játékban.");
        HU.put("task37", "Nyerd meg a kört sajtként %,d alkalommal a cheesehunter mini játékban.");
        HU.put("task38", "Élesszen újra a Pokol kutyája %,d alkalommal.");
        HU.put("task39", "Éld túl a Túlvilágot %,d alkalommal. ");
        HU.put("task40", "Halj meg %,d alkalommal a Túlvilágban.");
        HU.put("facebook", "Lájkoltad már a Facebook Oldalunkat? <font color='#76ffef'><a href='https://www.facebook.com/MiceForce/' target='_blank'>Kattints</a></font>");
        HU.put("new_forum", "Láttad már az új fórumunkat? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>Kattints</a></font>");
        HU.put("you_can_vote", "Szavazhatsz arra, hogy kihagyd ezt a kört.");
        HU.put("vote_stats", "Eddigi szavazatok: %,d / %,d <R>(/skip)</R>");
        HU.put("room_is_unlocked", "<VP>A szoba tulajdonosa, %s feloldotta a szobát.");
        HU.put("room_is_locked", "<R>A szoba tulajdonosa, %s lezárta a szobát.");
        HU.put("cant_set_password", "<R>Ezt nem tudod ebben a szobában használni.");

        HU.put("karma_message", "[•] A %s nevezetű játékosról érkezett jelentésed el lett intézve. (karma: %,d) ");
        HU.put("next_title_first", "<J>A következő  Első címhez  « %s » még  %,d   elsőként megszerzett sajtot kell gyűjtened.");

        HU.put("next_title_cheese", "<J>A következő Sajt címhez  « %s »  még %,d sajtot kell gyűjtened.");

        HU.put("next_title_defilante", "<J>A következő Defilante címhez « %s » még %,d  pontot kell gyűjtened.");

        HU.put("next_title_bootcamp", "<J>A következő Bootcamp címhez « %s » még  %,d bootcampet kell gyűjtened.");

        HU.put("completed_all", "<VP>Teljesítetted az összes %s címet.");
        HU.put("settings", "Beállítások");
        HU.put("settings_1", "<CH>Tag megváltoztatása.");
        HU.put("settings_2", "<CH>Chat szín megváltoztatása.");
        HU.put("settings_3", "A bundák kitűzőinek kikapcsolása a profilban.");
        HU.put("settings_4", "A rangsorok bejelentéseinek kikapcsolása.");
        HU.put("settings_5", "Az el nem használt sámán pontok felugró értesítésének kikapcsolása.");
        HU.put("settings_6", "A megszerzett raktári dolgok felugró értesítéseinek kikapcsolása.");
        HU.put("settings_7", "A képernyőn megjelenő bejelentések kikapcsolása.");
        HU.put("settings_8", "A beállítások gomb kikapcsolása.");
    }

    static {
        RO.put("bot_warning", "Boții au <VP>*<VP> în fața numelor lor. Aceștia imită rundele precedente ale altor jucători. În plus, scorul lor în clasament nu este tratat ca un scor oficial.");
        RO.put("settings", "Setări");
        RO.put("settings_1", "<CH>Schimbă tag-ul utilizatorului.");
        RO.put("settings_2", "<CH>Schimbă culoarea din chat a utilizatorului.");
        RO.put("settings_3", "Dezactivează insignele de blăni din profiluri.");
        RO.put("settings_4", "Dezactivează anunțurile clasamentelor.");
        RO.put("settings_5", "Dezactivează punctele de nivel neutilizate.");
        RO.put("settings_6", "Dezactivează ferestrele pop-up pentru elementele din inventar obținute.");
        RO.put("settings_7", "Dezactivează anunțurile de pe ecran");
        RO.put("settings_8", "Dezactivează butonul pentru setări.");
        RO.put("info_mod", "[%s] este un membru al echipei de Moderatori. Ei au rolul de a impune regulile în joc și de a acționa asupra celor care le încalcă!");
        RO.put("info_sentinel", "[%s] este un membru al echipei de Sentinele. Rolul Sentinelelor este de a modera forumul și de a se asigura că toată lumea respectă regulile!");
        RO.put("info_fc", "[%s] este un membru al echipei FunCorp. Cu această poziție, ei creează săli distractive pentru jucători!");
        RO.put("info_mc", "[%s] este un membru al echipei MapCrew. Această poziție ajută la introducerea mapelor și la asigurarea faptului că acestea sunt în cea mai bună stare posibilă!");
        RO.put("info_art", "[%s] este un membru al echipei de Artiști. În această poziție sunt artiștii, care desenează opere de artă pentru evenimente, blănuri și multe altele!");
        RO.put("info_pink", "[%s] nu este un membru staff. Credem că lui/ei îi place doar rozul.");
        RO.put("task60", "Ajută alți jucători să decoreze bradul de Crăciun de %,d ori");
        RO.put("announcement_ranking", "{username} este acum {title} în clasamentul {type}");
        RO.put("daily", "zilnic");
        RO.put("weekly", "săptămânal");
        RO.put("monthly", "lunar");
        RO.put("general", "general");
        RO.put("fca", "<J>%s</J><FC> a început FunCorp pe  /sala %s");
        RO.put("ads", "Vă rugăm apăsați pe reclame pentru a sprijini jocul");
        RO.put("new_forum", "Ai văzut noul forum? <font color='#76ffef'><a href='http://mforum.ist' target='_blank'>Click aici</a></font>");

        RO.put("next_title_first", "<J>Următorul titlu pentru First este « %s » trebuie să adunați %,d first-uri.");

        RO.put("next_title_cheese", "<J> Următorul titlu pentru Brânză este « %s » trebuie să adunați %,d brânze.");

        RO.put("next_title_defilante", "<J> Următorul titlu pentru Defilante este « %s » trebuie să adunați %,d puncte de la Defilante.");

        RO.put("next_title_bootcamp", "<J> Următorul titlu pentru Bootcamp este « %s » trebuie să adunați %,d bootcamp-uri.");

        RO.put("completed_all", "<VP>Ai completat toate cele %s titluri.");
    }

    static {
        HE.put("${trad#TG_7}", "Shaman Apprentice");
        HE.put("${trad#TG_6}", "Initiated");
        HE.put("${trad#tribu.nouv}", "New member");
        HE.put("${trad#TG_5}", "Hunteress");
        HE.put("${trad#TG_4}", "Recruiter");
        HE.put("${trad#TG_0}", "Stooge");
        HE.put("${trad#TG_8}", "Tribe\'s Shaman");
        HE.put("${trad#TG_9}", "Spiritual Chief");
        HE.put("${trad#tribu.chef}", "Spiritual chief");
        HE.put("${trad#tribu.memb}", "Member");
        HE.put("${trad#TG_3}", "Treasurer");
        HE.put("${trad#TG_2}", "Soldier");
        HE.put("${trad#TG_1}", "Cooker");
    }

    static {
        PL.put("${trad#TG_7}", "Uczeń Szamana");
        PL.put("${trad#TG_6}", "Wtajemniczony");
        PL.put("${trad#tribu.nouv}", "Nowy członek");
        PL.put("${trad#TG_5}", "Łowczyni");
        PL.put("${trad#TG_4}", "Rekruter");
        PL.put("${trad#TG_0}", "Pomagier");
        PL.put("${trad#TG_8}", "Szaman Plemienny");
        PL.put("${trad#TG_9}", "Duchowy Wódz");
        PL.put("${trad#tribu.chef}", "Duchowy wódz");
        PL.put("${trad#tribu.memb}", "Członek");
        PL.put("${trad#TG_3}", "Skarbnik");
        PL.put("${trad#TG_2}", "Żołnierz");
        PL.put("${trad#TG_1}", "Kucharz");
    }

    static {
        SK.put("${trad#TG_7}", "Šamanov učeň");
        SK.put("${trad#TG_6}", "Zahájený");
        SK.put("${trad#tribu.nouv}", "Nový člen");
        SK.put("${trad#TG_5}", "Lovec");
        SK.put("${trad#TG_4}", "Rekrút");
        SK.put("${trad#TG_0}", "Bábka");
        SK.put("${trad#TG_8}", "Šaman kmeňa");
        SK.put("${trad#TG_9}", "Duchovný vodca");
        SK.put("${trad#tribu.chef}", "Duchovný vodca");
        SK.put("${trad#tribu.memb}", "Člen");
        SK.put("${trad#TG_3}", "Pokladník");
        SK.put("${trad#TG_2}", "Vojak");
        SK.put("${trad#TG_1}", "Kúchár");
    }

    static {
        RO.put("lama_event", "Event Nou! /sala lama");
        RO.put("command_ayshe", "Deblochează titlul <font color='#626ca8'>MiceForce &lt;3</font>.");
        RO.put("command_mp3", "Aceasta îți permite să redai link-uri de pe Youtube ca mp3 e.g. /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        RO.put("command_music", "Aceasta îți permite să redai videoclipuri de pe Youtube în casa tribului e.g. /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        RO.put("command_crown", "Aceasta activează/dezactivează coroana în sălile racing/bootcamp/defilante.");
        RO.put("command_ping", "Aceasta îți arată ping-ul tău.");
        RO.put("command_task", "Aceasta îți arată sarcinile tale.");
        RO.put("command_isimrenk", "Aceasta îți permite să schimbi culoarea numelui tău.");
        RO.put("command_color", "Aceasta îți permite să schimbi culoarea șoricelului tău.");
        RO.put("command_mapcrew", "Aceasta îți arată membrii MapCrew online.");
        RO.put("command_mod", "Aceasta îți arată moderatorii online.");
        RO.put("command_sentinel", "Aceasta îți arată sentinelele online(moderatorii forumului).");
        RO.put("command_ping2", "Aceasta îți arată ping-ul tău | /ping (nume de utilizator) arată ping-ul acelui jucător.");
        RO.put("helpinfo", "<VP>Ți-ar plăcea să afli mai multe despre comenzile noastre?</VP> Folosește <R>/help</R>");
        RO.put("next", "Următorul");
        RO.put("prev", "Anterior");
        RO.put("tasks", "Sarcini");
        RO.put("page", "Pagină");
        RO.put("task0", "Adună %,d brânză");
        RO.put("task1", "Adună %,d brânze pe primul loc.");
        RO.put("task2", "Adună %,d bootcamp.");
        RO.put("task3", "Supraviețuiește %,d runde în sălile de Survivor.");
        RO.put("task4", "Supraviețuiește %,d runde în sălile de Kutier.");
        RO.put("task5", "Joacă jocul %s oră(e).");
        RO.put("task6", "Salvează %,d șoareci în modul normal.");
        RO.put("task7", "Salvează %,d șoareci în modul divin.");
        RO.put("task8", "Salvează %,d șoareci în modul greu.");
        RO.put("task12", "Completează %,d runde pe primul loc în modul Racing.");
        RO.put("task13", "Folosește-ți totemul de %,d ori.");
        RO.put("task14", "Obține %,d experiență ca shaman.");
        RO.put("task15", "Câștigă %,d puncte de karma prin raportarea jucătorilor care încalcă regulile.");
        RO.put("hours", "(%d ore rămase)");
        RO.put("minutes", "(%d minute rămase)");
        RO.put("prizeClaimed", "Premiul a fost revendicat");
        RO.put("failed", "Eșuat");
        RO.put("task30", "Colectează %,d ouă");
        RO.put("task31", "Prinde %,d iepuri");
        RO.put("task32", "Păstrează ouăle departe de șoricei de %,d ori");
        RO.put("task33", "Completează toate sarcinile de Paște");
        RO.put("task34", "Fii brânza de %,d ori în minijocul cheesehunter.");
        RO.put("task35", "Prinde brânza de %,d ori în minijocul cheesehunter.");
        RO.put("task36", "Omoară %,d șoricei ca brânză mucegăită în minjocul cheesehunter.");
        RO.put("task37", "Ca brânză, câștigă runda de %,d ori în minijocul cheesehunter.");
        RO.put("task38", "Fii înviat de demon de %,d ori");
        RO.put("task39", "Supraviețuiește în iad de %,d ori");
        RO.put("task40", "Mori de %,d ori în iad.");
        RO.put("${trad#TG_7}", "Ajutor de Șaman");
        RO.put("${trad#TG_6}", "Inițiat");
        RO.put("${trad#tribu.nouv}", "Membru nou");
        RO.put("${trad#TG_5}", "Vânător");
        RO.put("${trad#TG_4}", "Recrutor");
        RO.put("${trad#TG_0}", "Servitor");
        RO.put("${trad#TG_8}", "Șamanul Tribului");
        RO.put("${trad#TG_9}", "Șef Spiritual");
        RO.put("${trad#tribu.chef}", "Șef Spiritual");
        RO.put("${trad#tribu.memb}", "Membru");
        RO.put("${trad#TG_3}", "Trezorier");
        RO.put("${trad#TG_2}", "Soldat");
        RO.put("${trad#TG_1}", "Bucătar");
        RO.put("you_can_vote", "Poți vota pentru a schimba runda.");
        RO.put("vote_stats", "Voturi: %,d / %,d <R>(/skip)</R>");
        RO.put("room_is_unlocked", "<VP>Propietarul sălii %s a scos parola.");
        RO.put("room_is_locked", "<R>Propietarul sălii %s a pus o parolă.");
        RO.put("cant_set_password", "<R>Nu poți pune o parolă pe această sală.");

        RO.put("karma_message", "[•] Raportul împotriva jucătorului %s a fost verificat. (karma: %,d) ");
    }

    static {
        RU.put("settings", "Настройки");
        RU.put("settings_1", "<CH>Изменить тег пользователя.");
        RU.put("settings_2", "<CH>Изменить цвет чата.");
        RU.put("settings_3", "Отключить меховые значки в профилях.");
        RU.put("settings_4", "Отключить рейтинговые объявления.");
        RU.put("settings_5", "Отключить всплывающее окно с неизрасходованными уровнями.");
        RU.put("settings_6", "Отключить всплывающие окна для заработанных предметов инвентаря.");
        RU.put("settings_7", "Отключить экранные объявления.");
        RU.put("settings_8", "Отключить кнопку настроек.");
        RU.put("info_mod", "[%s] является членом команды Модерации. Их задача - обеспечивать соблюдение внутриигровых правил и действовать против тех, кто их не соблюдает!");
        RU.put("info_sentinel", "[%s] является членом команды Сентинель. Сентинельсы отвечают за модерирование форума и следят за тем, чтобы все соблюдали правила!");
        RU.put("info_fc", "[%s] является членом команды ФанКорп. На этой должности они могут создавать комнаты с управляемым хаосом, где они могут развлекаться с игроками!");
        RU.put("info_mc", "[%s] является членом команды МапКрю. Эта должность помогает отфильтровать карты в игре и убедиться, что они находятся в лучшем состоянии!");
        RU.put("info_art", "[%s] является членом команды Артист. На этой должности есть художники, которые создают работы, которые можно использовать для различных мероприятий, мехов и многое другое!");
        RU.put("info_pink", "[%s] не является членом персонала. Мы считаем, что ему / ей нравится просто розовый цвет.");
        RU.put("task60", "Помоги нарядить Рождественскую Ёлку следующее кол-во раз: %,d");
        RU.put("ads", "Пожалуйста кликайте на рекламные баннеры, чтобы поддержать игру");
        RU.put("${trad#TG_7}", "Начинающий шаман");
        RU.put("${trad#TG_6}", "Посвящённый");
        RU.put("${trad#tribu.nouv}", "Новый участник");
        RU.put("${trad#TG_5}", "Охотница");
        RU.put("${trad#TG_4}", "Вербовщик");
        RU.put("${trad#TG_0}", "Марионетка");
        RU.put("${trad#TG_8}", "Шаман племени");
        RU.put("${trad#TG_9}", "Духовный лидер");
        RU.put("${trad#tribu.chef}", "Духовный лидер");
        RU.put("${trad#tribu.memb}", "Член племени");
        RU.put("${trad#TG_3}", "Казначей");
        RU.put("${trad#TG_2}", "Солдат");
        RU.put("${trad#TG_1}", "Кухарка");
        RU.put("next", "Следующая");
        RU.put("prev", "Предыдущая");
        RU.put("tasks", "Задания");
        RU.put("page", "Страница");
        RU.put("task0", "Соберите %,d сыра.");
        RU.put("task1", "Соберите %,d первых мест.");
        RU.put("task2", "Соберите %,d bootcamp-ов.");
        RU.put("task3", "Продержитесь %,d раундов в Сурвиворе.");
        RU.put("task4", "Продержитесь %,d раундов в Кутиере.");
        RU.put("task5", "Играйте в игру %s часа(ов).");
        RU.put("task6", "Спасите %,d мышек в режиме шамана.");
        RU.put("task7", "Спасите %,d мышек в божественном режиме шамана.");
        RU.put("task8", "Спасите %,d мышек в сложном режиме шамана..");
        RU.put("task9", "Соберите %,d баллов в Defilante.");
        RU.put("task10", "Убейте %,d мышек в Кутиере.");
        RU.put("task11", "Убейте %,d мышек в Сурвиворе");
        RU.put("task12", "Соберите %,d первых мест в комнате Racing.");
        RU.put("task13", "Воспользуйся своим тотемом %,d раз.");
        RU.put("task14", "Соберите %,d опыта в режиме шамана.");
        RU.put("task15", "Заработайте %,d кармы, сообщая модерации об нарушителях.");
        RU.put("hours", "(Осталось %d часов)");
        RU.put("minutes", "(Осталось %d минут)");
        RU.put("prizeClaimed", "Получена награда");
        RU.put("failed", "Не закончено");
        RU.put("task30", "Собери %,d яичек");
        RU.put("task31", "Поймай %,d кроликов");
        RU.put("task32", "Храните свои яйца подальше от мышек %,d раз");
        RU.put("task33", "Завершить все пасхальные задания");
        RU.put("task34", "Будьте сыром %,d раз в мини-игре cheesehunter.");
        RU.put("task35", "Поймайте сыр %,d раз в мини-игре cheesehunter.");
        RU.put("task36", "Убейте %,d мышек плесневым сыром в мини-игре cheesehunter.");
        RU.put("task37", "Как сыр, выиграйте раунд %,d раз в мини-игре cheesehunter.");
        RU.put("command_ayshe", "Открывает титул <font color='#626ca8'>MiceForce &lt;3</font>.");
        RU.put("command_mp3", "Воспроизводит музыку из сервиса YouTube в режиме mp3. Пример: /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        RU.put("command_music", "Воспроизводить видео на экране в комнате племени. Пример: /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        RU.put("command_crown", "Включает или выключает корону в Racing/Bootcamp/Defilante комнатах.");
        RU.put("command_ping", "Показывает твой пинг.");
        RU.put("command_task", "Показывает список с заданиями.");
        RU.put("command_isimrenk", "Изменяет цвет твоего никнейма.");
        RU.put("command_color", "Изменяет цвет твоей мышки.");
        RU.put("command_mapcrew", "Показывает список с MapCrew персоналом, который сейчас находится в игре.");
        RU.put("command_mod", "Показывает список модераторов, которые сейчас находятся в игре.");
        RU.put("command_sentinel", "Показывает список Sentinel персонала (форумных работников), который сейчас находится в игре.");
        RU.put("command_ping2", "Показывает твой пинг. Команда /ping <ник> показывает пинг данного игрока.");
        RU.put("helpinfo", "<VP>Хочешь узнать какие у нас имеются команды? </VP>Впиши в чат <R>/help</R>");
        RU.put("announcement_ranking", "Игрок {username} получил титул {title} в  {type} рейтинге.");
        RU.put("daily", "ежедневном");
        RU.put("weekly", "недельном");
        RU.put("monthly", "месячном");
        RU.put("general", "общем");
        RU.put("you_can_vote", "Вы можете проголосовать, чтобы пропустить раунд.");
        RU.put("vote_stats", "Статистика голосования %,d / %,d <R>(/skip)</R>");
        RU.put("room_is_unlocked", "<VP>Владелец комнаты %s разблокировал комнату.");
        RU.put("room_is_locked", "<R>Владелец комнаты %s заблокировал комнату.");
        RU.put("cant_set_password", "<R>Вы не можете использовать это в комнате.");

        RU.put("karma_message", "[•] Ваш отчёт относительно игрока %s был обработан. (karma: %,d) ");

        RU.put("next_title_first", "<J>Следующий титул для фестов(первых мест) « %s » вам нужно собрать %,d больше фестов(первых мест).");

        RU.put("next_title_cheese", "<J>Следующий титул для сыра « %s » вам нужно собрать %,d больше сыра.");

        RU.put("next_title_defilante", "<J> Следующий титул для Defilante « %s » вам нужно собрать %,d больше очков 'Defilante' .");

        RU.put("next_title_bootcamp", "<J>Следующий титул для Bootcamp « %s » вам нужно собрать %,d больше 'Bootcamp' .");

        RU.put("completed_all", "<VP>Вы завершили все %s титулы.");

    }

    static {
        IT.put("ads", "Per favore clicca sugli annunci per aiutare il nostro gioco");
        IT.put("${trad#TG_7}", "Shaman Apprentice");
        IT.put("${trad#TG_6}", "Initiated");
        IT.put("${trad#tribu.nouv}", "Nuovo membro");
        IT.put("${trad#TG_5}", "Hunteress");
        IT.put("${trad#TG_4}", "Recruiter");
        IT.put("${trad#TG_0}", "Stooge");
        IT.put("${trad#TG_8}", "Tribe\'s Shaman");
        IT.put("${trad#TG_9}", "Spiritual Chief");
        IT.put("${trad#tribu.chef}", "Capo spirituale");
        IT.put("${trad#tribu.memb}", "Membro");
        IT.put("${trad#TG_3}", "Treasurer");
        IT.put("${trad#TG_2}", "Soldier");
        IT.put("${trad#TG_1}", "Cooker");
    }

    static {
        CZ.put("ads", "Ak chcete hru pomôcť, kliknite na reklamy");
        CZ.put("${trad#TG_7}", "Šamanův učeň");
        CZ.put("${trad#TG_6}", "Zasvěcený");
        CZ.put("${trad#tribu.nouv}", "Nový člen");
        CZ.put("${trad#TG_5}", "Lovec");
        CZ.put("${trad#TG_4}", "Rekrutér");
        CZ.put("${trad#TG_0}", "Poskok");
        CZ.put("${trad#TG_8}", "Šaman kmene");
        CZ.put("${trad#TG_9}", "Duchovní vůdce");
        CZ.put("${trad#tribu.chef}", "Duchovní vůdce");
        CZ.put("${trad#tribu.memb}", "Člen");
        CZ.put("${trad#TG_3}", "Pokladník");
        CZ.put("${trad#TG_2}", "Voják");
        CZ.put("${trad#TG_1}", "Kuchař");
        CZ.put("fca", "<J>%s</J><FC> začalo to a Funcorp in /room %s");
    }

    static {
        CN.put("${trad#TG_7}", "见习萨满");
        CN.put("${trad#TG_6}", "队长");
        CN.put("${trad#tribu.nouv}", "新成员");
        CN.put("${trad#TG_5}", "猎人");
        CN.put("${trad#TG_4}", "招聘员");
        CN.put("${trad#TG_0}", "平民");
        CN.put("${trad#TG_8}", "部落萨满");
        CN.put("${trad#TG_9}", "酋长");
        CN.put("${trad#tribu.chef}", "酋长 ");
        CN.put("${trad#tribu.memb}", "成员");
        CN.put("${trad#TG_3}", "财务秘书");
        CN.put("${trad#TG_2}", "士兵");
        CN.put("${trad#TG_1}", "厨师");
    }

    static {
        LV.put("${trad#TG_7}", "Šamaņa māceklis");
        LV.put("${trad#TG_6}", "Jauniņais");
        LV.put("${trad#tribu.nouv}", "Jauniņais");
        LV.put("${trad#TG_5}", "Mednieks");
        LV.put("${trad#TG_4}", "Aicinātājs");
        LV.put("${trad#TG_0}", "Marionete");
        LV.put("${trad#TG_8}", "Cilts Šamanis");
        LV.put("${trad#TG_9}", "Garīgais vadītājs");
        LV.put("${trad#tribu.chef}", "Garīgais vadītājs");
        LV.put("${trad#tribu.memb}", "Biedrs");
        LV.put("${trad#TG_3}", "Mantzinis");
        LV.put("${trad#TG_2}", "Kareivis");
        LV.put("${trad#TG_1}", "Pavārs");
    }

    static {
        HR.put("${trad#TG_7}", "Shaman - učenik");
        HR.put("${trad#TG_6}", "Pokretač");
        HR.put("${trad#tribu.nouv}", "Novi član.");
        HR.put("${trad#TG_5}", "Lovkinja");
        HR.put("${trad#TG_4}", "Regrut");
        HR.put("${trad#TG_0}", "Marioneta");
        HR.put("${trad#TG_8}", "Plemenski vođa");
        HR.put("${trad#TG_9}", "Duhovni vođa");
        HR.put("${trad#tribu.chef}", "Duhovni vođa.");
        HR.put("${trad#tribu.memb}", "Član");
        HR.put("${trad#TG_3}", "Rizničar");
        HR.put("${trad#TG_2}", "Vojnik");
        HR.put("${trad#TG_1}", "Kuhar");
    }

    static {
        HU.put("${trad#TG_7}", "Sámán Tanonc");
        HU.put("${trad#TG_6}", "Kezdeményezett");
        HU.put("${trad#tribu.nouv}", "Új tag");
        HU.put("${trad#TG_5}", "Vadász");
        HU.put("${trad#TG_4}", "Toborzó");
        HU.put("${trad#TG_0}", "Besúgó");
        HU.put("${trad#TG_8}", "A Törzs Sámánja");
        HU.put("${trad#TG_9}", "Szellemi Vezető");
        HU.put("${trad#tribu.chef}", "Szellemi Vezető");
        HU.put("${trad#tribu.memb}", "Tag");
        HU.put("${trad#TG_3}", "Kincstárnok");
        HU.put("${trad#TG_2}", "Katona");
        HU.put("${trad#TG_1}", "Szakács");
        HU.put("announcement_ranking", "{username} most {title} a {type} rangsorban.");
        HU.put("daily", "napi");
        HU.put("weekly", "heti");
        HU.put("monthly", "havi");
        HU.put("general", "általános");
        HU.put("task60", "Segíts másoknak feldíszíteni a Karácsonyfát %,d alkalommal");
        ;
    }

    static {
        NL.put("${trad#TG_7}", "Assistent-Sjamaan");
        NL.put("${trad#TG_6}", "Ingewijde");
        NL.put("${trad#tribu.nouv}", "Nieuw lid");
        NL.put("${trad#TG_5}", "Jager");
        NL.put("${trad#TG_4}", "Ledenwerver");
        NL.put("${trad#TG_0}", "Assistent");
        NL.put("${trad#TG_8}", "Sjamaan van de stam");
        NL.put("${trad#TG_9}", "Spirituele Leider");
        NL.put("${trad#tribu.chef}", "Spirituele Leider");
        NL.put("${trad#tribu.memb}", "Lid");
        NL.put("${trad#TG_3}", "Schatbewaarder");
        NL.put("${trad#TG_2}", "Soldaat");
        NL.put("${trad#TG_1}", "Kok");
    }

    static {
        BG.put("${trad#TG_7}", "Помощник Водач");
        BG.put("${trad#TG_6}", "Посветен");
        BG.put("${trad#tribu.nouv}", "Нов член");
        BG.put("${trad#TG_5}", "Ловец");
        BG.put("${trad#TG_4}", "Работодател");
        BG.put("${trad#TG_0}", "Аматьор");
        BG.put("${trad#TG_8}", "Водач на племето");
        BG.put("${trad#TG_9}", "Духовен водач");
        BG.put("${trad#tribu.chef}", "Духовен водач");
        BG.put("${trad#tribu.memb}", "Член");
        BG.put("${trad#TG_3}", "Ковчежник");
        BG.put("${trad#TG_2}", "Войник");
        BG.put("${trad#TG_1}", "Готвач");
    }

    static {
        BR.put("${trad#TG_7}", "Aprendiz de Shaman");
        BR.put("${trad#TG_6}", "Estagiário");
        BR.put("${trad#tribu.nouv}", "Novo membro");
        BR.put("${trad#TG_5}", "Caçador");
        BR.put("${trad#TG_4}", "Recrutador");
        BR.put("${trad#TG_0}", "Peão");
        BR.put("${trad#TG_8}", "Shaman da Tribo");
        BR.put("${trad#TG_9}", "Líder Espiritual");
        BR.put("${trad#tribu.chef}", "Chefe espiritual ");
        BR.put("${trad#tribu.memb}", "Membro");
        BR.put("${trad#TG_3}", "Tesoureiro");
        BR.put("${trad#TG_2}", "Soldado");
        BR.put("${trad#TG_1}", "Cozinheiro");
    }

    static {
        DE.put("ads", "Bitte klicken Sie auf der Werbungen, um unser Spiel zu unterstützen");
        DE.put("${trad#TG_7}", "Schamanen Lehrling");
        DE.put("${trad#TG_6}", "Eingeweihter");
        DE.put("${trad#tribu.nouv}", "Neues Mitglied");
        DE.put("${trad#TG_5}", "Jägerin");
        DE.put("${trad#TG_4}", "Rekrutierer");
        DE.put("${trad#TG_0}", "Handlanger");
        DE.put("${trad#TG_8}", "Stammes Schamanin");
        DE.put("${trad#TG_9}", "Geistiger Leiter");
        DE.put("${trad#tribu.chef}", "Geistiger Leiter");
        DE.put("${trad#tribu.memb}", "Mitglied");
        DE.put("${trad#TG_3}", "Schatzmeister");
        DE.put("${trad#TG_2}", "Soldat");
        DE.put("${trad#TG_1}", "Koch");
    }

    static {
        AR.put("fca", "<J>%s</J><FC> لقد تم تفعيل وضع الفان كورب في الغرفة التالية /room %s");
        AR.put("lama_event", "/room lama   مـوسـم جـديـد في هذة الغرفـة");
        AR.put("task60", "مرة %,d مساعدة الآخرين على تزيين شجرة عيد الميلاد");
        AR.put("command_ayshe", "تفتح<font color='#626ca8'>MiceForce <3</font> title.");
        AR.put("command_mp3", "تمكنك من تشغيل الموسيقى من يوتيب e.g. /mp3 https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        AR.put("command_music", "تمكنك من تشغيل الفيديوهات في بيت قبيلتك e.g. /music https://www.youtube.com/watch?v=fEzpsVi1Qd0");
        AR.put("command_crown", ".تشغل/تبطل التاج في السباق، سورفايفور و ديفيلانت");
        AR.put("command_ping", ".تريك سرعة الانترنيت");
        AR.put("command_task", ".تريك المهام");
        AR.put("command_isimrenk", ".تمكنك من تغيير لون اسمك");
        AR.put("command_color", ".تمكنك من تغيير لون فأرك");
        AR.put("command_mapcrew", "تريك الاعضاء المتصلين من فريق الخرائط");
        AR.put("command_mod", "تريك الاعضاء المتصلين من فريق المشرفين.");
        AR.put("command_sentinel", "تريك الاعضاء المتصلين من فريق المشرفين على المنتدى");
        AR.put("command_ping2", "تريك سرعة الانترنيت | /ping <اسم اللاعب> تريك سرعة الانترنيت الخاص بلاعب آخر");
        AR.put("helpinfo", "<VP>هل تريد ان تعرف المزيد على الاوامر الخاصة بنا؟</VP> استعمل <R>/help</R>");
        AR.put("ads", "المرجو الضغط لدعم اللعبة");
        AR.put("next", "التالي");
        AR.put("prev", "السابق");
        AR.put("tasks", "المهام");
        AR.put("page", "الصفحة");
        AR.put("task0", "اجمع %,d  جبن");
        AR.put("task1", "اجمع %,d فيرست");
        AR.put("task2", "اجمع %,d بووتكامب");
        AR.put("task3", "عش %,d جولة في سورفايفور");
        AR.put("task4", "عش %,d جولة في كوتيير");
        AR.put("task5", "العب اللعبة ل %sساعة");
        AR.put("task6", "انقذ %,d فأر بالطريقة العادية.");
        AR.put("task7", "انقذ %,d فأر  بالطريقة الإلهية");
        AR.put("task8", "انقذ %,d فأر  بالطريقة الصعبة");
        AR.put("task9", "احصل على %,d نقط في دفيلانت");
        AR.put("task10", "اقتل  %,d فأر في كوتيير");
        AR.put("task11", "اقتل  %,d فأر في سورفايفور");
        AR.put("task12", "أكمل  %,d دور في السباق");
        AR.put("task13", "استعمل سنبلة شامانك %,d مرة.");
        AR.put("task14", "اربح %,d نقط مهارة كشامان");
        AR.put("task15", "احصل على%,d نقط كارما بالتبليغ عن الاشخاص اللذين يخرقون القوانين");
        AR.put("hours", "(%d الساعات المتبقية)");
        AR.put("minutes", "(%d الدقائق المتبقية)");
        AR.put("prizeClaimed", "حصل على الجائزة");
        AR.put("failed", "فشل");
        AR.put("task30", "جمع %,d بيضة");
        AR.put("task31", "امسك ب %,d أرانب");
        AR.put("task32", "أبقي البيض بعيدا عن الفئران ل %,d من المرات");
        AR.put("task33", "أكمل جميع مهام عيد الفصح");
        AR.put("task34", "كن الجبن  %,d مرة في لعبة cheesehunter.");
        AR.put("task35", "التقط الجبن %,d مرة في لعبة cheesehunter.");
        AR.put("task36", "اقتل %,d فأرا بصفتك فأرا متعفنا في لعب cheesehunter.");
        AR.put("task37", "بصفتك جبنا، اربح الدور %,d مرة في لعبة cheesehunter.");
        AR.put("task38", "تم إحياؤه من كلب الجحيم  %,d مرات");
        AR.put("task39", "عاش في العالم السفلي %,d مرات. ");
        AR.put("task40", "مات%,d مرات في العالم السفلي");
        AR.put("${trad#TG_7}", "الشامان المبتدئ");
        AR.put("${trad#TG_6}", "المبادر");
        AR.put("${trad#tribu.nouv}", "عضو جديد");
        AR.put("${trad#TG_5}", "الصياد");
        AR.put("${trad#TG_4}", "المجند");
        AR.put("${trad#TG_0}", "المبتدئ");
        AR.put("${trad#TG_8}", "شامان القبيلة");
        AR.put("${trad#TG_9}", " القائد الروحى");
        AR.put("${trad#tribu.chef}", "القائد الروحي");
        AR.put("${trad#tribu.memb}", "عضو");
        AR.put("${trad#TG_3}", "أمين الصندوق");
        AR.put("${trad#TG_2}", "الجندي");
        AR.put("${trad#TG_1}", "الطباخ");
        AR.put("announcement_ranking", ". {type} في الترتيب {title} الآن {username} أصبح");
        AR.put("daily", "اليومي");
        AR.put("weekly", "الأسبوعي");
        AR.put("monthly", "الشهري");
        AR.put("general", "العام");
        AR.put("you_can_vote", "يمكنك التصويت لتخطي الجولة.");
        AR.put("vote_stats", "حالة التصويت %,d / %,d");
        AR.put("room_is_unlocked", "<VP>مالك الغرفة %s قام بفتح الغرفة.");
        AR.put("room_is_locked", "<R>مالك الغرفة %s قام بغلق الغرفة.");
        AR.put("cant_set_password", "<R>لا يمكنك استخدام ذلك في هذه الغرفة.");
        AR.put("karma_message", "[•] بلاغك عن اللاعب %s تم التعامل معه. (karma: %,d) ");
        AR.put("settings", "الاعادات ");
        AR.put("settings_1", "<CH>تغيير التاق.");
        AR.put("settings_2", "<CH>تغيير لون دردشة المستخدم.");
        AR.put("settings_3", "تعطيل الوسامات الخاصة  بالفراءات  في الملفات الشخصية");
        AR.put("settings_4", "نزع اعلانات الرانك .");
        AR.put("settings_5", "تعطيل الخانه الخاصه بالمستوى المنبثقه");
        AR.put("settings_6", "تعطيل النوافذ المنبثقة لعناصر المخزن  المكتسبة.");
        AR.put("settings_7", "تعطيل الإعلانات التي تظهر على الشاشة.");
        AR.put("settings_8", "تعطيل زر الإعدادات.");


    }

    static {
        PH.put("${trad#TG_7}", "Baguhang salamangkero");
        PH.put("${trad#TG_6}", "Tagasimula");
        PH.put("${trad#tribu.nouv}", "Bagong miyembro");
        PH.put("${trad#TG_5}", "Mangangaso");
        PH.put("${trad#TG_4}", "Mangangalap ng miyembro");
        PH.put("${trad#TG_0}", "Sangkalan");
        PH.put("${trad#TG_8}", "Salamangkero ng tribo");
        PH.put("${trad#TG_9}", "Banal na hepe");
        PH.put("${trad#tribu.chef}", "Banal na hepe");
        PH.put("${trad#tribu.memb}", "Miyembro");
        PH.put("${trad#TG_3}", "Tesorero");
        PH.put("${trad#TG_2}", "Sundalo");
        PH.put("${trad#TG_1}", "Kusinero");
    }

    static {
        VK.put("${trad#TG_7}", "Sjaman Lærling");
        VK.put("${trad#TG_6}", "Initiert");
        VK.put("${trad#tribu.nouv}", "Nytt medlem");
        VK.put("${trad#TG_5}", "Jeger");
        VK.put("${trad#TG_4}", "Rekruterer");
        VK.put("${trad#TG_0}", "Bonde");
        VK.put("${trad#TG_8}", "Stammens Sjaman");
        VK.put("${trad#TG_9}", "Spirituell Leder");
        VK.put("${trad#tribu.chef}", "Spirituell leder");
        VK.put("${trad#tribu.memb}", "Medlem");
        VK.put("${trad#TG_3}", "Kasserer");
        VK.put("${trad#TG_2}", "Soldat");
        VK.put("${trad#TG_1}", "Kokk");
    }

    static {
        TR.put("${trad#TG_7}", "Çırak Şaman");
        TR.put("${trad#TG_6}", "Üye");
        TR.put("${trad#tribu.nouv}", "Yeni üye");
        TR.put("${trad#TG_5}", "Avcı");
        TR.put("${trad#TG_4}", "Memur");
        TR.put("${trad#TG_0}", "Çırak");
        TR.put("${trad#TG_8}", "Kabile Şamanı");
        TR.put("${trad#TG_9}", "Kabile Reisi");
        TR.put("${trad#tribu.chef}", "Kabile reisi");
        TR.put("${trad#tribu.memb}", "Üye");
        TR.put("${trad#TG_3}", "Haznedar");
        TR.put("${trad#TG_2}", "Asker");
        TR.put("${trad#TG_1}", "Aşçı");
        TR.put("announcement_ranking", "{username}, {type} sıralamada artık {title}.");
        TR.put("daily", "günlük");
        TR.put("weekly", "haftalık");
        TR.put("monthly", "aylık");
        TR.put("general", "genel");
    }

    static {
        EN.put("${trad#TG_7}", "Shaman Apprentice");
        EN.put("${trad#TG_6}", "Initiated");
        EN.put("${trad#tribu.nouv}", "New member");
        EN.put("${trad#TG_5}", "Hunteress");
        EN.put("${trad#TG_4}", "Recruiter");
        EN.put("${trad#TG_0}", "Stooge");
        EN.put("${trad#TG_8}", "Tribe\'s Shaman");
        EN.put("${trad#TG_9}", "Spiritual Chief");
        EN.put("${trad#tribu.chef}", "Spiritual chief");
        EN.put("${trad#tribu.memb}", "Member");
        EN.put("${trad#TG_3}", "Treasurer");
        EN.put("${trad#TG_2}", "Soldier");
        EN.put("${trad#TG_1}", "Cooker");
    }

    static {
        LT.put("${trad#TG_7}", "Šamano Mokinys");
        LT.put("${trad#TG_6}", "Iniciatorius");
        LT.put("${trad#tribu.nouv}", "Naujas narys");
        LT.put("${trad#TG_5}", "Aukštesnysis");
        LT.put("${trad#TG_4}", "Samdytojas");
        LT.put("${trad#TG_0}", "Pavaldinys");
        LT.put("${trad#TG_8}", "Genties Šamanas");
        LT.put("${trad#TG_9}", "Genties Valdovas");
        LT.put("${trad#tribu.chef}", "Dvasių valdovas");
        LT.put("${trad#tribu.memb}", "Narys");
        LT.put("${trad#TG_3}", "Iždininkas");
        LT.put("${trad#TG_2}", "Eilinis");
        LT.put("${trad#TG_1}", "Jaunesnysis");
    }

    static {
        ID.put("${trad#TG_7}", "Murid Dukun");
        ID.put("${trad#TG_6}", "Yang Diinisiasi");
        ID.put("${trad#tribu.nouv}", "Anggota baru");
        ID.put("${trad#TG_5}", "Pemburu");
        ID.put("${trad#TG_4}", "Perekrut");
        ID.put("${trad#TG_0}", "Kaki Tangan");
        ID.put("${trad#TG_8}", "Dukun Suku");
        ID.put("${trad#TG_9}", "Ketua Spiritual");
        ID.put("${trad#tribu.chef}", "Ketua spiritual");
        ID.put("${trad#tribu.memb}", "Anggota");
        ID.put("${trad#TG_3}", "Bendahara");
        ID.put("${trad#TG_2}", "Tentara");
        ID.put("${trad#TG_1}", "Koki");
    }

    static {
        FI.put("${trad#TG_7}", "Harjoittelijashamaani");
        FI.put("${trad#TG_6}", "Alokas");
        FI.put("${trad#tribu.nouv}", "Uusi jäsen");
        FI.put("${trad#TG_5}", "Metsästäjätär");
        FI.put("${trad#TG_4}", "Rekrytoija");
        FI.put("${trad#TG_0}", "Juoksupoika");
        FI.put("${trad#TG_8}", "Heimoshamaani");
        FI.put("${trad#TG_9}", "Hengellinen johtaja");
        FI.put("${trad#tribu.chef}", "Hengellinen johtaja");
        FI.put("${trad#tribu.memb}", "Jäsen");
        FI.put("${trad#TG_3}", "Rahastonhoitaja");
        FI.put("${trad#TG_2}", "Sotilas");
        FI.put("${trad#TG_1}", "Keittäjä");
    }

    static {
        ES.put("${trad#TG_7}", "Aprendiz de Chamán");
        ES.put("${trad#TG_6}", "Iniciado");
        ES.put("${trad#tribu.nouv}", "Nuevo miembro");
        ES.put("${trad#TG_5}", "Cazadora");
        ES.put("${trad#TG_4}", "Reclutador");
        ES.put("${trad#TG_0}", "Peón");
        ES.put("${trad#TG_8}", "Chamán de la Tribu");
        ES.put("${trad#TG_9}", "Jefe Espiritual");
        ES.put("${trad#tribu.chef}", "Jefe espiritual");
        ES.put("${trad#tribu.memb}", "Miembro");
        ES.put("${trad#TG_3}", "Tesorero");
        ES.put("${trad#TG_2}", "Soldado");
        ES.put("${trad#TG_1}", "Cocinero");
        ES.put("task60", "Ayuda a otros a decorar el Árbol de Navidad %,d veces");
    }

    static {
        JP.put("${trad#TG_7}", "Shaman Apprentice");
        JP.put("${trad#TG_6}", "Initiated");
        JP.put("${trad#tribu.nouv}", "New member");
        JP.put("${trad#TG_5}", "Hunteress");
        JP.put("${trad#TG_4}", "Recruiter");
        JP.put("${trad#TG_0}", "Stooge");
        JP.put("${trad#TG_8}", "Tribe\'s Shaman");
        JP.put("${trad#TG_9}", "Spiritual Chief");
        JP.put("${trad#tribu.chef}", "Spirital chief");
        JP.put("${trad#tribu.memb}", "メンバー");
        JP.put("${trad#TG_3}", "Treasurer");
        JP.put("${trad#TG_2}", "Soldier");
        JP.put("${trad#TG_1}", "Cooker");
    }

    static {
        FR.put("how_many_friends", "Vous avez %,d amis dans votre liste d'amis (limite : <R>500</R>).");
        FR.put("ads", "Cliquez sur les annonces pour soutenir le jeu, s'il vous plaît");
        FR.put("next", "Suivant");
        FR.put("prev", "Précédent");
        FR.put("tasks", "Tâches");
        FR.put("page", "Page");
        FR.put("task0", "Collectez %,d cheese");
        FR.put("task1", "Collectez %,d fromage(s) en premier.");
        FR.put("task2", "Collectez %,d bootcamp.");
        FR.put("task3", "Survivre %,d tour(s) dans les salons de Survivor.");
        FR.put("task4", "Survivre %,d tour(s) dans les salons Kutier.");
        FR.put("task5", "Jouer au jeu pendant %s heure(s).");
        FR.put("task6", "Sauvez %,d souris en mode normal.");
        FR.put("task7", "Sauvez %,d souris en mode divin.");
        FR.put("task8", "Sauvez %,d souris en mode difficile.");
        FR.put("task9", "Get %,d points sur defilante.");
        FR.put("task10", "Kill %,d souris sur Kutier.");
        FR.put("task11", "Kill %,d souris sur Survivor.");
        FR.put("task12", "Complétez %,d niveaux étant premier en Racing.");
        FR.put("task13", "Utilisez votre totem %,d fois.");
        FR.put("task14", "Recevez %,d points comme chamane.");
        FR.put("task15", "Gagnez %,d points de karma en signalant les mauvais joueurs.");
        FR.put("hours", "(% d heures restantes)");
        FR.put("minutes", "(% d minutes restantes)");
        FR.put("prizeClaimed", "prix réclamé");
        FR.put("failed", "Échoué");
        FR.put("task30", "Collecter %,d œufs ");
        FR.put("task31", "Attraper %,d lapins");
        FR.put("task32", "Garder les œufs loin des souris %,d fois");
        FR.put("task33", "Completer tous les tâches de Pâques");
        FR.put("task34", "Soyez le fromage %,d fois dans le mini-jeu cheesehunter.");
        FR.put("task35", "Attrapez le fromage %,d fois dans le mini-jeu cheesehunter.");
        FR.put("task36", "Tuez %,d souris étant le fromage moisi dans le mini-jeu cheesehunter.");
        FR.put("task37", "Étant fromage, gagnez le niveau %,d fois dans le mini-jeu cheesehunter.");
        FR.put("${trad#TG_7}", "Apprentie Chamane");
        FR.put("${trad#TG_6}", "Initiée");
        FR.put("${trad#tribu.nouv}", "Nouveau membre");
        FR.put("${trad#TG_5}", "Chasseresse");
        FR.put("${trad#TG_4}", "Recruteuse");
        FR.put("${trad#TG_0}", "Bobonne");
        FR.put("${trad#TG_8}", "Chamane de la tribu");
        FR.put("${trad#TG_9}", "Chef Spirituel");
        FR.put("${trad#tribu.chef}", "Chef spirituel");
        FR.put("${trad#tribu.memb}", "Membre");
        FR.put("${trad#TG_3}", "Trésorière");
        FR.put("${trad#TG_2}", "Soldat");
        FR.put("${trad#TG_1}", "Cantinière");

        FR.put("record_new", "<J>%s</J> a établi un nouveau record en <V>%.3f</V> sec");
        FR.put("record", "Le record de cette carte a été battu par<J>%s</J>(<V>%.3f</V>s)");

        FR.put("announcement_ranking", "{username} est maintenant {title} dans le classement de {type} .");
        FR.put("daily", "quotidien");
        FR.put("weekly", "hebdomadaire");
        FR.put("monthly", "mensuel");
        FR.put("general", "général");
        FR.put("you_can_vote", "Vous pouvez voter pour sauter le niveau.");
        FR.put("vote_stats", "Vote %,d / %,d <R>(/skip)</R>");
        FR.put("room_is_unlocked", "<VP>Le propriétaire de la salle %s a ouvert la salle.");
        FR.put("room_is_locked", "<R>Le propriétaire de la salle %s a fermé la salle.");
        FR.put("cant_set_password", "<R>Vous ne pouvez pas l'utiliser dans cette salle.");
        FR.put("karma_message", "[•] Votre rapport concernant le joueur %s a été traité. (karma: %,d) ");
    }

    static{
        EN.put("task80","Help others decorate the Christmas tree %,d times");
        EN.put("task81","Give gift boxes to %,d mice on map 888");
        EN.put("task82","Receive %,d gift boxes on map 888");

        TR.put("task80","Diğerlerinin yılbaşı ağacını tasarlamasına %,d  kere yardım et");
        TR.put("task81","%,d fareye hediye kutusu hediye et");
        TR.put("task82","%,d tane hediye al");

        BR.put("task80","Ajude os outros a decorarem a árvore de Natal %,d times");
        BR.put("task81","Dê caixas de presentes para %,d mice");
        BR.put("task82","Receber %,d caixa de presentes");

        PL.put("task80","Pomóż innym udekorować choinkę %,d razy");
        PL.put("task81","Podaruj prezenty %,d myszkom");
        PL.put("task82","Otrzymaj %,d prezentów");

        HU.put("task80","Segíts a többieknek feldíszíteni a Karácsonyfát %,d szer");
        HU.put("task81","Adj ajándékokat  %,d egér");
        HU.put("task82","Kapj %,d ajándékokat");

        AR.put("task80","ساعد الاخريين في تزيين شجرة الكريسمس %,d مرات");
        AR.put("task81","اعطي صندوق الهدايا %,d فئران");
                AR.put("task82","تلقي %,d صندوق الهدايا");

        ES.put("task80","Ayuda a los demás en decorar el árbol de Navidad %,d times");
        ES.put("task81","Dar cajas de regalos %,d mice");
        ES.put("task82","Recibir %,d cajas de regalo");

        RO.put("task80","Ajută-i pe alții să decoreze bradul de Crăciun de %,d ori");
        RO.put("task81","Oferă cadouri la %,d şoricei");
        RO.put("task82","Primeşte %,d cadouri");
    }


    static {
        diller.put("TR", TR);
        diller.put("EN", EN);
        diller.put("ES", ES);
        diller.put("BR", BR);
        diller.put("HU", HU);
        diller.put("PL", PL);
        diller.put("RO", RO);
        diller.put("SK", SK);
        diller.put("RU", RU);
        diller.put("IT", IT);
        diller.put("CZ", CZ);
        diller.put("CN", CN);
        diller.put("LV", LV);
        diller.put("HR", HR);
        diller.put("NL", NL);
        diller.put("BG", BG);
        diller.put("DE", DE);
        diller.put("AR", AR);
        diller.put("PH", PH);
        diller.put("VK", VK);
        diller.put("LT", LT);
        diller.put("ID", ID);
        diller.put("FI", FI);
        diller.put("JP", JP);
        diller.put("FR", FR);
    }

    static {
        EN.put("task70", "Find all hidden titles on the Dragon map.");
        TR.put("task70", "Ejderha haritasındaki tüm gizli unvanları bul.");
        HU.put("task70", "Keresd meg mindegyik elrejtett címet a Sárkány mapon.");
        FR.put("task70", "Trouvez tout les titres cachés dans la carte du Dragon.");
        BR.put("task70", "Encontre todos os títulos escondidos no mapa do Dragão.");
        AR.put("task70", "أبحث عن الألقاب الخفية في خريطة التنين");
        PL.put("task70", "Odkryj wszystkie ukryte tytuły na Smoczej Mapie.");
        RO.put("task70", "Găsește toate titlurile ascunse pe mapa Dragon.");
        ES.put("task70", "Encuentra todos los títulos escondidos en el mapa del Dragón.");
    }

    public static String yazi(String dil, String anahtar, Object... args) {
        Map<String, String> dilmap = diller.get(dil);
        if (dilmap == null) {
            dilmap = diller.get("EN");
        }
        String yazi = dilmap.get(anahtar);
        if (yazi == null) {
            dilmap = diller.get("EN");
            yazi = dilmap.get(anahtar);
            if (yazi == null) return anahtar;
        }
        try {
            return String.format(yazi, args);
        } catch (Exception e) {
            System.out.println(e);
            System.out.println(anahtar + " " + dil);
            return yazi;
        }
    }
}

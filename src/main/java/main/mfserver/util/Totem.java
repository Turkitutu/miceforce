package mfserver.util;

import java.io.Serializable;

public class Totem implements Cloneable, Serializable {
    public int count = 0;
    public String data = "";

    public Totem clone() {

        Totem person = new Totem();
        person.data = data;
        person.count = count;
        return person;
    }
}

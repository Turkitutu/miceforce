package mfserver.util.sutun;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import mfserver.main.MFServer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by sevendr on 12/06/17.
 */
public class Inventory {
    public TreeMap<Integer, Nesne> nesneler = new TreeMap<>();

    public Inventory(String json) {

        Gson gson = MFServer.get_Gson();
        Type type = new TypeToken<HashMap<Integer, ArrayList>>() {
        }.getType();
        HashMap<Integer, ArrayList> esyalar = gson.fromJson(json, type);
        esyalar.forEach((k, v) -> {
            int sayi = MFServer.obj_to_int(v.get(0));
            boolean kullan = (boolean) v.get(1);
            nesneler.put(k, new Nesne(sayi, kullan));
        });

    }

    public String serialize() {

        JsonObject jsonArray = new JsonObject();

        nesneler.forEach((k, v) -> {
            JsonArray ja = new JsonArray();
            ja.add(new JsonPrimitive(v.sayi));
            ja.add(new JsonPrimitive(v.kullan));
            jsonArray.add(String.valueOf(k), ja);
        });

        return jsonArray.toString();
    }

    public static class Nesne {
        public int sayi = 0;
        public boolean kullan = false;

        public Nesne(int sayi, boolean kullan) {
            this.sayi = sayi;
            this.kullan = kullan;
        }
    }
}



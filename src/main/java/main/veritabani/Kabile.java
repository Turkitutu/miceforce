package veritabani;

import mfserver.main.MFServer;
import mfserver.main.Tribe;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by sevendr on 12.01.2017.
 */
public final class Kabile extends AltanPOJO {
    public static Object db;

    public Integer harita;
    public String isim;
    public String tag;
    public String ileti;
    @BsonProperty("_id")
    public int code;

}

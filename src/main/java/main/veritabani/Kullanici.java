package veritabani;

import io.netty.util.internal.ConcurrentSet;
import mfserver.util.Totem;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public final class Kullanici extends AltanPOJO {
    public static Object db;


    @BsonProperty("_id")
    public int code;
    public String isim;

    public ArrayList<String> eskiIsimler = new ArrayList<>();
    public String sifre;
    public int yetki = 1;
    public int unvan = 0;
    public int cevrimici = 0;
    public int peynir = 0;
    public int birinci = 0;
    public int bootcamp = 0;
    public int kurtarma = 0;
    public int samanPeynir = 0;
    public int hardKurtarma = 0;
    public int divineKurtarma = 0;
    public int tur = 0;
    public int marketPey = 500_000;
    public int marketCilek = 10_000;
    public int kayit;
    public int avatar = 0;
    public int ruhesi = 0;
    public int ikon = 0;
    public int cinsiyet = 0;
    public int samanTur = 0;
    public String tip = "1;0,0,0,0,0,0,0,0,0";
    public String samanRenk = "95d9d6";
    public String kurkRenk = "78583a";
    public HashSet<Integer> ikonlar = new HashSet<>();
    public HashSet<Integer> engellenenler = new HashSet<>();
    public LinkedHashMap<String, MarketEsya> market = new LinkedHashMap<>();
    public ConcurrentHashMap<String, Integer> beceriler = new ConcurrentHashMap<>();
    public LinkedHashMap<String, MarketEsya> marketSaman = new LinkedHashMap<>();
    public LinkedHashMap<String, Nesne> esyalar = new LinkedHashMap<>();
    public ArrayList<String> tipler = new ArrayList<>();
    public ArrayList<Document> arkadaslar = new ArrayList<>();
    public HashSet<Integer> rozetler = new HashSet<>();
    public ExtraStats extraStats = new ExtraStats();
    public int tribeCode = 0;
    public int tribeRank = 0;
    public int tribeJoinTime = 0;
    public Totem totem;
    public Ban ban;
    public boolean cafeBan = false;
    public EvInfo evInfo = new EvInfo();
    public boolean isLuacrew = false;
    public boolean isFuncorp = false;
    public boolean isMapcrew = false;
    public boolean isCouncil = false;
    public boolean isSentinel = false;
    public boolean isFashion=false;
    public boolean isTester=false;
    public ArrayList<Integer> karma = new ArrayList<>();
    public int fikicoin = 0;
    public int ensonIsim = 0;
    public Sessiz sessiz;
    public ArrayList<Integer> ozelKurkler = new ArrayList<>();
    public boolean fikicoinAlmis = false;
    public Seviye seviye = new Seviye();
    public int isimRenk;
    public boolean watchIps = false;
    public boolean showReports = false;
    public int ref = 0;
    public String tag = "";
    public int tagUsage = 0;
    public String mail = "";
    public boolean stand;
    public int chatColor = -1;
    public boolean optAnnounce = true;
    public boolean refComp = false;

    public boolean watch_pw = false;
    public Set<Integer> unvanlar = new HashSet<>();
    public Map<String, Integer> yildizlar = new ConcurrentHashMap<>();

    public boolean vpn = false;
    public long luaBan;
    public boolean isArtist = false;
    public long banTribe=0;
    public boolean rareTitles = false;
    public boolean disableSettings=false;
    public boolean disableBeceriler=false;
    public boolean disableKurkRozet=false;
    public boolean disableInventoryPopup=false;
    public boolean disableOnScreen=false;
    public boolean disableFriendReqs=false;
    public HashSet<Integer> bestFriends = new HashSet<>();
    public HashSet<String> bannedCommunities = new HashSet<>();
}

package veritabani;

public class Nesne {
    public int sayi = 0;
    public boolean kullan = false;

    public Nesne(int sayi, boolean kullan) {
        this.sayi = sayi;
        this.kullan = kullan;
    }

    public Nesne() {

    }

    @Override
    public String toString() {
        return "Nesne{" +
                "sayi=" + sayi +
                ", kullan=" + kullan +
                '}';
    }
}
package veritabani;

import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.ArrayList;

public final class Rutbe extends AltanPOJO {
    public static Object db;

    public int tribeCode;
    public int index;
    public String name;
    public String o_name;
    @BsonProperty("_id")
    public int code;
    public boolean blocked;
    public int order;
    public ArrayList<Boolean> droits;
    public Rutbe() {

    }
}
